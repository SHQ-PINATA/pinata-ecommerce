Piñata
======

A Symfony 3.2 project created on March 23, 2017, 10:11 am.

Paquetes de php 7.0 instalados en produccion:
--------------------------------------------
```
php70.x86_64         
php70-cli.x86_64     
php70-common.x86_64  
php70-intl.x86_64    
php70-json.x86_64    
php70-mysqlnd.x86_64 
php70-pdo.x86_64     
php70-process.x86_64 
php70-soap.x86_64    
php70-xml.x86_64     
php70-zip.x86_64     
```

Crones:
------

Hay 2 crones al momento. Uno que envia las consultas realizadas a un mail y otro que levanta las facturas creadas y no procesadas para procesarlas contra la AFIP.
Se realizan estas tareas a travez de un command.

```
  app:afip-procesar-facturas                Procesa las facturas contra el WS de Afip
  app:consultas-dequeue-mailer              Envia las consultas no procesadas en la base por mail
```
En bin/xxxxx.cron esta como debe configurarse el cron para cada proceso.

Migrations:
----------

El proyecto se empezó sin migraciones y se las agregó luego, asi que para levantar la base lo mejor es hacer una copia de producción o levantar hacer un schema:update y agregar a mano las versiones de las migraciones que no se corrieron.

