<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170915171050 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE factura (id INT AUTO_INCREMENT NOT NULL, pedido_id INT DEFAULT NULL, fecha_procesado DATETIME DEFAULT NULL, importe_total NUMERIC(9, 2) DEFAULT NULL, importe_iva NUMERIC(9, 2) DEFAULT NULL, cae VARCHAR(50) DEFAULT NULL, cae_vencimiento DATE DEFAULT NULL, comprobante INT UNSIGNED DEFAULT NULL, tipo_comprobante INT DEFAULT NULL, entorno_afip CHAR(4) NOT NULL, resultado CHAR(1) DEFAULT NULL, mail_enviado TINYINT(1) NOT NULL, mail_error_enviado DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_F9EBA0094854653A (pedido_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE factura ADD CONSTRAINT FK_F9EBA0094854653A FOREIGN KEY (pedido_id) REFERENCES pedido (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE factura');
    }
}
