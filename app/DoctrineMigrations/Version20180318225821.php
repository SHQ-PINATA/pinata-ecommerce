<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use AppBundle\Entity\Plataforma;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180318225821 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('INSERT INTO plataforma (id, nombre) VALUES("'.Plataforma::MERCADOLIBRE.'", "Mercadolibre")');
        $this->addSql('INSERT INTO plataforma_configuracion (id_plataforma, data_configuracion) VALUES("'.Plataforma::MERCADOLIBRE.'", "{}")');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {

        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('DELETE FROM plataforma where id = "'.Plataforma::MERCADOLIBRE.'")');
        $this->addSql('DELETE FROM plataforma where id_plataforma = "'.Plataforma::MERCADOLIBRE.'")');
    }
}
