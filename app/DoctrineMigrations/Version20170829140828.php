<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170829140828 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE descuento (id INT AUTO_INCREMENT NOT NULL, codigo VARCHAR(50) NOT NULL, tipo VARCHAR(5) NOT NULL, usoUnico TINYINT(1) NOT NULL, fechaDesde DATETIME NOT NULL, fechaHasta DATETIME NOT NULL, valor DOUBLE PRECISION DEFAULT NULL, montoMin DOUBLE PRECISION DEFAULT NULL, montoMax DOUBLE PRECISION DEFAULT NULL, activo TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_C5B0930420332D99 (codigo), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE descuento_personaje (descuento_id INT NOT NULL, personaje_id INT NOT NULL, INDEX IDX_AC18099FF045077C (descuento_id), INDEX IDX_AC18099F121EFAFB (personaje_id), PRIMARY KEY(descuento_id, personaje_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE descuento_categoria (descuento_id INT NOT NULL, categoria_id INT NOT NULL, INDEX IDX_B1AC0B3AF045077C (descuento_id), INDEX IDX_B1AC0B3A3397707A (categoria_id), PRIMARY KEY(descuento_id, categoria_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE descuento_aplicado (id INT AUTO_INCREMENT NOT NULL, pedido_id INT DEFAULT NULL, carritoId VARCHAR(100) DEFAULT NULL, codigo VARCHAR(50) NOT NULL, tipo VARCHAR(5) NOT NULL, monto_descontado DOUBLE PRECISION DEFAULT NULL, UNIQUE INDEX UNIQ_71763933FA2B9BC5 (carritoId), UNIQUE INDEX UNIQ_717639334854653A (pedido_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE descuento_personaje ADD CONSTRAINT FK_AC18099FF045077C FOREIGN KEY (descuento_id) REFERENCES descuento (id)');
        $this->addSql('ALTER TABLE descuento_personaje ADD CONSTRAINT FK_AC18099F121EFAFB FOREIGN KEY (personaje_id) REFERENCES personaje (id)');
        $this->addSql('ALTER TABLE descuento_categoria ADD CONSTRAINT FK_B1AC0B3AF045077C FOREIGN KEY (descuento_id) REFERENCES descuento (id)');
        $this->addSql('ALTER TABLE descuento_categoria ADD CONSTRAINT FK_B1AC0B3A3397707A FOREIGN KEY (categoria_id) REFERENCES categoria (id)');
        $this->addSql('ALTER TABLE descuento_aplicado ADD CONSTRAINT FK_717639334854653A FOREIGN KEY (pedido_id) REFERENCES pedido (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE descuento_personaje DROP FOREIGN KEY FK_AC18099FF045077C');
        $this->addSql('ALTER TABLE descuento_categoria DROP FOREIGN KEY FK_B1AC0B3AF045077C');
        $this->addSql('DROP TABLE descuento');
        $this->addSql('DROP TABLE descuento_personaje');
        $this->addSql('DROP TABLE descuento_categoria');
        $this->addSql('DROP TABLE descuento_aplicado');
    }
}
