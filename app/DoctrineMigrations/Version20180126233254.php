<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180126233254 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("INSERT INTO descuento (codigo, tipo, usoUnico, fechaDesde, fechaHasta, valor, montoMin, montoMax, excluir_sale, activo) VALUES ('CLARIN-365', 'PORCE', 0, '2018-02-01 00:00:00', '2099-02-01 00:00:00', 15, NULL, NULL, 1, 1)");

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql("DELETE FROM descuento WHERE 'codigo' = 'CLARIN-365'");
    }
}
