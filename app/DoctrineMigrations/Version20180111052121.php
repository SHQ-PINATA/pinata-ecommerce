<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180111052121 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE provincia ADD codigo_bejerman INT DEFAULT NULL');
        $orderedBejermanCodes = array(3, 2, 10, 22, 21, 8, 19, 5, 14, 23, 11, 6, 12, 13, 15, 16, 1, 9, 4, 24, 17, 7, 20, 18);

        $i = 1;
        foreach ($orderedBejermanCodes as $code) {
           $this->addSql("UPDATE provincia SET codigo_bejerman = ". $i ." where id = ". $code);
           $i++; 
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE provincia DROP codigo_bejerman');
    }
}
