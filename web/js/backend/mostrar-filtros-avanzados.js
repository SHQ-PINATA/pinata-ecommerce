$(document).ready(function(){
    $("#mostrar-filtros-avanzados").click(function(e){
        $(".filtros-avanzados").toggle();

        if($(".filtros-avanzados").is(":visible")){
	    	$("#mostrar-filtros-avanzados").html('Filtros Avanzados (-)')
        }else{
        	$("#mostrar-filtros-avanzados").html('Filtros Avanzados (+)')
        }
        e.preventDefault();
    });
});

