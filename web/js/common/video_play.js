$(document).ready(function(){
	
	$('.video').each(function(){
		if($(this).get(0).paused){
			$(this).parent().children(".playpause").css('display', 'none');
		}
		
	});
	$('.video').parent().click(function () {

	    if($(this).children(".video").get(0).paused){
	        $(this).children(".video").get(0).play();
	        $(this).children(".playpause").fadeOut();
	    }else{
	       $(this).children(".video").get(0).pause();
	        $(this).children(".playpause").fadeIn();
	    }
	});
});