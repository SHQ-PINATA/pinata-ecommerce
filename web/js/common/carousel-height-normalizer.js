$(window).on('load', function(){

	// Normalizo las alturas de los carruseles
	carouselNormalization();

	function carouselNormalization() {
		$('.carousel .carousel-inner').each(function(){
			var items = $(document).find('.item');

			if (items.length) {
			    normalizeHeights(items);
			}
		})
	}
	
	function normalizeHeights(items) {
		// reseteo el alto maximo porque sino al tomar la altura del elemento
		// me trae la altura maxima previa y no la altura del elemento
	    items.each(function() {
	        $(document).css('max-height',Number.MAX_SAFE_INTEGER + 'px');
	    });

	    var heights = []; // me guardo las alturas de los elementos de este carrusel
		var fullPageWidth = $(window).width();
	    items.each(function() { 
	    	// calculo la altura estimada en base al ancho de la pagina y el elemento
	    	heights.push(getEstimatedHeight($(document), fullPageWidth))
	    })
	    
	    shortest = Math.floor( Math.min.apply(null, heights) ); //cache shortest value
	    items.each(function() {
	        $(document).css('max-height',shortest + 'px');
	    });
	};

	// Calculo la altura estimativa. 
	function getEstimatedHeight(item, fullPageWidth){

		// Si el ancho de la pagina no coincide con el ancho del elemento, calculo el aprox
		if (Math.abs(fullPageWidth - item.outerWidth()) > 1){ 
    		if (item.width() > 0) {
    			return Math.floor( (fullPageWidth * item.height())/item.outerWidth() )
    			
    		}
    	}
    	// sino, devuelvo el original
		return Math.floor(item.height());
	}

	// Cada vez que resizeo, renormalizo los carruseles
    $(window).on('resize orientationchange', function () {
        carouselNormalization();
    });
});

