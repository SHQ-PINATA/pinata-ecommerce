
var count = 2;
var more_to_load = true;
var waiting_request = false; 
$(window).scroll(function(){
	if(!waiting_request && more_to_load && ($(window).scrollTop() + $(window).height()) >= $('.productos-container').height() - 700){
		loadProductos(count);
		count++;
	}
}); 

function loadProductos(pageNumber){
	waiting_request = true;
	var loader = '<div class="ajax-loader center" style="display:none"><img src="/img/loading-blue.gif"></div>'
	$('.productos-container .listado').append(loader).ready(function(){
		$('.productos-container .listado .ajax-loader').show('slow');
	})

	$.ajax({
		url: ajax_path+'?page='+pageNumber,
		type:'POST',
		data: $('.producto-filter-form').serialize(), 
		success: function(html){
			if (html.indexOf('No se encontraron resultados') !== -1) {
				more_to_load = false;
			}else{
				$('.productos-container .listado').append(html);   // This will be the div where our content will be loaded
			}
			waiting_request = false;
			$('.productos-container .listado .ajax-loader').animate({'opacity': 0}, 600, function(){
				$(this).hide('slow', function(){$(this).remove()})
			});
		}
	});
	
	return false;
}