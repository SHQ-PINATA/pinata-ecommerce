$(document).ready(function(){
	$(document).on('submit', '.productos-container form[name="agregar_producto_carrito_form"]', function(e) {
			e.preventDefault();
			// Estilos del boton durante el request
			// Tomo el boton
			var agregar_carrito_button = $( this ).find('.agregar-carrito');
			var carrito_container = $( this ).closest( '.agregar-carrito-container');
			
			disableAddButton(agregar_carrito_button)
			
			content = $( this ).serialize(),
			url = $( this ).attr( "action" );
			
			// Envio el form por ajax
			var posting = $.post( url, content )
				.done(function(data) {
						if (data.status == 'ok') {
							addItemToCartAnimation(data.cantidad);
							if (data.stock_disponible <= 0) {
								showOutOfStock(agregar_carrito_button);
							}
							showMsgAddingToCart(carrito_container, 'success', '¡Tu producto fue agregado con éxito!');
                            var previousItems = $('#carritoNumber').html();
                            $('#carritoNumberContainer').fadeOut();
                            var currentItems = parseInt(previousItems) + data.cantidad;
                            $('#carritoNumber').html(currentItems);
                            $('#carritoNumberContainer').fadeIn();
                            ga('send', 'event', 'add to cart', 'Producto agregado al carrito', '');

                            var precioFacebook = $('#precioFacebook').val(); 
                            var nombreProducto = $('#productName').val(); 
                            var skuProducto = $('#productSKU').val(); 

                            precioFacebook = precioFacebook.replace(/,/g, '-');

                            fbq('track', 'AddToCart', {
                                'content_name': nombreProducto,
                                'content_ids': [skuProducto], 
                                'content_type': 'product',
                                'value' : precioFacebook, 
                                'currency' : 'ARS'
                            });

						}else{
							if (data.stock_disponible <= 0) {
								showOutOfStock(agregar_carrito_button);
							}
							showMsgAddingToCart(carrito_container, 'success', data.error);
						}
					})
					.fail(function(data) {
						if ('404' == data.status) {
							showMsgAddingToCart(carrito_container, 'error', 'No se encontró el producto solicitado');
						}else{
							showMsgAddingToCart(carrito_container, 'error', 'No se pudo agregar el producto. Inténtelo más tarde nuevamente');
						}

					})
				.always(function() {
					enableAddButton(agregar_carrito_button)
				});
		
		})

		function showOutOfStock(button){
			button.addClass('out-of-stock');
			button.attr('disabled', true);
			button.html('Sin Stock');
			button.animate({opacity: .5})
		}

		function disableAddButton(button){
			button.attr('disabled', true);
			button.html('Agregando');
			button.animate({opacity: .5})
		}

		function enableAddButton(button){
			if (!button.hasClass('out-of-stock')) {
				button.attr('disabled', false);
				button.html('Agregar al carrito');
				button.animate({opacity: 1});
			}
		}

		function addItemToCartAnimation(cantidad){

			var prev_items = parseInt( $('.carrito-item-count').attr('data') );
			var new_count = prev_items + cantidad;
			if (1 == new_count) {
				newtext = '1 ítem';
			}else{
				newtext = new_count + ' ítems';
			}

			$('.carrito-item-count').animate({'opacity': 0}, 400, function(){
		        $(this).html(newtext).animate({'opacity': 1}, 400);
		        $(this).attr('data', newtext); 
		    });

	    	$('.carrito-container .add-success').animate({'opacity': 1}, 400);
			$('.carrito-container .carrito').addClass('carrito-added');

			setTimeout(function() {
				$('.carrito-container .add-success').animate({'opacity': 0}, 400);
				$('.carrito-added').removeClass('carrito-added');
			}, 7000);
		}

		function showMsgAddingToCart(container, type, msg){

			container.append('<div class="'+type+'" style="opacity:1; margin: 10px 0;">'+msg+'</div>');
			
			setTimeout(function() {
				container.find('.'+type).animate({'opacity': 0}, 400, function(){
					$(this).remove();
				});
			}, 4000);
		}

	/* Muestro imagenes de hover en el listado */
	$(".productos-container").on('mouseenter', '.item img', function() {
	    var firstImg = $(this).closest('.item').find('.hover-hide');
		var secondImg = $(this).closest('.item').find('.hover-show');

		if (firstImg.length && secondImg.length && firstImg.prop('complete') && secondImg.prop('complete')) {
			firstImg.addClass('hidden');
			secondImg.removeClass('hidden');
		}
	});
	$(".productos-container").on('mouseleave', '.item img', function() {
	    var firstImg = $(this).closest('.item').find('.hover-hide');
		var secondImg = $(this).closest('.item').find('.hover-show');

		if (firstImg.length && secondImg.length && firstImg.prop('complete') && secondImg.prop('complete')) {
			firstImg.removeClass('hidden');
			secondImg.addClass('hidden');
		}
	});

});
