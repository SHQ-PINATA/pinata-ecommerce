$(document).ready(function(){
	// =============================== Manejo formulario agregar producto al carrito ===============================
			$('.arrow-up').click(function(){
				var original = parseInt($('#agregar_producto_carrito_form_cantidad').val());
				var max = $('#agregar_producto_carrito_form_cantidad').attr('max');
				if (original < max) {
					$('#agregar_producto_carrito_form_cantidad').val(original + 1);
				}
			})

			$('.arrow-down').click(function(){
				var original = parseInt($('#agregar_producto_carrito_form_cantidad').val());
				if (original > 1 ) {
					$('#agregar_producto_carrito_form_cantidad').val(original - 1);
				}
			})

			$('form[name="agregar_producto_carrito_form"]').on('submit', function(e){
				e.preventDefault();
				// Estilos del boton durante el request
				var addButton = $( this ).find('.agregar-carrito');
				addButton.attr('disabled', true);
				addButton.html('Agregando');
				addButton.animate({opacity: .5})

				var form = $( this ); 
				content = form.serialize(),
				url = form.attr( "action" );

				// Envio el form por ajax
				var posting = $.post( url, content )
					.done(function(data) {
						if (data.status == 'ok') {
							updateMaxAddableQuantity(data.stock_disponible)
							addItemToCartAnimation(data.cantidad);
							if (data.stock_disponible <= 0) {
								showOutOfStock(addButton);
							}else{
								enableButton(addButton);
							}
							showSuccessAddingToCart(form, '¡Tu producto fue agregado con éxito!');
                            var previousItems = $('#carritoNumber').html();
                            $('#carritoNumberContainer').fadeOut();
                            var currentItems = parseInt(previousItems) + data.cantidad;
                            $('#carritoNumber').html(currentItems);
                            $('#carritoNumberContainer').fadeIn();
                            ga('send', 'event', 'add to cart', 'Producto agregado al carrito', '');

                            var precioFacebook = $('#precioFacebook').val(); 
                            var nombreProducto = $('#productName').val(); 
                            var skuProducto = $('#productSKU').val(); 

                            precioFacebook = precioFacebook.replace(/,/g, '-');

                            fbq('track', 'AddToCart', {
                                'content_name': nombreProducto,
                                'content_ids': [skuProducto], 
                                'content_type': 'product',
                                'value' : precioFacebook, 
                                'currency' : 'ARS'
                            });
						}else{
							showErrorAddingToCart(form, data.error);
							if (data.stock_disponible <= 0) {
								showOutOfStock(addButton);
							}else{
								enableButton(addButton);
							}
						}
					})
					.fail(function(data) {
						if ('404' == data.status) {
							showErrorAddingToCart('No se encontró el producto solicitado');
						}else{
							showErrorAddingToCart('No se pudo agregar el producto. Inténtelo más tarde nuevamente.');
						}
						enableButton(addButton);
					})
			
			})

			function showOutOfStock(addButton){
				addButton.attr('disabled', true);
				addButton.html('Sin Stock');
				addButton.animate({opacity: .5})
			}

			function enableButton(addButton){
				addButton.attr('disabled', false);
				addButton.html('Agregar al carrito');
				addButton.animate({opacity: 1});
			}

			// Le hago un update al maximo stock que puede elegir para agregar al stock
			function updateMaxAddableQuantity(stock_disponible){
				var current_max = $('.cantidad').attr('max');
				$('.cantidad').attr('max', stock_disponible);
			}

			// Hago la animacion del carrito al agregar un producto actualizando items
			function addItemToCartAnimation(cantidad){

				var prev_items = parseInt( $('.carrito-item-count').attr('data') );
				var new_count = prev_items + cantidad;
				if (1 == new_count) {
					newtext = '1 ítem';
				}else{
					newtext = new_count + ' ítems';
				}

				$('.carrito-item-count').animate({'opacity': 0}, 400, function(){
			        $(this).html(newtext).animate({'opacity': 1}, 400);
			        $(this).attr('data', newtext); 
			    });

		    	$('.carrito-container .add-success').animate({'opacity': 1}, 400);
				$('.carrito-container .carrito').addClass('carrito-added');

				setTimeout(function() {
					$('.carrito-container .add-success').animate({'opacity': 0}, 400);
					$('.carrito-added').removeClass('carrito-added');
				}, 7000);
			}

			function showErrorAddingToCart(form, error){
				append_to_element = form.closest('.agregar-carrito-container');
				append_to_element.append('<div class="error" style="opacity:0">'+error+'</div>');
				
				error_element = append_to_element.find('.error')
				error_element.animate({'opacity': 1}, 400);

				setTimeout(function() {
				  error_element.animate({'opacity': 0}, 400, function(){
					  error_element.remove();
					  
				})
				}, 7000);
			}

			function showSuccessAddingToCart(form, msg){
				append_to_element = form.closest('.agregar-carrito-container');
				append_to_element.append('<div class="success" style="opacity:0">'+msg+'</div>');

				success_element = append_to_element.find('.success');
				success_element.animate({'opacity': 1}, 400);

				setTimeout(function() {
				  	success_element.animate({'opacity': 0}, 400, function(){
						success_element.remove();
				})
				}, 7000);
			}
// ======================================= Zoom Imagen ==============================================
			// Zoom sobre la imagen principal
			$(".detalle-producto .main-image img").elevateZoom({
				zoomType : "inner",
				cursor: "crosshair"
			});
			// Obtengo la data para cambiarle la imagen de zoom cuando cambio
			// la imagen principal
			var ez = $(".detalle-producto .main-image img").data('elevateZoom');

			// Cambio de imagen principal al clickear las otras
			$('.img-producto').click(function(){
				var that = $(this);
				$('.main-image img').animate({'opacity': 0}, 200, function(){
					$('.main-image img').attr('src', that.attr('src'));
					$('.main-image img').attr('data-zoom-image', that.attr('data-zoom-image'));
					// cambio la imagn que usa para el zoom
					ez.swaptheimage(that.attr('src'), that.attr('src'));
				})
				$('.main-image img').animate({'opacity': 1}, 200);
			})

// =================================== Formulario cotizacion de envio =========================================

			$('form.cotizar-envio-form').on('submit', function(e){
				e.preventDefault();
				// Estilos del boton durante el request
				var submitButton = $( this ).find('input[type=submit]')
				submitButton.attr('disabled', true);
				submitButton.html('Consultando');
				submitButton.animate({opacity: .5})

				var form = $( this ); 
				content = form.serialize(),
				url = form.attr( "action" );

				// Envio el form por ajax
				var posting = $.post( url, content )
					.done(function(data) {
						if (data.status == 'ok') {
							loadCotizaciones(data.cotizaciones)
						}
					})
					.always(function() {
						submitButton.attr('disabled', false);
						submitButton.html('Consultar');
						submitButton.animate({opacity: 1})
					})
			
			})

			function loadCotizaciones(cotizaciones){
				$('.cotizaciones').empty();
				$('.cotizaciones').css('opacity', 0)
				var elemento_to_append = '';
				for (var i = 0; i < cotizaciones.length; i++) {
					var precio = cotizaciones[i].valor;
					var servicio = cotizaciones[i].servicio_nombre;
					var estimacion = cotizaciones[i].estimacion_entrega;
					elemento_to_append += '<p>Envío a Domicilio '+servicio+' $'+precio+'</p>';
				}
				elemento_to_append += '<p>Pickup Gratis, retiro en Nuñez - CABA</p>';
				elemento_to_append += '<p>Envío a Sucursal sujeto a correo</p>';
				$('.cotizaciones').append(elemento_to_append);
				$('.cotizaciones').css('opacity', 1)
			}

// =============================== Select talles ==============================================================

            $('#talleSelector').change(function() {
                var talleId = this.value;
                    if (talleId) {
                    document.location = '/producto/' + talleId; 
                }
            });
});
