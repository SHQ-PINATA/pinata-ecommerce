$(document).ready(function() {
	var showModal = !Cookies.get('modal_shown');
	Cookies.set('modal_shown', 'true');

		var imgBack = new Image();
		var imgMickey = new Image();
		var backLoaded = false;
		var mickLoaded = false;

        if (showModal) {
		imgMickey.onload = function () { 
			mickLoaded = true;
			if( mickLoaded && backLoaded ){
			    $('#subscribe-modal').modal('show');
			}
		};

		imgBack.onload = function () { 
			backLoaded = true;
			if( mickLoaded && backLoaded ){
			    $('#subscribe-modal').modal('show');
			}
		};

		imgBack.src = "/img/paper-background.png";
		imgMickey.src = "/img/mickey-pinata.png";
        {
});
