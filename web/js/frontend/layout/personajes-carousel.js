$(window).on('load', function(){

	var slider = $('.personajes-slider')
        .on('init', function(slick) {
            $('.personajes-slider').fadeIn(3000);
        }).slick({
	    /*variableWidth: true,*/
	    slidesToShow: 12,
		slidesToScroll: 4,
		autoplay: false,
		variableWidth: false,
		infinite: true,
		autoplaySpeed: 2000,
	  	responsive: [
	    {
	      breakpoint: 1200,
	      settings: {
	        slidesToShow: 9,
	        slidesToScroll: 4,
	        infinite: true,
	      }
	    },
	    {
	      breakpoint: 1000,
	      settings: {
	        slidesToShow: 7,
	        slidesToScroll: 4,
	        infinite: true,
	      }
	    },
	    {
	      breakpoint: 800,
	      settings: {
	        slidesToShow: 6,
	        slidesToScroll: 3,
	        infinite: true,
	      }
	    },
	    {
	      breakpoint: 700,
	      settings: {
	        slidesToShow: 5,
	        slidesToScroll: 3,
	        infinite: true,
	      }
	    },
	    {
	      breakpoint: 600,
	      settings: {
	        slidesToShow: 4,
	        slidesToScroll: 3,
	        infinite: true,
	      }
	    },
	    {
	      breakpoint: 500,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 3,
	        infinite: true,
	      }
	    },
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	});
});