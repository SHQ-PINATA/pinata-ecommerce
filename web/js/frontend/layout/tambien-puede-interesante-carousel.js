$(window).on('load', function(){

	var slider = $('.tambien-puede-interesarte-slider')
        .on('init', function(slick) {
            $('.tambien-puede-interesarte-slider').fadeIn(3000);
        }).slick({
	    /*variableWidth: true,*/
	    slidesToShow: 6,
		slidesToScroll: 1,
		autoplay: false,
		autoplaySpeed: 2000,
	  	responsive: [
	    {
	      breakpoint: 1200,
	      settings: {
	        slidesToShow: 5,
	        slidesToScroll: 1,
	        infinite: true,
	      }
	    },
		    {
	      breakpoint: 1000,
	      settings: {
	        slidesToShow: 4,
	        slidesToScroll: 1,
	        infinite: true,
	      }
	    },
	    {
	      breakpoint: 993,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 1,
	        infinite: true,
	      }
	    },
	    {
	      breakpoint: 600,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 1,
	        infinite: true,
	      }
	    },
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	});
});