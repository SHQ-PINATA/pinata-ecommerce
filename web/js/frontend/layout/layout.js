$(document).ready(function(){

    //MOBILE
    $('.categoriasToggle').click(function() {
        if ($('.categoriaMobileLi').last().is(':visible')) {
            $('.categoriaMobileLi').slideUp();
        } else {
            $('.categoriaMobileLi').slideDown();
        }
    });

    $('.search-containerMobile').click(function() {
        if ($('#searchboxMobile').is(':visible')) {
            $('#searchboxMobile').slideUp();
        } else {
            $('#searchboxMobile').slideDown();
        } 
    });

    $('.burger-container').click(function() {
        if ($('#burgerBox').is(':visible')) {
            $('#burgerBox').slideUp();
        } else {
            $('#burgerBox').slideDown();
        } 
    });


    //TOOLBAR
    
    $('.search-container').click(function() {
        if ($('#searchbox').is(':visible')) {
            $('#searchbox').hide();
        } else {
            $('#searchbox').show();
        } 
    });

    $('.login-container').click(function() {
        if ($('#userbox').is(':visible')) {
            $('#userbox').hide();
        } else {
            $('#userbox').show();
        } 
    });


    $('.carrito-container').click(function() {
        if ($('#cartbox').is(':visible')) {
            $('#cartbox').hide();
            $('#loadingCart').show();
        } else {
            $('#cartbox').show();
        } 
        $('#cartItemsContainer').html('');
        $.ajax({
            url: "/ajax/get_cart",
            global: false, type: "GET", 
            cache: false,
            success: function(data) {
                if (!data.length) {
                    $('#emptyCartMsgContainer').show();
                } else {
                    $('#emptyCartMsgContainer').hide(); 
                }

                for (var i = 0; i < data.length; i++) {
                    row = data[i];
                    if (row.nombre.length > 25) {
                        var formattedNombre = row.nombre.substring(0, 25) + '...';
                    } else {
                        var formattedNombre = row.nombre;
                    }
                    var html = $('#exampleCartRow').clone();
                    $(html).find('.qty').html(row.cantidad);
                    $(html).find('.nombre').html(formattedNombre);
                    $(html).find('.precio').html('$' + row.precio.toFixed(2));
                    $(html).removeAttr('id');
                    $('#cartItemsContainer').append(html);
                    $(html).show();
                } 
                $('#loadingCart').hide();
            }
        });
    });

    $(document).mouseup(function(e) {
        var container = $('.toolbar-box');
        if (!container.is(e.target) && container.has(e.target).length === 0) 
        {
            container.hide();
            $('#loadingCart').show();
        } 
    });

    //SCROLL UP ARROW

    var $arrow = $('.arrowContainer');
    $(document).scroll(function() {
            $arrow.css({display: $(this).scrollTop()>100 ? "block":"none"});
    });

    $arrow.click(function() {
        $("html, body").animate({ scrollTop: 0 }, "slow"); 
    });

});
