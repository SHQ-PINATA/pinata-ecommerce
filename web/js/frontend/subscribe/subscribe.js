$(document).ready(function(){
	$('.form-subscribe-modal, .form-subscribe-footer').on('submit', function(e){
			e.preventDefault();
			// Estilos del boton durante el request
			disableSendButtonModal();
			disableSendButtonFooter();

			content = $( this ).serialize(),
			url = $( this ).attr( "action" );
			
			// Envio el form por ajax
			var posting = $.post( url, content )
				.done(function(data) {
					if (data.status == 'ok') {
						showMessageSuccessModalAndDismiss();
						showMessageSuccessFooter();
					}else{
						showMessageErrorModal(data.msg);
						showMessageErrorFooter(data.msg);
					}
				})
				.fail(function(data) {
					showMessageErrorModal('No se pudo suscribir al newsletter. Inténtelo más tarde nuevamente.');
					showMessageErrorFooter('No se pudo suscribir al newsletter. Inténtelo más tarde nuevamente.');
				})
				.always(function() {
					enableSendButtonModal();
					enableSendButtonFooter();
				});		
		})


	/* Modal Functions */
	function disableSendButtonModal(){
		$('#subscribe-modal .subscribe-button').html('Enviando');
		$('#subscribe-modal .subscribe-button').animate({'opacity': 0.5}, 400, function(){
			$(this).attr('disabled', true);
		});
	}

	function enableSendButtonModal(){
		$('#subscribe-modal .subscribe-button').html('Suscribirme');
		$('#subscribe-modal .subscribe-button').animate({'opacity': 1}, 400, function(){
			$(this).removeAttr('disabled');
		});
	}

	function showMessageErrorModal(error){
		// Agrego el elemento
		var element = '<div class="error" style="display:none"><p>'+error+'</p></div>';
		$('#subscribe-modal .rounded-white').after(element).ready(function(){
			$('#subscribe-modal .error').show('slow');
		});

		// A los 5 segundos lo saco
		setTimeout(function() {
			$('#subscribe-modal .error').hide('slow', function(){$(this).remove()});
		}, 5000);
	}

	function showMessageSuccessModalAndDismiss(){

		$('#subscribe-modal .text-container').html('');
		
		$('#subscribe-modal form').hide('slow', function(){
			var element = '<div class="subscribe-success center" style="display:none;"><div class="text inline">¡Ya estás suscripto!</div><div class="inline mickey-wink"></div></div>';
			$('#subscribe-modal .text-container').append(element)
			$('#subscribe-modal .subscribe-success').show('slow');
		})
		
		
		setTimeout(function() {
		    $('#subscribe-modal').modal('hide');
		}, 5000);
		
		
	}

	/* End Modal Functions */

	/* Footer Functions */


	function disableSendButtonFooter(){
		$('footer .subscribe-button').html('Enviando');
		$('footer .subscribe-button').animate({'opacity': 0.5}, 400, function(){
			$(this).attr('disabled', true);
		});
	}

	function enableSendButtonFooter(){
		$('footer .subscribe-button').html('Suscribirme');
		$('footer .subscribe-button').animate({'opacity': 1}, 400, function(){
			$(this).removeAttr('disabled');
		});
	}
	
	function showMessageSuccessFooter(){
		var element = '<div class="subscribe-success" style="display:none"><div class="text inline">¡Ya estás suscripto!</div><div class="inline mickey-wink"></div></div>';
		$('footer .subscribe-container').append(element)
		$('footer .subscribe-success').show('slow');		
	}

	function showMessageErrorFooter(error){
		// Agrego el elemento
		var element = '<div class="error" style="display:none"><p>'+error+'</p></div>';
		$('footer .subscribe-container').append(element).ready(function(){
			$('footer .subscribe-container .error').show('slow');
		});

		// A los 5 segundos lo saco
		setTimeout(function() {
			$('footer .subscribe-container .error').hide('slow', function(){$(this).remove()});
		}, 5000);
	}	

	/* End Footer Functions */
});
