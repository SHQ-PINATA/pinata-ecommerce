$(document).ready(function(){
	$('form.tracking').on('submit', function(e){
		e.preventDefault();
		var form = $( this );
		url = form.attr( "action" );

		emptyList();

		if ($('#form_tracking').val() == '') {
			$('.tracking-information').append('<li>Complete el campo del numero de seguimiento</li>')
			return
		}

		showLoading();
		// Envio el form por ajax
		var jqxhr = $.getJSON( url, {tracking_number : $('#form_tracking').val() } )
			.done(function(data) {
				loadInfoToList(data)
			})
			.always(function() {
				hideLoading();
			});
	})

	function emptyList(){
		$('.tracking-information').empty();
	}

	function showLoading(){
		$('.consultar').val('Consultando');
		disableButton();
	}

	function hideLoading(){
		$('.consultar').val('Consultar');
		enableButton();
	}

	function loadInfoToList(data){
		if (isEmptyData(data)) {
			$('.tracking-information').append('<li>No se encontró el numero de seguimiento</li>')
			return;
		}

		data.tracking.forEach(function(element){
			var fecha = '<div class="fecha inline">'+element.fecha+'</div>';
			var info = '<div class="info inline">'+element.mensaje+'</div>';
			$('.tracking-information').append('<li>'+fecha+info+'</li>')
		})
	}

	function disableButton(){
		$('.consultar').attr('disabled', 'disabled');
		$('.consultar').css({'opacity':0.3, 'cursor':'default'});
	}

	function enableButton(){
		$('.consultar').removeAttr('disabled')
		$('.consultar').css({'opacity':1, 'cursor': 'pointer'})
	}

	function isEmptyData(data){
		return Object.keys(data).length === 0;
	}
/*
    $('.consultar').attr('disabled',true);
    $('#form_tracking').on("click change paste keyup", function(){
        if($(this).val().length !=0)
        	enableButton()
        else
            disableButton()
    })

    $('#form_tracking').trigger('change');*/
});