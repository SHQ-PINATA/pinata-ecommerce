$(document).ready(function(){
	// Refactor to: https://learn.jquery.com/code-organization/concepts/
	
	var sucursales_info = [];	

	// Esta funcion es para ver cuando termina de tipear el usuario
    $.fn.extend({
        donetyping: function(callback,timeout){
            timeout = timeout || 1e3; // 1 second default timeout
            var timeoutReference,
                doneTyping = function(el){
                    if (!timeoutReference) return;
                    timeoutReference = null;
                    callback.call(el);
                };
            return this.each(function(i,el){
                var $el = $(el);
                // Chrome Fix (Use keyup over keypress to detect backspace)
                // thank you @palerdot
                $el.is(':input') && $el.on('keyup keypress paste',function(e){
                    // This catches the backspace button in chrome, but also prevents
                    // the event from triggering too preemptively. Without this line,
                    // using tab/shift+tab will make the focused element fire the callback.
                    if (e.type=='keyup' && e.keyCode!=8) return;

                    // Check if timeout has been set. If it has, "reset" the clock and
                    // start over again.
                    if (timeoutReference) clearTimeout(timeoutReference);
                    timeoutReference = setTimeout(function(){
                        // if we made it here, our timeout has elapsed. Fire the
                        // callback
                        doneTyping(el);
                    }, timeout);
                }).on('blur',function(){
                    // If we can, fire the event since we're leaving the field
                    doneTyping(el);
                });
            });
        }
    });

	// Submit de la form que esta visible
	$('.continuar').click(function(e){
		var selected_type = $(' input[name=select-envio-type]:checked').attr('data')
		if (selected_type ) {
			$('form[name='+selected_type+']').submit();
		}
	});

	// Cambio las forms de acuerdo a la que se elija
	$('input[name=select-envio-type]').change(function(){
		var type = $('input[name=select-envio-type]:checked').val();
		hideAndShowForms(type);
	});

	function hideAndShowForms(type = 'domicilio'){
		$('.form-envio-container').hide();
		$('.'+type+'-container').show();
	}

	// Cotizar a Domicilio
	$('#envio_domicilio_form_provincia').on('change', function(){
		emptyDomicilioOptions();
		var prov = $('#envio_domicilio_form_provincia').val();
		var cp = $('#envio_domicilio_form_codigoPostal').val();
		if (prov && cp) {
			cotizarDomicilio(prov, cp);
		}
	});

	$('#envio_domicilio_form_codigoPostal').donetyping(function(){
		emptyDomicilioOptions();
		var prov = $('#envio_domicilio_form_provincia').val();
		var cp = $('#envio_domicilio_form_codigoPostal').val();
		if (prov && cp) {

			cotizarDomicilio(prov, cp);
		}
	});

	// Cagar Localidades Sucursal
	$('#envio_sucursal_correo_form_provincia').change(function(){
		emptySucursalOptions();
		showLoaderSucursal('localidades');
		loadLocalidadesDeSucursales();
	});

	// Cotizar a Sucursal
	$('#envio_sucursal_correo_form_localidadId').change(function(){
		emptySucursalOptions();
		var provincia_id =	$('#envio_sucursal_correo_form_provincia').val()
		var localidad_id = $('#envio_sucursal_correo_form_localidadId').val()
		if( provincia_id && localidad_id ){
			cotizarSucursal(provincia_id, localidad_id)
		}
	})

	function loadLocalidadesDeSucursales(){
		var provincia_id = $('#envio_sucursal_correo_form_provincia').find(':selected').val();
		var jqxhr = $.getJSON( url_localidades_sucursal, {provincia_id: provincia_id} )
			.done(function(data) {
				changeLocalidadesSucursal(data)
			})
			.fail(function(data) {
				//console.log( "error" );
			})
	}

	function cotizarDomicilio(prov, cp){
		showLoaderDomicilio();

		var selected_option = $('#envio_domicilio_form_servicio').val();
		var jqxhr = $.getJSON( url_cotizar_domicilio, {provincia_id : prov, codigo_postal : cp} )
			.done(function(data) {
				addCotizacionDomicilioOptions(data)
				//console.log( "success" );
			})
			.fail(function(data) {
				//console.log( "error" );
			})
			.always(function(data) {
				if (selected_option && domicilio_submitted) {
					$('.opciones-domicilio input#domicilio-'+selected_option).attr('checked', true);
					domicilio_submitted = false;
				}
				//console.log( "complete" );
			});
	}

	function cotizarSucursal(provincia, localidad){
		showLoaderSucursal('servicios de correo');
		var selected_suc = $('#envio_sucursal_correo_form_sucursal_id').val();
		var jqxhr = $.getJSON( url_cotizar_sucursal, {provincia_id : provincia, localidad_id : localidad} )
			.done(function(data) {
				addCotizacionSucursalOptions(data)
				//console.log( "success" );
			})
			.fail(function(data) {
				//console.log( "error" );
			})
			.always(function(data) {
				if (selected_suc) {
					$('.opciones-sucursal input#'+selected_suc).attr('checked', true);
					updateSucursalInfo(selected_suc);
				}
				//console.log( "complete" );
			});
	}


	// Cargo la tabla con la data de las cotizacions para envio a domicilio
	function addCotizacionDomicilioOptions(options){
		var content = '';
		if(options.length > 0){
			var table_start = '<table> \
				<thead> \
					<th>Servicio</th> \
					<th>Costo</th> \
					<th>Tiempo Estimado</th> \
				</thead> \
				<tbody>';

			for (var i = 0; i < options.length; i++) {

				var template =
				'<tr>\
					<td><input type="radio" name="select-envio-domicilio" value="__value__" id="domicilio-__index__" class="radio-blue">\
					<label for="domicilio-__index__" class="radio-blue-label">__servicio_nombre__</label></td> \
					<td>__valor__</td> \
					<td>__estimacion_entrega__</td> \
				</tr>';
				var newOption = template;
				newOption = newOption.replace(/__index__/g, options[i].servicio);
				newOption = newOption.replace(/__value__/g, options[i].modalidad +';'+options[i].servicio);
				newOption = newOption.replace(/__servicio_nombre__/g, options[i].servicio_nombre);
				newOption = newOption.replace(/__valor__/g, options[i].valor == 0 ? 'Gratis' : '$'+options[i].valor);
				newOption = newOption.replace(/__estimacion_entrega__/g, options[i].estimacion_entrega);
				
				table_start += newOption;
			}
			table_start += '</tbody></table>';
			content = table_start;
		}else{
			content = '<p>No se encontraron servicios de correo.</p>'
		}

		$('.opciones-domicilio').empty();
		$('.opciones-domicilio').append(content);
	}

	// Agrego las opciones del select de Localidades para Sucursal (me las pasan por parametro)
	function changeLocalidadesSucursal(options){
		var select = $('#envio_sucursal_correo_form_localidadId');
		select.empty();
		var newOptions = '<option value="" selected="selected">Localidad</option>';
		for (var i = 0; i < options.length; i++) {
			newOptions += '<option value="'+ options[i].id +'">'+ options[i].nombre +'</option>';
		}
		select.append(newOptions);
		emptySucursalOptions()
	}

	// Cargo la tabla con la data de las cotizacions para envio a domicilio
	function addCotizacionSucursalOptions(options){
		var content = '';
		if(options.length > 0){
			var table_options = '<table> \
				<thead> \
					<th>Sucursal</th> \
					<th>Costo</th> \
					<th>Tiempo Estimado</th> \
				</thead> \
				<tbody>';
			var template = '';
			var servicio_nombre = '';
			var newOption = '';
			template =
			'<tr>\
				<td><input type="radio" name="select-envio-sucursal" value="__value__" id="__sucursal_id__" class="radio-blue">\
					<label for="__sucursal_id__" class="radio-blue-label">__servicio_nombre__</label></td> \
				<td>__valor__</td> \
				<td>__estimacion_entrega__</td> \
			</tr>';
			for (var i = 0; i < options.length; i++) {
				newOption = template;
				servicio_nombre = '<div class="sucursal-option-nombre">'+options[i].sucursal.correo.nombre+'</div><div class="sucursal-option-direccion">'+options[i].sucursal.calle+' '+options[i].sucursal.numero+'</div>';
				newOption = newOption.replace(/__sucursal_id__/g, options[i].sucursal.id);
				newOption = newOption.replace(/__value__/g, options[i].modalidad +';'+options[i].servicio+';'+options[i].sucursal.id+';'+options[i].sucursal.correo.id);
				newOption = newOption.replace(/__valor__/g, options[i].valor == 0 ? 'Gratis' : '$'+options[i].valor);
				newOption = newOption.replace(/__estimacion_entrega__/g, options[i].estimacion_entrega);
				newOption = newOption.replace(/__servicio_nombre__/g, servicio_nombre);
				
				sucursales_info[options[i].sucursal.id] = options[i].sucursal;
				table_options += newOption;
			}

			table_options += '</tbody></table>';
			content = table_options;
		}else{
			content = '<p>No se encontraron sucursales en la zona.</p>'
		}

		emptySucursalInfo();
		$('.opciones-sucursal').empty();
		$('.opciones-sucursal').append(content);
	}

	// Seteo la opcion de Domicilio elegida en un campo hidden
	$('.opciones-domicilio').on('change', 'input[name=select-envio-domicilio]', function(){
		var values = $('.opciones-domicilio input[name=select-envio-domicilio]:checked').val().split(";");
		var modalidad = values[0];
		var servicio = values[1];
		$('#envio_domicilio_form_servicio').val(servicio)
	});

	// Seteo la opcion de Sucursal elegida en un campo hidden
	$('.opciones-sucursal').on('change', 'input[name=select-envio-sucursal]', function(){
		var values = $('.opciones-sucursal input[name=select-envio-sucursal]:checked').val().split(";");
		var modalidad = values[0];
		var servicio = values[1];
		var sucursal_id = values[2];
		var correo_id = values[3];
		$('#envio_sucursal_correo_form_servicio').val(servicio)
		$('#envio_sucursal_correo_form_sucursal_id').val(sucursal_id)
		updateSucursalInfo(sucursal_id);
	});

	// Borro las opciones de envios a domicilio y seteo en null los valores de los input hidden	
	function emptyDomicilioOptions(){	
		$('.opciones-domicilio').empty();
		$('#envio_domicilio_form_servicio').val('');
	}

	// Borro las opciones de sucursal y seteo en null los valores de los input hidden
	function emptySucursalOptions(){	
		$('.opciones-sucursal').empty();
		$('#envio_sucursal_correo_form_servicio_correo_id').val('');
		$('#envio_sucursal_correo_form_servicio_modalidad').val('');
		$('#envio_sucursal_correo_form_servicio').val('');
		$('#envio_sucursal_correo_form_sucursal_id').val('');
	}

	function emptySucursalInfo(){
		$('.informacion-sucursal').empty();
	}

	function updateSucursalInfo(sucursal_id){
		if (!sucursales_info[sucursal_id]) { return }
		var container = $('.informacion-sucursal');
		container.empty();
		var sucursal = sucursales_info[sucursal_id];
		var info = '\
		<div>\
			<h3 class="suc-info-title">Información de la sucursal</h3>\
			<br>';
		if(sucursal.calle && sucursal.numero){
			info += '<p>Dirección:<br>'+sucursal.calle +' '+ sucursal.numero+'</p>'
		}
		if(sucursal.horario){
			info += '<p>Horario de atencion:<br>'+sucursal.horario+'</p>\
			<br>'
		}
		if(sucursal.telefono){
			info += '<p>Telefono:<br>'+sucursal.telefono+'</p>'
		}
		info += '</div>';

		container.append(info);
	}

	function showLoaderDomicilio(){
		$('.opciones-domicilio').empty();
		$('.opciones-domicilio').append('\
			<div>Buscando servicios de correo...</div>\
			')
	}

	function showLoaderSucursal(search){
		$('.opciones-sucursal').empty();
		$('.opciones-sucursal').append('\
			<div>Buscando '+search+'...</div>\
			')
	}

	function hideLoaderDomicilio(){
		$('.opciones-domicilio').empty();
	}

	function hideLoaderSucursal(){
		$('.opciones-sucursal').empty();
	}



	var provincia_id =	$('#envio_sucursal_correo_form_provincia').val()
	var localidad_id = $('#envio_sucursal_correo_form_localidadId').val()
	if( provincia_id && localidad_id ){
			cotizarSucursal(provincia_id, localidad_id)
	}

	var provincia_id = $('#envio_domicilio_form_provincia').val();
	var cp = $('#envio_domicilio_form_codigoPostal').val();
	if (provincia_id && cp) {
		cotizarDomicilio(provincia_id, cp);
	}

	// Selecciono el form que se submiteo
	if (sucursal_submitted) {
		$('input[name=select-envio-type]').eq(1).attr('checked', true);
	} else if (pickup_submitted) {
		$('input[name=select-envio-type]').eq(2).attr('checked', true);
	} else if (domicilio_submitted){
		$('input[name=select-envio-type]').eq(0).attr('checked', true);
	}else{
		$('input[name=select-envio-type]').eq(0).attr('checked', true);
	}
	$('input[name=select-envio-type]').trigger('change');

	//$('#envio_domicilio_form_codigoPostal').trigger('change');

});

