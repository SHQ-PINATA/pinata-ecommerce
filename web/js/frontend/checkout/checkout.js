$(document).ready(function(){

    // custom fn para identificar cuándo un usuario termina de tipear en un input
    $.fn.extend({
        donetyping: function(callback,timeout){
            timeout = timeout || 1e3; // 1 second default timeout
            var timeoutReference,
                doneTyping = function(el){
                    if (!timeoutReference) return;
                    timeoutReference = null;
                    callback.call(el);
                };
            return this.each(function(i,el){
                var $el = $(el);
                $el.is(':input') && $el.on('keyup keypress paste',function(e){
                    if (e.type=='keyup' && e.keyCode!=8) return;
                    if (timeoutReference) clearTimeout(timeoutReference);
                    timeoutReference = setTimeout(function(){
                        doneTyping(el);
                    }, timeout);
                }).on('blur',function(){
                    doneTyping(el);
                });
            });
        }
    });


    // ======================== PASO 1 ==========================
    
    // . Register form display
    
    $('.registerLink').click(function(event) {
        event.preventDefault();
        $('.loginContainer').hide();
        $('.registerContainer').slideDown();
    });

    // . Login form display

    $('.loginLink').click(function(event) {
        event.preventDefault();
        $('.registerContainer').hide();
        $('.loginContainer').slideDown();
    });


    // ======================== PASO 2 ==========================

    // . Envio form display

	$('input[name=select-envio-type]').change(function(){
		var type = $('input[name=select-envio-type]:checked').val();
		hideAndShowForms(type);
	});

    var envioType = $('input[name=select-envio-type]:checked').val();
    $('#selectedEnvioForm').val(envioType);

	function hideAndShowForms(type = 'domicilio'){
		$('.form-envio-container').hide();
		$('.envio-'+type+'-container').slideDown();
        $('#selectedEnvioForm').val(type);
        $('.redBorder').removeClass('redBorder');
	}

    $('.envioSubmit').click(function(event) {
        event.preventDefault();    
        var formToSubmit = $('#selectedEnvioForm').val();
        if (formToSubmit == 'sucursal') {
            formToSubmit += '_correo';
        }

        $('form[name="envio_'+formToSubmit+'_form"] input').each(function() {
            console.log();
            var inputValue = $(this).val();
            if (!inputValue) {
                $(this).addClass('redBorder');
            } else {
                $(this).removeClass('redBorder');
            } 
        });

        var areEnvioFormErrors = $('.redBorder').length;
        if (!areEnvioFormErrors) {
            ga('send', 'event', 'Paso 3', 'Envio', '');
            $('form[name="envio_'+formToSubmit+'_form').submit();
        }
    });

    // . Envio clear form

    $('.clearEnvioForm').click( function(event) {
        event.preventDefault();
        $('form[name="envio_pickup_form"]').find(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
        $('form[name="envio_sucursal_correo_form"]').find(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
        $('form[name="envio_domicilio_form"]').find(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
    });

	// . Cotizacion de envios : A domicilio
    
	$('#envio_domicilio_form_provincia').on('change', function(){
		emptyDomicilioOptions();
		var prov = $('#envio_domicilio_form_provincia').val();
		var cp = $('#envio_domicilio_form_codigoPostal').val();
		if (prov && cp) {
			cotizarDomicilio(prov, cp);
		}
	});

	$('#envio_domicilio_form_codigoPostal').donetyping(function(){
		emptyDomicilioOptions();
		var prov = $('#envio_domicilio_form_provincia').val();
		var cp = $('#envio_domicilio_form_codigoPostal').val();
		if (prov && cp) {
			cotizarDomicilio(prov, cp);
		}
	});

	var provincia_id = $('#envio_domicilio_form_provincia').val();
	var cp = $('#envio_domicilio_form_codigoPostal').val();
	if (provincia_id && cp) {
		cotizarDomicilio(provincia_id, cp);
	}

	function cotizarDomicilio(prov, cp){
		showLoaderDomicilio();
		var selected_option = $('#envio_domicilio_form_servicio').val();
		var jqxhr = $.getJSON( url_cotizar_domicilio, {provincia_id : prov, codigo_postal : cp} )
			.done(function(data) {
				addCotizacionDomicilioOptions(data)
			})
			.fail(function(data) {
			})
			.always(function(data) {
				if (selected_option && domicilio_submitted) {
					$('.opciones-domicilio input#domicilio-'+selected_option).attr('checked', true);
					domicilio_submitted = false;
				}
            });
	}

	function showLoaderDomicilio(){
		$('.opciones-domicilio').empty();
		$('.opciones-domicilio').append("<div>Buscando servicios de correo...</div>");
	}

	function hideLoaderDomicilio(){
		$('.opciones-domicilio').empty();
	}

	function addCotizacionDomicilioOptions(options){
		var content = '';
		if(options.length > 0){
			var table_start = '<table> \
				<thead> \
					<th>Servicio</th> \
					<th>Costo</th> \
					<th>Tiempo Estimado</th> \
				</thead> \
				<tbody>';

			for (var i = 0; i < options.length; i++) {

				var template =
				'<tr>\
					<td><input type="radio" name="select-envio-domicilio" value="__value__" id="domicilio-__index__" class="radio-blue radioDomicilioOption">\
					<label for="domicilio-__index__" class="radio-blue-label domicilio-radio-label">__servicio_nombre__</label></td> \
					<td class="text-center">__valor__</td> \
					<td class="text-center promesaEntrega">__estimacion_entrega__</td> \
				</tr>';
				var newOption = template;
				newOption = newOption.replace(/__index__/g, options[i].servicio);
				newOption = newOption.replace(/__value__/g, options[i].modalidad +';'+options[i].servicio);
				newOption = newOption.replace(/__servicio_nombre__/g, options[i].servicio_nombre);
				newOption = newOption.replace(/__valor__/g, options[i].valor == 0 ? 'Gratis' : '$'+options[i].valor);
				newOption = newOption.replace(/__estimacion_entrega__/g, options[i].estimacion_entrega);
				
				table_start += newOption;
			}

			table_start += '</tbody></table>';
			content = table_start;
		}else{
			content = '<p>No se encontraron servicios de correo.</p>'
		}

		$('.opciones-domicilio').empty();
		$('.opciones-domicilio').append(content);
        $('.radioDomicilioOption').first().click();
	}

	function emptyDomicilioOptions() {	
		$('.opciones-domicilio').empty();
		$('#envio_domicilio_form_servicio').val('');
	}

	$('.opciones-domicilio').on('change', 'input[name=select-envio-domicilio]', function(){
		var values = $('.opciones-domicilio input[name=select-envio-domicilio]:checked').val().split(";");
		var modalidad = values[0];
		var servicio = values[1];
		$('#envio_domicilio_form_servicio').val(servicio)
	});

	

	// Cotizacion de envios : A sucursal
    
	var sucursales_info = [];	

	$('#envio_sucursal_correo_form_provincia').change(function(){
		emptySucursalOptions();
		showLoaderSucursal('localidades');
		loadLocalidadesDeSucursales();
	});

	$('#envio_sucursal_correo_form_localidadId').change(function(){
		emptySucursalOptions();
		var provincia_id =	$('#envio_sucursal_correo_form_provincia').val()
		var localidad_id = $('#envio_sucursal_correo_form_localidadId').val()
		if( provincia_id && localidad_id ){
			cotizarSucursal(provincia_id, localidad_id)
		}
	});

	$('.opciones-sucursal').on('change', 'input[name=select-envio-sucursal]', function(){
		var values = $('.opciones-sucursal input[name=select-envio-sucursal]:checked').val().split(";");
		var modalidad = values[0];
		var servicio = values[1];
		var sucursal_id = values[2];
		var correo_id = values[3];
		$('#envio_sucursal_correo_form_servicio').val(servicio)
		$('#envio_sucursal_correo_form_sucursal_id').val(sucursal_id)
		updateSucursalInfo(sucursal_id);
	});

	function emptySucursalOptions(){	
		$('.opciones-sucursal').empty();
		$('#envio_sucursal_correo_form_servicio_correo_id').val('');
		$('#envio_sucursal_correo_form_servicio_modalidad').val('');
		$('#envio_sucursal_correo_form_servicio').val('');
		$('#envio_sucursal_correo_form_sucursal_id').val('');
	}

	function updateSucursalInfo(sucursal_id){
		if (!sucursales_info[sucursal_id]) { return }
		var container = $('.informacion-sucursal');
		container.empty();
		var sucursal = sucursales_info[sucursal_id];
		var info = '\
		<div>\
			<h3 class="suc-info-title">Información de la sucursal:</h3>';
		if(sucursal.calle && sucursal.numero){
			info += '<p class="suc-info-text">Dirección:<br>'+sucursal.calle +' '+ sucursal.numero+'</p>'
		}
		if(sucursal.horario){
			info += '<p class="suc-info-text">Horario de atencion:<br>'+sucursal.horario+'</p>\
			<br>'
		}
		if(sucursal.telefono){
			info += '<p class="suc-info-text">Telefono:<br>'+sucursal.telefono+'</p>'
		}
		info += '</div>';

		container.append(info);
	}


	function loadLocalidadesDeSucursales(){
		var provincia_id = $('#envio_sucursal_correo_form_provincia').find(':selected').val();
		var jqxhr = $.getJSON( url_localidades_sucursal, {provincia_id: provincia_id} )
			.done(function(data) {
				changeLocalidadesSucursal(data)
			})
			.fail(function(data) {
				//console.log( "error" );
			})
	}

	function cotizarSucursal(provincia, localidad){
		showLoaderSucursal('servicios de correo');
		var selected_suc = $('#envio_sucursal_correo_form_sucursal_id').val();
		var jqxhr = $.getJSON( url_cotizar_sucursal, {provincia_id : provincia, localidad_id : localidad} )
			.done(function(data) {
				addCotizacionSucursalOptions(data)
				//console.log( "success" );
			})
			.fail(function(data) {
				//console.log( "error" );
			})
			.always(function(data) {
				if (selected_suc) {
					$('.opciones-sucursal input#'+selected_suc).attr('checked', true);
					updateSucursalInfo(selected_suc);
				}
				//console.log( "complete" );
			});
	}

	function changeLocalidadesSucursal(options){
		var select = $('#envio_sucursal_correo_form_localidadId');
		select.empty();
		var newOptions = '<option value="" selected="selected">Localidad</option>';
		for (var i = 0; i < options.length; i++) {
			newOptions += '<option value="'+ options[i].id +'">'+ options[i].nombre +'</option>';
		}
		select.append(newOptions);
		emptySucursalOptions()
	}

	function emptySucursalInfo(){
		$('.informacion-sucursal').empty();
	}

	function addCotizacionSucursalOptions(options) {
		var content = '';
		if(options.length > 0){
			var table_options = '<table> \
				<thead> \
					<th>Sucursal</th> \
					<th>Costo</th> \
					<th>Tiempo Estimado</th> \
				</thead> \
				<tbody>';
			var template = '';
			var servicio_nombre = '';
			var newOption = '';
			template =
			'<tr>\
				<td><input type="radio" name="select-envio-sucursal" value="__value__" id="__sucursal_id__" class="radio-blue radioSucursalOption">\
					<label for="__sucursal_id__" class="radio-blue-label">__servicio_nombre__</label></td> \
				<td class="text-center">__valor__</td> \
				<td class="text-center promesaEnvio">__estimacion_entrega__</td> \
			</tr>';
			for (var i = 0; i < options.length; i++) {
				newOption = template;
				servicio_nombre = '<div class="sucursal-option-direccion">'+options[i].sucursal.calle+' '+options[i].sucursal.numero+'</div><div class="sucursal-option-nombre">'+options[i].sucursal.correo.nombre+'</div>';
				newOption = newOption.replace(/__sucursal_id__/g, options[i].sucursal.id);
				newOption = newOption.replace(/__value__/g, options[i].modalidad +';'+options[i].servicio+';'+options[i].sucursal.id+';'+options[i].sucursal.correo.id);
				newOption = newOption.replace(/__valor__/g, options[i].valor == 0 ? 'Gratis' : '$'+options[i].valor);
				newOption = newOption.replace(/__estimacion_entrega__/g, options[i].estimacion_entrega);
				newOption = newOption.replace(/__servicio_nombre__/g, servicio_nombre);
				
				sucursales_info[options[i].sucursal.id] = options[i].sucursal;
				table_options += newOption;
			}

			table_options += '</tbody></table>';
			content = table_options;

		}else{
			content = '<p>No se encontraron sucursales en la zona.</p>'
		}

		emptySucursalInfo();
		$('.opciones-sucursal').empty();
		$('.opciones-sucursal').append(content);
        $('.radioSucursalOption').first().click();
	}

    $('.reformularEnvio').click(function(event) {
        event.preventDefault();
        $('.envioConfirmedContainer').hide();
        $('.envioOptionsContainer').show();
        $('.envioFormsContainer').slideDown();
        $('.confirmEnvio').show();
    });

	var provincia_id =	$('#envio_sucursal_correo_form_provincia').val()
	var localidad_id = $('#envio_sucursal_correo_form_localidadId').val()
	if( provincia_id && localidad_id ){
			cotizarSucursal(provincia_id, localidad_id)
	}

	function showLoaderSucursal(search){
		$('.opciones-sucursal').empty();
		$('.opciones-sucursal').append('\
			<div>Buscando '+search+'...</div>\
			')
	}

	function hideLoaderSucursal(){
		$('.opciones-sucursal').empty();
	}

	$('input[name=select-envio-type]').trigger('change');


    // ======================== PASO 3 ==========================

    $('.descuentoLinkContainer').click(function(event) {
        event.preventDefault();
        var containerVisible = $('.descuentoContent').is(":visible"); 
        
        if(containerVisible) {
            $('.descuentoContent').slideUp();
            $('.descuentoLink').text('Ver opciones de descuento');
            $('.descuentoPlus').text('+');
        } else {
            $('.descuentoContent').slideDown();
            $('.descuentoLink').text('Ocultar opciones de descuento');
            $('.descuentoPlus').text('-');
        }
    });

    $('#descuentoTipo365').change(function() {
        if($(this).is(':checked')) { 
            $('#descuento365FormContainer').show();
            $('#descuentoPinataFormContainer').hide();
            $('#descuentoAndesFormContainer').hide();
            $('#descuentoLaVozFormContainer').hide();
        } 
    });

    $('#descuentoTipoAndes').change(function() {
        if($(this).is(':checked')) { 
            $('#descuentoAndesFormContainer').show();
            $('#descuentoPinataFormContainer').hide();
            $('#descuento365FormContainer').hide();
            $('#descuentoLaVozFormContainer').hide();
        } 
    });

    $('#descuentoTipoPinata').change(function() {
        if($(this).is(':checked')) { 
            $('#descuentoPinataFormContainer').show();
            $('#descuento365FormContainer').hide();
            $('#descuentoAndesFormContainer').hide();
            $('#descuentoLaVozFormContainer').hide();
        } 
    });

    $('#descuentoTipoLaVoz').change(function() {
        if($(this).is(':checked')) { 
            $('#descuentoLaVozFormContainer').show();
            $('#descuento365FormContainer').hide();
            $('#descuentoAndesFormContainer').hide();
            $('#descuentoPinataFormContainer').hide();
        } 
    });

	$(document).on('submit', '.descuento-form', function(e) {
		e.preventDefault();
		
		disableDescuentoButton()
		
		content = $( this ).serialize(),
		url = $( this ).attr( "action" );
		
        var posting = $.post( url, content )
            .done(function(data) {
                if (data.applied) {
                    updatePrecioTotal(data.total_con_descuento);
                    updateDescuento(data.monto_descontado);
                }
                $('#descuento_form_codigo').val('');
                showCodigoMessage(data.applied, data.msg)
            })
        .fail(function(data) {

        })
        .always(function() {
            enableDescuentoButton()
        });

	});

    $('#verificar365').click(function() {
        var numeroCredencial = $('#tarjeta365').val();
        var url = "/ajax/check-365/" + numeroCredencial;
        $.get( url, function( data ) {
            var msg;
            var ok = false;
            json = JSON.parse(data);
            if (json.codigoResultado < 0 && json.codigoResultado >= -4) {
                msg = 'Credencial inválida';
            } else {
                ok = true;
            }
            showCodigoMessage(ok, msg);
            if(ok) {
                url = '/ajax/get-365-code';
                $.get( url, function( data ) {
                    $('#payment_clarin365').val(numeroCredencial);
                    $('#descuento_form_codigo').val(data);
                    $('.descuento-form').submit();
                });
            }
        });
    });

    $('#verificarAndes').click(function() {
        var numeroCredencial = $('#tarjetaAndes').val();
        var url = "/ajax/check-andes/" + numeroCredencial;
        $.get( url, function( data ) {
            var msg; 
            var esValida;
            json = data;
            esValida = json.valida;
            msg = json.msg;
            showCodigoMessage(esValida, msg);
            if(esValida) {
                url = '/ajax/get-andes-code';
                $.get( url, function( data ) {
                    $('#descuento_form_codigo').val(data);
                    $('.descuento-form').submit();
                });
            }
        });
    });

    $('#verificarLaVoz').click(function() {
        var numeroCredencial = $('#tarjetaLaVoz').val();
        var url = "/ajax/check-la-voz/" + numeroCredencial;
        $.get( url, function( data ) {
            var msg; 
            var esValida;
            json = data;
            esValida = json.valida;
            msg = json.msg;
            showCodigoMessage(esValida, msg);
            if(esValida) {
                url = '/ajax/get-la-voz-code';
                $.get( url, function( data ) {
                    $('#descuento_form_codigo').val(data);
                    $('.descuento-form').submit();
                });
            }
        });
    });

	function disableDescuentoButton(){
		var button = $('.descuento-button')
		button.attr('disabled', true);
		button.html('Verificando');
		button.animate({opacity: .5})
	}

	function enableDescuentoButton(){
		var button = $('.descuento-button')
		button.attr('disabled', false);
		button.html('Verificar');
		button.animate({opacity: 1});
	}

	function updatePrecioTotal(nuevoPrecio){
        var dniVisible = $('.dniRequireContainer').is(":visible"); 

        if (nuevoPrecio > 1000 && !dniVisible) {
            $('.dniRequireContainer').slideUp();
        }

		$('.precio-total').animate({'opacity': 0}, 1000, function(){
	        $(this).html('$'+nuevoPrecio.toFixed(2)).animate({'opacity': 1}, 1000)
		});
	}

	function updateDescuento(descuento){
		$('.descuento-monto').animate({'opacity': 0}, 1000, function(){
			$(this).html('- $'+descuento.toFixed(2)).animate({'opacity': 1}, 1000)
		});
        $('.descuentoTotalContainer').show();
	}

	function showCodigoMessage(success, msg){
		var msg_type = success ? 'success' : 'error';
		$('.descuento-message').removeClass('success');
		$('.descuento-message').removeClass('error');
		$('.descuento-message').addClass(msg_type);

		$('.descuento-message').animate({'opacity': 0}, 1000, function(){
	        $(this).html(msg).animate({'opacity': 1}, 1000)
		});

		setTimeout(function() {
			$('.descuento-message').animate({'opacity': 0}, 5000);
			$('.descuento-message').removeClass('msg_type');
		}, 7000);
		
	}


    //TRACKEO DE EVENTOS
    
    // . Login y registración

   $('.loginSubmit').click(function(){
        ga('send', 'event', 'Paso 2', 'Ingresar datos', '');
   });

   $('.registerSubmit').click(function(){
        ga('send', 'event', 'Paso 2', 'Ingresar datos', '');
   });

   $('.buy-button').click(function(event) {
        event.preventDefault();
        var dniVisible = $('#payment_DNI').is(":visible"); 
        var dniCompleted = $('#payment_DNI').val();
        if (dniVisible) {
            if (dniCompleted) {
                ga('send', 'event', 'Paso 4', 'elegirpago', '');
                $('form[name="payment"]').submit();
            } else {
                $('#payment_DNI').addClass('redBorder');
            }
        } else {
            ga('send', 'event', 'Paso 4', 'elegirpago', '');
            $('form[name="payment"]').submit();
        }
   });

});
