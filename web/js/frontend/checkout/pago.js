$(document).ready(function(){
    $('#descuentoTipoPinata').click();
    $('#descuentoTipo365').change(function() {
        if($(this).is(':checked')) { 
            $('#descuento365FormContainer').show();
            $('#descuentoPinataFormContainer').hide();
            $('#descuentoAndesFormContainer').hide();
            $('#descuentoLaVozFormContainer').hide();
        } 
    });

    $('#descuentoTipoAndes').change(function() {
        if($(this).is(':checked')) { 
            $('#descuentoAndesFormContainer').show();
            $('#descuentoPinataFormContainer').hide();
            $('#descuento365FormContainer').hide();
            $('#descuentoLaVozFormContainer').hide();
        } 
    });

    $('#descuentoTipoPinata').change(function() {
        if($(this).is(':checked')) { 
            $('#descuentoPinataFormContainer').show();
            $('#descuento365FormContainer').hide();
            $('#descuentoAndesFormContainer').hide();
            $('#descuentoLaVozFormContainer').hide();
        } 
    });

    $('#descuentoTipoLaVoz').change(function() {
        if($(this).is(':checked')) { 
            $('#descuentoLaVozFormContainer').show();
            $('#descuento365FormContainer').hide();
            $('#descuentoAndesFormContainer').hide();
            $('#descuentoPinataFormContainer').hide();
        } 
    });

    //Submit de form descuento piñata
	$(document).on('submit', '.descuento-form', function(e) {
		e.preventDefault();
		
		disableDescuentoButton()
		
		content = $( this ).serialize(),
		url = $( this ).attr( "action" );
		
		// Envio el form por ajax
        var posting = $.post( url, content )
            .done(function(data) {
                if (data.applied) {
                    updatePrecioTotal(data.total_con_descuento);
                    updateDescuento(data.monto_descontado);
                }
                $('#descuento_form_codigo').val('');
                showCodigoMessage(data.applied, data.msg)
            })
        .fail(function(data) {

        })
        .always(function() {
            enableDescuentoButton()
        });

	});

    //Submit form descuento 365
    $('#verificar365').click(function() {
        var numeroCredencial = $('#tarjeta365').val();
        var url = "/ajax/check-365/" + numeroCredencial;
        $.get( url, function( data ) {
            var msg;
            var ok = false;
            json = JSON.parse(data);
            if (json.codigoResultado < 0 && json.codigoResultado >= -4) {
                msg = 'Credencial inválida';
            } else {
                ok = true;
            }
            showCodigoMessage(ok, msg);
            if(ok) {
                url = '/ajax/get-365-code';
                $.get( url, function( data ) {
                    $('#payment_clarin365').val(numeroCredencial);
                    $('#descuento_form_codigo').val(data);
                    $('.descuento-form').submit();
                });
            }
        });
    });

    //Submit form descuento Andes
    $('#verificarAndes').click(function() {
        var numeroCredencial = $('#tarjetaAndes').val();
        var url = "/ajax/check-andes/" + numeroCredencial;
        $.get( url, function( data ) {
            var msg; 
            var esValida;
            json = data;
            esValida = json.valida;
            msg = json.msg;
            showCodigoMessage(esValida, msg);
            if(esValida) {
                url = '/ajax/get-andes-code';
                $.get( url, function( data ) {
                    $('#descuento_form_codigo').val(data);
                    $('.descuento-form').submit();
                });
            }
        });
    });

    //Submit form descuento La Voz
    $('#verificarLaVoz').click(function() {
        var numeroCredencial = $('#tarjetaLaVoz').val();
        var url = "/ajax/check-la-voz/" + numeroCredencial;
        $.get( url, function( data ) {
            var msg; 
            var esValida;
            json = data;
            esValida = json.valida;
            msg = json.msg;
            showCodigoMessage(esValida, msg);
            if(esValida) {
                url = '/ajax/get-la-voz-code';
                $.get( url, function( data ) {
                    $('#descuento_form_codigo').val(data);
                    $('.descuento-form').submit();
                });
            }
        });
    });



	function disableDescuentoButton(){
		var button = $('.descuento-button')
		button.attr('disabled', true);
		button.html('Verificando');
		button.animate({opacity: .5})
	}

	function enableDescuentoButton(){
		var button = $('.descuento-button')
		button.attr('disabled', false);
		button.html('Verificar');
		button.animate({opacity: 1});
	}

	function updatePrecioTotal(nuevoPrecio){
		$('.precio-total').animate({'opacity': 0}, 1000, function(){
	        $(this).html('$'+nuevoPrecio.toFixed(2)).animate({'opacity': 1}, 1000)
		});
	}

	function updateDescuento(descuento){
		$('.descuento-monto').animate({'opacity': 0}, 1000, function(){
			$(this).html('- $'+descuento.toFixed(2)).animate({'opacity': 1}, 1000)
		});
	}

	function showCodigoMessage(success, msg){
		var msg_type = success ? 'success' : 'error';
		$('.descuento-message').addClass(msg_type);

		$('.descuento-message').animate({'opacity': 0}, 1000, function(){
	        $(this).html(msg).animate({'opacity': 1}, 1000)
		});

		setTimeout(function() {
			$('.descuento-message').animate({'opacity': 0}, 400);
			$('.descuento-message').removeClass('msg_type');
		}, 7000);
		
	}
});
