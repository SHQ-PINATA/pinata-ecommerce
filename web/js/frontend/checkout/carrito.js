$(document).ready(function(){
			var timerids = [];
			$('input.cantidad').change(function(){
				if ($( this ).val() != '') {
					submitWithDelay($( this ).closest('form'))
				}
			})

			$('.arrow-up').click(function(){
				var widget = $(this).closest('.widget').find('.cantidad');
				var original = parseInt(widget.val())
				widget.val(original + 1);

				submitWithDelay(widget.closest('form'))
			})

			$('.arrow-down').click(function(){
				var widget = $(this).closest('.widget').find('.cantidad');
				var original = parseInt(widget.val())
				if (original > 1 ) {
					widget.val(original + -1);
				}

  				submitWithDelay(widget.closest('form'))
			})

			$(".continuar").on("click", function(event){
			    if ($(this).is("[disabled]")) {
			        event.preventDefault();
			    }
			});

			$('.delete').on('click', function(e){
				e.preventDefault()
				var delete_button = $(this);
				
				if (delete_button.is("[disabled]")) {
			        return
			    }

				var url = $(this).attr('data')
				var row = $(this).closest('tr')

				disableDelete(delete_button)
				disableContinuar()
				hideRow(row)
				
				var posting = $.post( url )
					.done(function(data) {
						if (data.status == 'ok') {
							row.remove()
							addItemToCartAnimation();
                        }else{
							showErrorAddingToCart(data.error);
						}
					})
					.fail(function(data) {
						showErrorAddingToCart('No se pudo remover el producto solicitado');
					})
					.always(function() {
						if (delete_button.leght) {
							enableDelete(delete_button)
						}
						if (row.leght) {
							showRow(row)
						}
						enableContinuar()
						recalculatePreciosAndSubtotal()
					});
			})

			function submitWithDelay(form){
				var id = form.find('.producto_id').val()
  				clearTimeout(timerids[id]);
  				timerids[id] = setTimeout(function() { form.submit(); }, 1000);
			}

			$('form[name="agregar_producto_carrito_form"]').on('submit', function(e){
				e.preventDefault();

				var form = $( this );
				content = form.serialize(),
				url = form.attr( "action" );

				// Envio el form por ajax
				showLoading(form);
				var posting = $.post( url, content )
					.done(function(data) {
						if (data.status == 'ok') {
							addItemToCartAnimation();
						}else{
							showErrorAddingToCart(data.error);
							setOriginalQuantityValue(form, data.cantidad)
						}
					})					
					.fail(function(data) {
						if ('404' == data.status) {
							showErrorAddingToCart('No se encontró el producto solicitado');
						}else{
							showErrorAddingToCart('No se pudo agregar el producto. Inténtelo más tarde nuevamente.');
						}
					})
					.always(function() {
						hideLoading(form);
						recalculatePreciosAndSubtotal()
					});
			})

			function recalculatePreciosAndSubtotal(){
				var total = 0;
			    $( ".checkout-carrito tr.item" ).each(function() {
			        var c = parseInt($( this ).find('input.cantidad').val())
			        var precio_unitario = $( this ).find('.precio').attr('unitario')
			        var precio_total = $( this ).find('.precio').attr('total')
			        var nuevo_precio_total = c*precio_unitario

			        if ( nuevo_precio_total != precio_total) {
			        	$( this ).find('.precio').attr('total', nuevo_precio_total)
			        	$( this ).find('.precio p').animate({'opacity': 0}, 1000, function(){
			        		$( this ).html('$'+nuevo_precio_total.toFixed(2)).animate({'opacity': 1}, 1000)
						})
			        }
			        total += nuevo_precio_total;
			    });
			    var subtotal = $('.checkout-carrito .precio-subtotal p');

			    subtotal.animate({'opacity': 0}, 1000, function(){
			        $(this).html('$'+total.toFixed(2)).animate({'opacity': 1}, 1000)
				});
			}

			function setOriginalQuantityValue(form, cantidad){
				form.find('.cantidad').val(cantidad);
			}

			function showLoading(form){
				form.find('.cantidad-arrows').prepend('<div class="wall" style="width:50px; height:50px; position:absolute"></div>')
				form.css({'opacity':0.4, 'transition': 'all 0.5s ease'})
				disableContinuar()
			}
			
			function hideLoading(form){
				form.find('.cantidad-arrows .wall').remove()
				form.css({'opacity':1})
				enableContinuar()
			}

			function disableContinuar(){
				$('.continuar').attr('disabled', 'disabled')
				$('.continuar').css({'opacity':0.3, 'cursor':'default'})
			}

			function enableContinuar(){
				$('.continuar').removeAttr('disabled')
				$('.continuar').css({'opacity':1, 'cursor': 'pointer'})
			}

			function disableDelete(delete_button){
				delete_button.attr('disabled', 'disabled')
				delete_button.css({'cursor':'default'})
			}

			function enableDelete(delete_button){
				delete_button.attr('disabled', '')
				delete_button.css({'cursor':'cursor'})
			}

			function hideRow(row){
				row.css({'opacity': 0.3, 'transition': 'all 0.5s ease'})
			}

			function showRow(row){
				row.css({'opacity': 1})
			}

			// Hago la animacion del carrito al agregar un producto actualizando items
			function addItemToCartAnimation(){
				var prev_items = parseInt( $('.carrito-item-count').attr('data') );
				var total = 0;
				$( ".checkout-carrito tr.item" ).each(function() {
			            total += parseInt($( this ).find('input.cantidad').val())
			        });

				var newtext = (1 == total) ? '1 ítem' : total + ' ítems';

				$('.carrito-item-count').animate({'opacity': 0}, 400, function(){
			        $(this).html(newtext).animate({'opacity': 1}, 400);
			        $(this).attr('data', newtext); 
			    });

				$('.carrito-container .carrito').addClass('carrito-added');

				if (prev_items < total) {
			    	$('.carrito-container .add-success').animate({'opacity': 1}, 400);
				}

				setTimeout(function() {
					$('.carrito-container .add-success').animate({'opacity': 0}, 400);
					$('.carrito-added').removeClass('carrito-added');
				}, 7000);
                $.get( "/ajax/get_cart_count", function( data ) {
                    if (parseInt(data) > 0) {
                        $("#carritoNumber").html( data );
                        $("#carritoNumberContainer").fadeIn();
                    } else {
                        $("#carritoNumberContainer").fadeOut();
                    }
                });
			}

			function showErrorAddingToCart(error){
				// TO DO
				/*$('.agregar-carrito-container').append('<div class="error" style="opacity:0">'+error+'</div>');
				$('.agregar-carrito-container .error').animate({'opacity': 1}, 400);

				setTimeout(function() {
				  $(".agregar-carrito-container .error").animate({'opacity': 0}, 400, function(){
					  $(".agregar-carrito-container .error").remove();
					  
				})
				}, 7000);*/
			}
		});
