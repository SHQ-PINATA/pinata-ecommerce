<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Provincia;

class LoadProvinciasData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $provincias = [
            array('codigo' => 'A', 'nombre' => 'Salta'),
            array('codigo' => 'B', 'nombre' => 'Buenos Aires'),
            array('codigo' => 'C', 'nombre' => 'Ciudad Autónoma de Buenos Aires'),
            array('codigo' => 'D', 'nombre' => 'San Luis'),
            array('codigo' => 'E', 'nombre' => 'Entre Ríos'),
            array('codigo' => 'F', 'nombre' => 'La Rioja'),
            array('codigo' => 'G', 'nombre' => 'Santiago del Estero'),
            array('codigo' => 'H', 'nombre' => 'Chaco'),
            array('codigo' => 'J', 'nombre' => 'San Juan'),
            array('codigo' => 'K', 'nombre' => 'Catamarca'),
            array('codigo' => 'L', 'nombre' => 'La Pampa'),
            array('codigo' => 'M', 'nombre' => 'Mendoza'),
            array('codigo' => 'N', 'nombre' => 'Misiones'),
            array('codigo' => 'P', 'nombre' => 'Formosa'),
            array('codigo' => 'Q', 'nombre' => 'Neuquén'),
            array('codigo' => 'R', 'nombre' => 'Río Negro'),
            array('codigo' => 'S', 'nombre' => 'Santa Fe'),
            array('codigo' => 'T', 'nombre' => 'Tucumán'),
            array('codigo' => 'U', 'nombre' => 'Chubut'),
            array('codigo' => 'V', 'nombre' => 'Tierra del Fuego'),
            array('codigo' => 'W', 'nombre' => 'Corrientes'),
            array('codigo' => 'X', 'nombre' => 'Córdoba'),
            array('codigo' => 'Y', 'nombre' => 'Jujuy'),
            array('codigo' => 'Z', 'nombre' => 'Santa Cruz'),
        ];
    	
        foreach ($provincias as $provincia) {
            $p = new Provincia();
            $p->setNombre($provincia['nombre']);
            $p->setCodigo($provincia['codigo']);
            $manager->persist($p);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 7;
    }
}