<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Genero;

class LoadGenerosData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
    	$genero_nene = new Genero();
        $genero_nene->setGenero('Nene');
        $genero_nene->setSlug('nene');
        $manager->persist($genero_nene);
        
        $genero_nena = new Genero();
        $genero_nena->setGenero('Nena');
        $genero_nena->setSlug('nena');
        $manager->persist($genero_nena);

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}