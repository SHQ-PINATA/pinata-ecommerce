<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\StockTipo;

class LoadStockTipoData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
    	$stockTipos = [
        array('nombre' => 'Carga inicial', 'codigo' => 'CARGI', 'es_de_sistema' => false),
        array('nombre' => 'Reajuste de stock', 'codigo' => 'REAJU', 'es_de_sistema' => false),
        array('nombre' => 'Otro', 'codigo' => 'OTROS', 'es_de_sistema' => false),
        array('nombre' => 'Baja de Pedido','codigo' => 'BAJAP', 'es_de_sistema' =>  true),
        array('nombre' => 'Venta','codigo' => 'VENTA', 'es_de_sistema' =>  true),
        array('nombre' => 'Reactivacion de Pedido','codigo' => 'REACP', 'es_de_sistema' =>  true)];

    	foreach ($stockTipos as $stockTipo) {
    		$tipo = new StockTipo();
    		$tipo->setNombre($stockTipo['nombre']);
            $tipo->setCodigo($stockTipo['codigo']);
            $tipo->setEsDeSistema($stockTipo['es_de_sistema']);
	        $manager->persist($tipo);
    	}

        $manager->flush();
    }

    public function getOrder()
    {
        return 6;
    }
}

