<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Modelo;

class LoadModelosData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $modelos = ['Unicorn', 'Pegasus', 'Lemur', 'Leopardo', 'Jirafa', 'Little Pig', 'Zebra'];
    	
        foreach ($modelos as $nombre) {
            $modelo = new Modelo();
            $modelo->setNombre($nombre);
            $manager->persist($modelo);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 3;
    }
}
