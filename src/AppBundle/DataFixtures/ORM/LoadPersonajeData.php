<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Personaje;

class LoadPersonajesData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
    	$personajes = ['Mickey','Minnie','Cars','Dra. Juguetes','Frozen','La Guardia del León','Junior Express','Princesas','Princesa Sofía','Buscando a Dory','Soy Luna','Spiderman','Star Wars','Avengers','Rápido y Furioso','ONCE','Emoji','TKM','Chicas Superpoderosas','Hora de Aventura','Paw Patrol','Simones','Minions','My Little Pony','Peppa Pig','Peter Rabbit','Shimmer & Shine','Shopkins','Hotwheels','Barbie','Fisher Price','La Granja de Zenón','Otros'];

    	foreach ($personajes as $nombre) {
    		$personaje = new Personaje();
    		$personaje->setNombre($nombre);
	        $manager->persist($personaje);

    	}

        $manager->flush();
    }

    public function getOrder()
    {
        return 4;
    }
}