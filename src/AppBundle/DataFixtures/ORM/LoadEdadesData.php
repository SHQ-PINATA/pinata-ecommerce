<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Edades;

class LoadEdadesData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $edades = [('0 a 2 años'), ('2 a 5 años'), ('5 a 8 años'), ('8 a 14 años'), ('Niños'), ('No tan niños')];
    	
        foreach ($edades as $rango) {
            $edad = new Edades();
            $edad->setEdades($rango);
            $manager->persist($edad);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}
