<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Configuracion;

class LoadConfiguracionData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
    	$configs = [
            array('nombre' => '[ Free Shipping ]', 'descripcion' => 'Valor a partir del cual se usa free shipping. El valor debe de ser un numero sin el signo $ | Ejemplo: 1399.99', 'codigo' => 'envio_gratis_precio_minimo', 'valor' =>  '2499.00'),
            array('nombre' => '[ Free Shipping Max]', 'descripcion' => 'Valor a partir del cual no se usa free shipping. El valor debe de ser un numero sin el signo $ | Ejemplo: 1399.99', 'codigo' => 'envio_gratis_precio_maximo', 'valor' =>  '7000.00')
        ];

    	foreach ($configs as $config) {
    		$c = new Configuracion();
    		$c->setNombre($config['nombre']);
            $c->setDescripcion($config['descripcion']);
            $c->setCodigo($config['codigo']);
            $c->setValor($config['valor']);
	        $manager->persist($c);
    	}

        $manager->flush();
    }

    public function getOrder()
    {
        return 8;
    }
}

