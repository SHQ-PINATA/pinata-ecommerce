<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Categoria;
use AppBundle\Entity\SubCategoria;

class LoadCategoriasData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $categorias = ['Sabanas', 'Acolchados', 'Cover Quilts', 'Frazadas', 'Almohadones', 'Toallas y Ponchos', 'Cortinas', 'Alfombras', 'Baby', 'SALE', ];

        $baby_subcategorias = ['Ropa de Blanco', 'Mamaderas', 'Chupetes', 'Baberos', 'Toallas', 'Alimentación', 'Cuidado del Bebe'];

        foreach ($categorias as $nombre) {
            $categoria = new Categoria();
            $categoria->setNombre($nombre);
            if ('Baby' == $nombre) {
                foreach ($baby_subcategorias as $sub_categoria) {
                    $sub = new Categoria();
                    $sub->setNombre($sub_categoria);
                    $sub->setParent($categoria);
                    $categoria->addChild($sub);
                    $manager->persist($sub);
                }
            }
	        $manager->persist($categoria);
    	}

        $manager->flush();
    }

    public function getOrder()
    {
        return 5;
    }
}