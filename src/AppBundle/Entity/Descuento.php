<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Descuento
 *
 * @ORM\Table(name="descuento")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DescuentoRepository")
 * @UniqueEntity("codigo")
 * @ORM\HasLifecycleCallbacks()
 */
class Descuento
{
    const PORCENTAJE   = 'PORCE';
    const MONTO_FIJO   = 'MONTO';
    const ENVIO_GRATIS = 'ENVIO';
    const GIFTCARD     = 'GIFTC';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=50, unique=true)
     * @Assert\NotBlank(message = "Ingrese un código")
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=5)
     * @Assert\NotBlank(message = "Seleccione el tipo")
     */
    private $tipo;

    /**
     * @var bool
     *
     * @ORM\Column(name="usoUnico", type="boolean")
     */
    private $usoUnico = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaDesde", type="datetime")
     * @Assert\NotBlank(message = "Seleccione una fecha")
     */
    private $fechaDesde;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaHasta", type="datetime")
     * @Assert\NotBlank(message = "Seleccione una fecha")
     */
    private $fechaHasta;

    /**
     * @var float
     *
     * @ORM\Column(name="valor", type="float", nullable=true)
     */
    private $valor;

    /**
     * @var float
     *
     * @ORM\Column(name="montoMin", type="float", nullable=true)
     */
    private $montoMin;

    /**
     * @var float
     *
     * @ORM\Column(name="montoMax", type="float", nullable=true)
     */
    private $montoMax;

    /**
     * @var float
     *
     * @ORM\Column(name="montoDisponible", type="float", nullable=true)
     */
    private $montoDisponible;

     /**
     * @var boolean
     *
     * @ORM\Column(name="activo", type="boolean")
     */
    
    private $activo = true;

     /**
     * @var boolean
     *
     * @ORM\Column(name="global", type="boolean")
     */
    
    private $global = false;


    /**
     * @ORM\ManyToMany(targetEntity="Personaje")
     * @ORM\JoinTable(name="descuento_personaje",
     *      joinColumns={@ORM\JoinColumn(name="descuento_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="personaje_id", referencedColumnName="id")}
     *      )
     */
    protected $personajes;
    
    /**
     * @ORM\ManyToMany(targetEntity="Categoria")
     * @ORM\JoinTable(name="descuento_categoria",
     *      joinColumns={@ORM\JoinColumn(name="descuento_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="categoria_id", referencedColumnName="id")}
     *      )
     */
    protected $categorias;

    /**
     * @var bool
     *
     * @ORM\Column(name="excluir_sale", type="boolean")
     */
    private $excluirSale = false;

    
    public function __construct() {
        $this->personajes = new ArrayCollection();
        $this->categorias = new ArrayCollection();
    }
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return Descuento
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return Descuento
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set usoUnico
     *
     * @param boolean $usoUnico
     *
     * @return Descuento
     */
    public function setUsoUnico($usoUnico)
    {
        $this->usoUnico = $usoUnico;

        return $this;
    }

    /**
     * Get usoUnico
     *
     * @return bool
     */
    public function getUsoUnico()
    {
        return $this->usoUnico;
    }

    /**
     * Set fechaDesde
     *
     * @param \DateTime $fechaDesde
     *
     * @return Descuento
     */
    public function setFechaDesde($fechaDesde)
    {
        $this->fechaDesde = $fechaDesde;

        return $this;
    }

    /**
     * Get fechaDesde
     *
     * @return \DateTime
     */
    public function getFechaDesde()
    {
        return $this->fechaDesde;
    }

    /**
     * Set fechaHasta
     *
     * @param \DateTime $fechaHasta
     *
     * @return Descuento
     */
    public function setFechaHasta($fechaHasta)
    {
        $this->fechaHasta = $fechaHasta;

        return $this;
    }

    /**
     * Get fechaHasta
     *
     * @return \DateTime
     */
    public function getFechaHasta()
    {
        return $this->fechaHasta;
    }

    /**
     * Set montoMin
     *
     * @param float $montoMin
     *
     * @return Descuento
     */
    public function setMontoMin($montoMin)
    {
        $this->montoMin = $montoMin;

        return $this;
    }

    /**
     * Get montoMin
     *
     * @return float
     */
    public function getMontoMin()
    {
        return $this->montoMin;
    }

    /**
     * Set montoMax
     *
     * @param float $montoMax
     *
     * @return Descuento
     */
    public function setMontoMax($montoMax)
    {
        $this->montoMax = $montoMax;

        return $this;
    }

    /**
     * Get montoMax
     *
     * @return float
     */
    public function getMontoMax()
    {
        return $this->montoMax;
    }

    /**
     * Set valor
     *
     * @param float $valor
     *
     * @return Descuento
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return float
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     *
     * @return Descuento
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Add personaje
     *
     * @param \AppBundle\Entity\Personaje $personaje
     *
     * @return Descuento
     */
    public function addPersonaje(\AppBundle\Entity\Personaje $personaje)
    {
        $this->personajes[] = $personaje;

        return $this;
    }

    /**
     * Remove personaje
     *
     * @param \AppBundle\Entity\Personaje $personaje
     */
    public function removePersonaje(\AppBundle\Entity\Personaje $personaje)
    {
        $this->personajes->removeElement($personaje);
    }

    /**
     * Get personajes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPersonajes()
    {
        return $this->personajes;
    }

    /**
     * Add categoria
     *
     * @param \AppBundle\Entity\Categoria $categoria
     *
     * @return Descuento
     */
    public function addCategoria(\AppBundle\Entity\Categoria $categoria)
    {
        $this->categorias[] = $categoria;

        return $this;
    }

    /**
     * Remove categoria
     *
     * @param \AppBundle\Entity\Categoria $categoria
     */
    public function removeCategoria(\AppBundle\Entity\Categoria $categoria)
    {
        $this->categorias->removeElement($categoria);
    }

    /**
     * Get categorias
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategorias()
    {
        return $this->categorias;
    }

    /**
     * Set excluirSale
     *
     * @param boolean $excluirSale
     *
     * @return Descuento
     */
    public function setExcluirSale($excluirSale)
    {
        $this->excluirSale = $excluirSale;

        return $this;
    }

    /**
     * Get excluirSale
     *
     * @return bool
     */
    public function getExcluirSale()
    {
        return $this->excluirSale;
    }
    
    public static function getTipoOptions(){
        return [
            'Porcentaje'    => self::PORCENTAJE,
            'Monto Fijo'    => self::MONTO_FIJO,
            'Envio Gratis'  => self::ENVIO_GRATIS,
            'Giftcard'      => self::GIFTCARD,
        ];
    }

    public function getTipoNombre(){
        $tipos = array_flip($this->getTipoOptions());
        return $tipos[$this->getTipo()];
    }


    public function getValorPrefix(){
        if($this->getTipo() == self::MONTO_FIJO){
            return '$';
        }
        if ($this->getTipo() == self::PORCENTAJE) {
            return '%';
        }
        
        return '';
    }

    /**
     * @Assert\Callback
     */
    public function validarValor(ExecutionContextInterface $context, $payload)
    {   
        if ( $this->getTipo() == self::MONTO_FIJO && $this->getValor() < 0 ) {
            $context->buildViolation('El valor debe de ser un monto positivo')
                ->atPath('valor')
                ->addViolation();
        }

        if ( $this->getTipo() == self::PORCENTAJE && ( $this->getValor() < 0 || $this->getValor() > 100 ) ) {
            $context->buildViolation('El valor debe de ser un porcentaje entre 0 y 100')
                ->atPath('valor')
                ->addViolation();
        }
    }

    public function estaVigente(){
       $ahora = new \DateTime();
        return $this->getFechaDesde() < $ahora && $this->getFechaHasta() > $ahora;
    }

    public function generarCodigo( $prefijo ){
        $codigo = $prefijo;

        $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        $max = strlen($str) - 1;
        for( $j = 0 ; $j < 10 ; $j++ ) {
            $codigo .= substr($str,rand(0,$max),1);
        }

        $this->setCodigo($codigo);

        return $codigo;
    }

    /**
     * Set montoDisponible
     *
     * @param float $montoDisponible
     *
     * @return Descuento
     */
    public function setMontoDisponible($montoDisponible)
    {
        $this->montoDisponible = $montoDisponible;

        return $this;
    }

    /**
     * Get montoDisponible
     *
     * @return float
     */
    public function getMontoDisponible()
    {
        return $this->montoDisponible;
    }

    /**
     * Set global
     *
     * @param boolean $global
     *
     * @return Descuento
     */
    public function setGlobal($global)
    {
        $this->global = $global;

        return $this;
    }

    /**
     * Get global
     *
     * @return boolean
     */
    public function getGlobal()
    {
        return $this->global;
    }
}
