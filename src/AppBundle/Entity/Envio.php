<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Esta clase representa un envio que puede ser un envio a domicilio o a una sucursal de correo.
 * Es abstracta para que el pedido guarde un id de envio generico, independiente del tipo de evio.
 * @ORM\Entity
 * @ORM\Table(name="envio")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EnvioRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="tipo", type="string", length=3)
 * @ORM\DiscriminatorMap( {"DOM" = "EnvioDomicilio", "SUC" = "EnvioSucursalCorreo", "PIC" = "EnvioPickup"} )
 */

abstract class Envio
{
    const DOMICILIO = 'DOM';
    const SUCURSARL_CORREO = 'SUC';
    const PICKUP = 'PIC';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Muchos Envios tienen un Usuario
     * @ORM\ManyToOne(targetEntity="Usuario")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    protected $usuario;

    /**
     * @var string
     *
     * @ORM\Column(name="carrito_id", type="string", length=100, nullable=true)
     */
    private $carritoId;

    /**
     * @var Pedido
     *
     * @ORM\OneToOne(targetEntity="Pedido", inversedBy="envio")
     * @ORM\JoinColumn(name="pedido_id", referencedColumnName="id")
     */
    private $pedido;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    public function __construct() {
        $this->createdAt = new \DateTimeImmutable();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set usuario
     *
     * @param string $usuario
     *
     * @return Envio
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return string
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Stock
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set carritoId
     *
     * @param string $carritoId
     *
     * @return Envio
     */
    public function setCarritoId($carritoId)
    {
        $this->carritoId = $carritoId;

        return $this;
    }

    /**
     * Get carritoId
     *
     * @return string
     */
    public function getCarritoId()
    {
        return $this->carritoId;
    }

    /**
     * Set pedido
     *
     * @param string $pedido
     *
     * @return Envio
     */
    public function setPedido($pedido)
    {
        $this->pedido = $pedido;

        return $this;
    }

    /**
     * Get pedido
     *
     * @return string
     */
    public function getPedido()
    {
        return $this->pedido;
    }

    public function getTypeName(){
        if ($this instanceof EnvioDomicilio) {
            return 'Envio a Domicilio';
        }elseif($this instanceof EnvioSucursalCorreo){
            return 'Envio a Sucursal';
        }elseif($this instanceof EnvioPickup){
            return 'Pickup';
        }
    }

    public function getType(){
        if ($this instanceof EnvioDomicilio) {
            return Envio::DOMICILIO;
        }elseif($this instanceof EnvioSucursalCorreo){
            return Envio::SUCURSARL_CORREO;
        }elseif($this instanceof EnvioPickup){
            return Envio::PICKUP;
        }
    }

    public static function getInstanceByType($type){
        if (Envio::DOMICILIO == $type ) {
            return 'AppBundle\Entity\EnvioDomicilio';
        }elseif(Envio::SUCURSARL_CORREO == $type){
            return 'AppBundle\Entity\EnvioSucursalCorreo';
        }elseif(Envio::PICKUP == $type ){
            return 'AppBundle\Entity\EnvioPickup';
        }
    }

    public function getNumSeguimiento(){
        return null;
    }

    public function isPickup(){
        return $this instanceof EnvioPickup;
    }

    abstract function getNombre();
    abstract function getApellido();

}
