<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MercadoPagoOrders
 *
 * @ORM\Table(name="mercado_pago_orders")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MercadoPagoOrderRepository")
 */
class MercadoPagoOrder
{
    //const NEW_ORDER = 'NEWOR';

    const STATUS_OPEN    = 'open';    // Order without payments
    const STATUS_CLOSED  = 'closed';  // Order with payments covering total amount
    const STATUS_EXPIRED = 'expired'; // Order expired

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Pedido
     *
     * @ORM\OneToOne(targetEntity="Pedido", inversedBy="mercadoPagoOrder")
     * @ORM\JoinColumn(name="pedido_id", referencedColumnName="id")
     */
    private $pedido;

    /**
     * @var string
     *
     * @ORM\Column(name="paymentUrl", type="string", length=512)
     */
    private $paymentUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=20)
     */
    private $status;
    
    /**
     * @var string
     *
     * @ORM\Column(name="preference_id", type="string", length=50, nullable=true)
     */
    private $preferenceId;

    /**
     * @var string
     *
     * @ORM\Column(name="merchant_order_id", type="string", length=50, nullable=true)
     */
    private $merchantOrderId;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updatedAt;

    public function __construct() {
        $this->status = self::STATUS_OPEN;
        $this->updatedAt = new \DateTime();
    }
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set paymentUrl
     *
     * @param string $paymentUrl
     *
     * @return MercadoPagoOrders
     */
    public function setPaymentUrl($paymentUrl)
    {
        $this->paymentUrl = $paymentUrl;

        return $this;
    }

    /**
     * Get paymentUrl
     *
     * @return string
     */
    public function getPaymentUrl()
    {
        return $this->paymentUrl;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return MercadoPagoOrders
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set pedido
     *
     * @param \AppBundle\Entity\Pedido $pedido
     *
     * @return MercadoPagoOrders
     */
    public function setPedido(\AppBundle\Entity\Pedido $pedido = null)
    {
        $this->pedido = $pedido;

        return $this;
    }

    /**
     * Get pedido
     *
     * @return \AppBundle\Entity\Pedido
     */
    public function getPedido()
    {
        return $this->pedido;
    }

    /**
     * Set preferenceId
     *
     * @param string $preferenceId
     *
     * @return MercadoPagoOrder
     */
    public function setPreferenceId($preferenceId)
    {
        $this->preferenceId = $preferenceId;

        return $this;
    }

    /**
     * Get preferenceId
     *
     * @return string
     */
    public function getPreferenceId()
    {
        return $this->preferenceId;
    }

    /**
     * Set merchantOrderId
     *
     * @param string $merchantOrderId
     *
     * @return MercadoPagoOrder
     */
    public function setMerchantOrderId($merchantOrderId)
    {
        $this->merchantOrderId = $merchantOrderId;

        return $this;
    }

    /**
     * Get merchantOrderId
     *
     * @return string
     */
    public function getMerchantOrderId()
    {
        return $this->merchantOrderId;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return MercadoPagoOrder
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public static function getStatusOptions(){
        return ['Abierto'  => MercadoPagoOrder::STATUS_OPEN, 
                'Cerrado'  => MercadoPagoOrder::STATUS_CLOSED, 
                'Expirado' => MercadoPagoOrder::STATUS_EXPIRED];
    }
}
