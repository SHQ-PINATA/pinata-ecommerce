<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * ProductoGenero
 * @ORM\Table(name="genero")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GeneroRepository")
 * @Vich\Uploadable
 */
class Genero
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="genero", type="string", length=50)
     */
    private $genero;

    /**
     * @var string
     *
     * @ORM\Column(name="imagen_banner", type="string", length=255, nullable=true)
     */
    private $imagen_banner;

    /**
     * @Vich\UploadableField(mapping="genero", fileNameProperty="imagen_banner")
     * @var File
     */
    private $imagen_banner_archivo;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updatedAt = null;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=20, unique=true, nullable=true)
     */
    private $slug;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set genero
     *
     * @param string $genero
     *
     * @return ProductoGenero
     */
    public function setGenero($genero)
    {
        $this->genero = $genero;

        return $this;
    }

    /**
     * Get genero
     *
     * @return string
     */
    public function getGenero()
    {
        return $this->genero;
    }

    public function __toString(){
        return $this->getGenero();
    }

    /**
     * Set imagenBanner
     *
     * @param string $imagenBanner
     *
     * @return Genero
     */
    public function setImagenBanner($imagenBanner)
    {
        $this->imagen_banner = $imagenBanner;

        return $this;
    }

    /**
     * Get imagenBanner
     *
     * @return string
     */
    public function getImagenBanner()
    {
        return $this->imagen_banner;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Genero
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Genero
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $imagen
     *
     * @return Genero
     */
    public function setImagenBannerArchivo(File $imagen = null)
    {
        $this->imagen_banner_archivo = $imagen;

        if ($imagen) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
        
        return $this;
    }

    /**
     * Get imagenBanner
     *
     * @return string
     */
    public function getImagenBannerArchivo()
    {
        return $this->imagen_banner_archivo;
    }
}
