<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pedido
 *
 * @ORM\Table(name="pedido")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PedidoRepository")
 */
class Pedido
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Muchos Envios tienen un Usuario
     * @ORM\ManyToOne(targetEntity="Usuario")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id", nullable=false)
     */
    protected $usuario;

    /**
     * @var string
     *
     * @ORM\Column(name="dni", type="string", length=255, nullable=true)
     */
    private $dni;


    /**
     * @ORM\OneToOne(targetEntity="Envio", mappedBy="pedido")
     * @ORM\JoinColumn(name="envio_id", referencedColumnName="id")
     */
    private $envio;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="PedidoItem", mappedBy="pedido", cascade={"persist"}, orphanRemoval=true)
     */
    private $items;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_pago", type="datetime", nullable=true)
     */
    private $fechaPago = null;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_envio", type="datetime", nullable=true)
     */
    private $fechaEnvio = null;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_facturacion", type="datetime", nullable=true)
     */
    private $fechaFacturacion = null;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_baja", type="datetime", nullable=true)
     */
    private $fechaBaja = null;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_creacion", type="datetime")
     */
    private $fechaCreacion;
    
    /**
     * @ORM\OneToOne(targetEntity="MercadoPagoOrder", mappedBy="pedido")
     */
    private $mercadoPagoOrder;

    /**
     * @ORM\OneToOne(targetEntity="DescuentoAplicado", mappedBy="pedido")
     */
    private $descuentoAplicado;

    /**
     * @ORM\OneToOne(targetEntity="Factura", mappedBy="pedido")
     */
    private $factura;

    /**
     * @var string
     *
     * @ORM\Column(name="clarin365", type="string", length=255, nullable=true)
     */
    private $clarin365;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->items = new \Doctrine\Common\Collections\ArrayCollection();
        $this->fechaCreacion = new \DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set envio
     *
     * @param string $envio
     *
     * @return Pedido
     */
    public function setEnvio(\AppBundle\Entity\Envio $envio)
    {
        $this->envio = $envio;

        return $this;
    }

    /**
     * Get envio
     *
     * @return \AppBundle\Entity\Envio
     */
    public function getEnvio()
    {
        return $this->envio;
    }

    /**
     * Set dni
     *
     * @param string $dni
     *
     * @return Pedido
     */
    public function setDni($dni)
    {
        $this->dni = $dni;

        return $this;
    }

    /**
     * Get dni
     *
     * @return string
     */
    public function getDni()
    {
        return $this->dni;
    }

    /**
     * Get items
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Add item
     *
     * @param \AppBundle\Entity\PedidoItem $item
     *
     * @return Pedido
     */
    public function addItem(\AppBundle\Entity\PedidoItem $item)
    {
        $this->items[] = $item;

        return $this;
    }

    /**
     * Remove item
     *
     * @param \AppBundle\Entity\PedidoItem $item
     */
    public function removeItem(\AppBundle\Entity\PedidoItem $item)
    {
        $this->items->removeElement($item);
    }

    /**
     * Set usuario
     *
     * @param \AppBundle\Entity\Usuario $usuario
     *
     * @return Pedido
     */
    public function setUsuario(\AppBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \AppBundle\Entity\Usuario
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set fechaPago
     *
     * @param \DateTime $fechaPago
     *
     * @return Pedido
     */
    public function setFechaPago($fechaPago)
    {
        $this->fechaPago = $fechaPago;

        return $this;
    }

    /**
     * Get fechaPago
     *
     * @return \DateTime
     */
    public function getFechaPago()
    {
        return $this->fechaPago;
    }

    /**
     * Set fechaEnvio
     *
     * @param \DateTime $fechaEnvio
     *
     * @return Pedido
     */
    public function setFechaEnvio($fechaEnvio)
    {
        $this->fechaEnvio = $fechaEnvio;

        return $this;
    }

    /**
     * Get fechaEnvio
     *
     * @return \DateTime
     */
    public function getFechaEnvio()
    {
        return $this->fechaEnvio;
    }

    /**
     * Set fechaFacturacion
     *
     * @param \DateTime $fechaFacturacion
     *
     * @return Pedido
     */
    public function setFechaFacturacion($fechaFacturacion)
    {
        $this->fechaFacturacion = $fechaFacturacion;

        return $this;
    }

    /**
     * Get fechaFacturacion
     *
     * @return \DateTime
     */
    public function getFechaFacturacion()
    {
        return $this->fechaFacturacion;
    }

    /**
     * Set fechaBaja
     *
     * @param \DateTime $fechaBaja
     *
     * @return Pedido
     */
    public function setFechaBaja($fechaBaja)
    {
        $this->fechaBaja = $fechaBaja;

        return $this;
    }

    /**
     * Get fechaBaja
     *
     * @return \DateTime
     */
    public function getFechaBaja()
    {
        return $this->fechaBaja;
    }

    public function getPrecioTotalItems(){
        $items = $this->getItems();
        $precio = 0;
        foreach ($items as $item) {
            $precio += $item->getPrecio() * $item->getCantidad();
        }

        return $precio;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     *
     * @return Pedido
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set mercadoPagoOrder
     *
     * @param \AppBundle\Entity\MercadoPagoOrder $mercadoPagoOrder
     *
     * @return Pedido
     */
    public function setMercadoPagoOrder(\AppBundle\Entity\MercadoPagoOrder $mercadoPagoOrder = null)
    {
        $this->mercadoPagoOrder = $mercadoPagoOrder;

        return $this;
    }

    /**
     * Get mercadoPagoOrder
     *
     * @return \AppBundle\Entity\MercadoPagoOrder
     */
    public function getMercadoPagoOrder()
    {
        return $this->mercadoPagoOrder;
    }
    
    public function isPaid(){
        return !is_null($this->getFechaPago());
    }

    public function isSent(){
        return !is_null($this->getFechaEnvio());
    }

    /**
     * Set descuentoAplicado
     *
     * @param \AppBundle\Entity\DescuentoAplicado $descuentoAplicado
     *
     * @return Pedido
     */
    public function setDescuentoAplicado(\AppBundle\Entity\DescuentoAplicado $descuentoAplicado = null)
    {
        $this->descuentoAplicado = $descuentoAplicado;

        return $this;
    }

    /**
     * Get descuentoAplicado
     *
     * @return \AppBundle\Entity\DescuentoAplicado
     */
    public function getDescuentoAplicado()
    {
        return $this->descuentoAplicado;
    }

    /**
     * Set factura
     *
     * @param \AppBundle\Entity\Factura $factura
     *
     * @return Pedido
     */
    public function setFactura(\AppBundle\Entity\Factura $factura = null)
    {
        $this->factura = $factura;

        return $this;
    }

    /**
     * Get factura
     *
     * @return \AppBundle\Entity\Factura
     */
    public function getFactura()
    {
        return $this->factura;
    }

    public function getPaymentUrl(){
        $mpo = $this->getMercadoPagoOrder();
        return $mpo ? $mpo->getPaymentUrl() : '';
    }

    /**
     * Set clarin365
     *
     * @param string $clarin365
     *
     * @return Pedido
     */
    public function setClarin365($clarin365)
    {
        $this->clarin365 = $clarin365;

        return $this;
    }

    /**
     * Get clarin365
     *
     * @return string
     */
    public function getClarin365()
    {
        return $this->clarin365;
    }
}
