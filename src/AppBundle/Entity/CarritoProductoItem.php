<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CarritoProductoItem
 *
 * @ORM\Table(name="carrito_producto_item")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CarritoProductoItemRepository")
 */
class CarritoProductoItem
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Producto")
     * @ORM\JoinColumn(name="producto_id", referencedColumnName="id")
     */
    private $producto;

    /**
     * @var string
     *
     * @ORM\Column(name="carrito_id", type="string", length=100)
     */
    private $carrito_id;

    /**
     * @var int
     *
     * @ORM\Column(name="cantidad", type="integer")
     */
    private $cantidad;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * Muchos Envios tienen un Usuario
     * @ORM\ManyToOne(targetEntity="Usuario")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    protected $usuario;

    public function __construct() {
        $this->fecha = new \DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set producto
     *
     * @param integer $producto
     *
     * @return CarritoProductoItem
     */
    public function setProducto($producto)
    {
        $this->producto = $producto;

        return $this;
    }

    /**
     * Get producto
     *
     * @return int
     */
    public function getProducto()
    {
        return $this->producto;
    }

    /**
     * Get producto
     *
     * @return int
     */
    public function getProductoId()
    {
        return $this->producto ? $this->producto->getId() : null;
    }

    /**
     * Set carrito_id
     *
     * @param string $carrito_id
     *
     * @return CarritoProductoItem
     */
    public function setCarritoId($carrito_id)
    {
        $this->carrito_id = $carrito_id;

        return $this;
    }

    /**
     * Get carrito_id
     *
     * @return string
     */
    public function getCarritoId()
    {
        return $this->carrito_id;
    }

    /**
     * Set cantidad
     *
     * @param integer $cantidad
     *
     * @return CarritoProductoItem
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return int
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fechaDesde
     *
     * @return CarritoProductoItem
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set usuario
     *
     * @param \AppBundle\Entity\Usuario $usuario
     *
     * @return CarritoProductoItem
     */
    public function setUsuario(\AppBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \AppBundle\Entity\Usuario
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

}
