<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ProductoImagen
 * @ORM\Table(name="producto_imagen")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ImagenRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Vich\Uploadable
 */
class ProductoImagen
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string 
     *
     * @ORM\Column(name="imagen", type="string", length=255, nullable=true)
     */
    private $imagen;

    /**
     * @Vich\UploadableField(mapping="producto_imagen", fileNameProperty="imagen")
     * @Assert\File(maxSize = "2M")
     * @var File
     */
    private $imagen_archivo;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updatedAt = null;

    /**
     * @var int
     * @ORM\Column(name="orden", type="integer", nullable=false)
     */
    private $orden;

    /**
     * Many Features have One Product.
     * @ORM\ManyToOne(targetEntity="Producto", inversedBy="imagenes")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */

    private $producto;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get imagen
     *
     * @return string
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * Set imagen
     *
     * @param string $imagen
     *
     * @return Banner
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * @return File|null
     */
    public function getImagenArchivo()
    {
        return $this->imagen_archivo;
    }

     /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $imagen
     * @return ProductoImagen
     */
    public function setImagenArchivo(File $imagen = null)
    {
        $this->imagen_archivo = $imagen;

        if ($imagen) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
        
        return $this;
    }
    
    /**
     * @ORM\PrePersist
     */
    public function updateTime(){
        $this->updatedAt = new \DateTimeImmutable();
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Banner
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set orden
     *
     * @param integer $orden
     *
     * @return ProductoImagen
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return int
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return ProductoImagen
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Set producto
     *
     * @param \AppBundle\Entity\Producto $producto
     *
     * @return ProductoImagen
     */
    public function setProducto(\AppBundle\Entity\Producto $producto = null)
    {
        $this->producto = $producto;

        return $this;
    }

    /**
     * Get producto
     *
     * @return \AppBundle\Entity\Producto
     */
    public function getProducto()
    {
        return $this->producto;
    }
}
