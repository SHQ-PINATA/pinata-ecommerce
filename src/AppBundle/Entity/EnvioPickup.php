<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * EnvioPickup
 *
 * @ORM\Table(name="envio_pickup")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EnvioPickupRepository")
 */
class EnvioPickup extends Envio
{
     /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     * @Assert\NotBlank(message = "Ingrese su nombre")
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apellido", type="string", length=255)
     * @Assert\NotBlank(message = "Ingrese su apellido")
     */
    private $apellido;
    
    /**
     * @var string
     * @ORM\Column(name="telefono", type="string", length=50, nullable=true)
     * @Assert\NotBlank(message = "Ingrese su numero de telefono")
     */
    private $telefono;

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return EnvioDomicilio
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellido
     *
     * @param string $apellido
     *
     * @return EnvioDomicilio
     */
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;

        return $this;
    }

    /**
     * Get apellido
     *
     * @return string
     */
    public function getApellido()
    {
        return $this->apellido;
    }


    /**
     * Get precio
     *
     * @return float
     */
    public function getPrecio()
    {
        return 0;
    }

    public function getTipo(){
        return ENVIO::PICKUP;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     *
     * @return EnvioPickup
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }
}
