<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="plataforma")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PlataformaRepository") 
 */
class Plataforma
{
    CONST WEB          = 'web';
    CONST MERCADOLIBRE = 'meli';
    CONST TIENDANUBE   = 'tiendanube';
    CONST MERCADOSHOPS = 'mshops';
   
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=20)
     */
    protected $id;

    /**
    * @ORM\Column(type="string", length=255)
    */
    protected $nombre;



    /**
     * Set id
     *
     * @param string $id
     * @return Plataforma
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Plataforma
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }
}
