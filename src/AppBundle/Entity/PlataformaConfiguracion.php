<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Plataforma;

/**
 * @ORM\Entity
 * @ORM\Table(name="plataforma_configuracion")
 */
class PlataformaConfiguracion
{
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Plataforma")
     * @ORM\JoinColumn(name="id_plataforma", referencedColumnName="id")
     */
    protected $plataforma;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $data_configuracion = '[]';

    /**
     * Set data_configuracion
     *
     * @param array $dataConfiguracion
     * @return PlataformaConfiguracion
     */
    public function setDataConfiguracion($dataConfiguracion)
    {
        if ( !is_array( $dataConfiguracion ) ) {
            $dataConfiguracion = array();
        }
        
        $this->data_configuracion = json_encode($dataConfiguracion);

        return $this;
    }

    /**
     * Get data_configuracion
     *
     * @return array
     */
    public function getDataConfiguracion()
    {
        $dataConfiguracion = json_decode( $this->data_configuracion, true );

        if ( is_array( $dataConfiguracion ) ) {
            return $dataConfiguracion;
        }

        return array();
    }

    /**
     * Set plataforma
     *
     * @return PlataformaConfiguracion
     */
    public function setPlataforma(Plataforma $plataforma)
    {
        $this->plataforma = $plataforma;

        return $this;
    }

    /**
     * Get plataforma
     */
    public function getPlataforma()
    {
        return $this->plataforma;
    }

    /**
     * Get Id de la plataforma
     *
     * @return string 
     */
    public function getIdPlataforma()
    {
        return $this->getPlataforma()->getId();
    }

    /**
     * Get nombre de la plataforma
     *
     * @return string 
     */
    public function getNombrePlataforma()
    {
        return $this->getPlataforma()->getNombre();
    }

    /**
     * Esta Activa
     *
     * @return boolean
     */
    public function estaActiva()
    {
        $dataConfiguracion = $this->getDataConfiguracion();

        if ( empty($dataConfiguracion) ) {
            return false;
        }

        switch ($this->getPlataforma()->getId()) {
            case Plataforma::MERCADOLIBRE:
                return isset( $dataConfiguracion['access_token'] ) && isset( $dataConfiguracion['refresh_token'] ) && isset( $dataConfiguracion['expires'] ) && isset( $dataConfiguracion['user_id'] );
                break;
            default:
                return false;
                break;
        }
    }
}
