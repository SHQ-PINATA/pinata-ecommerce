<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Cocur\Slugify\Slugify;

/**
 * Personaje
 *
 * @ORM\Table(name="personaje")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PersonajeRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Vich\Uploadable
 * @UniqueEntity("slug", message="Ya existe un personaje con ese nombre")
 */
class Personaje
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="imagen_carousel", type="string", length=255, nullable=true)
     */
    private $imagen_carousel;

    /**
     * @Vich\UploadableField(mapping="personaje", fileNameProperty="imagen_carousel")
     * @var File
     */
    private $imagen_carousel_archivo;

    /**
     * @var string
     *
     * @ORM\Column(name="imagen_banner", type="string", length=255, nullable=true)
     */
    private $imagen_banner;

    /**
     * @Vich\UploadableField(mapping="personaje", fileNameProperty="imagen_banner")
     * @var File
     */
    private $imagen_banner_archivo;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updatedAt = null;

    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     * @var \DateTime
     */
    private $deleted_at = null;

    /**
     * @var int
     *
     * @ORM\Column(name="orden", type="integer", options={"default" : 0})
     */
    private $orden = 0;
    
    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=128, unique=true, nullable=true)
     */
    private $slug;

    /**
     * @var boolean
     *
     * @ORM\Column(name="activo", type="boolean", options={"default" : true})
     */
    protected $activo = true;

    /**
     * @ORM\OneToMany(targetEntity="Producto", mappedBy="personaje")
     */
    private $productos;

     public function __construct() {
        $this->productos = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Personaje
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }


    public function __toString(){
        return $this->getNombre();
    }

    /**
     * Set slug
     *
     * @ORM\PreUpdate
     * @ORM\PrePersist
     *
     * @param string $slug
     *
     * @return Categoria
     */
    public function setSlug($slug)
    {
        $slugify = new Slugify();
        $this->slug = $slugify->slugify($this->nombre);

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Personaje
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Personaje
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deleted_at = $deletedAt;
        $this->slug = null;
        
        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deleted_at;
    }

    /**
     * Get imagenCarousel
     *
     * @return string
     */
    public function getImagenCarousel()
    {
        return $this->imagen_carousel;
    }

    /**
     * Get imagenCarousel
     *
     * @return string
     */
    public function getImagenCarouselArchivo()
    {
        return $this->imagen_carousel_archivo;
    }
    
    /**
     * Get imagenBanner
     *
     * @return string
     */
    public function getImagenBanner()
    {
        return $this->imagen_banner;
    }
    /**
     * Get imagenBanner
     *
     * @return string
     */
    public function getImagenBannerArchivo()
    {
        return $this->imagen_banner_archivo;
    }

    /**
     * Set imagen carousel
     *
     * @param string $imagen
     *
     * @return Personaje
     */
    public function setImagenCarousel($imagen)
    {
        $this->imagen_carousel = $imagen;

        return $this;
    }

    /**
     * Set imagen banner
     *
     * @param string $imagen
     *
     * @return Personaje
     */
    public function setImagenBanner($imagen)
    {
        $this->imagen_banner = $imagen;

        return $this;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $imagen
     *
     * @return Personaje
     */
    public function setImagenCarouselArchivo(File $imagen = null)
    {
        $this->imagen_carousel_archivo = $imagen;

        if ($imagen) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
        
        return $this;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $imagen
     *
     * @return Personaje
     */
    public function setImagenBannerArchivo(File $imagen = null)
    {
        $this->imagen_banner_archivo = $imagen;

        if ($imagen) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
        
        return $this;
    }


    /**
     * Set orden
     *
     * @param integer $orden
     *
     * @return Personaje
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return integer
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     *
     * @return Personaje
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean
     */
    public function getActivo()
    {
        return $this->activo;
    }
}
