<?php

namespace AppBundle\Entity;

/**
 * ProductoEnCarrito
 */
class CarritoResponse
{
    /**
     * @var string
     */
    private $status;

    /**
     * @var int
     */
    private $cantidad;

    /**
     * @var int
     */
    private $stock_disponible;

    /**
     * @var string
     */
    private $error;


    public function getCantidad()
    {
        return $this->cantidad;
    }

    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }


    public function getStockDisponible()
    {
        return $this->stock_disponible;
    }

    public function setStockDisponible($stock_disponible)
    {
        $this->stock_disponible = $stock_disponible;

        return $this;
    }


    public function getError()
    {
        return $this->error;
    }

    public function setError($error)
    {
        $this->error = $error;

        return $this;
    }

    public function isStatusOk(){
        return $this->status == 'ok';
    }

    public function setStatusToOk(){
        $this->status = 'ok';   
        return $this;
    }

    public function isStatusError(){
        return $this->status == 'error';
    }

    public function setStatusToError(){
        $this->status = 'error';
        return $this;
    }

    public function getArrayResponse(){
        if ($this->isStatusOk()) {
            $res = array('status' => 'ok');
        }else{
            $res = array('status' => 'error', 'error' => $this->getError());
        }

        if ( !is_null($this->getCantidad() )) {
           $res['cantidad'] = $this->getCantidad();
        }
        if ( !is_null($this->getStockDisponible()) ) {
            $res['stock_disponible'] = $this->getStockDisponible();
        }

        return $res;
    }
}

