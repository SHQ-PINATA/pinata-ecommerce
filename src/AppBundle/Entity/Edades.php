<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductoEdades
 *
 * @ORM\Table(name="edades")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EdadesRepository")
 */
class Edades
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="edades", type="string", length=255)
     */
    private $edades;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set edades
     *
     * @param string $edades
     *
     * @return ProductoEdades
     */
    public function setEdades($edades)
    {
        $this->edades = $edades;

        return $this;
    }

    /**
     * Get edades
     *
     * @return string
     */
    public function getEdades()
    {
        return $this->edades;
    }

    public function __toString(){
        return $this->getEdades();
    }
}
