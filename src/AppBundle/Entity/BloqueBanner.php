<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Doctrine\Common\Collections\Criteria;

/**
 * Banner
 *
 * @ORM\Table(name="bloque_banner")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BloqueBannerRepository")
 */
class BloqueBanner
{
    const MARQUESINA_TOP        = 'MARQTOP';
    const MARQUESINA_TOP_MOBILE = 'MARQMOB';
    const PRINCIPAL             = 'MAINBAN';
    const PRINCIPAL_MOBILE      = 'MAINMOB';
    const SECUNDARIO            = 'SECUBAN';
    const SECUNDARIO_MOBILE     = 'SECUMOB';
    const TERCIARIO_X4          = 'TERCIX4';
    const TERCIARIO_X3          = 'TERCIX3';
    const MARQUESINA_FOOTER     = 'MARQFOO';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     * @Assert\NotNull()
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=7, options={"fixed" = true})
     * @Assert\Choice(callback = "getTipos")
     * @Assert\NotNull(message = "Elija un tipo de banner")
     */
    private $tipo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_desde", type="datetime", nullable=true)
     */
    private $fechaDesde;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_hasta", type="datetime", nullable=true)
     */
    private $fechaHasta;

    /**
     * @var bool
     *
     * @ORM\Column(name="activo", type="boolean")
     * @Assert\NotNull()
     */
    private $activo = false;

    /**
     * @var int
     *
     * @ORM\Column(name="orden", type="integer", nullable=true)
     */
    private $orden;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $ver_mobile = false;

    /**
     * Un bloque tiene muchos banners.
     * @ORM\OneToMany(targetEntity="Banner", mappedBy="bloque", cascade={"all"}, orphanRemoval=true)
     * @ORM\OrderBy({"orden" = "ASC"})
     * @Assert\Valid
     */
    protected $banners;


    public function __construct() {
        $this->banners = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Banner
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set fechaDesde
     *
     * @param \DateTime $fechaDesde
     *
     * @return Banner
     */
    public function setFechaDesde($fechaDesde)
    {
        $this->fechaDesde = $fechaDesde;

        return $this;
    }

    /**
     * Get fechaDesde
     *
     * @return \DateTime
     */
    public function getFechaDesde()
    {
        return $this->fechaDesde;
    }

    /**
     * Set fechaHasta
     *
     * @param \DateTime $fechaHasta
     *
     * @return Banner
     */
    public function setFechaHasta($fechaHasta)
    {
        $this->fechaHasta = $fechaHasta;

        return $this;
    }

    /**
     * Get fechaHasta
     *
     * @return \DateTime
     */
    public function getFechaHasta()
    {
        return $this->fechaHasta;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     *
     * @return Banner
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return bool
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return Banner
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set orden
     *
     * @param integer $orden
     *
     * @return Banner
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return integer
     */
    public function getOrden()
    {
        return $this->orden;
    }

    public static function getTipos(){
        return array(
            'Marquesina top'                => self::MARQUESINA_TOP,
            'Marquesina top (mobile)'       => self::MARQUESINA_TOP_MOBILE,
            'Carrousel Principal'           => self::PRINCIPAL,
            'Carrousel Principal (mobile)'  => self::PRINCIPAL_MOBILE,
            'Fila de 4 banners'             => self::TERCIARIO_X4,
            'Carrousel Secundario'          => self::SECUNDARIO,
            'Carrousel Secundario (mobile)' => self::SECUNDARIO_MOBILE,
            'Fila de 3 banners'             => self::TERCIARIO_X3,
            'Marquesina footer'             => self::MARQUESINA_FOOTER
            );
    }

    public function getTipoDescripcion(){
        foreach ($this->getTipos() as $descripcion => $tipo) {
            if ($this->getTipo() == $tipo) {
                return $descripcion;
            }
        }
    }

    /**
     * Add banner
     *
     * @param \AppBundle\Entity\Banner $banner
     *
     * @return BloqueBanner
     */
    public function addBanner(\AppBundle\Entity\Banner $banner)
    {
        if($banner->hasImageOrVideoSet()){
            $this->banners->add($banner);
            $banner->setBloque($this);
        }

        return $this;
    }

    /**
     * Remove banner
     *
     * @param \AppBundle\Entity\Banner $banner
     */
    public function removeBanner(\AppBundle\Entity\Banner $banner)
    {
        $this->banners->removeElement($banner);
    }

    /**
     * Get banners
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBanners()
    {
        $criteria = Criteria::create()->orderBy(array("orden" => Criteria::ASC));

        return $this->banners->matching($criteria);
    }

    /**
     * Get banner
     *
     * @return \AppBundle\Entity\Banner
     */
    public function getBannerByOrden($orden)
    {
        foreach ($this->banners as $banner) {
            if ($orden == $banner->getOrden()) {
                return $banner;
            }
        }
    }

    public function hasVariableBanners(){
        return $this->getTipo() == self::PRINCIPAL;
    }

    public function estaVigente(){
       $ahora = new \DateTime();
        return $this->getFechaDesde() < $ahora
            && $this->getFechaHasta() > $ahora;
    }

    /**
     * @Assert\Callback
     */
    public function validarFechas(ExecutionContextInterface $context, $payload)
    {
        if ($this->getFechaDesde() > $this->getFechaHasta()) {
            $context->buildViolation('El campo "Fecha Desde" debe ser anterior a "Fecha Hasta" ')
                ->atPath('fechaDesde')
                ->addViolation();
        }
    }

    /**
     * @Assert\Callback
     */
    public function validarBanners(ExecutionContextInterface $context, $payload)
    {
        $cant_banners = 0;
        $banners = $this->getBanners();
        foreach ($banners as $banner) {
            $tiene_imagen_o_video = $banner->getImagen() || $banner->getVideo() || $banner->getImagenArchivo() || $banner->getVideoArchivo();
            $cant_banners += $tiene_imagen_o_video ? 1 : 0;
        }

        $tipo_bloque = $this->getTipo();
        if (self::PRINCIPAL == $tipo_bloque || self::PRINCIPAL_MOBILE == $tipo_bloque || self::MARQUESINA_TOP == $tipo_bloque || self::MARQUESINA_TOP_MOBILE == $tipo_bloque || self::MARQUESINA_FOOTER == $tipo_bloque || self::SECUNDARIO == $tipo_bloque || self::SECUNDARIO == $tipo_bloque || self::SECUNDARIO_MOBILE == $tipo_bloque) {
            if (0 == $cant_banners) {
                $context->buildViolation('Se requiere que se seleccione al menos 1 banner')
                ->atPath('banners')
                ->addViolation();
            }
        }elseif (self::TERCIARIO_X4 == $tipo_bloque) {
            if (4 != $cant_banners) {
                $context->buildViolation('Se requiere que se seleccionen exactamente 4 banners')
                ->atPath('banners')
                ->addViolation();
            }
        }elseif (self::TERCIARIO_X3 == $tipo_bloque) {
            if (3 != $cant_banners) {
                $context->buildViolation('Se requiere que se seleccionen exactamente 3 banners')
                ->atPath('banners')
                ->addViolation();
            }
        }
    }

    /**
     * Set verMobile
     *
     * @param boolean $verMobile
     *
     * @return BloqueBanner
     */
    public function setVerMobile($verMobile)
    {
        $this->ver_mobile = $verMobile;

        return $this;
    }

    /**
     * Get verMobile
     *
     * @return boolean
     */
    public function getVerMobile()
    {
        return $this->ver_mobile;
    }
}
