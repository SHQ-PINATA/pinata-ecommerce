<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Service\Afip;

/**
 * Factura
 *
 * @ORM\Table(name="factura")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FacturaRepository")
 */
class Factura
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var Pedido
     *
     * @ORM\OneToOne(targetEntity="Pedido", inversedBy="factura")
     * @ORM\JoinColumn(name="pedido_id", referencedColumnName="id")
     */
    private $pedido;

    /**
    * @ORM\Column(name="fecha_procesado", type="datetime", nullable=true)
    */
    protected $fechaProcesada;

    /**
    * @ORM\Column(name="importe_total", type="decimal", precision=9, scale=2, nullable=true)
    */
    protected $importeTotal;

    /**
    * @ORM\Column(name="importe_iva", type="decimal", precision=9, scale=2, nullable=true)
    */
    protected $importeIva;

    /**
    * @ORM\Column(name="cae", type="string", length=50, nullable=true)
    */
    protected $CAE;

    /**
    * @ORM\Column(name="cae_vencimiento", type="date", nullable=true)
    */
    protected $CAEVencimiento;

    /**
    * @ORM\Column(name="comprobante", type="integer", options={"unsigned"=true}, nullable=true)
    */
    protected $comprobante;

    /**
    * @ORM\Column(name="tipo_comprobante", type="integer", nullable=true)
    */
    protected $tipoComprobante;

    /**
    * @ORM\Column(name="entorno_afip", type="string", length=4, options={"fixed":true})
    */
    protected $entornoAfip;

    /**
    * @ORM\Column(name="resultado", type="string", length=1, options={"fixed":true}, nullable=true)
    */
    protected $resultado;

    /**
    * @ORM\Column(name="mail_enviado", type="boolean")
    */
    protected $mailEnviado = false;

    /**
    * @ORM\Column(name="mail_error_enviado", type="datetime", nullable=true)
    */
    protected $mailErrorEnviado;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pedido
     *
     * @param \AppBundle\Entity\Pedido $pedido
     *
     * @return Factura
     */
    public function setPedido(\AppBundle\Entity\Pedido $pedido = null)
    {
        $this->pedido = $pedido;

        return $this;
    }

    /**
     * Get pedido
     *
     * @return \AppBundle\Entity\Pedido
     */
    public function getPedido()
    {
        return $this->pedido;
    }


    /**
     * Set fechaProcesada
     *
     * @param \DateTime $fechaProcesada
     *
     * @return Factura
     */
    public function setFechaProcesada($fechaProcesada)
    {
        $this->fechaProcesada = $fechaProcesada;

        return $this;
    }

    /**
     * Get fechaProcesada
     *
     * @return \DateTime
     */
    public function getFechaProcesada()
    {
        return $this->fechaProcesada;
    }

    /**
     * Set importeTotal
     *
     * @param string $importeTotal
     *
     * @return Factura
     */
    public function setImporteTotal($importeTotal)
    {
        $this->importeTotal = $importeTotal;

        return $this;
    }

    /**
     * Get importeTotal
     *
     * @return string
     */
    public function getImporteTotal()
    {
        return $this->importeTotal;
    }

    /**
     * Set importeIva
     *
     * @param string $importeIva
     *
     * @return Factura
     */
    public function setImporteIva($importeIva)
    {
        $this->importeIva = $importeIva;

        return $this;
    }

    /**
     * Get importeIva
     *
     * @return string
     */
    public function getImporteIva()
    {
        return $this->importeIva;
    }

    /**
     * Set cAE
     *
     * @param string $cAE
     *
     * @return Factura
     */
    public function setCAE($cAE)
    {
        $this->CAE = $cAE;

        return $this;
    }

    /**
     * Get cAE
     *
     * @return string
     */
    public function getCAE()
    {
        return $this->CAE;
    }

    /**
     * Set cAEVencimiento
     *
     * @param \DateTime $cAEVencimiento
     *
     * @return Factura
     */
    public function setCAEVencimiento($cAEVencimiento)
    {
        $this->CAEVencimiento = $cAEVencimiento;

        return $this;
    }

    /**
     * Get cAEVencimiento
     *
     * @return \DateTime
     */
    public function getCAEVencimiento()
    {
        return $this->CAEVencimiento;
    }

    /**
     * Set comprobante
     *
     * @param integer $comprobante
     *
     * @return Factura
     */
    public function setComprobante($comprobante)
    {
        $this->comprobante = $comprobante;

        return $this;
    }

    /**
     * Get comprobante
     *
     * @return integer
     */
    public function getComprobante()
    {
        return $this->comprobante;
    }

    /**
     * Set tipoComprobante
     *
     * @param integer $tipoComprobante
     *
     * @return Factura
     */
    public function setTipoComprobante($tipoComprobante)
    {
        $this->tipoComprobante = $tipoComprobante;

        return $this;
    }

    /**
     * Get tipoComprobante
     *
     * @return integer
     */
    public function getTipoComprobante()
    {
        return $this->tipoComprobante;
    }

    /**
     * Set entornoAfip
     *
     * @param string $entornoAfip
     *
     * @return Factura
     */
    public function setEntornoAfip($entornoAfip)
    {
        $this->entornoAfip = $entornoAfip;

        return $this;
    }

    /**
     * Get entornoAfip
     *
     * @return string
     */
    public function getEntornoAfip()
    {
        return $this->entornoAfip;
    }

    /**
     * Set resultado
     *
     * @param string $resultado
     *
     * @return Factura
     */
    public function setResultado($resultado)
    {
        $this->resultado = $resultado;

        return $this;
    }

    /**
     * Get resultado
     *
     * @return string
     */
    public function getResultado()
    {
        return $this->resultado;
    }

    /**
     * Set mailEnviado
     *
     * @param boolean $mailEnviado
     *
     * @return Factura
     */
    public function setMailEnviado($mailEnviado)
    {
        $this->mailEnviado = $mailEnviado;

        return $this;
    }

    /**
     * Get mailEnviado
     *
     * @return boolean
     */
    public function getMailEnviado()
    {
        return $this->mailEnviado;
    }


    public function getLetra()
    {
        return $this->getTipoComprobante() === Afip::FACTURA_A ? 'A' : 'B'; 
    }

    /**
     * Set mailErrorEnviado
     *
     * @param \DateTime $mailErrorEnviado
     *
     * @return Factura
     */
    public function setMailErrorEnviado($mailErrorEnviado)
    {
        $this->mailErrorEnviado = $mailErrorEnviado;

        return $this;
    }

    /**
     * Get mailErrorEnviado
     *
     * @return \DateTime
     */
    public function getMailErrorEnviado()
    {
        return $this->mailErrorEnviado;
    }
}
