<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\Criteria;
use Cocur\Slugify\Slugify;
use AppBundle\Entity\ProductoImagen;

/**
 * Producto.
 * @ORM\Table(name="producto")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Producto
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="nombre", type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     */
    protected $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo_ean", type="string", length=255)
     * @Assert\NotBlank()
     */
    protected $codigo_ean;

    /**
     * @var string
     *
     * @ORM\Column(name="dimensiones", type="string", length=255, nullable=true)
     */
    protected $dimensiones;

    /**
     * @var string (max 64 KiB)
     *
     * @ORM\Column(name="descripcion", type="text", length=2048, nullable=true)
     */
    protected $descripcion = null;

    /**
     * @var boolean
     *
     * @ORM\Column(name="es_combo", type="boolean")
     */
    protected $es_combo = false;

    /**
     * @var float
     *
     * @ORM\Column(name="precio", type="float")
     * @Assert\NotBlank(message = "Ingrese el precio")
     */
    private $precio;

    /**
     * @var float
     *
     * @ORM\Column(name="precio_oferta", type="float", nullable=true)
     */
    private $precio_oferta = null;

    /**
     * @var float
     *
     * @ORM\Column(name="peso", type="float", nullable=true)
     * @Assert\NotBlank(message = "Ingrese el peso")
     */
    private $peso = null;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="sale", type="boolean")
     */
    
    protected $sale = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="novedad", type="boolean")
     */
    
    protected $novedad = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="activo", type="boolean")
     */
    
    protected $activo = true;

    /**
     * @var boolean
     *
     * @ORM\Column(name="discontinuado", type="boolean")
     */
    
    protected $discontinuado = false;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="destacado", type="boolean")
     */
    
    protected $destacado = false;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255)
     */
    private $slug;

    /**
     * @var integer
     *
     * @ORM\Column(name="stock", type="integer", options={"default" : 0})
     */
    private $stock = 0;

    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     * @var \DateTime
     */
    private $deleted_at = null;

    // Relaciones:
    /**
     * Muchos productos tienen una categoria
     * @ORM\ManyToOne(targetEntity="Categoria", inversedBy="productos")
     * @ORM\JoinColumn(name="categoria_id", referencedColumnName="id")
     */
    protected $categoria;

    /**
     * Muchos productos tienen muchos generos
     * @ORM\ManyToMany(targetEntity="Genero")
     * @ORM\JoinTable(name="productos_generos",
     *      joinColumns={@ORM\JoinColumn(name="producto_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="genero_id", referencedColumnName="id")}
     *      )
     */
    protected $generos;

    /**
     * Un Producto tiene muchas imagenes.
     * @ORM\OneToMany(targetEntity="ProductoImagen", mappedBy="producto", indexBy="id", cascade={"persist"}, orphanRemoval=true)
     * @ORM\OrderBy({"orden" = "ASC"})
     * @Assert\Valid
     */
    protected $imagenes;

    /**
     * Muchos productos tienen una personaje
     * @ORM\ManyToOne(targetEntity="Personaje", inversedBy="productos")
     * @ORM\JoinColumn(name="personaje_id", referencedColumnName="id")
     */
    protected $personaje;

    /**
     * Muchos productos tienen un rango de edades
     * @ORM\ManyToOne(targetEntity="Edades")
     * @ORM\JoinColumn(name="edades_id", referencedColumnName="id")
     */
    protected $edades;

    /**
     * Muchos productos tienen un modelo
     * @ORM\ManyToOne(targetEntity="Modelo")
     * @ORM\JoinColumn(name="modelo_id", referencedColumnName="id", nullable=true)
     */
    protected $modelo;

    /**
     * @var int
     *
     * @ORM\Column(name="orden", type="integer", nullable=true)
     */
    private $orden = 0;

    /**
     * @ORM\OneToMany(targetEntity="Stock", mappedBy="producto", cascade={"persist"})
     */
    protected $stock_actions;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo_temporada", type="string", length=3, nullable=true)
     */
    protected $codigo_temporada;

    /**
     * @var string
     *
     * @ORM\Column(name="meli_id", type="string", nullable=true)
     */
    protected $meliId;

    /**
     * @var string
     *
     * @ORM\Column(name="dafiti_sku", type="string", nullable=true)
     */
    protected $dafitiSku;


    /**
     * @ORM\ManyToOne(targetEntity="Producto", inversedBy="talles")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="Producto", mappedBy="parent")
     */
    private $talles;

    /**
     * @var boolean
     *
     * @ORM\Column(name="google_shopping", type="boolean")
     */
    protected $google_shopping = false;

    /**
     * @var string
     *
     * @ORM\Column(name="circulo_descuento", type="string", nullable=true)
     */
    protected $circulo_descuento;

    /**
     * @var boolean
     *
     * @ORM\Column(name="alta_demanda", type="boolean")
     */
    protected $alta_demanda = false;


    public function __construct() {
        $this->imagenes = new ArrayCollection();
        $this->generos = new ArrayCollection();
        $this->stock_actions = new ArrayCollection();
    }

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion_talle", type="string", nullable=true)
     */
    protected $descripcionTalle;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Producto
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Producto
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set esCombo
     *
     * @param boolean $esCombo
     *
     * @return Producto
     */
    public function setEsCombo($esCombo)
    {
        $this->es_combo = $esCombo;

        return $this;
    }

    /**
     * Get esCombo
     *
     * @return boolean
     */
    public function getEsCombo()
    {
        return $this->es_combo;
    }

    /**
     * Set precio
     *
     * @param float $precio
     *
     * @return Producto
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return float
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set precioOferta
     *
     * @param float $precioOferta
     *
     * @return Producto
     */
    public function setPrecioOferta($precioOferta)
    {
        $this->precio_oferta = $precioOferta;

        return $this;
    }

    /**
     * Get precioOferta
     *
     * @return float
     */
    public function getPrecioOferta()
    {
        return $this->precio_oferta;
    }

    /**
     * Set peso
     *
     * @param float $peso
     *
     * @return Producto
     */
    public function setPeso($peso)
    {
        $this->peso = $peso;

        return $this;
    }

    /**
     * Get peso
     *
     * @return float
     */
    public function getPeso()
    {
        return $this->peso;
    }

    /**
     * Set sale
     *
     * @param boolean $sale
     *
     * @return Producto
     */
    public function setSale($sale)
    {
        $this->sale = $sale;

        return $this;
    }

    /**
     * Get sale
     *
     * @return boolean
     */
    public function getSale()
    {
        return $this->sale;
    }

    /**
     * Set novedad
     *
     * @param boolean $novedad
     *
     * @return Producto
     */
    public function setNovedad($novedad)
    {
        $this->novedad = $novedad;

        return $this;
    }

    /**
     * Get novedad
     *
     * @return boolean
     */
    public function getNovedad()
    {
        return $this->novedad;
    }

    /**
     * Set destacado
     *
     * @param boolean $destacado
     *
     * @return Producto
     */
    public function setDestacado($destacado)
    {
        $this->destacado = $destacado;

        return $this;
    }

    /**
     * Get esCombo
     *
     * @return boolean
     */
    public function getDestacado()
    {
        return $this->destacado;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     *
     * @return Producto
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set discontinuado
     *
     * @param boolean $discontinuado
     *
     * @return Producto
     */
    public function setDiscontinuado($discontinuado)
    {
        $this->discontinuado = $discontinuado;

        return $this;
    }

    /**
     * Get discontinuado
     *
     * @return boolean
     */
    public function getDiscontinuado()
    {
        return $this->discontinuado;
    }


    /**
     * Set categoria
     *
     * @param \AppBundle\Entity\Categoria $categoria
     *
     * @return Producto
     */
    public function setCategoria(\AppBundle\Entity\Categoria $categoria = null)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return \AppBundle\Entity\Categoria
     */
    public function getCategoria()
    {
        return $this->categoria;
    }


    /**
     * Add imagen
     *
     * @param \AppBundle\Entity\Imagen $imagen
     *
     * @return Producto
     */
    public function addImagen(\AppBundle\Entity\ProductoImagen $imagen)
    {
        $this->imagenes->add($imagen);
        $imagen->setProducto($this);

        return $this;
    }

    /**
     * Remove imagen
     *
     * @param \AppBundle\Entity\Imagen $imagene
     */
    public function removeImagen(\AppBundle\Entity\ProductoImagen $imagen)
    {
        $this->imagenes->removeElement($imagen);
    }

    /**
     * Get imagenes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImagenes()
    {
        return $this->imagenes;
    }

    /**
     * Set personaje
     *
     * @param \AppBundle\Entity\Personaje $personaje
     *
     * @return Producto
     */
    public function setPersonaje(\AppBundle\Entity\Personaje $personaje = null)
    {
        $this->personaje = $personaje;

        return $this;
    }

    /**
     * Get personaje
     *
     * @return \AppBundle\Entity\Personaje
     */
    public function getPersonaje()
    {
        return $this->personaje;
    }

    /**
     * Set edades
     *
     * @param \AppBundle\Entity\Edades $edades
     *
     * @return Producto
     */
    public function setEdades(\AppBundle\Entity\Edades $edades = null)
    {
        $this->edades = $edades;

        return $this;
    }

    /**
     * Get edades
     *
     * @return \AppBundle\Entity\Edades
     */
    public function getEdades()
    {
        return $this->edades;
    }

    /**
     * Set modelo
     *
     * @param \AppBundle\Entity\Modelo $modelo
     *
     * @return Producto
     */
    public function setModelo(\AppBundle\Entity\Modelo $modelo = null)
    {
        $this->modelo = $modelo;

        return $this;
    }

    /**
     * Get modelo
     *
     * @return \AppBundle\Entity\Modelo
     */
    public function getModelo()
    {
        return $this->modelo;
    }

    /**
     * Set codigoEan
     *
     * @param string $codigoEan
     *
     * @return Producto
     */
    public function setCodigoEan($codigoEan)
    {
        $this->codigo_ean = $codigoEan;

        return $this;
    }

    /**
     * Get codigoEan
     *
     * @return string
     */
    public function getCodigoEan()
    {
        return $this->codigo_ean;
    }

    /**
     * Get codigoSku
     * El codigo SKU son los ultimos 5 digitos del codigo EAN sin el ultimo digito
     * @return string
     */
    public function getCodigoSku()
    {
        return substr($this->codigo_ean, strlen($this->codigo_ean)-5, 4);
    }

    public static function getSkuFromEan($ean)
    {
        return substr($ean, strlen($ean)-5, 4);
    }

    /**
     * Add genero
     *
     * @param \AppBundle\Entity\Genero $genero
     *
     * @return Producto
     */
    public function addGenero(\AppBundle\Entity\Genero $genero)
    {
        $this->generos[] = $genero;

        return $this;
    }

    /**
     * Remove genero
     *
     * @param \AppBundle\Entity\Genero $genero
     */
    public function removeGenero(\AppBundle\Entity\Genero $genero)
    {
        $this->generos->removeElement($genero);
    }

    /**
     * Get generos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGeneros()
    {
        return $this->generos;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Producto
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deleted_at = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deleted_at;
    }

    /**
     * Set slug
     *
     * @ORM\PreUpdate
     * @ORM\PrePersist
     *
     * @param string $slug
     *
     * @return Categoria
     */
    public function setSlug($slug)
    {
        $slugify = new Slugify();
        $this->slug = $slugify->slugify($this->nombre);

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }



    /**
     * Set stock
     *
     * @param integer $stock
     *
     * @return Producto
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return integer
     */
    public function getStock()
    {
        return $this->stock;
    }


    /**
     * Add stock
     *
     * @param \AppBundle\Entity\Stock $stock
     *
     * @return Producto
     */
    public function addStockAction(\AppBundle\Entity\Stock $stock)
    {
        $this->stock_actions->add($stock);
        $stock->setProducto($this);

        return $this;
    }

    /**
     * Remove stock
     *
     * @param \AppBundle\Entity\Stock $stock
     */
    public function removeStockAction(\AppBundle\Entity\Stock $stock)
    {
        $this->stock_actions->removeElement($stock);
    }

    /**
     * Get Stocks
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStockActions()
    {
        $criteria = Criteria::create()->orderBy(array("fecha" => Criteria::DESC));

        return $this->stock_actions->matching($criteria);
    }


    /**
     * @Assert\Callback
     */
    public function validarGeneros(ExecutionContextInterface $context, $payload)
    {
        if (0 == count($this->getGeneros())) {
            $context->buildViolation('Elija por lo menos un género')
                ->atPath('generos')
                ->addViolation();
        }
    }

    /**
     * @Assert\Callback
     */
    public function validarPrecioOferta(ExecutionContextInterface $context, $payload)
    {
        if ( $this->getPrecioOferta() && ($this->getPrecioOferta() >= $this->getPrecio()) ) {
            $context->buildViolation('El precio oferta debe ser menor al precio')
                ->atPath('precio_oferta')
                ->addViolation();
        }
    }

    public function getFirstImage(){
        foreach ($this->getImagenes() as $producto_imagen) {
            if ($producto_imagen->getImagen()) {
                return $producto_imagen;
            }
        }
        return null;
    }

    public function getSecondImage(){
        $count = 1;
        foreach ($this->getImagenes() as $producto_imagen) {
            if ($producto_imagen->getImagen() && $count == 2) {
                return $producto_imagen;
            }
            $count++;
        }
        return null;
    }

    /**
     * Set dimensiones
     *
     * @param string $dimensiones
     *
     * @return Producto
     */
    public function setDimensiones($dimensiones)
    {
        $this->dimensiones = $dimensiones;

        return $this;
    }

    /**
     * Get dimensiones
     *
     * @return string
     */
    public function getDimensiones()
    {
        return $this->dimensiones;
    }

    /**
     * Set orden
     *
     * @param integer $orden
     *
     * @return Producto
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return integer
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Set codigoTemporada
     *
     * @param string $codigoTemporada
     *
     * @return Producto
     */
    public function setCodigoTemporada($codigoTemporada)
    {
        $this->codigo_temporada = $codigoTemporada;

        return $this;
    }

    /**
     * Get codigoTemporada
     *
     * @return string
     */
    public function getCodigoTemporada()
    {
        return $this->codigo_temporada;
    }

    /**
     * Add imagene
     *
     * @param \AppBundle\Entity\ProductoImagen $imagene
     *
     * @return Producto
     */
    public function addImagene(\AppBundle\Entity\ProductoImagen $imagene)
    {
        $this->imagenes[] = $imagene;

        return $this;
    }

    /**
     * Remove imagene
     *
     * @param \AppBundle\Entity\ProductoImagen $imagene
     */
    public function removeImagene(\AppBundle\Entity\ProductoImagen $imagene)
    {
        $this->imagenes->removeElement($imagene);
    }

    /**
     * Set meliId
     *
     * @param string $meliId
     *
     * @return Producto
     */
    public function setMeliId($meliId)
    {
        $this->meliId = $meliId;

        return $this;
    }

    /**
     * Get meliId
     *
     * @return string
     */
    public function getMeliId()
    {
        return $this->meliId;
    }

    /**
     * Set parent
     *
     * @param \AppBundle\Entity\Producto $parent
     *
     * @return Producto
     */
    public function setParent(\AppBundle\Entity\Producto $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \AppBundle\Entity\Producto
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add talle
     *
     * @param \AppBundle\Entity\Producto $talle
     *
     * @return Producto
     */
    public function addTalle(\AppBundle\Entity\Producto $talle)
    {
        $this->talles[] = $talle;

        return $this;
    }

    /**
     * Remove talle
     *
     * @param \AppBundle\Entity\Producto $talle
     */
    public function removeTalle(\AppBundle\Entity\Producto $talle)
    {
        $this->talles->removeElement($talle);
    }

    /**
     * Get talles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTalles()
    {
        return $this->talles;
    }

    /**
     * Set descripcionTalle
     *
     * @param string $descripcionTalle
     *
     * @return Producto
     */
    public function setDescripcionTalle($descripcionTalle)
    {
        $this->descripcionTalle = $descripcionTalle;

        return $this;
    }

    /**
     * Get descripcionTalle
     *
     * @return string
     */
    public function getDescripcionTalle()
    {
        return $this->descripcionTalle;
    }

    /**
     * Set circuloDescuento
     *
     * @param string $circuloDescuento
     *
     * @return Producto
     */
    public function setCirculoDescuento($circuloDescuento)
    {
        $this->circulo_descuento = $circuloDescuento;

        return $this;
    }

    /**
     * Get circuloDescuento
     *
     * @return string
     */
    public function getCirculoDescuento()
    {
        return $this->circulo_descuento;
    }

    public function getCirculoDescuentoClass() 
    {
        $circuloDescuento = $this->getCirculoDescuento();
        return 'circulo-' . $circuloDescuento;
    }

    /**
     * Set googleShopping
     *
     * @param boolean $googleShopping
     *
     * @return Producto
     */
    public function setGoogleShopping($googleShopping)
    {
        $this->google_shopping = $googleShopping;

        return $this;
    }

    /**
     * Get googleShopping
     *
     * @return boolean
     */
    public function getGoogleShopping()
    {
        return $this->google_shopping;
    }

    /**
     * Set altaDemanda
     *
     * @param boolean $altaDemanda
     *
     * @return Producto
     */
    public function setAltaDemanda($altaDemanda)
    {
        $this->alta_demanda = $altaDemanda;

        return $this;
    }

    /**
     * Get altaDemanda
     *
     * @return boolean
     */
    public function getAltaDemanda()
    {
        return $this->alta_demanda;
    }


    /**
     * Set dafitiSku
     *
     * @param string $dafitiSku
     *
     * @return Producto
     */
    public function setDafitiSku($dafitiSku)
    {
        $this->dafitiSku = $dafitiSku;

        return $this;
    }

    /**
     * Get dafitiSku
     *
     * @return string
     */
    public function getDafitiSku()
    {
        return $this->dafitiSku;
    }
}
