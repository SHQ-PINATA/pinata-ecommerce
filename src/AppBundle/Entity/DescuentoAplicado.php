<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DescuentoAplicado
 *
 * @ORM\Table(name="descuento_aplicado")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DescuentoAplicadoRepository")
 */
class DescuentoAplicado
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="carritoId", type="string", length=100, nullable=true, unique=true)
     */
    private $carritoId;

    /**
     * @var string
     *
     * @ORM\OneToOne(targetEntity="Pedido", inversedBy="descuentoAplicado")
     * @ORM\JoinColumn(name="pedido_id", referencedColumnName="id", nullable=true, unique=true)
     */
    private $pedido;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=50)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=5)
     */
    private $tipo;

    /**
     * @var float
     *
     * @ORM\Column(name="monto_descontado", type="float", nullable=true)
     */
    private $montoDescontado;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set carritoId
     *
     * @param string $carritoId
     *
     * @return DescuentoAplicado
     */
    public function setCarritoId($carritoId)
    {
        $this->carritoId = $carritoId;

        return $this;
    }

    /**
     * Get carritoId
     *
     * @return string
     */
    public function getCarritoId()
    {
        return $this->carritoId;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return DescuentoAplicado
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return DescuentoAplicado
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set montoDescontado
     *
     * @param float $montoDescontado
     *
     * @return DescuentoAplicado
     */
    public function setMontoDescontado($montoDescontado)
    {
        $this->montoDescontado = $montoDescontado;

        return $this;
    }

    /**
     * Get montoDescontado
     *
     * @return float
     */
    public function getMontoDescontado()
    {
        return $this->montoDescontado;
    }

    /**
     * Set pedido
     *
     * @param \AppBundle\Entity\Pedido $pedido
     *
     * @return DescuentoAplicado
     */
    public function setPedido(\AppBundle\Entity\Pedido $pedido = null)
    {
        $this->pedido = $pedido;

        return $this;
    }

    /**
     * Get pedido
     *
     * @return \AppBundle\Entity\Pedido
     */
    public function getPedido()
    {
        return $this->pedido;
    }
}
