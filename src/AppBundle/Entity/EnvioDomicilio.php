<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * EnvioDomicilio
 *
 * @ORM\Table(name="envio_domicilio")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EnvioDomicilioRepository")
 */
class EnvioDomicilio extends Envio
{
    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     * @Assert\NotBlank(message = "Ingrese su nombre")
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apellido", type="string", length=255)
     * @Assert\NotBlank(message = "Ingrese su apellido")
     */
    private $apellido;

    /**
     * @var string
     *
     * @ORM\Column(name="calle", type="string", length=255)
     * @Assert\NotBlank(message = "Ingrese la calle")
     */
    private $calle;

    /**
     * @var int
     *
     * @ORM\Column(name="numero", type="integer")
     * @Assert\NotBlank(message = "Ingrese el numero de la dirección")
     */
    private $numero;

    /**
     * @var string
     *
     * @ORM\Column(name="piso", type="integer", nullable=true)
     */
    private $piso;

    /**
     * @var string
     *
     * @ORM\Column(name="depto_nro", type="string", length=10, nullable=true)
     */
    private $deptoNro;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=50, nullable=true)
     * @Assert\NotBlank(message = "Ingrese su numero de telefono")
     */
    private $telefono;

    /**
     * Muchos Envios tienen un Usuario
     * @ORM\ManyToOne(targetEntity="Provincia")
     * @ORM\JoinColumn(name="provincia_id", referencedColumnName="id")
     * @Assert\NotBlank(message = "Seleccione una provincia")
     */
    private $provincia;

    /**
     * @var string
     *
     * @ORM\Column(name="localidad", type="string", length=255)
     * @Assert\NotBlank(message = "Ingrese una localidad")
     */
    private $localidad;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo_postal", type="string", length=8)
     * @Assert\NotBlank(message = "Ingrese su código postal")
     */
    private $codigoPostal;

    /**
     * @var string
     *
     * @ORM\Column(name="correo_id", type="string", length=255, nullable=true)
     */
    private $correo_id;

    /**
     * @var string
     *
     * @ORM\Column(name="correo_nombre", type="string", length=255, nullable=true)
     */
    private $correo_nombre;

    /**
     * @var float
     *
     * @ORM\Column(name="precio", type="float", nullable=true)
     */
    private $precio;
    
    /**
     * @var string
     *
     * @ORM\Column(name="servicio", type="string", length=1)
     */
    private $servicio;

    /**
     * @var string
     *
     * @ORM\Column(name="num_seguimiento", type="string", length=50, nullable=true)
     */
    private $num_seguimiento;


    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return EnvioDomicilio
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellido
     *
     * @param string $apellido
     *
     * @return EnvioDomicilio
     */
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;

        return $this;
    }

    /**
     * Get apellido
     *
     * @return string
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * Set calle
     *
     * @param string $calle
     *
     * @return EnvioDomicilio
     */
    public function setCalle($calle)
    {
        $this->calle = $calle;

        return $this;
    }

    /**
     * Get calle
     *
     * @return string
     */
    public function getCalle()
    {
        return $this->calle;
    }

    /**
     * Set numero
     *
     * @param integer $numero
     *
     * @return EnvioDomicilio
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return int
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set piso
     *
     * @param string $piso
     *
     * @return EnvioDomicilio
     */
    public function setPiso($piso)
    {
        $this->piso = $piso;

        return $this;
    }

    /**
     * Get piso
     *
     * @return string
     */
    public function getPiso()
    {
        return $this->piso;
    }

    /**
     * Set deptoNro
     *
     * @param string $deptoNro
     *
     * @return EnvioDomicilio
     */
    public function setDeptoNro($deptoNro)
    {
        $this->deptoNro = $deptoNro;

        return $this;
    }

    /**
     * Get deptoNro
     *
     * @return string
     */
    public function getDeptoNro()
    {
        return $this->deptoNro;
    }

    /**
     * Set provincia
     *
     * @param integer $provincia
     *
     * @return EnvioDomicilio
     */
    public function setProvincia($provincia)
    {
        $this->provincia = $provincia;

        return $this;
    }

    /**
     * Get provinciaId
     *
     * @return int
     */
    public function getProvincia()
    {
        return $this->provincia;
    }

    /**
     * Set localidad
     *
     * @param string $localidad
     *
     * @return EnvioDomicilio
     */
    public function setLocalidad($localidad)
    {
        $this->localidad = $localidad;

        return $this;
    }

    /**
     * Get localidad
     *
     * @return string
     */
    public function getLocalidad()
    {
        return $this->localidad;
    }

    /**
     * Set codigoPostal
     *
     * @param string $codigoPostal
     *
     * @return EnvioDomicilio
     */
    public function setCodigoPostal($codigoPostal)
    {
        $this->codigoPostal = $codigoPostal;

        return $this;
    }

    /**
     * Get codigoPostal
     *
     * @return string
     */
    public function getCodigoPostal()
    {
        return $this->codigoPostal;
    }

    /**
     * @Assert\Callback
     */
    public function validateServicio(ExecutionContextInterface $context, $payload)
    {
        $servicios = array(EnvioServicio::SERVICIO_ESTANDAR, EnvioServicio::SERVICIO_PRIORITARIO, EnvioServicio::SERVICIO_EXPRESS, EnvioServicio::SERVICIO_DEVOLUCIONES, EnvioServicio::SERVICIO_CAMBIOS);
        
        $servicio = $this->getServicio();
        if (! in_array($servicio, $servicios)) {
            $context->buildViolation('Escoja un servicio de entrega válido')
                        ->atPath('servicio')
                        ->addViolation();
        }
    }

    /**
     * Set correoId
     *
     * @param string $correoId
     *
     * @return EnvioDomicilio
     */
    public function setCorreoId($correoId)
    {
        $this->correo_id = $correoId;

        return $this;
    }

    /**
     * Get correoId
     *
     * @return string
     */
    public function getCorreoId()
    {
        return $this->correo_id;
    }

    /**
     * Set correoNombre
     *
     * @param string $correoNombre
     *
     * @return EnvioDomicilio
     */
    public function setCorreoNombre($correoNombre)
    {
        $this->correo_nombre = $correoNombre;

        return $this;
    }

    /**
     * Get correoNombre
     *
     * @return string
     */
    public function getCorreoNombre()
    {
        return $this->correo_nombre;
    }

    /**
     * Set precio
     *
     * @param float $precio
     *
     * @return EnvioDomicilio
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return float
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set servicio
     *
     * @param string $servicio
     *
     * @return EnvioDomicilio
     */
    public function setServicio($servicio)
    {
        $this->servicio = $servicio;

        return $this;
    }

    /**
     * Get servicio
     *
     * @return string
     */
    public function getServicio()
    {
        return $this->servicio;
    }

    public function getServicioNombre(){
        switch ($this->servicio) {
            case 'N':
                $nombre = 'Estándar';
                break; 
            case 'P':
                $nombre = 'Prioritario';
                break; 
            case 'X':
                $nombre = 'Express';
                break; 
            case 'R':
                $nombre = 'Devolución';
                break; 
            case 'C':
                $nombre = 'Cambio';
                break; 
            default:
                $nombre = '';
                break;
            }

        return $nombre;
    }

    public function getTipo(){
        return ENVIO::DOMICILIO;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     *
     * @return EnvioDomicilio
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set numSeguimiento
     *
     * @param string $numSeguimiento
     *
     * @return EnvioDomicilio
     */
    public function setNumSeguimiento($numSeguimiento)
    {
        $this->num_seguimiento = $numSeguimiento;

        return $this;
    }

    /**
     * Get numSeguimiento
     *
     * @return string
     */
    public function getNumSeguimiento()
    {
        return $this->num_seguimiento;
    }
}
