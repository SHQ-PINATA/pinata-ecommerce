<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Cocur\Slugify\Slugify;
use Doctrine\Common\Collections\Criteria;

/**
 * ProductoCategoria
 * @ORM\Table(name="categoria")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategoriaRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Vich\Uploadable
 * @UniqueEntity("slug", message="Ya existe una categoría con este nombre")
 */
class Categoria
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $nombre;

    /**
     * @ORM\OneToOne(targetEntity="CategoriaImagen", mappedBy="categoria", cascade={"persist"}, orphanRemoval=true)
     */
    protected $imagen;

    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     * @var \DateTime
     */
    private $deleted_at = null;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, unique=true, nullable=true)
     */
    private $slug;
   
    /**
     * @var int
     *
     * @ORM\Column(name="orden", type="integer", options={"default" : 0})
     */
    private $orden = 0;

    /**
     * One Category has Many SubCategories.
     * @ORM\OneToMany(targetEntity="Categoria", mappedBy="parent", cascade={"persist"}, orphanRemoval=true)
     */
    private $children;

    /**
     * Many SubCategories have One Category.
     * @ORM\ManyToOne(targetEntity="Categoria", inversedBy="children", cascade={"persist"})
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     * @Assert\Valid
     */
    private $parent;

    /**
     * Many Features have One Product.
     * @ORM\OneToMany(targetEntity="Producto", mappedBy="categoria")
     */
    private $productos;

    public function __construct() {
        $this->children = new ArrayCollection();
        $this->productos = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Categoria
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    public function __toString(){
        return $this->getNombre();
    }

    /**
     * Set slug
     *
     * @ORM\PrePersist
     *
     * @param string $slug
     *
     * @return Categoria
     */
    public function setSlug($slug = null)
    {
        $slugify = new Slugify();
        $parent = $this->getParent();
        if ($parent) {
            $this->slug = $slugify->slugify($parent->getNombre().' '.$this->nombre);
        }else{
            $this->slug = $slugify->slugify($this->nombre);
        }

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Categoria
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deleted_at = $deletedAt;
        $this->slug = null;

        foreach ($this->children as $sub) {
            $sub->setDeletedAt($deletedAt);
        }

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deleted_at;
    }

    /**
     * Add child
     *
     * @param \AppBundle\Entity\Category $child
     *
     * @return Categoria
     */
    public function addChild(\AppBundle\Entity\Categoria $child)
    {
        $child->setParent($this);
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \AppBundle\Entity\Category $child
     */
    public function removeChild(\AppBundle\Entity\Categoria $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        $criteria = Criteria::create()->orderBy(array("orden" => Criteria::ASC));

        return $this->children->matching($criteria);
    }

    /**
     * Set parent
     *
     * @param \AppBundle\Entity\Category $parent
     *
     * @return Categoria
     */
    public function setParent(\AppBundle\Entity\Categoria $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \AppBundle\Entity\Category
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set imagen
     *
     * @param \AppBundle\Entity\CategoriaImagen $imagen
     *
     * @return Categoria
     */
    public function setImagen(\AppBundle\Entity\CategoriaImagen $imagen = null)
    {
        $this->imagen = $imagen;
        $imagen->setCategoria($this);

        return $this;
    }

    /**
     * Get imagen
     *
     * @return \AppBundle\Entity\CategoriaImagen
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    public function getSubcategoriasIds(){
        $ids = array();
        foreach ($this->getChildren() as $sub) {
            $ids[] = $sub->getId();
        }
        return $ids;
    }

    /**
     * @Assert\Callback
     */
    public function validarSubCategoriasUnicas(ExecutionContextInterface $context, $payload)
    {
        $SubCategories = $this->getChildren();

        $nombres = array();
        foreach ($SubCategories as $sub) {
            $nombres[] = $sub->getNombre();
        }

        for ($i=0; $i < count($nombres); $i++) { 
            for ($j=$i+1; $j < count($nombres); $j++) { 
                if ($nombres[$i] == $nombres[$j]) {
                    $context->buildViolation('Las subcategorias deben ser unicas')
                        ->atPath('children')
                        ->addViolation();
                }
            }
        }
    }


    /**
     * Set orden
     *
     * @param integer $orden
     *
     * @return Categoria
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return integer
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Add producto
     *
     * @param \AppBundle\Entity\Producto $producto
     *
     * @return Categoria
     */
    public function addProducto(\AppBundle\Entity\Producto $producto)
    {
        $this->productos[] = $producto;

        return $this;
    }

    /**
     * Remove producto
     *
     * @param \AppBundle\Entity\Producto $producto
     */
    public function removeProducto(\AppBundle\Entity\Producto $producto)
    {
        $this->productos->removeElement($producto);
    }

    /**
     * Get productos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductos()
    {

        return $this->productos;
    }
}
