<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * EnvioSucursalCorreo
 *
 * @ORM\Table(name="envio_sucursal_correo")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EnvioSucursalCorreoRepository")
 */
class EnvioSucursalCorreo extends Envio
{
    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     * @Assert\NotBlank(message = "Ingrese su nombre")
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apellido", type="string", length=255)
     * @Assert\NotBlank(message = "Ingrese su apellido")
     */
    private $apellido;
    
    /**
     * @var string
     * @ORM\Column(name="telefono", type="string", length=50, nullable=true)
     * @Assert\NotBlank(message = "Ingrese su numero de telefono")
     */
    private $telefono;
    
    /**
     * Muchos Envios tienen un Usuario
     * @ORM\ManyToOne(targetEntity="Provincia")
     * @ORM\JoinColumn(name="provincia_id", referencedColumnName="id")
     * @Assert\NotBlank(message = "Seleccione una provincia")
     */
    private $provincia;

    /**
     * @var string
     *
     * @ORM\Column(name="localidad_id", type="integer", nullable=true)
     * @Assert\NotBlank(message = "Seleccione una localidad")
     */
    private $localidadId;

    /**
     * @var string
     *
     * @ORM\Column(name="correo_id", type="string", length=255, nullable=true)
     */
    private $correo_id;

    /**
     * @var string
     *
     * @ORM\Column(name="correo_nombre", type="string", length=255, nullable=true)
     */
    private $correo_nombre;

    /**
     * @var float
     *
     * @ORM\Column(name="precio", type="float", nullable=true)
     */
    private $precio;

    /**
     * @var string
     *
     * @ORM\Column(name="servicio", type="string", length=1)
     */
    private $servicio;

    /**
     * @var string
     *
     * @ORM\Column(name="sucursal_id", type="string", length=255, nullable=true)
     */
    private $sucursal_id;

    /**
     * @var string
     *
     * @ORM\Column(name="sucursal_info", type="string", length=1024, nullable=true)
     */
    private $sucursalInfo;
    
    /**
     * @var string
     *
     * @ORM\Column(name="num_seguimiento", type="string", length=50, nullable=true)
     */
    private $num_seguimiento;

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return EnvioSucursalCorreo
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellido
     *
     * @param string $apellido
     *
     * @return EnvioSucursalCorreo
     */
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;

        return $this;
    }

    /**
     * Get apellido
     *
     * @return string
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * Set localidadId
     *
     * @param integer $localidadId
     *
     * @return EnvioSucursalCorreo
     */
    public function setLocalidadId($localidadId)
    {
        $this->localidadId = $localidadId;

        return $this;
    }

    /**
     * Get localidadId
     *
     * @return integer
     */
    public function getLocalidadId()
    {
        return $this->localidadId;
    }

    /**
     * Set provincia
     *
     * @param \AppBundle\Entity\Provincia $provincia
     *
     * @return EnvioSucursalCorreo
     */
    public function setProvincia(\AppBundle\Entity\Provincia $provincia = null)
    {
        $this->provincia = $provincia;

        return $this;
    }

    /**
     * Get provincia
     *
     * @return \AppBundle\Entity\Provincia
     */
    public function getProvincia()
    {
        return $this->provincia;
    }


    /**
     * @Assert\Callback
     */
    public function validateServicio(ExecutionContextInterface $context, $payload)
    {
        $servicios = array(EnvioServicio::SERVICIO_ESTANDAR, EnvioServicio::SERVICIO_PRIORITARIO, EnvioServicio::SERVICIO_EXPRESS, EnvioServicio::SERVICIO_DEVOLUCIONES, EnvioServicio::SERVICIO_CAMBIOS);
        
        $servicio = $this->getServicio();
        if (! in_array($servicio, $servicios)) {
            $context->buildViolation('Escoja un servicio de entrega válido')
                        ->atPath('servicio')
                        ->addViolation();
        }
    }

    /**
     * Set correoId
     *
     * @param string $correoId
     *
     * @return EnvioSucursalCorreo
     */
    public function setCorreoId($correoId)
    {
        $this->correo_id = $correoId;

        return $this;
    }

    /**
     * Get correoId
     *
     * @return string
     */
    public function getCorreoId()
    {
        return $this->correo_id;
    }

    /**
     * Set correoNombre
     *
     * @param string $correoNombre
     *
     * @return EnvioSucursalCorreo
     */
    public function setCorreoNombre($correoNombre)
    {
        $this->correo_nombre = $correoNombre;

        return $this;
    }

    /**
     * Get correoNombre
     *
     * @return string
     */
    public function getCorreoNombre()
    {
        return $this->correo_nombre;
    }

    /**
     * Set precio
     *
     * @param float $precio
     *
     * @return EnvioSucursalCorreo
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return float
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set servicio
     *
     * @param string $servicio
     *
     * @return EnvioSucursalCorreo
     */
    public function setServicio($servicio)
    {
        $this->servicio = $servicio;

        return $this;
    }

    /**
     * Get servicio
     *
     * @return string
     */
    public function getServicio()
    {
        return $this->servicio;
    }

    /**
     * Set sucursalId
     *
     * @param string $sucursalId
     *
     * @return EnvioSucursalCorreo
     */
    public function setSucursalId($sucursalId)
    {
        $this->sucursal_id = $sucursalId;

        return $this;
    }

    /**
     * Get sucursalId
     *
     * @return string
     */
    public function getSucursalId()
    {
        return $this->sucursal_id;
    }

    /**
     * Set sucursalInfo
     *
     * @param string $sucursalInfo
     *
     * @return EnvioSucursalCorreo
     */
    public function setSucursalInfo($sucursalInfo)
    {
        $this->sucursalInfo = $sucursalInfo;

        return $this;
    }

    /**
     * Get sucursalInfo
     *
     * @return string
     */
    public function getSucursalInfo()
    {
        return $this->sucursalInfo;
    }

    public function getServicioNombre(){
        switch ($this->servicio) {
            case 'N':
                $nombre = 'Estándar';
                break; 
            case 'P':
                $nombre = 'Prioritario';
                break; 
            case 'X':
                $nombre = 'Express';
                break; 
            case 'R':
                $nombre = 'Devolución';
                break; 
            case 'C':
                $nombre = 'Cambio';
                break; 
            default:
                $nombre = '';
                break;
            }

        return $nombre;
    }

    public function getTipo(){
        return ENVIO::SUCURSARL_CORREO;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     *
     * @return EnvioSucursalCorreo
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set numSeguimiento
     *
     * @param string $numSeguimiento
     *
     * @return EnvioSucursalCorreo
     */
    public function setNumSeguimiento($numSeguimiento)
    {
        $this->num_seguimiento = $numSeguimiento;

        return $this;
    }

    /**
     * Get numSeguimiento
     *
     * @return string
     */
    public function getNumSeguimiento()
    {
        return $this->num_seguimiento;
    }
}
