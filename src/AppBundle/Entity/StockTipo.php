<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * StockTipo
 *
 * @ORM\Table(name="stock_tipo")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StockTipoRepository")
 */
class StockTipo
{
    const CODIGO_CARGA_INICIAL        = 'CARGI';
    const CODIGO_REAJUSTE             = 'REAJU';
    const CODIGO_OTRO                 = 'OTROS';
    const CODIGO_BAJA_PEDIDO          = 'BAJAP';
    const CODIGO_VENTA                = 'VENTA';
    const CODIGO_REACTIVACION_PEDIDO  = 'REACP';
    const CODIGO_DAFITI               = 'DAFIT';
    const CODIGO_MERCADOLIBRE         = 'MERCA';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=5)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var bool
     *
     * @ORM\Column(name="es_de_sistema", type="boolean")
     */
    private $esDeSistema;

    function __toString(){
        return $this->getNombre();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return StockTipo
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set esDeSistema
     *
     * @param boolean $esDeSistema
     *
     * @return StockTipo
     */
    public function setEsDeSistema($esDeSistema)
    {
        $this->esDeSistema = $esDeSistema;

        return $this;
    }

    /**
     * Get esDeSistema
     *
     * @return bool
     */
    public function getEsDeSistema()
    {
        return $this->esDeSistema;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return StockTipo
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }
}
