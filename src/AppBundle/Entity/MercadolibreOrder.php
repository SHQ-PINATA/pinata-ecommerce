<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MercadoPagoOrders
 *
 * @ORM\Table(name="mercadolibre_orders")
 * @ORM\Entity
 */
class MercadolibreOrder
{

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="producto_id", type="integer")
     */
    private $productoId;

    /**
     * @var string
     *
     * @ORM\Column(name="meli_order_id", type="string", length=512)
     */
    private $meliOrderId;

    /**
     * @ORM\Column(name="cantidad", type="integer")
     */
    private $cantidad;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set productoId
     *
     * @param integer $productoId
     *
     * @return MercadolibreOrder
     */
    public function setProductoId($productoId)
    {
        $this->productoId = $productoId;

        return $this;
    }

    /**
     * Get productoId
     *
     * @return integer
     */
    public function getProductoId()
    {
        return $this->productoId;
    }

    /**
     * Set meliOrderId
     *
     * @param string $meliOrderId
     *
     * @return MercadolibreOrder
     */
    public function setMeliOrderId($meliOrderId)
    {
        $this->meliOrderId = $meliOrderId;

        return $this;
    }

    /**
     * Get meliOrderId
     *
     * @return string
     */
    public function getMeliOrderId()
    {
        return $this->meliOrderId;
    }

    /**
     * Set cantidad
     *
     * @param integer $cantidad
     *
     * @return MercadolibreOrder
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return integer
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }
}
