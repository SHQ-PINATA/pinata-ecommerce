<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Entity\StockTipo;
use AppBundle\Entity\Producto;
use AppBundle\Validator\StockValidators;
use AppBundle\Validator\Constraints as CustomAssert;

/**
 * @ORM\Table(name="stock")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StockRepository")
 * @CustomAssert\StockPositivo
 */

class Stock
{
    const TIPO_CARGA_INICIAL        = 'CARGI';
    const TIPO_REAJUSTE             = 'REAJU';
    const TIPO_OTRO                 = 'OTROS';
    const TIPO_BAJA_PEDIDO          = 'BAJAP';
    const TIPO_VENTA                = 'VENTA';
    const TIPO_REACTIVACION_PEDIDO  = 'REACP';
    const TIPO_DAFITI               = 'DAFIT';
    const TIPO_MERCADOLIBRE         = 'MERCA';
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * @var Producto
     *
     * @ORM\ManyToOne(targetEntity="Producto", inversedBy="stock_actions")
     * @ORM\JoinColumn(name="producto_id", referencedColumnName="id")
     */
    private $producto;

    /**
     * @var int
     *
     * @ORM\Column(name="cantidad", type="integer")
     */
    private $cantidad;

    /**
     * @var string
     *
     * @ORM\Column(name="observacion", type="text", length=2048, nullable=true)
     */
    private $observacion;

    /**
     * @var PedidoItem
     *
     * @ORM\ManyToOne(targetEntity="PedidoItem")
     * @ORM\JoinColumn(name="pedido_item_id", referencedColumnName="id", nullable=true)
     */
    private $pedidoItem;

    /**
     * @var StockTipo
     *
     * @ORM\ManyToOne(targetEntity="StockTipo")
     * @ORM\JoinColumn(name="stock_tipo", referencedColumnName="id")
     */
    private $stock_tipo;

    public function __construct() {
        $this->fecha = new \DateTimeImmutable();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return Stock
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set producto
     *
     * @param Producto $producto
     *
     * @return Stock
     */
    public function setProducto(Producto $producto)
    {
        $this->producto = $producto;

        return $this;
    }

    /**
     * Get Producto
     *
     * @return Producto
     */
    public function getProducto()
    {
        return $this->producto;
    }

    /**
     * Set cantidad
     *
     * @param integer $cantidad
     *
     * @return Stock
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return int
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set observacion
     *
     * @param string $observacion
     *
     * @return Stock
     */
    public function setObservacion($observacion)
    {
        $this->observacion = $observacion;

        return $this;
    }

    /**
     * Get observacion
     *
     * @return string
     */
    public function getObservacion()
    {
        return $this->observacion;
    }

    /**
     * Set stockTipo
     *
     * @param integer $stockTipoId
     *
     * @return Stock
     */
    public function setStockTipo(StockTipo $stockTipo)
    {
        $this->stock_tipo = $stockTipo;

        return $this;
    }

    /**
     * Get stockTipo
     *
     * @return StockTipo
     */
    public function getStockTipo()
    {
        return $this->stock_tipo;
    }

    /**
     * Set pedidoItem
     *
     * @param \AppBundle\Entity\PedidoItem $pedidoItem
     *
     * @return Stock
     */
    public function setPedidoItem(\AppBundle\Entity\PedidoItem $pedidoItem = null)
    {
        $this->pedidoItem = $pedidoItem;

        return $this;
    }

    /**
     * Get pedidoItem
     *
     * @return \AppBundle\Entity\PedidoItem
     */
    public function getPedidoItem()
    {
        return $this->pedidoItem;
    }
}
