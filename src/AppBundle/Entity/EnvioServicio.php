<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * EnvioServicio
 *
 * @ORM\Table(name="envio_servicio")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EnvioServicioRepository")
 */
class EnvioServicio
{
    const MODALIDAD_DOMICILIO = 'D'; // para envíos a domicilio 
    const MODALIDAD_SUCURSARL = 'S'; // para envíos a sucursal

    const SERVICIO_ESTANDAR     =  'N'; // para el servicio estándar 
    const SERVICIO_PRIORITARIO  =  'P'; // para el servicio prioritario 
    const SERVICIO_EXPRESS      =  'X'; // para el servicio express 
    const SERVICIO_DEVOLUCIONES =  'R'; // para el servicio de devoluciones 
    const SERVICIO_CAMBIOS      =  'C'; // para el servicio de cambios

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="correo_id", type="string", length=255, nullable=true)
     */
    private $correo_id;

    /**
     * @var string
     *
     * @ORM\Column(name="correo_nombre", type="string", length=255, nullable=true)
     */
    private $correo_nombre;

    /**
     * @var float
     *
     * @ORM\Column(name="precio", type="float", nullable=true)
     */
    private $precio;

    /**
     * @var string
     *
     * @ORM\Column(name="modalidad", type="string", length=1)
     */
    private $modalidad;

    /**
     * @var string
     *
     * @ORM\Column(name="servicio", type="string", length=1)
     */
    private $servicio;

    /**
     * @var string
     *
     * @ORM\Column(name="sucursal_id", type="string", length=255, nullable=true)
     */
    private $sucursal_id;

    /**
     * @var string
     */
    private $horas_entrega;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set precio
     *
     * @param float $precio
     *
     * @return EnvioServicio
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return float
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set tiempoEstimado
     *
     * @param string $tiempoEstimado
     *
     * @return EnvioServicio
     */
    public function setTiempoEstimado($tiempoEstimado)
    {
        $this->tiempoEstimado = $tiempoEstimado;

        return $this;
    }

    /**
     * Get tiempoEstimado
     *
     * @return string
     */
    public function getTiempoEstimado()
    {
        return $this->tiempoEstimado;
    }

    /**
     * Set correoId
     *
     * @param string $correoId
     *
     * @return EnvioServicio
     */
    public function setCorreoId($correoId)
    {
        $this->correo_id = $correoId;

        return $this;
    }

    /**
     * Get correoId
     *
     * @return string
     */
    public function getCorreoId()
    {
        return $this->correo_id;
    }

    /**
     * Set correoNombre
     *
     * @param string $correoNombre
     *
     * @return EnvioServicio
     */
    public function setCorreoNombre($correoNombre)
    {
        $this->correo_nombre = $correoNombre;

        return $this;
    }

    /**
     * Get correoNombre
     *
     * @return string
     */
    public function getCorreoNombre()
    {
        return $this->correo_nombre;
    }

    /**
     * Set modalidad
     *
     * @param string $modalidad
     *
     * @return EnvioServicio
     */
    public function setModalidad($modalidad)
    {
        $this->modalidad = $modalidad;

        return $this;
    }

    /**
     * Get modalidad
     *
     * @return string
     */
    public function getModalidad()
    {
        return $this->modalidad;
    }

    /**
     * Set servicio
     *
     * @param string $servicio
     *
     * @return EnvioServicio
     */
    public function setServicio($servicio)
    {
        $this->servicio = $servicio;

        return $this;
    }

    /**
     * Get servicio
     *
     * @return string
     */
    public function getServicio()
    {
        return $this->servicio;
    }

    /**
     * Set sucursalId
     *
     * @param string $sucursalId
     *
     * @return EnvioServicio
     */
    public function setSucursalId($sucursalId = null)
    {
        $this->sucursal_id = $sucursalId;

        return $this;
    }

    /**
     * Get sucursalId
     *
     * @return string
     */
    public function getSucursalId()
    {
        return $this->sucursal_id;
    }

    /**
     * Set horas_entrega
     *
     * @param string $horas_entrega
     *
     * @return EnvioServicio
     */
    public function setHorasEntrega($horas_entrega = null)
    {
        $this->horas_entrega = $horas_entrega;

        return $this;
    }

    /**
     * Get horas_entrega
     *
     * @return string
     */
    public function getHorasEntrega()
    {
        return $this->horas_entrega;
    }


    public function getServicioNombre(){
        switch ($this->servicio) {
            case 'N':
                $nombre = 'Estándar';
                break; 
            case 'P':
                $nombre = 'Prioritario';
                break; 
            case 'X':
                $nombre = 'Express';
                break; 
            case 'R':
                $nombre = 'Devolución';
                break; 
            case 'C':
                $nombre = 'Cambio';
                break; 
            default:
                $nombre = '';
                break;
            }

        return $nombre;
    }

    public function isValidServicioDomicilio()
    {
        $servicios = [self::SERVICIO_ESTANDAR, self::SERVICIO_PRIORITARIO, self::SERVICIO_EXPRESS];
        return self::MODALIDAD_DOMICILIO == $this->getModalidad() && in_array($this->getServicio(), $servicios);
    }

    public function isValidServicioSucursal()
    {
        $servicios = [self::SERVICIO_ESTANDAR, self::SERVICIO_PRIORITARIO, self::SERVICIO_EXPRESS];
        return self::MODALIDAD_SUCURSARL == $this->getModalidad() 
                && in_array($this->getServicio(), $servicios)
                && $this->getSucursalId();
    }

}
