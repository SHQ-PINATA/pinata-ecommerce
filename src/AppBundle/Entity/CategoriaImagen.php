<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CategoriaImagen
 * @ORM\Table(name="categoria_imagen")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategoriaImagenRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Vich\Uploadable
 */
class CategoriaImagen
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string 
     *
     * @ORM\Column(name="imagen", type="string", length=255, nullable=true)
     */
    private $imagen;

    /**
     * @Vich\UploadableField(mapping="categoria", fileNameProperty="imagen")
     * @Assert\File(maxSize = "2M")
     * @var File
     */
    private $imagen_archivo;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updatedAt = null;

    /**
     * @ORM\OneToOne(targetEntity="Categoria", inversedBy="imagen")
     * @ORM\JoinColumn(name="categoria_id", referencedColumnName="id")
     */

    private $categoria;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get imagen
     *
     * @return string
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * Set imagen
     *
     * @param string $imagen
     *
     * @return CategoriaImagen
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * @return File|null
     */
    public function getImagenArchivo()
    {
        return $this->imagen_archivo;
    }

     /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $imagen
     * @return CategoriaImagen
     */
    public function setImagenArchivo(File $imagen = null)
    {
        $this->imagen_archivo = $imagen;

        if ($imagen) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
        
        return $this;
    }
    
    /**
     * @ORM\PrePersist
     */
    public function updateTime(){
        $this->updatedAt = new \DateTimeImmutable();
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return CategoriaImagen
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set categoria
     *
     * @param \AppBundle\Entity\Categoria $categoria
     *
     * @return CategoriaImagen
     */
    public function setCategoria(\AppBundle\Entity\Categoria $categoria = null)
    {
        $this->categoria = $categoria;
        //$categoria->setImagen($this);

        return $this;
    }

    /**
     * Get categoria
     *
     * @return \AppBundle\Entity\Categoria
     */
    public function getCategoria()
    {
        return $this->categoria;
    }
}
