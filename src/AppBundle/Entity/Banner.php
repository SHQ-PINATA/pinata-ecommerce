<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Banner
 *
 * @ORM\Table(name="banner")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BannerRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Vich\Uploadable
 * @Assert\GroupSequence({"Banner", "Strict"})
 */

class Banner
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="orden", type="smallint")
     */
    private $orden = 0;

    /**
     * @var string (link)
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @var string 
     *
     * @ORM\Column(name="imagen", type="string", length=255, nullable=true)
     */
    private $imagen;

    /**
     * @Vich\UploadableField(mapping="banner", fileNameProperty="imagen")
     * @Assert\File(maxSize = "2M")
     * @var File
     */
    private $imagen_archivo;

    /**
     * @var string 
     *
     * @ORM\Column(name="video", type="string", length=255, nullable=true)
     */
    private $video;

    /**
     * @Vich\UploadableField(mapping="banner", fileNameProperty="video")
     * @Assert\File(maxSize = "2M")
     * @var File
     */
    private $video_archivo;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * Muchas imagenes/video tiene un bloque.
     * @ORM\ManyToOne(targetEntity="BloqueBanner", inversedBy="banners")
     * @ORM\JoinColumn(name="bloque_id", referencedColumnName="id")
     */
    private $bloque;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set orden
     *
     * @param integer $orden
     *
     * @return BannerMultimedia
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return int
     */
    public function getOrden()
    {
        return $this->orden;
    }


    /**
     * Set url
     *
     * @param string $url
     *
     * @return BannerMultimedia
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set imagen
     *
     * @param string $imagen
     *
     * @return Banner
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * Get imagen
     *
     * @return string
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * Set video
     *
     * @param string $video
     *
     * @return Banner
     */
    public function setVideo($video)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get video
     *
     * @return string
     */
    public function getVideo()
    {
        return $this->video;
    }

     /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $imagen
     * @return Banner
     */
    public function setImagenArchivo(File $imagen = null)
    {
        $this->imagen_archivo = $imagen;

        if ($imagen) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
        
        return $this;
    }

    /**
     * @return File|null
     */
    public function getImagenArchivo()
    {
        return $this->imagen_archivo;
    }

     /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $video
     * @return Banner
     */
    public function setVideoArchivo(File $video = null)
    {
        $this->video_archivo = $video;

        if ($video) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
        
        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function updateTime(){
        $this->updatedAt = new \DateTimeImmutable();
    }


    /**
     * @return File|null
     */
    public function getVideoArchivo()
    {
        return $this->video_archivo;
    }


    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Banner
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set bloque
     *
     * @param \AppBundle\Entity\BloqueBanner $bloque
     *
     * @return Banner
     */
    public function setBloque(\AppBundle\Entity\BloqueBanner $bloque = null)
    {
        $this->bloque = $bloque;

        return $this;
    }

    /**
     * Get bloque
     *
     * @return \AppBundle\Entity\BloqueBanner
     */
    public function getBloque()
    {
        return $this->bloque;
    }

    public function hasImageOrVideoSet(){
        return $this->getImagen() || $this->getImagenArchivo() || $this->getVideo() || $this->getVideoArchivo();
    }

   
    /**
     * @Assert\Callback
     */
    public function validarImagenVideo(ExecutionContextInterface $context, $payload){
        if(!$this->getId() && !$this->getImagen() && !$this->getImagenArchivo() && !$this->getVideo() && !$this->getVideoArchivo()){
            $context->buildViolation('Se debe subir al menos una imagen o video.')
                ->atPath('imagen_archivo')
                ->addViolation();
            $context->buildViolation('')
                ->atPath('video_archivo')
                ->addViolation();
        }
    }

    /**
     * @Assert\Callback(groups={"Strict"})
     */
    public function validarMIMETypes(ExecutionContextInterface $context, $payload)
    {   
        if ($this->imagen_archivo) {
           if (! in_array($this->imagen_archivo->getMimeType(), array(
                   'image/jpeg',
                   'image/gif',
                   'image/png'
            ))) {
            $context->buildViolation('Error de tipo de imagen. Se admiten jpg, gif y png.')
                ->atPath('imagen_archivo')
                ->addViolation();
           }
        }
        if ($this->video_archivo) {
           if (! in_array($this->video_archivo->getMimeType(), array(
                   'video/avi',
                   'video/quicktime',
                   'video/mpeg',
                   'video/x-mpeg',
                   'video/mp4',
            ))) {
            $context->buildViolation('Error de tipo de video. Se admiten avi, mov y mp4.')
                ->atPath('video_archivo')
                ->addViolation();
           }
        }
           
    }
}
