<?php
namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class StockPositivo extends Constraint
{
    public $message = 'El stock total no puede quedar en un valor negativo. El valor mínimo que puede insertar es {{ permitido }}';

    public function getTargets()
	{
	    return self::CLASS_CONSTRAINT;
	}
}