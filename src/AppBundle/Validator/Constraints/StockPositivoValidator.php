<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;
use Doctrine\ORM\EntityManager;

/**
 * @Annotation
 */

// Valido que el stock total no me vaya a quedar negativo y que no me inserten 0 como valor de modificacion

class StockPositivoValidator extends ConstraintValidator
{
	private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function validate($stock, Constraint $constraint)
    {
        $total = $this->em->getRepository('AppBundle:Stock')->getStockSumByProduct($stock->getProducto());

        if ($stock->getCantidad() == 0) {
            $this->context->buildViolation('El stock a insertar debe ser distinto de 0')
                ->atPath('cantidad')
                ->addViolation();
        }        

        if ($total + $stock->getCantidad() < 0) {
            $permitido = ($total == 0 ) ? 1 : (-1 * $total);
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ permitido }}', $permitido)
                ->atPath('cantidad')
                ->addViolation();
        }
    }

    public function getTargets()
	{
	    return self::CLASS_CONSTRAINT;
	}
}