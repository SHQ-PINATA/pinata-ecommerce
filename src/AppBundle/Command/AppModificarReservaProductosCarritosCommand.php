<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class AppModificarReservaProductosCarritosCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:modificar-reserva-productos-carritos')
            ->setDescription('Borra los items de los carritos inactivos');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /*      
        $output->writeln('Modificando reserva de items de carritos.');
        $output->writeln($rows . ' items borrados.');
        */
    }

}
