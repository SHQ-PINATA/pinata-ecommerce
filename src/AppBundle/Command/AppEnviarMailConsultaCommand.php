<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class AppEnviarMailConsultaCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('app:consultas-dequeue-mailer')
             ->setDescription('Envia las consultas no procesadas en la base por mail');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $repo = $this->getContainer()->get('consulta_repository');
        $consultas = $repo->getConsultasNotSent();
        
        if (count($consultas) == 0) {
            return;
        }

        $mailer = $this->getContainer()->get('mailer');
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        foreach ($consultas as $consulta) {
            $body  = "Nombre: ".$consulta->getNombre()." \n";
            $body .= "Mail: ".$consulta->getEmail()." \n";
            $body .= "Fecha: ".$consulta->getCreatedAt()->format('d/m/Y H:i')." \n";
            $body .= "Consulta: ".$consulta->getConsulta();

            $mail = new \Swift_Message('Consulta Piñata #'.$consulta->getId());
            $mail->setTo($consulta->getSendTo());
            $mail->setBody($body);

            $sent = $mailer->send($mail);

            if ($sent) {
                $output->writeln('Consulta #'.$consulta->getId().' enviada');
                $consulta->setSentAt(new \Datetime);
                $em->persist($consulta);
            }

        }
        
        $em->flush();
    }

}
