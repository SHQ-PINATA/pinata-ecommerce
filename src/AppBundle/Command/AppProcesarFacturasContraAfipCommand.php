<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class AppProcesarFacturasContraAfipCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('app:afip-procesar-facturas')
             ->setDescription('Procesa las facturas contra el WS de Afip');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $factura_manager = $this->getContainer()->get('factura_manager');
        $logger = $this->getContainer()->get('logger');
        
        $result = $factura_manager->facturar();
        $facturados = $result['pedidos_facturados'];
        $errores = $result['errores'];

        $output->writeln('Se procesaron '.count($facturados).' facturas.');
        if (count($errores) ){
            $logger->alert('[Afip Error]', $errores);

            $em = $this->getContainer()->get('doctrine.orm.entity_manager');
            $mailer = $this->getContainer()->get('mailer');
            $sendTo = $this->getContainer()->getParameter('consultas.mail');
            
            $body = '';
            foreach ($errores as $error) {
                $factura = $error['factura'];
                // Si ya le mande mail por esta factura, no le vuelvo a mandar.
                if ($factura->getMailErrorEnviado()) {
                    continue;
                }

                $body .= $error['msg'] . "\n";
                $body .= $error['error_msg'] . "\n\n\n";
                $factura->setMailErrorEnviado(new \Datetime());
            }
            $em->flush();

            if ($body) {
                $mail = new \Swift_Message('Error del proceso de facturación');
                $mail->setTo($sendTo);
                $mail->setBody($body);

                $sent = $mailer->send($mail);

                // Trato de enviarlo 5 veces si no se envío porque 
                // el server a veces hace tiemout. Esto deberia hacerse con una cola.
                for ($i=0; $i < 5 && $sent==0; $i++) { 
                    $sent = $mailer->send($mail);
                }
                if ($sent) {
                    $output->writeln('Se envío el mail para chequeo de error de procesamiento');
                    $em->persist($factura);
                }else{
                    $output->writeln('Falló el envío de mail para el chequeo de error de procesamiento');
                    $logger->alert('[Afip Mail Error]', 'Falló el envio de mails.');
                }
            }

        }
    }

}
