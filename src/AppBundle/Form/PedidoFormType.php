<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class PedidoFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
               
        $builder->add('dni')
                ->add('fechaPago', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'required' => false,
                'label' => 'Fecha de Pago',
                'format' => 'dd/MM/yyyy HH:mm',
                'attr' => ['class' => 'js-datepicker'],
                    ])
                ->add('fechaEnvio', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'required' => false,
                'label' => 'Fecha de Envio',
                'format' => 'dd/MM/yyyy HH:mm',
                'attr' => ['class' => 'js-datepicker'],
                    ])
                ->add('fechaFacturacion', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'required' => false,
                'label' => 'Fecha de Facturacion',
                'format' => 'dd/MM/yyyy HH:mm',
                'attr' => ['class' => 'js-datepicker'],
                    ]); 
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Pedido'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_pedido';
    }


}
