<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use AppBundle\Form\ProductoOrdenFormType;

class ProductosOrdenFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('productos', CollectionType::class, array(
            'entry_type'   => ProductoOrdenFormType::class,
            'entry_options'  => array(
                'attr'      => array('class' => 'item')
            ),
        ));
    }
}
