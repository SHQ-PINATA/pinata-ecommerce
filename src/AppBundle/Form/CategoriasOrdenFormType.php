<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use AppBundle\Form\CategoriaOrdenFormType;

class CategoriasOrdenFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('categorias', CollectionType::class, array(
            // each entry in the array will be an "email" field
            'entry_type'   => CategoriaOrdenFormType::class,
            // these options are passed to each "email" type
            'entry_options'  => array(
                'attr'      => array('class' => 'item')
            ),
        ));
    }
}