<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use AppBundle\Entity\BloqueBanner;
//use AppBundle\Entity\Banner;

class BloqueBannerFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nombre')
        ->add('tipo', ChoiceType::class, [
            'choices' => BloqueBanner::getTipos(),
            'required' => true,
            'multiple' => false,
            'label' => 'Tipo de Banner',
            'placeholder' => 'Elija el tipo de bloque'])
        ->add('fechaDesde', DateType::class, [
                'widget' => 'single_text',
                'attr' => ['class' => 'js-datepicker'],
                'html5' => false,
                'format' => 'dd/MM/yyyy hh:mm',
            ])
        ->add('fechaHasta', DateType::class, [
                'widget' => 'single_text',
                'attr' => ['class' => 'js-datepicker'],
                'html5' => false,
                'format' => 'dd/MM/yyyy hh:mm',
            ])
        ->add('activo',null, ['label' => 'Activo?'])
        ->add('orden', HiddenType::class)
        ->add('banners', CollectionType::class, array(
            'entry_type'   => BannerFormType::class,
            'entry_options'  => array('attr' => array('class' => 'form-control')),
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference' => false,
            'error_bubbling' => true,
            'delete_empty' => true,
            'prototype' => false, // no renderear el form prototipo para el js.
            ))
        
            ->add('ver_mobile', CheckboxType::class, array(
                'label'    => 'Es para mobile?',
                'required' => false,
            ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\BloqueBanner',
            'cascade_validation' => true,
        ));
    }
}
