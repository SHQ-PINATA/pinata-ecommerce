<?php

namespace AppBundle\Form\Frontend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;

class EnvioSucursalCorreoFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('nombre', null, [
            'label' => 'Nombre',
            ])
        ->add('apellido', null, [
            'label' => 'Apellido',
            ])
		->add('provincia', EntityType::class, [
			'class' => 'AppBundle:Provincia',
			'choice_label' => 'nombre',
			'query_builder' => function (EntityRepository $er) {
                                    return $er->createQueryBuilder('p')
                                              ->orderBy('p.nombre', 'ASC');
                                },
            'placeholder' => 'Provincia',
            'choice_value' => 'codigo',
			])
         ->add('telefono', null, [
            'label' => 'Teléfono',
            ])
        ->add('servicio', HiddenType::class, [])
        ->add('sucursal_id', HiddenType::class, []);

        // Agrego las opciones de las localidades dependiendo del valor elegido de provincia
        // Esta funcion agrega el campo dependiendo del valor que tome la Provincia
        // https://symfony.com/doc/current/form/dynamic_form_modification.html#form-events-submitted-data
        $formModifier = function (FormInterface $form, $provincia = null, $ep) {
            $localidades = null === $provincia ? array() : $ep->getLocalidadesOptionsByProvincia($provincia->getCodigo());

            $form->add('localidadId', ChoiceType::class, array(
                'choices'     => $localidades,
                'label' => 'Localidad',
                'placeholder' => 'Localidad',
                'mapped' => true,
                'error_bubbling' => false,
            ));
        };

        // Cuando Se triggerea el evento de PRE_SET_DATA, tomo la provincia (si tiene) del envio al que mapea el form
        // Esto hace que ya esten cargadas las opciones de localidades cuando tiene seteado la provincia
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formModifier, $options) {
                $envio = $event->getData();
                $provincia = $envio ? $envio->getProvincia() : null;
                $formModifier($event->getForm(), $provincia, $options['enviopack']);
            }
        );

        // Cuando Se triggerea el evento de POST_SUBMIT, tomo la provincia a travéz del campo provincia
        // y se lo paso al formModifier. Así cuando mando la data, puedo validar que la localidad sea
        // de esa provincia (porque las opciones se cargan por ajax y pueden no ser las misma que las cargadas en el PRE_SET_DATA)
        $builder->get('provincia')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifier, $options) {
                $provincia = $event->getForm()->getData();
                $formModifier($event->getForm()->getParent(), $provincia, $options['enviopack']);
            }
        );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\EnvioSucursalCorreo',
            'error_mapping' => array(
                'validateServicio' => 'servicio',
            ),
        ));

        $resolver->setRequired('enviopack');

    }
}