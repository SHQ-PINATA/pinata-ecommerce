<?php

namespace AppBundle\Form\Frontend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Validator\Constraints\NotBlank;

class PaymentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('payment', ChoiceType::class, [
            'choices' => array(
                'Mercadopago' => 'Mercadopago',
                ),
            'data' => 'Mercadopago',
            'label' => false,
            'multiple' => false,
            'expanded' => true,
            'constraints' => new NotBlank(),
            ])
        ->add('clarin365', HiddenType::class, [])
        ->add('DNI', TextType::class, [
            'label' => 'Para pedidos mayores a $1000 ingresá tu DNI',
            'constraints' => $options['monto'] >= 1000 ? new NotBlank(array('message' => 'Ingrese su DNI')) : null,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'monto' => null,
        ));
    }
}
