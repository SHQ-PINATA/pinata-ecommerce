<?php

namespace AppBundle\Form\Frontend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
//use AppBundle\Form\Frontend\EnvioServicioFormType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Doctrine\ORM\EntityRepository;

class EnvioDomicilioFormType extends AbstractType
{
/*    public function __construct ($servicios)
    {
        $this->servicios = $servicios;
    }*/
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('nombre', null, [
            'attr' => ['autofocus' => true],
            'label' => 'Nombre',
            ])
        ->add('apellido', null, [
            'label' => 'Apellido',
            ])
        ->add('calle', null, [
            'label' => 'Calle',
            ])
        ->add('numero', IntegerType::class, [
            'label' => 'Numero',
            ])
        ->add('piso', null, [
            'label' => 'Piso',
            ])
		->add('deptoNro', null, [
			'label' => 'Depto N°'
			])
        ->add('telefono', null, [
            'label' => 'Teléfono',
            ])
		->add('provincia', EntityType::class, [
			'class' => 'AppBundle:Provincia',
			'choice_label' => 'nombre',
            'error_bubbling' => false,
			'placeholder' => 'Provincia',
            'choice_value' => 'codigo',
            'query_builder' => function (EntityRepository $er) {
                                    return $er->createQueryBuilder('p')
                                              ->orderBy('p.nombre', 'ASC');
                                },
			])
		->add('localidad', null, [
			'label' => 'Localidad'
			])
		->add('codigoPostal', null, [
			'label' => 'Código Postal'
			])

        ->add('servicio', HiddenType::class, []);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\EnvioDomicilio',
        ));
    }
}