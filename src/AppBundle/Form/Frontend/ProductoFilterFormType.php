<?php

namespace AppBundle\Form\Frontend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

class ProductoFilterFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->addGenero($builder, $options);
        $this->addPersonaje($builder, $options);
        $this->addCategoria($builder, $options);
        $this->addEdades($builder, $options);
        $this->addModelos($builder, $options);
    }

    protected function addPersonaje($builder, $options){
        $builder
        ->add('personaje', EntityType::class, [
            'class' => 'AppBundle:Personaje',
            'multiple' => true,
            'expanded' => true,
            'choice_label' => 'nombre',
            'required' => false,
            'query_builder' => function (EntityRepository $er) use ($options) {
                                    $q = $er->createQueryBuilder('p')
                                              ->innerJoin('AppBundle\Entity\Producto', 'pr', Join::WITH, 'p = pr.personaje')
                                              ->andWhere('p.activo = 1')
                                              ->orderBy('p.nombre', 'ASC');
                                    $q = $this->filterProductoAvailable($q, $options);

                                    return $q;
                                },
            ]);
    }

    protected function addCategoria($builder, $options){
        $builder->add('categoria', EntityType::class, [
            'class' => 'AppBundle:Categoria',
            'multiple' => true,
            'expanded' => true,
            'choice_label' => 'nombre',
            'required' => false,
            'query_builder' => function (EntityRepository $er) use ($options) {
                                    $q = $er->createQueryBuilder('c')
                                            ->andWhere('c.deleted_at IS NULL')
                                              ->innerJoin('AppBundle\Entity\Producto', 'pr', Join::WITH, 'c = pr.categoria OR pr.categoria in (SELECT s.id from AppBundle\Entity\Categoria s where s.parent = c)')
                                              ->orderBy('c.nombre', 'ASC');
                                    $q = $this->filterProductoAvailable($q, $options);

                                    return $q;
                                },
            'group_by' => function($val, $key, $index) {
                   if ($val->getParent()) {
                       return $val->getParent()->getNombre();
                   }
                   return $val->getNombre();
                }
            ]);
    }

    protected function addEdades($builder, $options){
        $builder->add('edades', EntityType::class, [
            'class' => 'AppBundle:Edades',
            'multiple' => true,
            'expanded' => true,
            'choice_label' => 'edades',
            'required' => false,
            'query_builder' => function (EntityRepository $er) use ($options) {
                                    $q = $er->createQueryBuilder('e')
                                              ->innerJoin('AppBundle\Entity\Producto', 'pr', Join::WITH, 'e = pr.edades')
                                              ->orderBy('e.edades', 'ASC');
                                    $q = $this->filterProductoAvailable($q, $options);

                                    return $q;
                                },
            ]);
    }

    protected function addGenero($builder, $options){
        $builder->add('genero', EntityType::class, [
            'class' => 'AppBundle:Genero',
            'multiple' => true,'multiple' => true,
            'choice_label' => 'genero',
            'required' => false,
            'multiple' => true,
            'expanded' => true,
            'query_builder' => function (EntityRepository $er) {
                                    return $er->createQueryBuilder('g')
                                              ->orderBy('g.genero', 'ASC');
                                },
            ]);
    }

    protected function addModelos($builder, $options){
        $builder->add('modelos', EntityType::class, [
            'class' => 'AppBundle:Modelo',
            'multiple' => true,
            'choice_label' => 'nombre',
            'expanded' => true,
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('m')
                    ->andWhere('m.deleted_at is NULL')
                    ->orderBy('m.nombre', 'ASC');
            },
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'productos_ids' => null,
        ));
    }

    private function filterProductoAvailable($query, $options){
        $productos_ids = $options['productos_ids'];
        if (count($productos_ids) == 0) {
            return $query->andWhere('1 = 0');    
        }

        return $query->andWhere('pr.id IN (:ids)')
                     ->setParameter('ids', $productos_ids);
    }
}
