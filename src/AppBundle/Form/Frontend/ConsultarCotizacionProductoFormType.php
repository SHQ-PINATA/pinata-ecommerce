<?php

namespace AppBundle\Form\Frontend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Constraints\NotBlank;

class ConsultarCotizacionProductoFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('provincia', EntityType::class, [
                      'class' => 'AppBundle:Provincia',
                      'choice_label' => 'nombre',
                       'placeholder' => 'Provincia',
                       'constraints' => array(new NotBlank()),
                       'query_builder' => function (EntityRepository $er) {
                                                return $er->createQueryBuilder('p')
                                                          ->orderBy('p.nombre', 'ASC');
                                            },
                        ])
                ->add('codigoPostal', null, ['label' => 'Código Postal']);
    }
}