<?php

namespace AppBundle\Form\Frontend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

abstract class LoginFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username', EmailType::class, [
                    'attr' => ['autofocus' => true],
                    'label' => 'E-Mail',
                    'trim' => true,
                    ])
                ->add('password', PasswordType::class, [
                    'label' => 'Apellido',
                    'trim' => true,
                    ]);
    }
}
