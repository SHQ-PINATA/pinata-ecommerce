<?php

namespace AppBundle\Form\Frontend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Validator\Constraints\Range;

class AgregarProductoCarritoFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
            $builder->add('producto_id', HiddenType::class, array())
                    ->add('cantidad', IntegerType::class, array(
                            'attr' => array('min' => 1, 'max' => $options['max']),
                            'constraints' => array(new Range(array(
                                'min' => 1, 
                                'max' => $options['max'],
                                'minMessage' => "Se requiere agregar al menos un producto",
                                'maxMessage' => "No se puede agregar la cantidad solicitada",
                                ))),
                            ))
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'max' => null,
        ));
    }
}