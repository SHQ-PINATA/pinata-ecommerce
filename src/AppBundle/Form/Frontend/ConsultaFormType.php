<?php

namespace AppBundle\Form\Frontend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ConsultaFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('nombre', TextType::class, ['label' => 'Nombre'])
        ->add('email', EmailType::class, ['label' => 'Mail'])
        ->add('sendTo', ChoiceType::class, array(
            'label'     => 'Motivo de consulta',
            'choices'   => array(
                'Estado de pedido / Envíos' => 'logistica.ecommerce@picapau.com',
                'Ventas al por mayor'       => 'ventaspormayor@picapau.com',
                'Otras consultas'           => 'tiendaoficial@pinataweb.com',
            ),
        ))
        ->add('consulta', TextareaType::class, ['label' => 'Consulta']);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Consulta',
        ));
    }
}
