<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use AppBundle\Entity\Envio;

class PedidoFormFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('idPedido', NumberType::class, ['label' => 'Id del pedido'])
            ->add('DNI', TextType::class, ['label' => 'DNI del comprador'])
            ->add('nombre', TextType::class, ['label' => 'Nombre del comprador'])
            ->add('apellido', TextType::class, ['label' => 'Apellido del comprador'])
            ->add('fechaCreacionDesde', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'required' => false,
                'label' => false,
                'format' => 'dd/MM/yyyy',
                'attr' => ['class' => 'js-datepicker'],
                ])
            ->add('fechaCreacionHasta', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'required' => false,
                'label' => false,
                'format' => 'dd/MM/yyyy',
                'attr' => ['class' => 'js-datepicker'],
                ])
            ->add('fechaPagoDesde', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'required' => false,
                'label' => false,
                'format' => 'dd/MM/yyyy',
                'attr' => ['class' => 'js-datepicker'],
                ])
            ->add('fechaPagoHasta', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'required' => false,
                'label' => false,
                'format' => 'dd/MM/yyyy',
                'attr' => ['class' => 'js-datepicker'],
                ])
            ->add('fechaEnvioDesde', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'required' => false,
                'label' => false,
                'format' => 'dd/MM/yyyy',
                'attr' => ['class' => 'js-datepicker'],
                ])
            ->add('fechaEnvioHasta', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'required' => false,
                'label' => false,
                'format' => 'dd/MM/yyyy',
                'attr' => ['class' => 'js-datepicker'],
                ])
            ->add('fechaFacturacionDesde', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'required' => false,
                'label' => false,
                'format' => 'dd/MM/yyyy',
                'attr' => ['class' => 'js-datepicker'],
                ])
            ->add('fechaFacturacionHasta', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'required' => false,
                'label' => false,
                'format' => 'dd/MM/yyyy',
                'attr' => ['class' => 'js-datepicker'],
                ])
            ->add('baja', CheckboxType::class, [
                'required' => false,
                'label' => 'Cancelado',
                ])
            ->add('envio', ChoiceType::class, [
                'choices' => array(
                    'Envio a Domicilio' => Envio::DOMICILIO,
                    'Envio a Sucursal' => Envio::SUCURSARL_CORREO,
                    'Pickup' => Envio::PICKUP,
                    ),
                'label' => 'Tipos de Envío',
                'choice_label' => function ($value, $key, $index) {
                                        return $key;
                                    },
                'choice_value' => function ($value) {
                                        return $value;
                                    },
                'multiple' => true,
                'expanded' => true,
                'data' => array(Envio::DOMICILIO, Envio::SUCURSARL_CORREO, Envio::PICKUP)
                ]);
            ;
    }
}
