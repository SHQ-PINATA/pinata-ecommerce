<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Doctrine\ORM\EntityRepository;

class ProductoFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('nombre', null, [
            'attr' => ['autofocus' => true],
            'label' => 'Nombre del producto',
            ])
        ->add('codigo_ean', null, [
            'label' => 'Código EAN',
            ])
        ->add('descripcion', TextareaType::class, [
            'attr' => ['rows' => "4"],
            'label' => 'Descripción',
            ])
        ->add('es_combo', null, [
            'label' => 'Es combo?',
            ])
        ->add('precio', null, [
            'label' => 'Precio',
            ])
        ->add('precio_oferta', null, [
            'label' => 'Precio Oferta',
            ])
        ->add('peso', null, [
            'label' => 'Peso',
            ])
        ->add('dimensiones', null, [
            'label' => 'Dimensiones',
            ])
        ->add('sale', null, [
            'label' => 'Sale?',
            ])
        ->add('novedad', null, [
            'label' => 'Es novedad?',
            ])
        ->add('destacado', null, [
            'label' => 'Destacado?',
            ])
        ->add('activo', null, [
            'label' => 'Activo?',
            ])
        ->add('google_shopping', null, [
            'label' => 'Google shopping',
            ])
        ->add('alta_demanda', null, [
            'label' => 'Alta demanda',
            ])
        ->add('discontinuado', null, [
            'label' => 'Discontinuado?',
            ])
        ->add('personaje', EntityType::class, [
            'class' => 'AppBundle:Personaje',
            'choice_label' => 'nombre',
            'query_builder' => function (EntityRepository $er) {
                                    return $er->createQueryBuilder('p')
                                              ->andWhere('p.deleted_at IS NULL')
                                              ->orderBy('p.nombre', 'ASC');
                                },
            ])
        ->add('categoria', EntityType::class, [
            'class' => 'AppBundle:Categoria',
            'choice_label' => 'nombre',
            'required' => true,
            'query_builder' => function (EntityRepository $er) {
                                    return $er->createQueryBuilder('c')
                                              ->andWhere('c.deleted_at IS NULL')
                                              ->orderBy('c.nombre', 'ASC');
                                },
            'group_by' => function($val, $key, $index) {
                           if ($val->getParent()) {
                               return $val->getParent()->getNombre();
                           }
                           return null;
                        }
            ])
        ->add('edades', EntityType::class, [
            'class' => 'AppBundle:Edades',
            'choice_label' => 'edades',
            ])
        ->add('generos', EntityType::class, [
            'class' => 'AppBundle:Genero',
            'choice_label' => 'genero',
            'multiple' => true,
            'expanded' => true,
            'required' => true,
            ])
        ->add('modelo', EntityType::class, [
            'class' => 'AppBundle:Modelo',
            'choice_label' => 'nombre',
            'placeholder' => 'Si el producto es pijama, elija un modelo',
            'query_builder' => function (EntityRepository $er) {
                                    return $er->createQueryBuilder('m')
                                              ->andWhere('m.deleted_at IS NULL')
                                              ->orderBy('m.nombre', 'ASC');
                                },
            ])
        ->add('codigo_temporada', null, [
            'label' => 'Código de temporada',
            ])
        ->add('meli_id', null, [
            'label' => 'Id en MercadoLibre',
            ])
        ->add('dafiti_sku', null, [
            'label' => 'SKU en Dafiti',
            ])
        ->add('parent_id', HiddenType::class, [
            'label' => 'Id del producto padre',
            'mapped' => false,
            ])
         ->add('imagenes', CollectionType::class, array(
            'entry_type'   => ProductoImagenFormType::class,
            'label' => false,
            'allow_add' => true,
            'allow_delete' => false,
            'by_reference' => false,
            'error_bubbling' => true,
            'delete_empty' => true,
            'prototype' => true,
        ))
        ->add('descripcionTalle', null, [
            'label' => 'Texto a mostrar en select de talle',
        ])
        ->add('circulo_descuento', ChoiceType::class, array(
            'choices'  => array(
                'Sin círculo' => null,
                '10%' => 10,
                '15%' => 15,
                '20%' => 20,
                '25%' => 25,
                '30%' => 30,
                '40%' => 40,
                '50%' => 50,
                '60%' => 60,
                '70%' => 70,
                'Comodín' => 'comodin',
            ),
            'choices_as_values' => true,
             'choice_value' => function ($choice) {
                 return $choice;
             },
        )); 
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Producto',
        ));
    }
}
