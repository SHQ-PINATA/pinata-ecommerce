<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UsuarioFormFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('username', null, [
            'attr' => ['autofocus' => true],
            'label' => 'Nombre de usuario',
            ])
        ->add('email', null, [
            'label' => 'E-Mail',
            ])
        ->add('roles', ChoiceType::class, [
            'choices' => array(
                'Todos' => '',
                'Super-Admin' => 'ROLE_SUPER_ADMIN',
                'Admin' => 'ROLE_ADMIN',
                'Usuario' => 'ROLE_USER',
                ),
            'label' => 'Roles',
            'choice_label' => function ($value, $key, $index) {
                                    return $key;
                                },
            'choice_value' => function ($value) {
                                    return $value;
                                },
            'multiple' => true,
            ]);
    }
}
