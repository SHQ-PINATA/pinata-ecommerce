<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class PedidosBulkFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('pedidos', EntityType::class, [
            'class' => 'AppBundle:Pedido',
            'choice_label' => false,
            'label' => false,
            'multiple' => true,
            'expanded' => true,
            ])
        ->add('facturar', SubmitType::class, [
            'label' => 'Crear Facturas', 
            'attr' => ['class' => 'btn btn-default'],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'pedidos' => array()
        ));
    }
}
