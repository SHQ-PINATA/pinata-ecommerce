<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ProductoFormFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('nombre', TextType::class, [
            'attr' => ['autofocus' => true],
            'label' => 'Nombre del producto',
            'required' => false,
            ])
        ->add('codigo_ean', TextType::class, [
            'label' => 'Código EAN',
            'required' => false,
            ])
        ->add('codigo_sku', TextType::class, [
            'label' => 'Código SKU',
            'required' => false,
            ])
        ->add('es_combo', CheckboxType::class, [
            'label' => 'Es combo?',
            'required' => false,
            ])
        ->add('precio_desde', TextType::class, [
            'label' => false,
            'required' => false,
            ])
        ->add('precio_hasta', TextType::class, [
            'label' => false,
            'required' => false,
            ])
        ->add('sale', CheckboxType::class, [
            'label' => 'Sale?',
            'required' => false,
            ])
        ->add('novedad', CheckboxType::class, [
            'label' => 'Es novedad?',
            'required' => false,
            ])
        ->add('personaje', EntityType::class, [
            'class' => 'AppBundle:Personaje',
            'choice_label' => 'nombre',
            'placeholder' => 'Elija un personaje',
            'required' => false,
            'query_builder' => function (EntityRepository $er) {
                                    return $er->createQueryBuilder('p')
                                              ->orderBy('p.nombre', 'ASC');
                                },
            ])
        ->add('categoria', EntityType::class, [
            'class' => 'AppBundle:Categoria',
            'choice_label' => 'nombre',
            'placeholder' => 'Elija una categoría',
            'required' => false,
            'query_builder' => function (EntityRepository $er) {
                                    return $er->createQueryBuilder('c')
                                              ->andWhere('c.deleted_at IS NULL')
                                              ->orderBy('c.nombre', 'ASC');
                                },
            ])
        ->add('edades', EntityType::class, [
            'class' => 'AppBundle:Edades',
            'choice_label' => 'edades',
            'placeholder' => 'Elija un rango de edades',
            'required' => false,
            'query_builder' => function (EntityRepository $er) {
                                    return $er->createQueryBuilder('e')
                                              ->orderBy('e.edades', 'ASC');
                                },
            ])
        ->add('genero', EntityType::class, [
            'class' => 'AppBundle:Genero',
            'choice_label' => 'genero',
            'required' => false,
            'multiple' => true,
            'expanded' => true,
            'query_builder' => function (EntityRepository $er) {
                                    return $er->createQueryBuilder('g')
                                              ->orderBy('g.genero', 'ASC');
                                },
            ])
        ->add('modelo', EntityType::class, [
            'class' => 'AppBundle:Modelo',
            'choice_label' => 'nombre',
            'placeholder' => 'Elija un modelo',
            'query_builder' => function (EntityRepository $er) {
                                    return $er->createQueryBuilder('m')
                                              ->orderBy('m.nombre', 'ASC');
                                },
            ])
        ;
    }

    /**
     * @param array $filter
     * @return bool Chequea si tiene seteado algun parámetro de los filtros avanzados
     */
    public static function hasAdvancedFiltersData($filters){
        return isset($filters['personaje'])
            || isset($filters['categoria'])
            || isset($filters['edades'])
            || (isset($filters['genero']) && 0 < count($filters['genero']))
            || isset($filters['modelo']);
    }
}
