<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use AppBundle\Form\BaseUsuarioFormType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Validator\Constraints\IsTrue;

class RegistrationType extends BaseUsuarioFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->add('terms', CheckboxType::class, array(
                'label' => 'He leído y acepto los términos y condiciones',
                'mapped' => false,
                'constraints' => new IsTrue(array('message' => 'Debe aceptar los términos y condiciones')),
            ));
    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }
}
