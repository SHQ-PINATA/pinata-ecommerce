<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use AppBundle\Entity\Descuento;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class DescuentoFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('codigo')

        ->add('tipo', ChoiceType::class, [
            'choices' => Descuento::getTipoOptions(),
            'multiple' => false,
            'label' => 'Tipo de Descuento',
            'placeholder' => 'Elija el tipo de Descuento'])
        ->add('usoUnico')
        ->add('activo')
        ->add('global')
        ->add('fechaDesde', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'required' => false,
                'label' => 'Vigente Desde',
                'format' => 'dd/MM/yyyy HH:mm',
                'attr' => ['class' => 'js-datepicker'],
                    ])
        ->add('fechaHasta', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'required' => false,
                'label' => 'Vigente hasta',
                'format' => 'dd/MM/yyyy HH:mm',
                'attr' => ['class' => 'js-datepicker'],
                    ])
        ->add('valor', NumberType::class, ['label' => 'Valor del Descuento (% ó $)'])
        ->add('montoMin', NumberType::class, ['label' => 'Monto Mínimo ($)'])
        ->add('montoMax', NumberType::class, ['label' => 'Monto Máximo ($)'])
        ->add('montoDisponible', NumberType::class, ['label' => 'Monto disponible ($)'])
        ->add('excluirSale', CheckboxType::class, ['label' => 'El descuento no es aplicable a productos de Sale'])
        ->add('personajes', EntityType::class, [
            'class' => 'AppBundle:Personaje',
            'choice_label' => 'nombre',
            'multiple' => true,
            'expanded' => true,
            'placeholder' => 'Elija el personaje',
            'query_builder' => function (EntityRepository $er) {
                                    return $er->createQueryBuilder('p')
                                              ->andWhere('p.deleted_at IS NULL')
                                              ->orderBy('p.nombre', 'ASC');
                                },
            ])
        ->add('categorias', EntityType::class, [
            'class' => 'AppBundle:Categoria',
            'choice_label' => 'nombre',
            'multiple' => true,
            'expanded' => true,
            'query_builder' => function (EntityRepository $er) {
                                    return $er->createQueryBuilder('c')
                                              ->andWhere('c.deleted_at IS NULL')
                                              ->orderBy('c.nombre', 'ASC');
                                },
            'group_by' => function($val, $key, $index) {
                   if ($val->getParent()) {
                       return $val->getParent()->getNombre();
                   }
                   //return $val->getNombre();
                }
            ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Descuento'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_descuento';
    }


}
