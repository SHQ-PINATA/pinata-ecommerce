<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use FOS\UserBundle\Util\LegacyFormHelper;

abstract class BaseUsuarioFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('name', null, [
            'attr' => ['autofocus' => true],
            'label' => 'Nombre',
            'trim' => true,
            ])
        ->add('lastname', null, [
            'label' => 'Apellido',
            'trim' => true,
            ])
        ->add('email', EmailType::class, [
            'label' => 'E-Mail',
            ])
        ->add('telefono', null, [
            'label' => 'Teléfono',
            ])
        ->add('plainPassword', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\RepeatedType'), array(
                'type' => LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\PasswordType'),
                'options' => array('translation_domain' => 'FOSUserBundle'),
                'first_options' => array('label' => 'Contraseña'),
                'second_options' => array('label' => 'Confirmar contraseña'),
                'invalid_message' => 'Las contraseñas no coinciden',
                'required' => false
            ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Usuario',
        ));
    }

    public function addEnabled(FormBuilderInterface $builder, array $options){
        $builder->add('enabled', null, [
            'label' => 'Activo',
            'data' => is_null($options['data']->getId()) ? true : $options['data']->isEnabled(), 
            ]);
    }

    public function addRoles(FormBuilderInterface $builder, array $options){
        $builder->add('roles', ChoiceType::class, [
            'choices' => array(
                'SUPER-ADMIN' => 'ROLE_SUPER_ADMIN',
                'ADMIN' => 'ROLE_ADMIN',
                'USUARIO' => 'ROLE_USER',
                ),
            'required' => true,
            'multiple' => true,
            'label' => 'Roles',
            ]);
    }
}
