<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;


class PersonajeFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('nombre', null, [
            'attr' => ['autofocus' => true],
            'label' => 'Nombre del Personaje',])
        ->add('activo', CheckboxType::class, [
            'label' => 'Activo',])
        ->add('imagen_carousel_archivo', VichFileType::class, [
            'label' =>  'Imagen del Carousel de Personajes',
            'required' => false,])
        ->add('imagen_banner_archivo', VichFileType::class, [
            'label' =>  'Banner para la seccion del Personaje',
            'required' => false,])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Personaje',
        ));
    }
}
