<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormEvents;

class StockFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('cantidad', IntegerType::class)
                ->add('observacion', TextareaType::class)
                ->add('stock_tipo', EntityType::class, [
                      'label' => 'Accion',
                      'class' => 'AppBundle:StockTipo',
                      'choice_label' => 'nombre',
                      'placeholder' => false,
                      'query_builder' => function (EntityRepository $er) {
                                              return $er->createQueryBuilder('t')
                                                        ->andWhere('t.esDeSistema = false')
                                                        ->orderBy('t.nombre', 'DESC');
                                          },
                      ]);
          /*->add('accion', ChoiceType::class, [
              'choices' => [
                  'Sumar' => 'sumar',
                  'Restar' => 'restar',
                  ],
              'mapped' => false,
              ])*/
                 /*->addEventListener(FormEvents::POST_SUBMIT, function ($event) {
                        // Si la accion es Restar, la cantidad es negativa.
                        $stock = $event->getData();
                        if ($stock['accion'] == 'restar') {
                            $stock['cantidad'] = -1 * abs($stock['cantidad']);
                        }else{
                            $stock['cantidad'] = abs($stock['cantidad']);
                        }

                        $event->setData($stock);
                    })*/
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Stock',
        ));
    }
}