<?php

namespace AppBundle\Controller\Backend;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormBuilder;
use AppBundle\Entity\Caja;
use AppBundle\Form\CajaFormType;

/**
 * Banner controller.
 *
 * @Route("/backend/cajas")
 */
class CajaController extends Controller
{

     /**
     * Lists all caja entities.
     *
     * @Route("/", name="backend_caja_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $cajas = $this->getDoctrine()->getRepository(Caja::class)->findAll();

        return $this->render('backend/caja/index.html.twig', array(
            'cajas' => $cajas,
        ));
    }

    /**
     * @Route("/new", name="backend_caja_new")
     * @Method({"GET", "POST"}) 
     */
    public function newAction(Request $request){
        $caja = new Caja();
        $form = $this->createForm(CajaFormType::class, $caja);
        
        $form->handleRequest($request);


        if ($form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();

            $caja = $form->getData();
            $em->persist($caja);
            $em->flush();
             
            $this->addFlash('success', 'La caja se creó exitosamente');
            return $this->redirectToRoute('backend_caja_edit', array('id' => $caja->getId() ));
        }

        return $this->render('backend/caja/new.html.twig', array(
            'titulo' => 'Nueva caja',
            'caja' => $caja,
            'form' => $form->createView(),
            ));
    }

    /**
     * Displays a form to edit an existing banner entity.
     *
     * @Route("/edit/{id}", name="backend_caja_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Caja $caja)
    {
        $form = $this->createForm(CajaFormType::class, $caja);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            $caja = $form->getData();

            $em->persist($caja);
            $em->flush();

            $this->addFlash('success', 'La caja se editó exitosamente');
            return $this->redirectToRoute('backend_caja_edit', array('id' => $caja->getId()));
        }

        return $this->render('backend/caja/edit.html.twig', array(
            'titulo' => 'Edición de caja',
            'caja' => $caja,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/delete/{id}", name="backend_caja_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, Caja $caja)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($caja);
        $em->flush();
        $this->addFlash('success', 'La caja se borró exitosamente');

        return $this->redirectToRoute('backend_caja_index');
    }
}
