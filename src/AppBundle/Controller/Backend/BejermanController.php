<?php

namespace AppBundle\Controller\Backend;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormBuilder;
use AppBundle\Form\ExportacionBejermanFormType;

/**
 * Reportes controller.
 *
 * @Route("/backend/bejerman")
 */

class BejermanController extends Controller
{
    /**
     * @Route("/comprobantes", name="backend_bejerman_comprobantes")
     */
    public function comprobantesAction(Request $request)
    {
        $form = $this->createForm(ExportacionBejermanFormType::class, null, array(
            'method' => 'GET',
        ));
        $form->handleRequest($request);
        $parameters = $form->getData();

        if ($form->isSubmitted() && $form->isValid()) {
            $desde = $parameters['desde']->format('Y-m-d');
            $hasta = $parameters['hasta']->format('Y-m-d');

            return $this->redirectToRoute('backend_bejerman_exportar', array(
                    'desde' => $desde,
                    'hasta' => $hasta
                )
            );
        }

        return $this->render('backend/bejerman/comprobantes.html.twig', array(
            'form' => $form->createView()
        ));

    }

    /**
     * @Route("/exportar", name="backend_bejerman_exportar")
     */
    public function exportarAction(Request $request)
    {
        $fechaDesde = $request->query->get('desde');
        $fechaHasta = $request->query->get('hasta');

        $bejerman = $this->container->get('bejerman_export');

        $zipPath = $bejerman->exportFiles($fechaDesde, $fechaHasta);

        return $this->file($zipPath);
    }
}
