<?php

namespace AppBundle\Controller\Backend;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Usuario;
use Symfony\Component\Form\FormBuilder;
use AppBundle\Form\UsuarioFormType;
use AppBundle\Form\UsuarioFormFilterType;

/**
 * Usuario controller.
 *
 * @Route("/backend/usuario")
 */

class UsuariosController extends Controller
{
    const ITEMS_PER_PAGE = 10;
    /**
     * @Route("/", name="backend_usuario_index")
     */
    public function indexAction(Request $request)
    {
        $form = $this->createForm(UsuarioFormFilterType::class, null, array(
            'action' => $this->generateUrl('backend_usuario_index'),
            'method' => 'GET',
            ));

        $form->handleRequest($request);

        $parameters = $data = $form->getData();

        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository(Usuario::class)->buscarUsuarios($parameters);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query,
                $request->query->getInt('page', 1),
                self::ITEMS_PER_PAGE
            );
        
        $deleteForms = array();
        foreach ($pagination as $usuario) {
            $deleteForms[$usuario->getId()] = $this->createDeleteForm($usuario)->createView();
        }


        return $this->render('backend/usuario/index.html.twig', 
            array('pagination' => $pagination,
                  'form' => $form->createView(),
                  'delete_forms' => $deleteForms,
            ));
    }

    /**
     * @Route("/edit/{id}", name="backend_usuario_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Usuario $usuario){

        $form = $this->createForm(UsuarioFormType::class, $usuario);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $formData = $form->getData();
            $encoder = $this->container->get('security.encoder_factory')->getEncoder($usuario);
            $password = $encoder->encodePassword($formData->getPlainPassword(), $usuario->getSalt());
            $usuario->setPassword($password);
            $em->persist($usuario);
            $em->flush();

            $this->addFlash('success', 'El usuario se editó exitosamente');
        }

        return $this->render('backend/usuario/edit.html.twig', array(
            'usuario' => $usuario,
            'form' => $form->createView(),
            ));
    }

    /**
     * @Route("/new", name="backend_usuario_new")
     * @Method({"GET", "POST"}) 
     */
    public function newAction(Request $request){
        $usuario = new Usuario();
        $form = $this->createForm(UsuarioFormType::class, $usuario);
        
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $usuario = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($usuario);
            $em->flush();
            
            $this->addFlash('success', 'El usuario se creó exitosamente');

            return $this->redirectToRoute('backend_usuario_edit', array('id' => $usuario->getId() ));
        }


        return $this->render('backend/usuario/edit.html.twig', array(
            'usuario' => $usuario,
            'form' => $form->createView(),
            ));
    }

    /**
     * @Route("/show/{id}", name="backend_usuario_show")
     * @Method({"GET"})
     */
    public function showAction(Request $request, Usuario $usuario){
        return $this->render('backend/usuario/show.html.twig', array(
            'usuario' => $usuario,
            ));
    }


    /**
     * @Route("/delete/{id}", name="backend_usuario_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Usuario $usuario)
    {
        $form = $this->createDeleteForm($usuario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $usuario->setDeletedAt(new \DateTime());
            $em->flush();
            $this->addFlash('success', 'El usuario se borro exitosamente');
        }

        return $this->redirectToRoute('backend_usuario_index');
    }

    /**
     * Creates a form to delete a usuario entity.
     * @param usuario $usuario
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Usuario $usuario)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('backend_usuario_delete', array('id' => $usuario->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
