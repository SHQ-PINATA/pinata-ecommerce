<?php

namespace AppBundle\Controller\Backend;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Entity\Categoria;
use AppBundle\Form\CategoriaFormType;
use AppBundle\Form\CategoriasOrdenFormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Categoria controller.
 *
 * @Route("/backend/categoria")
 */

class CategoriaController extends Controller
{
    const ITEMS_PER_PAGE = 10;
    /**
     * @Route("/", name="backend_categoria_index")
     */
    public function indexAction(Request $request)
    {

        $form = $this->createFormBuilder(array())
                    ->add('nombre', TextType::class)
                    ->setMethod('GET')
                    ->getForm();

        $form->handleRequest($request);

        $parameters = $data = $form->getData();

        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository(Categoria::class)->buscarCategorias($parameters);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query,
                $request->query->getInt('page', 1),
                self::ITEMS_PER_PAGE
            );

        // seteo los forms de delete
        $deleteForms = array();
        foreach ($pagination as $categoria) {
            $deleteForms[$categoria->getId()] = $this->createDeleteForm($categoria)->createView();
        }

        return $this->render('backend/categoria/index.html.twig', 
            array('pagination' => $pagination,
                  'form' => $form->createView(),
                  'delete_forms' => $deleteForms,
           ));
    }

    /**
     * @Route("/edit/{id}", name="backend_categoria_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Categoria $categoria){
        $form = $this->createForm(CategoriaFormType::class, $categoria);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($categoria);
            $em->flush();

            $form = $this->createForm(CategoriaFormType::class, $categoria);
            $this->addFlash('success', 'La categoría se editó exitosamente');
        }

        return $this->render('backend/categoria/edit.html.twig', array(
            'title' => 'Edición de Categoría',
            'categoria' => $categoria,
            'form' => $form->createView(),
            ));
    }
    /**
     * @Route("/new", name="backend_categoria_new")
     * @Method({"GET", "POST"}) 
     */
    public function newAction(Request $request){
        $categoria = new Categoria();
        $form = $this->createForm(CategoriaFormType::class, $categoria);
        
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $categoria = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($categoria);
            $em->flush();
            
            $this->addFlash('success', 'La categoría se creó exitosamente');
            return $this->redirectToRoute('backend_categoria_edit', array('id' => $categoria->getId() ));
        }

        return $this->render('backend/categoria/edit.html.twig', array(
            'title' => 'Nueva Categoría',
            'categoria' => $categoria,
            'form' => $form->createView(),
            ));
    }

    /**
     * @Route("/show/{id}", name="backend_categoria_show")
     * @Method({"GET"})
     */
    public function showAction(Request $request, Categoria $categoria){
      return $this->render('backend/categoria/show.html.twig', array(
            'categoria' => $categoria,
            ));
    }

    /**
     * @Route("/delete/{id}", name="backend_categoria_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Categoria $categoria)
    {
        $form = $this->createDeleteForm($categoria);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $categoria->setDeletedAt(new \DateTime);
            $em->flush();
        }

        return $this->redirectToRoute('backend_categoria_index');
    }

    /**
     * Creates a form to delete a Categoria entity.
     * @param Categoria $categoria
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Categoria $categoria)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('backend_categoria_delete', array('id' => $categoria->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     *  El orden de las catategorias y subcategorias es asi:
     *  1 categoria A
     *  2 categoria B
     *      3 subcategoria de B
     *      4 subcategoria de B
     *  5 categoria C
     * 
     * @Route("/ordenar", name="backend_categoria_order")
     */
    public function orderAction(Request $request)
    {
        $categorias = $this->getDoctrine()->getRepository(Categoria::class)->getAllCategoriasAndSubcategoria();

        $form = $this->createForm(CategoriasOrdenFormType::class, ['categorias' => $categorias]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            // Este uasort hace que se reordene segun el orden, pero preservando la key que es
            // el id de la categoria, que se usa para acceder al elemento del form. Sin esto
            // despues de guardar, se va ver el orden viejo pero se guarda el nuevo. Se puede
            // reemplazar pegandole a la base de nuevo, pero lo evitamos.
            uasort($categorias, function($a, $b){
                return $a->getOrden() <=> $b->getOrden();
            });
            //$categorias = $this->getDoctrine()->getRepository(Categoria::class)->getAllCategoriasAndSubcategoria();
            $form = $this->createForm(CategoriasOrdenFormType::class, ['categorias' => $categorias]);
            
            $this->addFlash('success', 'El orden de las categorias se editó exitosamente');
        }

        return $this->render('backend/categoria/orden.html.twig', 
            array('categorias' => $categorias,
                  'form' => $form->createView(),
            ));
    }
}
