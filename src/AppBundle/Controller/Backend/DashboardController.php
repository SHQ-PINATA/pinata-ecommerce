<?php

namespace AppBundle\Controller\Backend;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Dashboard controller.
 *
 * @Route("/backend")
 */

class DashboardController extends Controller
{
    /**
     * @Route("/", name="backend_dashboard_index")
     */
    public function indexAction(Request $request)
    {
    	return $this->redirectToRoute('backend_producto_index');
    }

}
