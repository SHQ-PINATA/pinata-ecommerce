<?php

namespace AppBundle\Controller\Backend;

use AppBundle\Entity\Descuento;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Descuento controller.
 *
 * @Route("/backend/descuento")
 */
class DescuentoController extends Controller
{
    /**
     * Lists all descuento entities.
     *
     * @Route("/", name="backend_descuento_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $descuentos = $em->getRepository('AppBundle:Descuento')->listBackend();

        $deleteForms = array();
        foreach ($descuentos as $descuento) {
            $deleteForms[$descuento->getId()] = $this->createDeleteForm($descuento)->createView();
        }

        return $this->render('backend/descuento/index.html.twig', array(
            'descuentos' => $descuentos,
            'delete_forms' => $deleteForms,
        ));
    }

    /**
     * Creates a new descuento entity.
     *
     * @Route("/new", name="backend_descuento_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $descuento = new Descuento();
        $form = $this->createForm('AppBundle\Form\DescuentoFormType', $descuento);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($descuento);
            $em->flush();

            return $this->redirectToRoute('backend_descuento_show', array('id' => $descuento->getId()));
        }

        return $this->render('backend/descuento/edit.html.twig', array(
            'descuento' => $descuento,
            'form' => $form->createView(),
            'title' => 'Nuevo Descuento',
        ));
    }

    /**
     * Finds and displays a descuento entity.
     *
     * @Route("/show/{id}", name="backend_descuento_show")
     * @Method("GET")
     */
    public function showAction(Descuento $descuento)
    {
        $deleteForm = $this->createDeleteForm($descuento);

        return $this->render('backend/descuento/show.html.twig', array(
            'descuento' => $descuento,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing descuento entity.
     *
     * @Route("/edit/{id}", name="backend_descuento_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Descuento $descuento)
    {
        $deleteForm = $this->createDeleteForm($descuento);
        $editForm = $this->createForm('AppBundle\Form\DescuentoFormType', $descuento);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            
            $this->addFlash('success', 'El descuento se editó exitosamente');
            return $this->redirectToRoute('backend_descuento_edit', array('id' => $descuento->getId()));
        }

        return $this->render('backend/descuento/edit.html.twig', array(
            'descuento' => $descuento,
            'form' => $editForm->createView(),
            'title' => 'Edición de Descuento',
            /*'delete_form' => $deleteForm->createView(),*/
        ));
    }

    /**
     * Deletes a descuento entity.
     *
     * @Route("/delete/{id}", name="backend_descuento_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Descuento $descuento)
    {
        $form = $this->createDeleteForm($descuento);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($descuento);
            $em->flush();
            $this->addFlash('success', 'El descuento se borró exitosamente');
        }

        return $this->redirectToRoute('backend_descuento_index');
    }

    /**
     * Creates a form to delete a descuento entity.
     *
     * @param Descuento $descuento The descuento entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Descuento $descuento)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('backend_descuento_delete', array('id' => $descuento->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
