<?php

namespace AppBundle\Controller\Backend;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Entity\Modelo;
use AppBundle\Form\ModeloFormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Modelo controller.
 *
 * @Route("/backend/modelo")
 */

class ModeloController extends Controller
{
    const ITEMS_PER_PAGE = 10;
    /**
     * @Route("/", name="backend_modelo_index")
     */
    public function indexAction(Request $request)
    {

        $form = $this->createFormBuilder(array())
                    ->add('nombre', TextType::class)
                    ->setMethod('GET')
                    ->getForm();

        $form->handleRequest($request);

        $parameters = $data = $form->getData();

        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository(Modelo::class)->buscarModelos($parameters);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query,
                $request->query->getInt('page', 1),
                self::ITEMS_PER_PAGE
            );

        // seteo los forms de delete
        $deleteForms = array();
        foreach ($pagination as $modelo) {
            $deleteForms[$modelo->getId()] = $this->createDeleteForm($modelo)->createView();
        }

        return $this->render('backend/modelos/index.html.twig', 
            array('pagination' => $pagination,
                  'form' => $form->createView(),
                  'delete_forms' => $deleteForms,
           ));
    }

    /**
     * @Route("/edit/{id}", name="backend_modelo_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Modelo $modelo){
        $form = $this->createForm(ModeloFormType::class, $modelo);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($modelo);
            $em->flush();

            $this->addFlash('success', 'El modelo se editó exitosamente');
        }

        return $this->render('backend/modelos/edit.html.twig', array(
            'usuario' => $modelo,
            'form' => $form->createView(),
            ));
    }
    /**
     * @Route("/new", name="backend_modelo_new")
     * @Method({"GET", "POST"}) 
     */
    public function newAction(Request $request){
        $modelo = new Modelo();
        $form = $this->createForm(ModeloFormType::class, $modelo);
        
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $modelo = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($modelo);
            $em->flush();
            
            $this->addFlash('success', 'El modelo se creó exitosamente');
            return $this->redirectToRoute('backend_modelo_edit', array('id' => $modelo->getId() ));
        }

        return $this->render('backend/modelos/edit.html.twig', array(
            'modelo' => $modelo,
            'form' => $form->createView(),
            ));
    }

    /**
     * @Route("/show/{id}", name="backend_modelo_show")
     * @Method({"GET"})
     */
    public function showAction(Request $request, Modelo $modelo){
      return $this->render('backend/modelos/show.html.twig', array(
            'modelo' => $modelo,
            ));
    }
    

    /**
     * @Route("/delete/{id}", name="backend_modelo_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Modelo $modelo)
    {
        $form = $this->createDeleteForm($modelo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $modelo->setDeletedAt(new \DateTime);
            $em->flush();
            $this->addFlash('success', 'El modelo se borró exitosamente');
        }

        return $this->redirectToRoute('backend_modelo_index');
    }

    /**
     * Creates a form to delete a modelo entity.
     * @param modelo $modelo
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Modelo $modelo)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('backend_modelo_delete', array('id' => $modelo->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
