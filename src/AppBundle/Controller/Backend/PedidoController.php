<?php

namespace AppBundle\Controller\Backend;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Form\Extension\Core\Type\TextType;

use AppBundle\Entity\Pedido;
use AppBundle\Entity\PedidoMail;
use AppBundle\Entity\Envio;

use AppBundle\Form\PedidoFormFilterType;
use AppBundle\Form\PedidoMailEnvioFormType;
use AppBundle\Form\PedidoMailCompraFormType;
use AppBundle\Form\PedidoMailPagoFormType;
use AppBundle\Form\EnvioDomicilioFormType;
use AppBundle\Form\EnvioSucursalCorreoFormType;
use AppBundle\Form\EnvioPickupFormType;
use AppBundle\Form\PedidosBulkFormType;

use AppBundle\Presenter\PedidoPresenter;
use AppBundle\Presenter\FacturaPresenter;

/**
 * Pedido controller.
 *
 * @Route("/backend/pedido")
 */
class PedidoController extends Controller
{
    const ITEMS_PER_PAGE = 10;
    /**
     * Lists all pedido entities.
     *
     * @Route("/", name="backend_pedido_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $form = $this->createForm(PedidoFormFilterType::class, null, array(
            'action' => $this->generateUrl('backend_pedido_index'),
            'method' => 'GET',
            ));

        $form->handleRequest($request);

        $parameters = $form->getData();

        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository(Pedido::class)->buscarPedidos($parameters);
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query,
                $request->query->getInt('page', 1),
                self::ITEMS_PER_PAGE
            );

        $desarmarPedidosForms = []; $presenters = []; $pedidos_ids = [];
        foreach ($pagination->getItems() as $pedido) {
            $desarmarPedidosForms[$pedido->getId()] = $this->createDesarmarPedidoForm($pedido)->createView();
            $presenters[$pedido->getId()] = new PedidoPresenter($pedido);
            $pedidos_ids[$pedido->getId()] = $pedido->getId();
        }

      //  $form_bulk = $this->createForm(PedidosBulkFormType::class, null, array(
      //      'action' => $this->generateUrl('backend_pedidos_bulk'),
      //      'pedidos' => $pedidos_ids,
      //      ));
      //  die(var_dump($form_bulk));

        return $this->render('backend/pedido/index.html.twig', array(
            'form_filter' => $form->createView(),
            'pagination' => $pagination,
            'desarmar_pedidos_forms' => $desarmarPedidosForms,
            'presenters' => $presenters,
            'form_bulk' => null//$form_bulk->createView(),
        ));
    }

    /**
     * Finds and displays a pedido entity.
     *
     * @Route("/show/{id}", name="backend_pedido_show")
     * @Method("GET")
     */
    public function showAction(Pedido $pedido)
    {   
        $presenter = new PedidoPresenter($pedido);
        return $this->render('backend/pedido/show.html.twig', array(
            'pedido' => $pedido,
            'presenter' => $presenter,
        ));
    }

    /**
     * @Route("/cajas/{id}", name="backend_pedido_cajas")
     * @Method("GET")
     */
    public function cajasAction(Pedido $pedido)
    {   
        $items = $pedido->getItems();
        $dimensiones = '';
        $i = 1;

        foreach ($items as $item) {
            $dimensiones .= $item->getProducto()->getDimensiones();
            if ($i != count($items)) {
                $dimensiones .= ',';
            }
            $i++;
        }

        $envioPackService = $this->container->get('enviopack');
        $cajas = $envioPackService->acomodarEnCajas($dimensiones);
        
        return $this->render('backend/pedido/cajas.html.twig', array(
            'cajas' => $cajas,
        ));
    }


    /**
     * Displays a form to edit an existing pedido entity.
     *
     * @Route("/edit/{id}", name="backend_pedido_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Pedido $pedido)
    {
        $editForm = $this->createForm('AppBundle\Form\PedidoFormType', $pedido);
        $editForm->handleRequest($request);

        $envioForm = $this->getEnvioForm($pedido);
        $envioForm->handleRequest($request);

        $desarmarForm = $this->createDesarmarPedidoForm($pedido);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $pedido_mailer = $this->container->get('pedidos_mailer');
            $pedido_mailer->sendCompraMailIfNotSent($pedido);

            $this->addFlash('success', 'El pedido se editó exitosamente');
            return $this->redirectToRoute('backend_pedido_edit', array('id' => $pedido->getId()));
        }

        if ($envioForm->isSubmitted() && $envioForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            // TODO: Hacer esto de una forma mejor, probablemente data transformes o
            // relegar el nombre al servicio de enviopack
            $envio = $envioForm->getData();
            if ($envio->getType() == Envio::DOMICILIO) {
                $enviopack = $this->container->get('enviopack');
                $correos = array_flip($enviopack->getCorreosOptions());
                $envio->setCorreoNombre(null);
                if (isset($correos[$envio->getCorreoId()])) {
                    $envio->setCorreoNombre($correos[$envio->getCorreoId()]);
                }
            }
            $em->persist($envio);
            $em->flush();

            $this->addFlash('success', 'El envío del pedido se editó exitosamente');
            return $this->redirectToRoute('backend_pedido_edit', array('id' => $pedido->getId()));
        }

        return $this->render('backend/pedido/edit.html.twig', array(
            'title' => 'Edición de Pedido',
            'pedido' => $pedido,
            'form' => $editForm->createView(),
            'desarmar_form' => $desarmarForm->createView(),
            'envio_form' => $envioForm->createView(),
        ));
    }

    /**
     * .
     *
     * @Route("/reactivate/{id}", name="backend_pedido_reactivate")
     * @Method({"GET"})
     */
    public function reactivateAction(Request $request, Pedido $pedido)
    {
        $em = $this->getDoctrine()->getManager();
        $pedido->setFechaBaja(null);
        $em->persist($pedido);
        $em->flush();

        $this->addFlash('success', 'El pedido ha sido reactivado exitosamente.');
        return $this->redirectToRoute('backend_pedido_index');
    }

    /**
     * Deletes a pedido entity.
     *
     * @Route("desarmar/{id}", name="pedido_desarmar")
     * @Method("POST")
     */
    public function desarmarAction(Request $request, Pedido $pedido)
    {
        $form = $this->createDesarmarPedidoForm($pedido);
        $form->handleRequest($request);

        if ($pedido->getFechaBaja()) {
            $this->addFlash('success', 'El pedido ya se había dado de baja');
        }

        if ($form->isSubmitted() && $form->isValid() && !$pedido->getFechaBaja()) {
            $pedido_manager = $this->container->get('pedido_manager');
            $pedido_manager->undoPedido($pedido);

            $this->addFlash('success', 'El pedido se dió de baja exitosamente');
        }

        return $this->redirectToRoute('backend_pedido_show', array('id' => $pedido->getId()));
    }

    /**
     * Creates a form to delete a pedido entity.
     *
     * @param Pedido $pedido The pedido entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDesarmarPedidoForm(Pedido $pedido)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('pedido_desarmar', array('id' => $pedido->getId())))
            ->setMethod('POST')
            ->getForm()
        ;
    }

    /**
     *
     * @Route("/mails/{id}", name="backend_pedido_mails")
     * @Method({"GET", "POST"})
     */
    public function mailsAction(Request $request, Pedido $pedido){
        $em = $this->getDoctrine()->getManager();
        
        $envio = $pedido->getEnvio();
        $num_seguimiento = $envio->getNumSeguimiento();

        $compra_mail_form = $this->createForm(PedidoMailCompraFormType::class, ['paid' => $pedido->isPaid() ], ['action' => $this->generateUrl('backend_pedido_mails', array('id' => $pedido->getId()))]
        );

        $envio_mail_form = $this->createForm(PedidoMailEnvioFormType::class, ['num_seguimiento' => $num_seguimiento, 'sent' => $pedido->isSent() ], ['action' => $this->generateUrl('backend_pedido_mails', array('id' => $pedido->getId()))]
        );

        $pago_mail_form = $this->createForm(PedidoMailPagoFormType::class, ['paid' => $pedido->isSent() ], ['action' => $this->generateUrl('backend_pedido_mails', array('id' => $pedido->getId()))]
        );
        

        $compra_mail_form->handleRequest($request);
        $envio_mail_form->handleRequest($request);
        $pago_mail_form->handleRequest($request);

        $flashMsg = null;
        if ($compra_mail_form->isSubmitted() && $compra_mail_form->isValid()) {
            $pedido_mailer = $this->container->get('pedidos_mailer');
            $sent = $pedido_mailer->sendCompraMail($pedido);
            $flashMsg = $sent ? 'Se envió el mail de compra' : 'No se envió el mail de compra';
        }

        if ($envio_mail_form->isSubmitted() && $envio_mail_form->isValid()) {
            $data = $envio_mail_form->getData();
            $num_seguimiento = $data['num_seguimiento'];

            $envio = $pedido->getEnvio();
            if (method_exists($envio, 'setNumSeguimiento')) {
                $envio->setNumSeguimiento($num_seguimiento);
                $em->persist($envio);
                $em->flush();
            }

            $pedido_mailer = $this->container->get('pedidos_mailer');
            $sent = $pedido_mailer->sendEnvioMail($pedido, $num_seguimiento );
            $flashMsg = $sent ? 'Se envió el mail de envío' : 'No se envió el mail de envío';
        }

        if ($pago_mail_form->isSubmitted() && $pago_mail_form->isValid()) {
            $mpOrder = $pedido->getMercadoPagoOrder();
            if ($mpOrder &&  $mpOrder->getPaymentUrl()) {
                $pedido_mailer = $this->container->get('pedidos_mailer');
                $sent = $pedido_mailer->sendPagoMail($pedido);
                $flashMsg = $sent ? 'Se envió el mail de pago' : 'No se envió el mail de pago';
            }else{
                $sent = false;
                $flashMsg = 'No se envió el mail porque no existe la orden de pago en la base de datos';
            }
        }

        if ($flashMsg) {
            if ($sent) {
                $this->addFlash('success', $flashMsg);
            }else{
                $this->addFlash('error', $flashMsg);
            }
        }

        $compra_mails_count = $em->getRepository(PedidoMail::class)->countByPedidoAndTipo($pedido, PedidoMail::COMPRA);
        $envio_mails_count = $em->getRepository(PedidoMail::class)->countByPedidoAndTipo($pedido, PedidoMail::ENVIO);
        $pago_mails_count = $em->getRepository(PedidoMail::class)->countByPedidoAndTipo($pedido, PedidoMail::PAGO);

        return $this->render('backend/pedido/mails.html.twig', array(
            'pedido' => $pedido,
            'envio_mail_form' => $envio_mail_form->createView(),
            'compra_mail_form' => $compra_mail_form->createView(),
            'pago_mail_form' => $pago_mail_form->createView(),
            'compra_mails_count' => $compra_mails_count,
            'envio_mails_count' => $envio_mails_count,
            'pago_mails_count' => $pago_mails_count,
            'envio_is_pickup' => $envio->isPickup(),
        ));


    }

    private function getEnvioForm(Pedido $pedido){
        $envio = $pedido->getEnvio();
        switch ($envio->getType()) {
            case Envio::DOMICILIO:
                $enviopack = $this->container->get('enviopack');
                $correos = $enviopack->getCorreosOptions();
                $envioForm = $this->createForm(EnvioDomicilioFormType::class, $envio, array(
                    'action' => $this->generateUrl('backend_pedido_edit', array('id' => $pedido->getId())),
                    'correos' => $correos,
                ));
                break;
            case Envio::SUCURSARL_CORREO:
                $envioForm = $this->createForm(EnvioSucursalCorreoFormType::class, $envio, array(
                    'action' => $this->generateUrl('backend_pedido_edit', array('id' => $pedido->getId())),
                ));
                break;
            case Envio::PICKUP:
                $envioForm = $this->createForm(EnvioPickupFormType::class, $envio, array(
                    'action' => $this->generateUrl('backend_pedido_edit', array('id' => $pedido->getId())),
                ));
            break;
        }


        return $envioForm;
    }

    /**
     * @Route("/bulk-action", name="backend_pedidos_bulk")
     * @Method("POST")
     */
    public function bulkAction(Request $request)
    {
        $form = $this->createForm(PedidosBulkFormType::class);
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid() ) {
            if( $form->get('facturar')->isClicked() ){
                $pedidos = $form->getData()['pedidos'];
                if ( count($pedidos) ){
                    $factura_manager = $this->container->get('factura_manager');
                    $result = $factura_manager->createFacturas($pedidos);
                    $factura_manager->facturar();
                    $flashMsg = '';

                    if (count($result['pedidos_ids_nuevas_facturas']) > 1 ){
                        $flashMsg .= "Se crearon las facturas para los pedidos ".implode(', ', $result['pedidos_ids_nuevas_facturas']).'. ';
                    }elseif (count($result['pedidos_ids_nuevas_facturas']) == 1) {
                        $flashMsg .= "Se creó la factura para el pedido {$result['pedidos_ids_nuevas_facturas'][0]}. ";
                    }

                    if (count($result['pedidos_ids_facturas_ya_creadas']) > 1 ){
                        $flashMsg .= "Ya se habían creado las facturas para los pedidos ".implode(', ', $result['pedidos_ids_facturas_ya_creadas']).'.';
                    }elseif (count($result['pedidos_ids_facturas_ya_creadas']) == 1 ) {
                        $flashMsg .= "Ya se había creado la factura para el pedido {$result['pedidos_ids_facturas_ya_creadas'][0]}. ";
                    }

                    $flashWarningMsg = '';
                    if (count($result['pedidos_no_pagados']) > 1 ){
                        $flashWarningMsg .= "No se crearon las facturas para los pedidos ".implode(', ', $result['pedidos_no_pagados']).' porque no están pagos.';
                    }elseif (count($result['pedidos_no_pagados']) == 1 ) {
                        $flashWarningMsg .= "No se creó la factura para el pedido {$result['pedidos_no_pagados'][0]} porque no está pago. ";
                    }

                    if ($flashMsg) {
                        $this->addFlash('success', $flashMsg);
                    }
                    if ($flashWarningMsg) {
                        $this->addFlash("warning", $flashWarningMsg);
                    }
                }
            }
        }

        return $this->redirectToRoute('backend_pedido_index');
    }

    /**
     * @Route("/factura/{id}", name="backend_pedido_factura_show")
     * @Method("GET")
     */
    public function facturaShowAction(Pedido $pedido)
    {   
        $factura = $pedido->getFactura();


        $pedido_presenter = new PedidoPresenter($pedido);
        if (!$pedido_presenter->hasFacturaCreated() ){
            $this->addFlash('error', 'El pedido no tiene una factura creada');
            return $this->redirectToRoute('backend_pedido_index');
        }
        if (!$pedido_presenter->facturaIsProcessed() ){
            $this->addFlash('error', 'La factura del pedido todavía no está procesada.');
            return $this->redirectToRoute('backend_pedido_index');
        }
 
        $factura_manager = $this->container->get('factura_manager');
        $factura_presenter = new FacturaPresenter($factura, $pedido);
        $afipConfig = $this->getParameter('afip');
        $html = $factura_manager->getFacturaAsHTML($factura_presenter, $afipConfig);

        $filename = 'Piñata Factura Pedido '.$pedido->getId().'.pdf';
        return new Response($this->get('knp_snappy.pdf')->getOutputFromHtml($html),
                200,
                array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'attachment; filename="'.$filename.'"'
                )
            );
    }

    /**
     * @Route("/factura/{id}/crear", name="backend_pedido_factura_create")
     * @Method("GET")
     */
    public function facturaCreateAction(Pedido $pedido)
    {   

        $factura_manager = $this->container->get('factura_manager');
        $result = $factura_manager->createFacturas(array($pedido));
        $factura_manager->facturar();
        $flashMsg = '';

        if (count($result['pedidos_ids_nuevas_facturas']) > 1 ){
            $flashMsg .= "Se crearon las facturas para los pedidos ".implode(', ', $result['pedidos_ids_nuevas_facturas']).'. ';
        }elseif (count($result['pedidos_ids_nuevas_facturas']) == 1) {
            $flashMsg .= "Se creó la factura para el pedido {$result['pedidos_ids_nuevas_facturas'][0]}. ";
        }

        if (count($result['pedidos_ids_facturas_ya_creadas']) > 1 ){
            $flashMsg .= "Ya se habían creado las facturas para los pedidos ".implode(', ', $result['pedidos_ids_facturas_ya_creadas']).'.';
        }elseif (count($result['pedidos_ids_facturas_ya_creadas']) == 1 ) {
            $flashMsg .= "Ya se había creado la factura para el pedido {$result['pedidos_ids_facturas_ya_creadas'][0]}. ";
        }

        $flashWarningMsg = '';
        if (count($result['pedidos_no_pagados']) > 1 ){
            $flashWarningMsg .= "No se crearon las facturas para los pedidos ".implode(', ', $result['pedidos_no_pagados']).' porque no están pagos.';
        }elseif (count($result['pedidos_no_pagados']) == 1 ) {
            $flashWarningMsg .= "No se creó la factura para el pedido {$result['pedidos_no_pagados'][0]} porque no está pago. ";
        }

        if ($flashMsg) {
            $this->addFlash('success', $flashMsg);
        }
        if ($flashWarningMsg) {
            $this->addFlash("warning", $flashWarningMsg);
        }

        return $this->redirectToRoute('backend_pedido_index');
    }
}
