<?php

namespace AppBundle\Controller\Backend;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Entity\Producto;
use AppBundle\Entity\Stock;
use AppBundle\Entity\CarritoProductoItem;
use AppBundle\Form\ProductoFormFilterType;
use AppBundle\Form\ProductoFormType;
use AppBundle\Form\StockFormType;
use AppBundle\Form\ProductosOrdenFormType;

/**
 * Producto controller.
 *
 * @Route("/backend/producto")
 */

class ProductoController extends Controller
{
    const ITEMS_PER_PAGE = 10;
    /**
     * @Route("/", name="backend_producto_index")
     */
    public function indexAction(Request $request)
    {
    	$form = $this->createForm(ProductoFormFilterType::class, null, array(
            'action' => $this->generateUrl('backend_producto_index'),
            'method' => 'GET',
            ));

        $em = $this->getDoctrine()->getManager();
        $form->handleRequest($request);

        $parameters = $data = $form->getData();
        $parent_id = $request->query->get('parent_id');
        $parent = $parent_id ? $em->getRepository(Producto::class)->findOneById($parent_id) : null;

        $query = $em->getRepository(Producto::class)->buscarProductos($parameters, $parent);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query,
                $request->query->getInt('page', 1),
                self::ITEMS_PER_PAGE
            );
        
        $deleteForms = array();
        foreach ($pagination as $producto) {
            $deleteForms[$producto->getId()] = $this->createDeleteForm($producto)->createView();
        }

        $reserved_stocks = $em->getRepository(CarritoProductoItem::class)->getStockReservadoByProductos($pagination->getItems(), $this->getParameter('app.interval_reserve_minutes_cart'));

        $talleTitle = $parent ? 'Listado de talles - "' . $parent->getNombre() . '"': null;
        $talleParentId = $parent ? $parent->getId() : null;
        return $this->render('backend/producto/index.html.twig', 
            array('pagination' => $pagination,
                  'form' => $form->createView(),
                  'filtros_avanzandos_abierto' => ProductoFormFilterType::hasAdvancedFiltersData($parameters),
                  'delete_forms' => $deleteForms,
                  'reserved_stocks' => $reserved_stocks,
                  'talleTitle' => $talleTitle,
                  'talleParentId' => $talleParentId,
            ));
    }

    /**
     * @Route("/edit/{id}", name="backend_producto_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Producto $producto){
        $form = $this->createForm(ProductoFormType::class, $producto);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $mercadolibreService = $this->get('mercadolibre');
            $mercadolibreService->updatePrecio($producto, $producto->getPrecio());
            $em->persist($producto);

            $parent_id = $form['parent_id']->getData();
            if ($parent_id) {
                $parent = $em->getRepository(Producto::class)->findOneById($parent_id);
                if ($parent) {
                    $parent->addTalle($producto);
                    $producto->setParent($parent);
                    $em->persist($producto);
                    $em->persist($parent);
                } else {
                    $this->addFlash('error', 'El id ingresado para el producto padre no corresponde con un producto válido, por favor chequéelo e ingréselo nuevamente.');
                }
            }
            $em->flush();
            $em->refresh($producto);

            // si no le pongo el valor nuevo del producto al form, se queda con los viejos
            $form = $this->createForm(ProductoFormType::class, $producto);

            $this->addFlash('success', 'El producto se editó exitosamente');
        }

        $title = $producto->getParent() ? 'Edición de talle' : 'Edición de producto';
        $parent_id = $producto->getParent() ? $producto->getParent()->getId() : null;

        return $this->render('backend/producto/edit.html.twig', array(
            'title' => $title,
            'producto' => $producto,
            'form' => $form->createView(),
            'parentId' => $parent_id,
            ));
    }

    /**
     * @Route("/new", name="backend_producto_new")
     * @Method({"GET", "POST"}) 
     */
    public function newAction(Request $request){
        $producto = new Producto();
        $form = $this->createForm(ProductoFormType::class, $producto);
        
        $parent_id = $request->query->get('parent_id');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $producto = $form->getData();

            $repository = $em->getRepository(Producto::class);
            $producto->setOrden($repository->getLastOrden() + 1);

            $mercadolibreService = $this->get('mercadolibre');
            $mercadolibreService->updatePrecio($producto, $producto->getPrecio());
            $em->persist($producto);
            $form_parent_id = $form['parent_id']->getData();

            if ($form_parent_id) {
                $parent = $em->getRepository(Producto::class)->findOneById($form_parent_id);
                if ($parent) {
                    $parent->addTalle($producto);
                    $producto->setParent($parent);
                    $em->persist($producto);
                    $em->persist($parent);
                } else {
                    $this->addFlash('error', 'El id ingresado para el producto padre no corresponde con un producto válido, por favor chequéelo e ingréselo nuevamente.');
                }
            }

            $em->flush();
            
            $this->addFlash('success', 'El producto se creó exitosamente');
            return $this->redirectToRoute('backend_producto_edit', array('id' => $producto->getId() ));
        }

        $title = $parent_id ? 'Nuevo talle' : 'Nuevo producto';
        return $this->render('backend/producto/edit.html.twig', array(
            'title' => $title,
            'producto' => $producto,
            'form' => $form->createView(),
            'parentId' => $parent_id,
            ));
    }

    /**
     * @Route("/show/{id}", name="backend_producto_show")
     * @Method({"GET"})
     */
    public function showAction(Request $request, Producto $producto){
        $reserved_stock = $this->getDoctrine()->getManager()->getRepository(CarritoProductoItem::class)->getStockReservadoByProducto($producto, $this->getParameter('app.interval_reserve_minutes_cart'));

      return $this->render('backend/producto/show.html.twig', array(
            'producto' => $producto,
            'reserved_stock' => $reserved_stock,
            ));
    }


    /**
     * @Route("/delete/{id}", name="backend_producto_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Producto $producto)
    {
        $form = $this->createDeleteForm($producto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $producto->setDeletedAt(new \DateTime);
            $em->flush();
            $this->addFlash('success', 'El producto se borró exitosamente');
        }

        return $this->redirectToRoute('backend_producto_index');
    }

    /**
     * @Route("/stock/{id}", name="backend_producto_stock_edit")
     * @Method({"GET", "POST"})
     */
    public function editStockAction(Request $request, Producto $producto){
        $stock = new Stock();
        $stock->setProducto($producto);

        $form = $this->createForm(StockFormType::class, $stock);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($stock);
            $em->flush();

            $stock_manager = $this->container->get('stock_manager');
            $stock_manager->recalculateStock($producto);

            $stock = new Stock();
            $stock->setProducto($producto);
            $form = $this->createForm(StockFormType::class, $stock);

            $this->addFlash('success', 'El stock se editó exitosamente');
        }

        return $this->render('backend/producto/stock.html.twig', array(
            'title' => 'Stock de Producto',
            'producto' => $producto,
            'stock' => $stock,
            'form' => $form->createView(),
            ));
    }

    /**
     * Creates a form to delete a producto entity.
     * @param producto $producto
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Producto $producto)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('backend_producto_delete', array('id' => $producto->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }


    /**
     * @Route("/ordenar", name="backend_producto_order")
     */
    public function orderAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Producto::class);
        $productos = $repository->findBy(
            array('deleted_at' => null, 'parent' => null, 'activo' => 1),
            array('orden' => 'ASC')
        );

        $form = $this->createForm(ProductosOrdenFormType::class, ['productos' => $productos]);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            usort($productos, function($a, $b){
                return $a->getOrden() <=> $b->getOrden();
            });
            $form = $this->createForm(ProductosOrdenFormType::class, ['productos' => $productos]);
            
            $this->addFlash('success', 'El orden de los productos se editó exitosamente');
        }

        return $this->render('backend/producto/orden.html.twig', 
            array('productos' => $productos,
                  'form' => $form->createView(),
            ));
    }
}
