<?php

namespace AppBundle\Controller\Backend;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormBuilder;
use AppBundle\Entity\BloqueBanner;
use AppBundle\Form\BloqueBannerFormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

/**
 * Banner controller.
 *
 * @Route("/backend/banner")
 */
class BannerController extends Controller
{

    const ITEMS_PER_PAGE = 20;
     /**
     * Lists all banner entities.
     *
     * @Route("/", name="backend_banner_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository(BloqueBanner::class)->getByOrdenQuery();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query,
                $request->query->getInt('page', 1),
                self::ITEMS_PER_PAGE
            );

        // seteo los forms de delete y para reordenar
        $deleteForms = array();
        $reorderForms = array();
        foreach ($pagination as $bloque) {
            $deleteForms[$bloque->getId()] = $this->createDeleteForm($bloque)->createView();
            $reorderForms[$bloque->getId()] = $this->createReorderForm($bloque)->createView();
        }

        // chequeo si muestro la flecha para arriba para el primero del listado
        // y lo mismo para el ultimo        
        $show_arrow_for_first = $pagination->getCurrentPageNumber() > 1;
        $show_arrow_for_last = $pagination->getCurrentPageNumber() < $pagination->getPageCount(); 

        return $this->render('backend/banner/index.html.twig', array(
            'pagination' => $pagination,
            'delete_forms' => $deleteForms,
            'reorder_forms' => $reorderForms,
            'show_arrow_for_first' => $show_arrow_for_first,
            'show_arrow_for_last' => $show_arrow_for_last,
        ));
    }

    /**
     * @Route("/new", name="backend_banner_new")
     * @Method({"GET", "POST"}) 
     */
    public function newAction(Request $request){
        $bloque_banner = new BloqueBanner();
        $form = $this->createForm(BloqueBannerFormType::class, $bloque_banner);
        
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $bloque_banner = $form->getData();
            $bloque_banner->SetOrden($this->getDoctrine()->getManager()->getRepository(BloqueBanner::class)->getNewPosition());

            $em->persist($bloque_banner);
            $em->flush();
             
            $this->addFlash('success', 'El bloque de banners se creó exitosamente');
            return $this->redirectToRoute('backend_banner_edit', array('id' => $bloque_banner->getId() ));
        }

        return $this->render('backend/banner/new.html.twig', array(
            'titulo' => 'Nuevo Bloque de Banners',
            'bloque_banner' => $bloque_banner,
            'form' => $form->createView(),
            ));
    }

    /**
     * Displays a form to edit an existing banner entity.
     *
     * @Route("/edit/{id}", name="backend_banner_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, BloqueBanner $bloque_banner)
    {
        $deleteForm = $this->createDeleteForm($bloque_banner);
        $form = $this->createForm(BloqueBannerFormType::class, $bloque_banner);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $bloque_banner = $form->getData();

            $banners = $bloque_banner->getBanners();
            foreach ($banners as $banner) {
                // no guardo el banner del form que no se le subio una imagen/video
                if (!$banner->hasImageOrVideoSet()) {
                    $bloque_banner->removeBanner($banner);
                }
            }

            $em->persist($bloque_banner);
            $em->flush();

            $this->addFlash('success', 'El bloque de banners se editó exitosamente');
            return $this->redirectToRoute('backend_banner_edit', array('id' => $bloque_banner->getId()));
        }

        return $this->render('backend/banner/edit.html.twig', array(
            'titulo' => 'Edición de Bloque de Banners',
            'bloque_banner' => $bloque_banner,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Route("/show/{id}", name="backend_banner_show")
     * @Method({"GET"})
     */
    public function showAction(Request $request, BloqueBanner $bloque){
      return $this->render('backend/banner/show.html.twig', array(
            'bloque' => $bloque,
            ));
    }


    /**
     * @Route("/delete/{id}", name="backend_banner_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, BloqueBanner $bloque)
    {
        $form = $this->createDeleteForm($bloque);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($bloque);
            $em->flush();
        }

        return $this->redirectToRoute('backend_banner_index');
    }

    /**
     * Creates a form to delete a BloqueBanner entity.
     * @param BloqueBanner $banner
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(BloqueBanner $bloque)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('backend_banner_delete', array('id' => $bloque->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * @Route("/reorder/{id}", name="backend_banner_reorder")
     * @Method("POST")
     */
    public function reorderAction(Request $request, BloqueBanner $bloque)
    {
        $form = $this->createReorderForm($bloque);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $bloqueBannerManager = $this->container->get('bloque_banner_manager');
            $data = $form->getData();
            $bloqueBannerManager->reorderBloque($bloque, $data['mover']);
        }

        return $this->redirectToRoute('backend_banner_index');
    }

    /**
     * Creates a form to delete a BloqueBanner entity.
     * @param BloqueBanner $banner
     * @return \Symfony\Component\Form\Form The form
     */
    private function createReorderForm(BloqueBanner $bloque)
    {   
        return $this->createFormBuilder()
            ->add('mover', HiddenType::class)
            ->setAction($this->generateUrl('backend_banner_reorder', array('id' => $bloque->getId())))
            ->setMethod('POST')
            ->getForm()
        ;
    }    
}
