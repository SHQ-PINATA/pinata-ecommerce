<?php

namespace AppBundle\Controller\Backend;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Entity\Personaje;
use AppBundle\Form\PersonajeFormType;
use AppBundle\Form\PersonajesOrdenFormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Personaje controller.
 *
 * @Route("/backend/personaje")
 */

class PersonajeController extends Controller
{
    const ITEMS_PER_PAGE = 10;
    /**
     * @Route("/", name="backend_personaje_index")
     */
    public function indexAction(Request $request)
    {

        $form = $this->createFormBuilder(array())
                    ->add('nombre', TextType::class)
                    ->setMethod('GET')
                    ->getForm();

        $form->handleRequest($request);

        $parameters = $data = $form->getData();

        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository(Personaje::class)->buscarPersonajes($parameters);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query,
                $request->query->getInt('page', 1),
                self::ITEMS_PER_PAGE
            );
        
        $deleteForms = array();
        foreach ($pagination as $personaje) {
            $deleteForms[$personaje->getId()] = $this->createDeleteForm($personaje)->createView();
        }

        return $this->render('backend/personaje/index.html.twig', 
            array('pagination' => $pagination,
                  'form' => $form->createView(),
                  'delete_forms' => $deleteForms,
           ));
    }

    /**
     * @Route("/edit/{id}", name="backend_personaje_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Personaje $personaje){
        $form = $this->createForm(PersonajeFormType::class, $personaje);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($personaje);
            $em->flush();

            $this->addFlash('success', 'El personaje se editó exitosamente');
        }

        return $this->render('backend/personaje/edit.html.twig', array(
            'title' => 'Edición de Personaje',
            'personaje' => $personaje,
            'form' => $form->createView(),
            ));
    }
    /**
     * @Route("/new", name="backend_personaje_new")
     * @Method({"GET", "POST"}) 
     */
    public function newAction(Request $request){
        $personaje = new Personaje();
        $form = $this->createForm(PersonajeFormType::class, $personaje);
        
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $personaje = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($personaje);
            $em->flush();
            
            $this->addFlash('success', 'El personaje se creó exitosamente');
            return $this->redirectToRoute('backend_personaje_edit', array('id' => $personaje->getId() ));
        }

        return $this->render('backend/personaje/edit.html.twig', array(
            'title' => 'Nuevo Personaje',
            'personaje' => $personaje,
            'form' => $form->createView(),
            ));
    }

    /**
     * @Route("/show/{id}", name="backend_personaje_show")
     * @Method({"GET"})
     */
    public function showAction(Request $request, Personaje $personaje){
      return $this->render('backend/personaje/show.html.twig', array(
            'personaje' => $personaje,
            ));
    }

    /**
     * @Route("/delete/{id}", name="backend_personaje_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Personaje $personaje)
    {
        $form = $this->createDeleteForm($personaje);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $personaje->setDeletedAt(new \DateTime);
            $em->flush();
          $this->addFlash('success', 'El personaje se borró exitosamente');
        }

        return $this->redirectToRoute('backend_personaje_index');
    }

    /**
     * Creates a form to delete a personaje entity.
     * @param personaje $personaje
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Personaje $personaje)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('backend_personaje_delete', array('id' => $personaje->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }


    /**
     * @Route("/ordenar", name="backend_personaje_ordenar")
     */
    public function orderAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Personaje::class);
        $personajes = $repository->getAllPersonajesCarousel();

        $form = $this->createForm(PersonajesOrdenFormType::class, ['personajes' => $personajes]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            usort($personajes, function($a, $b){
                return $a->getOrden() <=> $b->getOrden();
            });
            $form = $this->createForm(PersonajesOrdenFormType::class, ['personajes' => $personajes]);
            
            $this->addFlash('success', 'El orden de los personajes se editó exitosamente');
        }

        return $this->render('backend/personaje/orden.html.twig', 
            array('personajes' => $personajes,
                  'form' => $form->createView(),
            ));
    }
}
