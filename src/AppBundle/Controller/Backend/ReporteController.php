<?php

namespace AppBundle\Controller\Backend;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormBuilder;
use AppBundle\Form\ReporteFacturaFormFilterType;
use AppBundle\Form\ReportePedidoFormFilterType;

/**
 * Reportes controller.
 *
 * @Route("/backend/reportes")
 */

class ReporteController extends Controller
{
    /**
     * @Route("/usuarios-compras", name="backend_reporte_usuarios")
     */
    public function usuariosAction(Request $request)
    {
        $reportes_service = $this->container->get('reportes_service');
        $reporte = $reportes_service->getUsuariosReporte();

        return $reporte;
    }

    /**
     * @Route("/suscriptos", name="backend_reporte_suscriptos")
     */
    public function suscriptosAction(Request $request)
    {
        $reportes_service = $this->container->get('reportes_service');
        $reporte = $reportes_service->getSuscriptosReporte();

        return $reporte;
    }


    /**
     * @Route("/productos-ventas", name="backend_reporte_productos_compras")
     */
    public function productosVentasAction(Request $request)
    {
        $reportes_service = $this->container->get('reportes_service');
        $reporte = $reportes_service->getProductosVentasReporte();

        return $reporte;
    }

    /**
     * @Route("/productos-google-shopping", name="backend_reporte_productos_google_shopping")
     */
    public function productosGSAction(Request $request)
    {
        $reportes_service = $this->container->get('reportes_service');
        $reporte = $reportes_service->getProductosGoogleShopping();

        return $reporte;
    }


    /**
     * @Route("/facturas", name="backend_reporte_facturas")
     */
    public function facturasAction(Request $request)
    {
        $form = $this->createForm(ReporteFacturaFormFilterType::class, null, array(
            'method' => 'GET',
            ));

        $form->handleRequest($request);

        $parameters = $form->getData();

        if ($form->isSubmitted() && $form->isValid()) {
            $desde = $parameters['desde']->format('Y-m-d');
            $hasta = $parameters['hasta']->format('Y-m-d');

            return $this->redirectToRoute('backend_reporte_facturas_descargar', array(
                    'desde' => $desde,
                    'hasta' => $hasta
                )
            );
        }


        return $this->render('backend/reporte/rangoFechas.html.twig', array(
            'title' => 'Reporte de Facturas', 
            'form_filter' => $form->createView()
        ));
    }

    /**
     * @Route("/facturas/descargar", name="backend_reporte_facturas_descargar")
     */
    public function facturasDescargarAction(Request $request)
    {
        $fechaDesde = $request->query->get('desde');
        $fechaHasta = $request->query->get('hasta');


        $reportes_service = $this->container->get('reportes_service');
        $reporte = $reportes_service->getFacturasReporte($fechaDesde, $fechaHasta);

        return $reporte;
    }

    /**
     * @Route("/pedidos", name="backend_reporte_pedidos")
     */
    public function pedidosAction(Request $request)
    {
        $form = $this->createForm(ReportePedidoFormFilterType::class, null, array(
            'method' => 'GET',
            ));

        $form->handleRequest($request);

        $parameters = $form->getData();

        if ($form->isSubmitted() && $form->isValid()) {
            $desde = $parameters['desde']->format('Y-m-d');
            $hasta = $parameters['hasta']->format('Y-m-d');

            return $this->redirectToRoute('backend_reporte_pedidos_descargar', array(
                    'desde' => $desde,
                    'hasta' => $hasta
                )
            );
        }


        return $this->render('backend/reporte/rangoFechas.html.twig', array(
            'title' => 'Reporte de Pedidos',
            'form_filter' => $form->createView()
        ));
    }

    /**
     * @Route("/pedidos/descargar", name="backend_reporte_pedidos_descargar")
     */
    public function pedidosDescargarAction(Request $request)
    {
        $fechaDesde = $request->query->get('desde');
        $fechaHasta = $request->query->get('hasta');


        $reportes_service = $this->container->get('reportes_service');
        $reporte = $reportes_service->getPedidosReporte($fechaDesde, $fechaHasta);

        return $reporte;
    }

}
