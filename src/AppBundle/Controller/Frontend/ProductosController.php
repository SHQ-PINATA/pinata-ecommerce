<?php

namespace AppBundle\Controller\Frontend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Producto;
use AppBundle\Entity\Categoria;
use AppBundle\Entity\Personaje;
use AppBundle\Entity\Genero;
use AppBundle\Form\Frontend\ProductoFilterCategoriaFormType;
use AppBundle\Form\Frontend\ProductoFilterPersonajeFormType;
use AppBundle\Form\Frontend\ProductoFilterGeneroFormType;
use AppBundle\Form\Frontend\AgregarProductoCarritoFormType;
use AppBundle\Form\Frontend\ProductoFilterBusquedaFormType;
use AppBundle\Form\Frontend\ConsultarCotizacionProductoFormType;

class ProductosController extends Controller
{	
    const ITEMS_PER_PAGE = 12;
    /**
     * @Route("/categorias/{slug}", name="frontend_categorias")
     */
    public function categoriasAction(Request $request, Categoria $categoria)
    {
        $data = $this->getCategoriaData($request, $categoria);
        $form = $data['form'];

        return $this->render('frontend/productos/index.html.twig', [
            'categoria' => $categoria,
            'form_filter' => $form->createView(),
            'pagination' => $data['pagination'],
            'add_forms' => $data['add_form_views'],
            'ajax_path' => $this->generateUrl('frontend_categorias_ajax', array('slug' => $categoria->getSlug())),
        ]);
    }

    /**
     * @Route("/ajax/categorias/{slug}", name="frontend_categorias_ajax")
     */
    public function ajaxCategoriasAction(Request $request, Categoria $categoria)
    {
        $data = $this->getCategoriaData($request, $categoria);

        return $this->render('frontend/productos/ajax_listado.html.twig', [
            'categoria' => $categoria,
            'pagination' => $data['pagination'],
            'add_forms' => $data['add_form_views'],
        ]);
    }

    private function getCategoriaData(Request $request, Categoria $categoria){
        $searcher = $this->container->get('productos_searchear');
        $productos_ids = $searcher->buscarProductosIdsEnCategoria($categoria);

        $form = $this->createForm(ProductoFilterCategoriaFormType::class, null, array(
            'action' => $this->generateUrl('frontend_categorias', array('slug' => $categoria->getSlug())),
            'productos_ids' => $productos_ids,
            ));

        $form->handleRequest($request);

        $parameters = $form->getData();
        $query = $searcher->buscarProductosEnCategoriaQuery($parameters, $categoria);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query,
                $request->query->getInt('page', 1),
                self::ITEMS_PER_PAGE
            );
        
        $add_profuct_form_factory = $this->container->get('agregar_carrito_form_factory');
        $add_form_views = $add_profuct_form_factory->createFormsViewsForProducts($pagination->getItems());
        
        return array('form' => $form, 'pagination' => $pagination, 'add_form_views' => $add_form_views);
    }

    /**
     * @Route("/personajes/{slug}", name="frontend_personajes")
     */
    public function personajesAction(Request $request, Personaje $personaje)
    {
        $data = $this->getPersonajesData($request, $personaje);

        return $this->render('frontend/productos/index.html.twig', [
            'personaje' => $personaje,
            'form_filter' => $data['form_filter'],
            'pagination' => $data['pagination'],
            'add_forms' => $data['add_forms'],
            'ajax_path' => $this->generateUrl('frontend_personajes_ajax', array('slug' => $personaje->getSlug())),
        ]);
    }

    /**
     * @Route("/ajax/personajes/{slug}", name="frontend_personajes_ajax")
     */
    public function ajaxPersonajesAction(Request $request, Personaje $personaje)
    {
        $data = $this->getPersonajesData($request, $personaje);

        return $this->render('frontend/productos/ajax_listado.html.twig', [
            'personaje' => $personaje,
            'pagination' => $data['pagination'],
            'add_forms' => $data['add_forms'],            
        ]);
    }

    private function getPersonajesData(Request $request, Personaje $personaje)
    {
        $searcher = $this->container->get('productos_searchear');
        $productos_ids = $searcher->buscarProductosIdsEnPersonaje($personaje);

        $form = $this->createForm(ProductoFilterPersonajeFormType::class, null, array(
            'action' => $this->generateUrl('frontend_personajes', array('slug' => $personaje->getSlug())),
            'productos_ids' => $productos_ids,
            ));

        $form->handleRequest($request);

        $parameters = $form->getData();
        $query = $searcher->buscarProductosEnPersonajeQuery($parameters, $personaje);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query,
                $request->query->getInt('page', 1),
                self::ITEMS_PER_PAGE
            );

        
        $add_profuct_form_factory = $this->container->get('agregar_carrito_form_factory');
        $add_form_views = $add_profuct_form_factory->createFormsViewsForProducts($pagination->getItems());

        return array('form_filter' => $form->createView(),
                    'pagination' => $pagination,
                    'add_forms' => $add_form_views);
    }

    /**
     * @Route("/producto/{id}/{slug}", name="frontend_producto", defaults={"slug"=null})
     */
    public function detalleAction(Request $request, Producto $producto)
    {
        $add_product_form_factory = $this->container->get('agregar_carrito_form_factory');
        $add_form_view = $add_product_form_factory->createFormViewForProduct($producto);

        
        $form_cotizar_envio = $this->createForm(ConsultarCotizacionProductoFormType::class, null, array(
            'action' => $this->generateUrl('ajax_cotizar_envio_producto', ['id' => $producto->getId()])            
            ));

        $stock_manager = $this->container->get('stock_manager');
        $available_stock = $stock_manager->getAvailableStockForProduct($producto);

        return $this->render('frontend/productos/detalle.html.twig', [
            'available_stock' => $available_stock,
            'producto' => $producto,
            'form'  => $add_form_view,
            'form_cotizar_envio' => $form_cotizar_envio->createView(),
        ]);
    }


     /**
     * @Route("/genero/{slug}", name="frontend_generos")
     */
    public function generosAction(Request $request, Genero $genero)
    {
        $data = $this->getGeneroData($request, $genero);
        $form = $data['form'];

        return $this->render('frontend/productos/index.html.twig', [
            'genero' => $genero,
            'form_filter' => $form->createView(),
            'pagination' => $data['pagination'],
            'add_forms' => $data['add_form_views'],
            'ajax_path' => $this->generateUrl('frontend_generos_ajax', array('slug' => $genero->getSlug())),
        ]);
    }

    /**
     * @Route("/ajax/generos/{slug}", name="frontend_generos_ajax")
     */
    public function ajaxgenerosAction(Request $request, Genero $genero)
    {
        $data = $this->getGeneroData($request, $genero);

        return $this->render('frontend/productos/ajax_listado.html.twig', [
            'genero' => $genero,
            'pagination' => $data['pagination'],
            'add_forms' => $data['add_form_views'],
        ]);
    }

    private function getGeneroData(Request $request, Genero $genero){
        $searcher = $this->container->get('productos_searchear');
        $productos_ids = $searcher->buscarProductosIdsEnGenero($genero);

        $form = $this->createForm(ProductoFilterGeneroFormType::class, null, array(
            'action' => $this->generateUrl('frontend_generos', array('slug' => $genero->getSlug())),
            'productos_ids' => $productos_ids,
            ));

        $form->handleRequest($request);

        $parameters = $form->getData();
        $query = $searcher->buscarProductosEnGeneroQuery($parameters, $genero);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query,
                $request->query->getInt('page', 1),
                self::ITEMS_PER_PAGE
            );

        $add_profuct_form_factory = $this->container->get('agregar_carrito_form_factory');
        $add_form_views = $add_profuct_form_factory->createFormsViewsForProducts($pagination->getItems());

        return array('form' => $form, 'pagination' => $pagination, 'add_form_views' => $add_form_views);
    }

    public function tambienPuedeInteresarteAction($producto_id)
    {
        $repository = $this->container->get('productos_searchear');
        $producto = $repository->find($producto_id);

        $productos = $repository->productosRelacionadosPorPersonaje($producto);

        $add_profuct_form_factory = $this->container->get('agregar_carrito_form_factory');
        $add_form_views = $add_profuct_form_factory->createFormsViewsForProducts($productos);

        return $this->render('frontend/productos/common/tambien_puede_interesarte.html.twig', [
            'productos' => $productos,
            'add_forms' => $add_form_views,
            'title' => 'También puede interesarte',
        ]);
    }

    public function productosDestacadosAction()
    {
        $repository = $this->container->get('productos_searchear');
        $productos = $repository->getDestacados();

        $add_profuct_form_factory = $this->container->get('agregar_carrito_form_factory');
        $add_form_views = $add_profuct_form_factory->createFormsViewsForProducts($productos);

        return $this->render('frontend/productos/common/tambien_puede_interesarte.html.twig', [
            'productos' => $productos,
            'add_forms' => $add_form_views,
            'title' => '',
        ]);
    }



     /**
     * @Route("/busqueda/{search}", name="frontend_busqueda")
     */
    public function busquedaAction(Request $request, $search = null)
    {
        $data = $this->getBusquedaData($request, $search);
        $form = $data['form'];

        return $this->render('frontend/productos/index.html.twig', [
            'search' => $search,
            'form_filter' => $form->createView(),
            'pagination' => $data['pagination'],
            'add_forms' => $data['add_form_views'],
            'ajax_path' => $this->generateUrl('frontend_busqueda_ajax', array('search' => $search)),
        ]);
    }

    /**
     * @Route("/ajax/busqueda/{search}", name="frontend_busqueda_ajax")
     */
    public function ajaxBusquedaAction(Request $request, $search)
    {
        $data = $this->getBusquedaData($request, $search);

        return $this->render('frontend/productos/ajax_listado.html.twig', [
            'pagination' => $data['pagination'],
            'add_forms' => $data['add_form_views'],
        ]);
    }

    private function getBusquedaData(Request $request, $term){
        $searcher = $this->container->get('productos_searchear');
        $productos_ids = $searcher->buscarProductosIdsEnBusqueda($term);

        $form = $this->createForm(ProductoFilterBusquedaFormType::class, null, array(
            'action' => $this->generateUrl('frontend_busqueda', array('search' => $term)),
            'productos_ids' => $productos_ids,
            ));

        $form->handleRequest($request);

        $parameters = $form->getData();
        $query = $searcher->buscarProductosEnBusquedaQuery($parameters, $term);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query,
                $request->query->getInt('page', 1),
                self::ITEMS_PER_PAGE
            );

        $add_profuct_form_factory = $this->container->get('agregar_carrito_form_factory');
        $add_form_views = $add_profuct_form_factory->createFormsViewsForProducts($pagination->getItems());

        return array('form' => $form, 'pagination' => $pagination, 'add_form_views' => $add_form_views, 'search' => $term);
    }


    /**
     * @Route("/sale", name="frontend_sale")
     */
    public function saleAction(Request $request)
    {
        $data = $this->getSaleData($request);
        $form = $data['form'];

        return $this->render('frontend/productos/index.html.twig', [
            'form_filter' => $form->createView(),
            'pagination' => $data['pagination'],
            'add_forms' => $data['add_form_views'],
            'is_sale' => true,
            'ajax_path' => $this->generateUrl('frontend_sale_ajax'),
        ]);
    }

    /**
     * @Route("/ajax/sale", name="frontend_sale_ajax")
     */
    public function ajaxSaleAction(Request $request)
    {
        $data = $this->getSaleData($request);

        return $this->render('frontend/productos/ajax_listado.html.twig', [
            'pagination' => $data['pagination'],
            'add_forms' => $data['add_form_views'],
        ]);
    }

    private function getSaleData(Request $request){
        $searcher = $this->container->get('productos_searchear');
        $productos_ids = $searcher->buscarProductosIdsEnSale();

        $form = $this->createForm(ProductoFilterBusquedaFormType::class, null, array(
            'action' => $this->generateUrl('frontend_sale'),
            'productos_ids' => $productos_ids,
            ));

        $form->handleRequest($request);

        $parameters = $form->getData();
        $query = $searcher->buscarProductosEnSaleQuery($parameters);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query,
                $request->query->getInt('page', 1),
                self::ITEMS_PER_PAGE
            );
        
        $add_profuct_form_factory = $this->container->get('agregar_carrito_form_factory');
        $add_form_views = $add_profuct_form_factory->createFormsViewsForProducts($pagination->getItems());
        
        return array('form' => $form, 'pagination' => $pagination, 'add_form_views' => $add_form_views);
    }
}
