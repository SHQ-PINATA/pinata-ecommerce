<?php

namespace AppBundle\Controller\Frontend;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Producto;
use AppBundle\Entity\Usuario as Usuario;
use AppBundle\Entity\CarritoResponse;
use AppBundle\Entity\Envio;
use AppBundle\Form\Frontend\AgregarProductoCarritoFormType;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Form\Frontend\DescuentoFormType;
use AppBundle\Form\Frontend\ConsultarCotizacionProductoFormType;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;


/**
 * @Route("/ajax")
 */

class AjaxController extends Controller
{

    /**
     * @Route("/add_to_cart/{id}", name="ajax_add_to_cart")
     * @Method({"POST"})
     */
    public function addToCartAction(Request $request, Producto $producto)
    {
        
		$form = $this->createForm(AgregarProductoCarritoFormType::class, array('producto_id' => $producto->getId()), array(
			'action' => $this->generateUrl('ajax_add_to_cart', array('id' => $producto->getId())),
			'max' => null,
			));
		
		$form->handleRequest($request);
        
        $added = false;
        $result = null;
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $cantidad = $data['cantidad'];

            $carrito_manager = $this->container->get('carrito_manager');
            $result = $carrito_manager->addToCarrito($producto, $cantidad);
        }

        $res = $this->getResponseForCartAction($result);

    	return new JsonResponse($res);
    }

    /**
     * @Route("/modify_cart/{id}", name="ajax_modify_quantity_to_cart")
     * @Method({"POST"})
     */
    public function modifyQuantityCartAction(Request $request, Producto $producto){
        $form = $this->createForm(AgregarProductoCarritoFormType::class, array('producto_id' => $producto->getId()), array(
            'action' => $this->generateUrl('ajax_modify_quantity_to_cart', array('id' => $producto->getId())),
            'max' => null,
            ));
        
        $form->handleRequest($request);
        
        $result = null;
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $cantidad = $data['cantidad'];

            $carrito_manager = $this->container->get('carrito_manager');
            $result = $carrito_manager->modifyQuantityCarrito($producto, $cantidad);
        }

        $res = $this->getResponseForCartAction($result);

        return new JsonResponse($res);
    }

    private function getResponseForCartAction(CarritoResponse $result = null){
        if (!is_null($result)) {
            return $result->getArrayResponse();
        }

        return array('status' => 'error', 'error' => 'Formulario Inválido');
    }
    
    /**
     * @Route("/delete_from_cart/{id}", name="ajax_delete_cart")
     * @Method({"POST"})
     */
    public function deleteFromCartAction(Request $request, Producto $producto){

        $carrito_manager = $this->container->get('carrito_manager');
        $result = $carrito_manager->deleteFromCarrito($producto);

        return new JsonResponse($result->getArrayResponse());
    }

    /**
     * @Route("/get_cart", name="ajax_get_cart")
     * @Method({"GET"})
     */
    public function getCartAction(Request $request) {
        $carritoManager = $this->container->get('carrito_manager');
        $productoItems = $carritoManager->getCurrentCarrito();
        $cartData = array();

        foreach($productoItems as $productoItem) {
            $precio = $productoItem['producto']->getPrecioOferta() ? $productoItem['producto']->getPrecioOferta() : $productoItem['producto']->getPrecio();
            $cartData[]= array(
                'cantidad'  => $productoItem['item']->getCantidad(),
                'nombre'    => $productoItem['producto']->getNombre(),
                'precio'    => $precio,
            );
        }

        return new JsonResponse($cartData);
    }

    /**
     * @Route("/get_cart_count", name="ajax_get_cart_count")
     * @Method({"GET"})
     */
    public function carritoCountAction(Request $request)
    {
        $carrito_manager = $this->container->get('carrito_manager');
        $count = $carrito_manager->getCurrentCarritoCount();

        return new JsonResponse($count);
    }    

    /**
     * @Route("/envios/cotizar/domicilio", name="ajax_envios_cotizar_domicilio")
     * @Method({"GET"})
     */
    public function envioCotizarDomicilioAction(Request $request){
        $provincia_id = $request->query->get('provincia_id');
        $codigo_postal = $request->query->get('codigo_postal');
        
        if ( !($provincia_id && $codigo_postal) ){
            return new JsonResponse(['status' => 'error', 'error' => 'No se seleccionó una Provincia o no se ingresó el código postal']);
        }

        $cotizador = $this->container->get('envio_cotizador');
        $result = $cotizador->cotizarEnvioDomicilio($provincia_id, $codigo_postal);

        return new JsonResponse($result);
    }

    /**
     * @Route("/envios/cotizar/sucursal", name="ajax_envios_cotizar_sucursal")
     * @Method({"GET"})
     */
    public function envioCotizarSucursalAction(Request $request){
        $provincia_id = $request->query->get('provincia_id');
        $localidad_id = $request->query->get('localidad_id');
        
        if ( !($provincia_id && $localidad_id) ){
            return new JsonResponse(['status' => 'error', 'error' => 'No se seleccionó una Provincia o Localidad']);
        }

        $cotizador = $this->container->get('envio_cotizador');
        $result = $cotizador->cotizarEnvioSucursal($provincia_id, $localidad_id);

        return new JsonResponse($result);
    }

    /**
     * @Route("/envios/localidades", name="ajax_envios_localidades")
     * @Method({"GET"})
     */
    public function envioGetLocalidadesAction(Request $request){

        $enviopack = $this->container->get('enviopack');
        $result = $enviopack->getLocalidadesByProvincia( $request->query->get('provincia_id') );

        return new JsonResponse($result);
    }

    /**
     * @Route("/tracking", name="ajax_mi_cuenta_tracking")
     * @Method({"GET"})
     */
    public function miCuentaGetTrackingAction(Request $request){

        $tracking_service = $this->container->get('envio_tracking');
        $result = $tracking_service->getByTrackingNumber( $request->query->get('tracking_number') );

        return new JsonResponse($result);
    }

    /**
     * @Route("/fblogin", name="ajax_fblogin")
     * @Method({"POST"})
     */
    public function fbLoginAction(Request $request)
    {
	$postedData = json_decode($request->request->all());
	$email = $request->request->get('email');;
	$em = $this->getDoctrine()->getManager();
	$user = $em->getRepository(Usuario::class)->findOneByEmail($email);
	
	if ($user) {
 	    $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
            $this->get('security.token_storage')->setToken($token);
            $this->get('session')->set('_security_main', serialize($token));
	    $res = true;	
	} else {
	    $user = new Usuario();
	    $user->setEmail($email);
	    $fullName = $request->request->get('nombre');
	    $names = explode(" ", $fullName);
	    $user->setNombre($names[0]);
	    $user->setApellido(array_pop($names));
	    $user->setPlainPassword($request->request->get('id'));
	    $em->persist($user);
	    $em->flush();

 	    $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
            $this->get('security.token_storage')->setToken($token);
            $this->get('session')->set('_security_main', serialize($token));
	    $res = true;	
	}
	
        return new JsonResponse(json_encode($res));
    }


    /**
     * @Route("/check-descuento", name="ajax_check_descuento")
     * @Method({"POST"})
     */
    public function checkDescuentoAction(Request $request)
    {
        $form = $this->createForm(DescuentoFormType::class);
        
        $form->handleRequest($request);
        
        $res = ['applied' => false, 'msg' => 'El Formulario es inválido'];
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $codigo = $data['codigo'];

            $carrito_manager = $this->container->get('carrito_manager');

            $carrito_id = $carrito_manager->getCarritoId();
            $precio_items = $carrito_manager->getCarritoPrecioTotal();
            $items = $carrito_manager->getCarritoItems();

            $envio = $this->getDoctrine()->getManager()->getRepository(Envio::class)->getLastEnvioByCarritoId($carrito_manager->getCarritoId());
            $precio_envio = $envio->getPrecio();

            $descuento_manager = $this->container->get('descuento_manager');
            $res = $descuento_manager->applyDescuento($carrito_id, $codigo, $items, $precio_envio);

            if ($res['applied']) {
                $res['total_con_descuento'] = $precio_items + $precio_envio - $res['monto_descontado'];
            }
        }

        return new JsonResponse($res);
    }

    /**
     * @Route("/check-365/{numeroTarjeta}", name="ajax_check_365")
     * @Method({"GET"})
     */
    public function checkTarjeta365($numeroTarjeta, Request $request)
    {
        $rawURL = $this->getParameter('clarin_365_validate_url');
        $url = $rawURL . $numeroTarjeta;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);

        return new JsonResponse($response);
    }

    /**
     * @Route("/check-andes/{numeroTarjeta}", name="ajax_check_andes")
     * @Method({"GET"})
     */
    public function checkTarjetaAndes($numeroTarjeta, Request $request)
    {
        $rawURL = $this->getParameter('andes_pass_validate_url');
        $url = $rawURL . $numeroTarjeta;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);

        $valida = preg_match_all('#<\s*?Vigente\b[^>]*>(.*?)</Vigente\b[^>]*>#s',$response);

        $msg = $valida ? '' : 'Credencial inválida';

        $response = array(
            'valida' => $valida, 
            'msg' => $msg,
        );

        return new JsonResponse($response);
    }

    /**
     * @Route("/check-la-voz/{numeroTarjeta}", name="ajax_check_la_voz")
     * @Method({"GET"})
     */
    public function checkTarjetaLaVoz($numeroTarjeta, Request $request)
    {
        $rawURL = $this->getParameter('la_voz_validate_url');
        $url = $rawURL . $numeroTarjeta;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);

        $response = json_decode($response);

        $msg = $response->resultado ? '' : 'Credencial inválida';

        $response = array(
            'valida' => $response->resultado, 
            'msg' => $msg,
        );

        return new JsonResponse($response);
    }


    /**
     * @Route("/get-365-code", name="ajax_get_365_code")
     * @Method({"GET"})
     */
    public function get365Code(Request $request)
    {
        $code = $this->getParameter('clarin_365_code');
        return new JsonResponse($code);
    }

    /**
     * @Route("/get-andes-code", name="ajax_get_andes_code")
     * @Method({"GET"})
     */
    public function getAndesCode(Request $request)
    {
        $code = $this->getParameter('andes_pass_code');
        return new JsonResponse($code);
    }

    /**
     * @Route("/get-la-voz-code", name="ajax_get_la_voz_code")
     * @Method({"GET"})
     */
    public function getLaVozCode(Request $request)
    {
        $code = $this->getParameter('la_voz_code');
        return new JsonResponse($code);
    }


    /**
     * @Route("/cotizar-envio-producto/{id}", name="ajax_cotizar_envio_producto")
     * @Method({"POST"})
     */
    public function cotizarEnvioProductoAction(Request $request, Producto $producto){
        $form = $this->createForm(ConsultarCotizacionProductoFormType::class);
        
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $provincia_codigo = $data['provincia']->getCodigo();
            $codigo_postal = $data['codigoPostal'];
            $peso = $producto->getPeso();
            $enviopack = $this->container->get('enviopack');
            $cotizaciones = $enviopack->getCotizacionPorProvincia($provincia_codigo, $codigo_postal, $peso);

            $result = ['status' => 'ok', 'cotizaciones' => $cotizaciones];
        }elseif ($form->isSubmitted() && !$form->isValid()) {
            $result = ['status' => 'error', 'msg' => 'Formulario Inválido' ];
        }

        return new JsonResponse($result);
    }    
}
