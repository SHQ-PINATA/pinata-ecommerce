<?php

namespace AppBundle\Controller\Frontend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use AppBundle\Entity\Pedido;
use AppBundle\Form\Frontend\DatosPersonalesFormType;
use AppBundle\Presenter\PedidoPresenter;
use AppBundle\Presenter\FacturaPresenter;
/**
 * MiCuenta controller.
 *
 * @Route("/cuenta")
 */
class MiCuentaController extends Controller
{
    /**
     * @Route("/pedidos", name="frontend_cuenta_pedidos")
     */
    public function pedidosAction(Request $request)
    {
        $usuario = $this->getUser();
        if (!$usuario) {
            throw $this->createNotFoundException('No se encontró el usuario');
        }

    	$em = $this->getDoctrine()->getManager();
        $pedidos = $em->getRepository(Pedido::class)->getByUsuario($usuario);
        
        $presenters = [];
        foreach ($pedidos as $pedido) {
            $presenters[$pedido->getId()] = new PedidoPresenter($pedido);
        }

        return $this->render('frontend/cuenta/pedidos.html.twig', [
            'activo' => 'pedidos',
            'pedidos' => $pedidos,
            'presenters' => $presenters,
        ]);
    }

    /**
     * @Route("/datos-personales", name="frontend_cuenta_datos_personales")
     */
    public function datosPersonalesAction(Request $request)
    {
        $usuario = $this->getUser();
        if (!$usuario) {
            throw $this->createNotFoundException('No se encontró el usuario');
        }

        $form = $this->createForm(DatosPersonalesFormType::class, $usuario);
        $form->handleRequest($request);
        
        $data_modified = false;
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $data_modified = true;
        }

        return $this->render('frontend/cuenta/datos.html.twig', [
            'activo' => 'datos_personles',
            'data_modified' => $data_modified,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/consultar-envios", name="frontend_cuenta_tracking")
     */
    public function consultarEnviosAction(Request $request)
    {
        $usuario = $this->getUser();
        if (!$usuario) {
            throw $this->createNotFoundException('No se encontró el usuario');
        }

        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('ajax_mi_cuenta_tracking'))
            ->add('tracking', null, ['label' => 'Numero de Seguimiento'])
            ->setMethod('GET')
            ->getForm();

        return $this->render('frontend/cuenta/consultar_envios.html.twig', [
            'activo' => 'consultar_envios',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/factura/{id}", name="frontend_cuenta_factura")
     */
    public function facturaShowAction(Pedido $pedido)
    {   
        $factura = $pedido->getFactura();
        $factura_manager = $this->container->get('factura_manager');
        $usuario = $this->getUser();

        $processed = $factura_manager->facturaIsProcessed($factura);
        $belongsToUser = $factura_manager->facturaBelongsToUser($factura, $usuario);
        if ( !$processed || !$belongsToUser) {
            return $this->redirectToRoute('frontend_cuenta_pedidos');
        }

        $factura_presenter = new FacturaPresenter($factura, $pedido);
        $afipConfig = $this->getParameter('afip');
        
        $html = $factura_manager->getFacturaAsHTML($factura_presenter, $afipConfig);
        $filename = 'Piñata Factura Pedido '.$pedido->getId().'.pdf';
        return new Response($this->get('knp_snappy.pdf')->getOutputFromHtml($html),
                200,
                array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'attachment; filename="'.$filename.'"'
                )
            );
    }
}
