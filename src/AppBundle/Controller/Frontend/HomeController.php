<?php

namespace AppBundle\Controller\Frontend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\BloqueBanner;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
    	$em = $this->getDoctrine()->getManager();
        $bloques = $em->getRepository(BloqueBanner::class)->getBloquesActivos();

        $viewerManager = $this->container->get('bloque_banner_viewer');
        $bloques = $viewerManager->fuseFullscreenBloques($bloques);

        return $this->render('frontend/homepage/homepage.html.twig', [
        	'bloques' => $bloques,
        ]);
    }

     /**
     * @Route("/check_health", name="check_health")
     */
    public function healtAction(){
        return new Response();
    }	

     /**
     * @Route("/categorias/cybermonday", name="cybermonday_cace_redirect")
     */
    public function cybermondayCaceRedirect(){
        return $this->redirectToRoute('homepage');
    }	
    /**
     * @Route("/test_dafiti", name="test_dafiti")
     */
    public function dafitiAction(){

        $dafitiService = $this->container->get('dafiti');
        $dafitiService->refreshStock();
        die();

    }	

   
}
