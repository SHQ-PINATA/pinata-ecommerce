<?php

namespace AppBundle\Controller\Frontend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\CarritoProductoItem;
use AppBundle\Entity\Envio;
use AppBundle\Entity\EnvioDomicilio;
use AppBundle\Entity\EnvioSucursalCorreo;
use AppBundle\Entity\EnvioPickup;
use AppBundle\Entity\Pedido;
use AppBundle\Entity\DescuentoAplicado;
use AppBundle\Entity\Descuento;
use AppBundle\Entity\MercadoPagoOrder;
use AppBundle\Entity\PedidoItem;
use AppBundle\Presenter\PedidoPresenter;
use AppBundle\Form\Frontend\AgregarProductoCarritoFormType;
use AppBundle\Form\Frontend\EnvioDomicilioFormType;
use AppBundle\Form\Frontend\EnvioSucursalCorreoFormType;
use AppBundle\Form\Frontend\EnvioPickupFormType;
use AppBundle\Form\Frontend\PaymentType;
use AppBundle\Form\Frontend\DescuentoFormType;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use YourNameSpace\UserBundle\Entity\User;

/**
 * Checkout controller.
 *
 * @Route("/checkout")
 */
class CheckoutController extends Controller
{
    /**
     * @Route("/carrito", name="frontend_checkout_carrito")
     */
    public function carritoAction(Request $request)
    {
        $carrito_manager = $this->container->get('carrito_manager');
        $carrito_id = $carrito_manager->getCarritoId();
        $productos_borrados = $carrito_manager->updateReservation();
        
        $producto_items = $carrito_manager->getCurrentCarrito();

        $productos = array_map(function($item){ return $item['producto'];}, $producto_items);

        $em = $this->getDoctrine()->getManager();
        $reservados_por_producto = $em->getRepository(CarritoProductoItem::class)->getStockReservadoByProductos($productos, $this->getParameter('app.interval_reserve_minutes_cart'));
        $descuentoAplicado = $em->getRepository(DescuentoAplicado::class)->findOneByCarritoId($carrito_id);

        if ($descuentoAplicado) {
            $em->remove($descuentoAplicado);
            $em->flush();
        }

        $forms = array();
        $total = 0;
        foreach ($producto_items as $producto_id => $producto_item) {
            $data = array('producto_id' => $producto_id,
                          'cantidad' => $producto_item['item']->getCantidad() 
                    );
            
            $form = $this->createForm(AgregarProductoCarritoFormType::class, $data, array(
                'action' => $this->generateUrl('ajax_modify_quantity_to_cart', array('id' => $producto_id)),
                'max' => null,
                ));
            
            $forms[$producto_id] = $form->createView();

            $precio = !is_null($producto_item['producto']->getPrecioOferta()) ? $producto_item['producto']->getPrecioOferta() : $producto_item['producto']->getPrecio();
            $total += $precio * $producto_item['item']->getCantidad();
        }


        $response = $this->render('frontend/checkout/carrito.html.twig', [
            'activo' => 'carrito',
            'producto_items' => $producto_items,
            'forms' => $forms,
            'total' => $total,
        ]);

        $response->setPrivate();
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('no-store', true);

        return $response;
    }

    /**
     * @Route("/envio", name="frontend_checkout_envio")
     */
    public function envioAction(Request $request){
        
        $redirect = $this->getRedirectionIfCarritoNotReservedOrEmpty();
        if ($redirect) {
            return $redirect;
        }

        $enviopack = $this->container->get('enviopack');
        $envio_manager = $this->container->get('envio_manager');
        $em = $this->getDoctrine()->getManager();
        $usuario = $this->getUser();

        $ultimoEnvioDomicilio = $em->getRepository(EnvioDomicilio::class)->getLastEnvioByUsuario($usuario);
        $domicilioData = $envio_manager->cloneEnvio($ultimoEnvioDomicilio);
        $form_domicilio = $this->createForm(EnvioDomicilioFormType::class, $domicilioData, array(
                'action' => $this->generateUrl('frontend_checkout_envio'),
                ));
        
        $ultimoEnvioSucursal = $em->getRepository(EnvioSucursalCorreo::class)->getLastEnvioByUsuario($usuario);
        $sucursalData = $envio_manager->cloneEnvio($ultimoEnvioSucursal);
        $form_sucursal = $this->createForm(EnvioSucursalCorreoFormType::class, $sucursalData, array(
                'action' => $this->generateUrl('frontend_checkout_envio'),
                'enviopack' => $enviopack,
                ));

        $ultimoEnvioPickup = $em->getRepository(EnvioPickup::class)->getLastEnvioByUsuario($usuario);
        $pickupData = $envio_manager->cloneEnvio($ultimoEnvioPickup);
        $form_pickup = $this->createForm(EnvioPickupFormType::class, $pickupData, array(
                'action' => $this->generateUrl('frontend_checkout_envio'),
                ));

        $forms = [$form_domicilio, $form_sucursal, $form_pickup];

        foreach ($forms as &$form) {
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                
                $carrito_manager = $this->container->get('carrito_manager');
                $carrito_id = $carrito_manager->getCarritoId();

                $user = $this->getUser();
                $envio = $form->getData();
                $envio->setUsuario($user);
                $envio->setCarritoId($carrito_id);
                $envio->setCreatedAt(new \Datetime('now'));
                $envio->setPedido(null);

                $cotizador = $this->container->get('envio_cotizador');
                $cotizador->setDataFromCotizacion($envio);
                
                $em->persist($envio);
                $em->flush();
                
                return $this->redirectToRoute('frontend_checkout_pago');
            }  
        }

        $domicilio_submitted = $form_domicilio->isSubmitted();
        $sucursal_submitted = $form_sucursal->isSubmitted();
        $pickup_submitted = $form_pickup->isSubmitted();
        
        return $this->render('frontend/checkout/envio.html.twig', [
            'activo' => 'envio',
            'form_domicilio' => $form_domicilio->createView(),
            'form_sucursal' => $form_sucursal->createView(),
            'form_pickup' => $form_pickup->createView(),
            'domicilio_submitted' => $domicilio_submitted,
            'sucursal_submitted' => $sucursal_submitted,
            'pickup_submitted' => $pickup_submitted,
        ]);
    }

    /**
     * @Route("/pago", name="frontend_checkout_pago")
     */
    public function pagoAction(Request $request){
        $redirect = $this->getRedirectionIfCarritoNotReservedOrEmpty();
        if ($redirect) {
            return $redirect;
        }

        $carrito_manager = $this->container->get('carrito_manager');
        
        $em = $this->getDoctrine()->getManager();
        $envio = $em->getRepository(Envio::class)->getLastEnvioByCarritoId($carrito_manager->getCarritoId());

        $redirect = $this->getRedirectionIfEnvioInconsistent($envio);
        if ($redirect) {
            return $redirect;
        }

        $precio_envio = $envio->getPrecio();
        $precio_items = $carrito_manager->getCarritoPrecioTotal();

        $carrito_items = $carrito_manager->getCarritoItems();
        $carrito_id = $carrito_manager->getCarritoId();

        $redirect = $this->getRedirectionIfDescuentoInconsistent($carrito_id, $carrito_items, $precio_envio);
        if ($redirect) {
            return $redirect;
        }
        
        $descuentoAplicado = $em->getRepository(DescuentoAplicado::class)->getByCarritoId($carrito_id);
        $monto_descuento = $descuentoAplicado ? $descuentoAplicado->getMontoDescontado() : 0;

        $precio_total = $precio_items + $precio_envio - $monto_descuento;

        $form_descuento = $this->createForm(DescuentoFormType::class, null, array(
                'action' => $this->generateUrl('ajax_check_descuento'),
                ));

        $form = $this->createForm(PaymentType::class, null, array(
                'action' => $this->generateUrl('frontend_checkout_pago'),
                'monto' => $precio_total,
                ));

        $form->handleRequest( $request );

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $dni = $data['DNI'] ? $data['DNI'] : null;
            $tarjeta365 = $data['clarin365'] ? $data['clarin365'] : null;
            $usuario = $this->getUser();

            // Creo el pedido, modifico el envio para que no apunte al carrito
            // Modifico el stock, el stock en producto y borro el carrito
            $pedido_manager = $this->container->get('pedido_manager');
            $pedido = $pedido_manager->makePedido($carrito_items, $envio, $usuario, $dni, $tarjeta365);

            $descuento_manager = $this->container->get('descuento_manager');
            $descuento_manager->updateWithPedido($descuentoAplicado, $pedido);

            $montoAPagar = $pedido->getPrecioTotalItems() + $precio_envio - $monto_descuento;
            if ($montoAPagar > 0) {
                switch ($data['payment']) {
                case 'Mercadopago':
                default:
                $mp = $this->container->get('mercadopago');
                $mpUrl = $mp->checkout($pedido, $precio_envio, $usuario, $monto_descuento);

                return $this->redirect($mpUrl);
                }
            } else {
                $pedido_manager->setPedidoAndOrderAsPaid($pedido->getId(), null);
                return $this->redirect($this->generateUrl('frontend_checkout_pago_success', array('external_reference'=>$pedido->getId())));
            }
        }

        return $this->render('frontend/checkout/pago.html.twig', [
            'activo' => 'pago',
            'items' => $carrito_items,
            'precio_envio' => $precio_envio,
            'precio_items' => $precio_items,
            'precio_total' => $precio_total,
            'envio_is_pickup' => $envio->isPickup(),
            'monto_descuento' => $monto_descuento,
            'form' => $form->createView(),
            'form_descuento' => $form_descuento->createView(),
        ]);
    }


    /**
     * @Route("/pago/exito", name="frontend_checkout_pago_success")
     */
    public function pagoSuccessAction(Request $request){
        $pedidoId = $request->query->get('external_reference');

        $repository = $this->getDoctrine()->getRepository(Pedido::class);
        $repositoryMPO = $this->getDoctrine()->getRepository(MercadoPagoOrder::class);
        $repositoryPedidoItem = $this->getDoctrine()->getRepository(PedidoItem::class);
        $pedido = $repository->findOneById($pedidoId);

        $pedidoItems = $repositoryPedidoItem->findByPedido($pedido);

        $descuentoAplicado = $pedido->getDescuentoAplicado();

        if ($descuentoAplicado) {
            $codigo = $descuentoAplicado->getCodigo();
            $descuento = $this->getDoctrine()->getRepository(Descuento::class)->getByCodigo($codigo);
            $precioTotalItems = $pedido->getPrecioTotalItems();
            $montoDescontado = $descuentoAplicado->getMontoDescontado();
            $montoDisponible = $descuento->getMontoDisponible(); 


            if ($montoDisponible !== null) {
                $montoRestante = $montoDisponible - $montoDescontado;
                $descuento->setMontoDisponible($montoRestante);
                $entityManager = $this->getDoctrine()->getManager();

                $entityManager->persist($descuento);
                $entityManager->flush();
            }
        }

        $itemsGA = array();


        //Items para google Analytics
        foreach ($pedidoItems as $pedidoItem) {
            $pedido = $pedidoItem->getPedido();
            $producto = $pedidoItem->getProducto();
            $sku = substr(substr($producto->getCodigoEan(), -7), 0, 4); 

            $item = array();                
            $item['id']         = strval($pedido->getId());
            $item['name']       = $producto->getNombre();
            $item['sku']        = strval($sku);
            $item['category']   = $producto->getCategoria()->getNombre();
            $item['price']      = strval($pedidoItem->getPrecio());
            $item['quantity']   = strval($pedidoItem->getCantidad());
            $itemsGA[] = $item;
        }

        $e = $pedido->getEnvio();
        $envioType = $e->getType();
        $envio = $e->getInstanceByType($envioType);
		$pedidoPresenter = new PedidoPresenter();
        $pedidoPresenter->setPedido($pedido);

        $baseImponible = $pedidoPresenter->getPrecioTotal() / 1.21;
        $baseImponibleRedondeado = round($baseImponible, 2, PHP_ROUND_HALF_EVEN);
        $importeIva = $baseImponible * 0.21;
        $importeIvaRedondeado = round($importeIva, 2, PHP_ROUND_HALF_EVEN);


        $transaccionGA = array(
            'id'            => strval($pedido->getId()),
            'affiliation'   => 'Piñata Web',
            'revenue'       => strval($pedidoPresenter->getPrecioTotal()),
            'shipping'      => strval($pedidoPresenter->getPrecioEnvio()),
            'tax'           => strval($importeIvaRedondeado), 
            'currency'      => 'ARS'
        );


        $facebookPixelContents = array();

        //Items para FacebookPixel
        foreach ($pedidoItems as $pedidoItem) {
            $pedido = $pedidoItem->getPedido();
            $producto = $pedidoItem->getProducto();
            $sku = substr(substr($producto->getCodigoEan(), -7), 0, 4); 

            $item['sku']        = strval($sku);
            $item['quantity']   = strval($pedidoItem->getCantidad());

            $facebookPixelContents[] = $item;
        }

        $facebookPixelData = array(
            'value'         => strval($pedidoPresenter->getPrecioTotal()),
            'currency'      => 'ARS',
            'contents'      => $facebookPixelContents,
            'content_type'  => 'product'
        );

        

        if ($pedido && $pedido->getUsuario()->getId() == $this->getUser()->getId()) {
           return $this->render('frontend/checkout/pagoReturn.html.twig', [
                'action'            => 'success',
                'transaccionGA'     => json_encode($transaccionGA, true),
                'itemsGA'           => json_encode($itemsGA, true),
                'facebookPixelData' => json_encode($facebookPixelData, true),
                'pedido_id'         => $pedido->getId(),
           ]);
            
        }

        return $this->redirectToRoute('homepage');

    }

    /**
     * @Route("/pago/pendiente", name="frontend_checkout_pago_pending")
     */
    public function pagoPendingAction(Request $request){
       return $this->render('frontend/checkout/pagoReturn.html.twig', [
            'action' => 'pendiente',
        ]);
    }

    /**
     * @Route("/pago/error", name="frontend_checkout_pago_error")
     */
    public function pagoErrorAction(Request $request){
        $pedidoId = $request->query->get('external_reference');

        $repository = $this->getDoctrine()->getRepository(Pedido::class);
        $pedido = $repository->findOneById($pedidoId);
        $precio_envio = $pedido->getEnvio()->getPrecio();
        $usuario = $this->getUser();

        if ($pedido && $pedido->getUsuario()->getId() == $usuario->getId()){
            $mp = $this->container->get('mercadopago');
            $mpUrl = $mp->checkout($pedido, $precio_envio, $usuario);

            return $this->render('frontend/checkout/pagoReturn.html.twig', [
                'action' => 'error',
                'mpUrl' => $mpUrl,
            ]);

        }

        return $this->redirectToRoute('homepage');

    }

    /**
     * @Route("/compra", name="frontend_checkout")
     */
    public function checkoutAction(Request $request){

        $dafitiService = $this->container->get('dafiti');
        $latestPurchases = $dafitiService->refreshStock();

        //LOGIN - REGISTER
        
        $usuario = $this->getUser();

        $loginHasErrors = false;
        $registerHasErrors = false;

        if (!$usuario) {
            $login_response = $this->forward('FOSUserBundle:Security:login', array( $request ));
            $register_response = $this->forward('FOSUserBundle:Registration:register', array( $request ));

            if (strpos($register_response, 'validation-error-container') !== false) {
                $registerHasErrors = true;
            } 

            if (strpos($login_response, 'validation-error-container') !== false) {
                $loginHasErrors = true;
            } 

            $loginForm = $login_response->getContent();
            $registerForm = $register_response->getContent();
            
            $usuario = $this->getUser();

            if ($usuario) {
                $token = new UsernamePasswordToken($usuario, null, 'main', $usuario->getRoles());
                $this->get('security.token_storage')->setToken($token);
                $this->get('session')->set('_security_main', serialize($token));
                return $this->redirect($request->getUri());
            }

        } else {
            $loginForm = null;
            $registerForm = null;
        }

        //ENVIO 
        
        $enviopack = $this->container->get('enviopack');
        $envio_manager = $this->container->get('envio_manager');
        $em = $this->getDoctrine()->getManager();

        if ($usuario) {
            $redirect = $this->getRedirectionIfCarritoNotReservedOrEmpty();
            if ($redirect) {
                return $redirect;
            }

            $ultimoEnvioDomicilio = $em->getRepository(EnvioDomicilio::class)->getLastEnvioByUsuario($usuario);
            $domicilioData = $envio_manager->cloneEnvio($ultimoEnvioDomicilio);
            $formEnvioDomicilio = $this->createForm(EnvioDomicilioFormType::class, $domicilioData, array(
                'action' => $this->generateUrl('frontend_checkout'),
            ));

            $ultimoEnvioSucursal = $em->getRepository(EnvioSucursalCorreo::class)->getLastEnvioByUsuario($usuario);
            $sucursalData = $envio_manager->cloneEnvio($ultimoEnvioSucursal);
            $formEnvioSucursal = $this->createForm(EnvioSucursalCorreoFormType::class, $sucursalData, array(
                'action' => $this->generateUrl('frontend_checkout'),
                'enviopack' => $enviopack,
            ));

            $ultimoEnvioPickup = $em->getRepository(EnvioPickup::class)->getLastEnvioByUsuario($usuario);
            $pickupData = $envio_manager->cloneEnvio($ultimoEnvioPickup);
            $formEnvioPickup = $this->createForm(EnvioPickupFormType::class, $pickupData, array(
                'action' => $this->generateUrl('frontend_checkout'),
            ));

            $formEnvioDomicilioView = $formEnvioDomicilio->createView();
            $formEnvioSucursalView = $formEnvioSucursal->createView();
            $formEnvioPickupView = $formEnvioPickup->createView();

            $formsEnvio = [$formEnvioDomicilio, $formEnvioSucursal, $formEnvioPickup];

            foreach ($formsEnvio as &$form) {
                $form->handleRequest($request);
                if ($form->isSubmitted() && $form->isValid()) {

                    $carritoManager = $this->container->get('carrito_manager');
                    $carritoId = $carritoManager->getCarritoId();

                    $user = $this->getUser();
                    $envio = $form->getData();
                    $envio->setUsuario($user);
                    $envio->setCarritoId($carritoId);
                    $envio->setCreatedAt(new \Datetime('now'));
                    $envio->setPedido(null);

                    $cotizador = $this->container->get('envio_cotizador');
                    $cotizador->setDataFromCotizacion($envio);

                    $em->persist($envio);
                    $em->flush();
                } 
            }

            $domicilio_submitted = $formEnvioDomicilio->isSubmitted();
            $sucursal_submitted = $formEnvioSucursal->isSubmitted();
            $pickup_submitted = $formEnvioPickup->isSubmitted();

            //CHECK IF DESCUENTO GLOBAL AND APPLY
            $global = $this->getDoctrine()->getManager()->getRepository(Descuento::class)->getCurrentGlobal();

            if ($global) {
                $carrito_manager = $this->container->get('carrito_manager');
                $carrito_id = $carrito_manager->getCarritoId();
                $items = $carrito_manager->getCarritoItems();

                $envio = $this->getDoctrine()->getManager()->getRepository(Envio::class)->getLastEnvioByCarritoId($carrito_manager->getCarritoId());
                $precio_envio = $envio ? $envio->getPrecio() : 0;

                $descuento_manager = $this->container->get('descuento_manager');
                $descuento_manager->applyDescuento($carrito_id, $global->getCodigo(), $items, $precio_envio);
            }

        } else {
            $formEnvioDomicilioView = null;
            $formEnvioSucursalView = null;
            $formEnvioPickupView = null;

            $domicilio_submitted = null;
            $sucursal_submitted = null;
            $pickup_submitted = null;
        }


        //Pago

        $carritoManager = $this->container->get('carrito_manager');
        $envio = $em->getRepository(Envio::class)->getLastEnvioByCarritoId($carritoManager->getCarritoId());



        $precioEnvio = $envio ? $envio->getPrecio() : 0;
        $envioIsPickup = $envio ? $envio->isPickup() : 0;

        $precioItems = $carritoManager->getCarritoPrecioTotal();

        $carritoItems = $carritoManager->getCarritoItems();
        $carritoId = $carritoManager->getCarritoId();

        $descuentoAplicado = $em->getRepository(DescuentoAplicado::class)->getByCarritoId($carritoId);
        $montoDescuento = $descuentoAplicado ? $descuentoAplicado->getMontoDescontado() : 0;

        $precioTotal = $precioItems + $precioEnvio - $montoDescuento;

        $formDescuento = $this->createForm(DescuentoFormType::class, null, array(
            'action' => $this->generateUrl('ajax_check_descuento'),
        ));

        $formPago = $this->createForm(PaymentType::class, null, array(
            'action' => $this->generateUrl('frontend_checkout'),
            'monto' => $precioTotal,
        ));

        $formPago->handleRequest( $request );

        if ($formPago->isSubmitted() && $formPago->isValid()) {
            $redirect = $this->getRedirectionIfCarritoNotReservedOrEmpty();
            if ($redirect) {
                return $redirect;
            }
            $data = $formPago->getData();
            $dni = $data['DNI'] ? $data['DNI'] : null;
            $tarjeta365 = $data['clarin365'] ? $data['clarin365'] : null;
            $usuario = $this->getUser();

            $pedidoManager = $this->container->get('pedido_manager');
            $pedido = $pedidoManager->makePedido($carritoItems, $envio, $usuario, $dni, $tarjeta365);

            $descuentoManager = $this->container->get('descuento_manager');
            $descuentoManager->updateWithPedido($descuentoAplicado, $pedido);

            $montoAPagar = $pedido->getPrecioTotalItems() + $precioEnvio - $montoDescuento;
            if ($montoAPagar > 0) {
                switch ($data['payment']) {
                case 'Mercadopago':
                default:
                $mp = $this->container->get('mercadopago');
                $mpUrl = $mp->checkout($pedido, $precioEnvio, $usuario, $montoDescuento);

                return $this->redirect($mpUrl);
                }
            } else {
                $pedido_manager->setPedidoAndOrderAsPaid($pedido->getId(), null);
                return $this->redirect($this->generateUrl('frontend_checkout_pago_success', array('external_reference'=>$pedido->getId())));
            }
        }

        $productosBorrados = $carritoManager->updateReservation();
        $productoItems = $carritoManager->getCurrentCarrito();
        $productos = array_map(function($item){ return $item['producto'];}, $productoItems);
        $em = $this->getDoctrine()->getManager();
        $reservadosPorProducto = $em->getRepository(CarritoProductoItem::class)->getStockReservadoByProductos($productos, $this->getParameter('app.interval_reserve_minutes_cart'));
        $total = 0;

        foreach ($productoItems as $productoId => $productoItem) {
            $data = array(
                'producto_id' => $productoId,
                'cantidad' => $productoItem['item']->getCantidad() 
            );

            $precio = !is_null($productoItem['producto']->getPrecioOferta()) ? $productoItem['producto']->getPrecioOferta() : $productoItem['producto']->getPrecio();
            $total += $precio * $productoItem['item']->getCantidad();
        }


        return $this->render('frontend/checkout/compra.html.twig', [
            'productoItems'        => $productoItems,
            'total'                 => $total,
            'loginForm'             => $loginForm,
            'registerForm'          => $registerForm,
            'formEnvioDomicilio'    => $formEnvioDomicilioView,
            'formEnvioSucursal'     => $formEnvioSucursalView,
            'formEnvioPickup'       => $formEnvioPickupView,
            'formPago'              => $formPago->createView(),
            'domicilioSubmitted'    => $domicilio_submitted,
            'sucursalSubmitted'     => $sucursal_submitted,
            'pickupSubmitted'       => $pickup_submitted,
            'envioSubmitted'        => $domicilio_submitted || $sucursal_submitted || $pickup_submitted ? true : false,
            'precioEnvio'           => $precioEnvio,
            'precioItems'           => $precioItems,
            'precioTotal'           => $precioTotal,
            'envioIsPickup'         => $envioIsPickup,
            'montoDescuento'        => $montoDescuento,
            'formDescuento'         => $formDescuento->createView(),
            'loginHasErrors'        => $loginHasErrors,
            'registerHasErrors'     => $registerHasErrors,
        ]);
    }


    private function getRedirectionIfCarritoNotReservedOrEmpty(){
        $carrito_manager = $this->container->get('carrito_manager');
        $productos_borrados = $carrito_manager->updateReservation();
        if (count($productos_borrados)) {
            $this->get('session')->getFlashBag()->clear();
            $this->addFlash('carrito_error', 'No se cuenta con stock de los siguientes productos que agregaste a tu carrito: '.implode(', ', $productos_borrados));
            return $this->redirectToRoute('frontend_checkout_carrito');
        }

        if(0 == $carrito_manager->getCurrentCarritoCount()){
            return $this->redirectToRoute('frontend_checkout_carrito');
        }

        return null;
    }

    private function getRedirectionIfEnvioInconsistent($envio){
        $cotizador = $this->container->get('envio_cotizador');
        
        if (!$envio || !$cotizador->envioIsValid($envio)) {
            $this->get('session')->getFlashBag()->clear();
            $this->addFlash('envio_error', 'Actualice el envío elegido por favor');
            return $this->redirectToRoute('frontend_checkout');
        }
        return null;
    }

    private function getRedirectionIfDescuentoInconsistent($carrito_id, $carrito_items, $precio_envio){
        $descuento_manager = $this->container->get('descuento_manager');
        if (!$descuento_manager->appliedDescuentoIsOk($carrito_id, $carrito_items, $precio_envio)) {
            $descuento_manager->deleteDescuentoAplicado($carrito_id);
            return $this->redirectToRoute('frontend_checkout_pago');
        }
        
        return null;
    }
}
