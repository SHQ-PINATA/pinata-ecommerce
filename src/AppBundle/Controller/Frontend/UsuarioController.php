<?php

namespace AppBundle\Controller\Frontend;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Usuario controller.
 *
 */

class UsuarioController extends Controller
{
    /**
     * @Route("/ingresar", name="frontend_login")
     * @Method({"GET", "POST"}) 
     */
    public function loginAndRegisterAction(Request $request){

        $redirect = $this->redirectIfLogged();
        if ($redirect) {
            return $redirect;
        }

        $login_response = $this->forward('FOSUserBundle:Security:login', array( $request ));
        $register_response = $this->forward('FOSUserBundle:Registration:register', array( $request ));

        // Al registrarme exitosamente, me redirige a la pagina de chequeo de mail.
        // Devuelvo esa redirección directamente si es el caso.
        if ($register_response instanceof RedirectResponse) {
            return $register_response;
        }

        return $this->render('frontend/usuario/login_register.html.twig', array(
            'login' => $login_response->getContent(),
            'register' => $register_response->getContent(),
            ));
    }

    private function redirectIfLogged(){
        $authChecker = $this->container->get('security.authorization_checker');
        $router = $this->container->get('router');

        if ($authChecker->isGranted('ROLE_ADMIN')) {
            return new RedirectResponse($router->generate('backend_dashboard_index'), 307);
        } 

        if ($authChecker->isGranted('ROLE_USER') ) {
            return new RedirectResponse($router->generate('homepage'), 307);
        }

        return false;
    }

}
