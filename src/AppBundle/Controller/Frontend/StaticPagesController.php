<?php

namespace AppBundle\Controller\Frontend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\Frontend\ConsultaFormType;

class StaticPagesController extends Controller
{
    /**
     * @Route("/como-registrarse", name="frontend_static_registrarse")
     */
    public function registrarseAction(){
        return $this->render('frontend/static/registrarse.html.twig', [
            ]);
    }

    /**
     * @Route("/como-comprar", name="frontend_static_comprar")
     */
    public function comprarAction(){
        return $this->render('frontend/static/comprar.html.twig', [
            ]);
    }

    /**
     * @Route("/envios", name="frontend_static_envios")
     */
    public function enviosAction(){
        return $this->render('frontend/static/envios.html.twig', [
            ]);
    }

    /**
     * @Route("/politicas-de-cambio", name="frontend_static_cambios")
     */
    public function cambiosAction(){
        return $this->render('frontend/static/cambios.html.twig', [
            ]);
    }

    /**
     * @Route("/conocenos", name="frontend_static_conocenos")
     */
    public function conocenosAction(){
        return $this->render('frontend/static/conocenos.html.twig', [
            ]);
    }

    /**
     * @Route("/consultas", name="frontend_static_consultas")
     */
    public function consultasAction(Request $request){
        $form = $this->createForm(ConsultaFormType::class, null,array(
                'action' => $this->generateUrl('frontend_static_consultas'),
                ));
        
        $form->handleRequest($request);
        
        $sent = false;
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            $consulta = $form->getData();
            
            $em->persist($consulta);
            $em->flush();
            $sent = true;
        }  


        return $this->render('frontend/static/consultas.html.twig', [
            'form' => $form->createView(),
            'sent' => $sent,
            ]);
    }

    /**
     * @Route("/politica-privacidad", name="frontend_static_privacidad")
     */
    public function privacidadAction(){
        return $this->render('frontend/static/privacidad.html.twig', [
            ]);
    }

    /**
     * @Route("/terminos-y-condiciones", name="frontend_static_terminos")
     */
    public function terminosAction(){
        return $this->render('frontend/static/terminos.html.twig', [
            ]);
    }

    /**
     * @Route("/faq", name="frontend_static_faq")
     */
    public function faqAction(){
        return $this->render('frontend/static/faq.html.twig', [
        ]);
    }

    /**
     * @Route("/formas-de-pago", name="frontend_static_formas_pago")
     */
    public function formasDePagoAction(){
        return $this->render('frontend/static/formas_pago.html.twig', [
            ]);
    }

    /**
     * @Route("/postales-navidad", name="frontend_static_postales_navidad")
     */
    public function postalesAction(){
        return $this->render('frontend/static/postales.html.twig', [
            ]);
    }
    /**
     * @Route("/comerciales", name="frontend_static_comerciales")
     */
    public function comercialesAction(){
        return $this->render('frontend/static/comerciales.html.twig', [
            ]);
    }

}
