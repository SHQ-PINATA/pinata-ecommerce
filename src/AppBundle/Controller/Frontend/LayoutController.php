<?php

namespace AppBundle\Controller\Frontend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use AppBundle\Entity\Personaje;
use AppBundle\Entity\BloqueBanner;
use AppBundle\Entity\Categoria;
use AppBundle\Entity\NewsletterEmail;
use AppBundle\Entity\Producto;
use AppBundle\Entity\Descuento;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Form\Frontend\NewsletterEmailFormType;
use AppBundle\Form\Frontend\BusquedaProductosFormType;

class LayoutController extends Controller
{
    public function marquesinaAction(){
        $em = $this->getDoctrine()->getManager();        
        $marquesinaTop = $em->getRepository(BloqueBanner::class)->getMarquesinaTop();
        return $this->render('frontend/layout/marquesina.html.twig', [
            'bloque' => $marquesinaTop,
            ]);
        
    }

    public function marquesinaMobileAction(){
        $em = $this->getDoctrine()->getManager();        
        $marquesinaTop = $em->getRepository(BloqueBanner::class)->getMarquesinaTopMobile();
        return $this->render('frontend/layout/marquesina.html.twig', [
            'bloque' => $marquesinaTop,
            ]);
    }


    public function personajesAction(){
        $em = $this->getDoctrine()->getManager();        
        $personajes = $em->getRepository(Personaje::class)->getAllPersonajesCarousel();
        return $this->render('frontend/layout/personajes.html.twig', [
            'personajes' => $personajes,
            ]);
        
    }

    public function categoriasAction($active = null)
    {
        $em = $this->getDoctrine()->getManager(); 
        $categorias = $em->getRepository(Categoria::class)->getAllCategorias();

        $em->getRepository(Producto::class)->setParam($this->getParameter('app.interval_reserve_minutes_cart'));
        $mostrarSale = (bool) $em->getRepository(Producto::class)->countProductosEnSaleQuery();

        return $this->render('frontend/layout/categorias.html.twig', [
            'categorias' => $categorias,
            'mostrarSale' => $mostrarSale,
            'active' => $active,
        	]);
    }

    public function carritoAction(Request $request)
    {
        $carrito_manager = $this->container->get('carrito_manager');
        $count = $carrito_manager->getCurrentCarritoCount();

        return $this->render('frontend/layout/carrito.html.twig', [
            'item_count' => $count,
            ]);
    }    

    public function carritoCountAction(Request $request)
    {
        $carrito_manager = $this->container->get('carrito_manager');
        $count = $carrito_manager->getCurrentCarritoCount();

        return $this->render('frontend/layout/carritoCount.html.twig', [
            'item_count' => $count,
            ]);
    }    



    public function searchAction($search){
        return $this->render('frontend/layout/search.html.twig', [
            'form' => $this->getSearchForm($search)->createView(),
        ]);   
    }

    /**
     * @Route("/buscar", name="frontend_search")
     */
    public function newSearchAction(Request $request){
        $form = $this->getSearchForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $search = $form->getData()['search'];

            if ($search) {
                return $this->redirectToRoute('frontend_busqueda', array('search' => $search));
            }
        }

        return $this->redirectToRoute('homepage');
    }

    private function getSearchForm($search = null){
         return $this->createForm(BusquedaProductosFormType::class, ['search' => $search], array(
            'action' => $this->generateUrl('frontend_search'),
            'method' => 'POST',
            ));
    }


    public function subscribeAction(){
        return $this->render('frontend/layout/suscribe.html.twig', [
            'form_subscribe' => $this->getSuscribeForm()->createView(),
        ]);
    }

    public function subcribeModalAction(){
        return $this->render('frontend/common/subscribe_modal.html.twig', [
            'form_subscribe' => $this->getSuscribeForm()->createView(),
        ]);
    }

    /**
     * @Route("/suscribirse", name="frontend_suscribe")
     * @Method("POST")
     */
    public function newSuscribeAction(Request $request){
        $form = $this->getSuscribeForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $newsletterEmail = $form->getData();
            $em = $this->getDoctrine()->getManager(); 
            $existingEmail = $em->getRepository(NewsletterEmail::class)->getByEmail($newsletterEmail->getEmail());
            if (!$existingEmail) {

                $res = ['status' => 'ok', 'msg' => 'Se agregó tu email al newsletter'];
                $res = ['status' => 'ok', 'msg' => var_dump($response)];


                $descuentoBase = $em->getRepository(Descuento::class)->findOneById(2);

                $descuento = clone $descuentoBase;
                $descuento->generarCodigo('NEWS-');
                $em->persist( $descuento );
                $em->flush();

                $em->persist($newsletterEmail);
                $em->flush();


                //ICOMM
                $icommData = $this->getParameter('icomm');
                $response = $this->sendToICOMM($newsletterEmail->getEmail(), $icommData);

                $this->enviarEmailNewsletter($newsletterEmail->getEmail(), $descuento );

            }else{
                $res = ['status' => 'ok', 'msg' => 'Ya estabas suscripto al newsletter'];
                $existingEmail->setActivo(true);
                $em->persist($existingEmail);
                $em->flush();
            }

        }else{
            $res = ['status' => 'error', 'msg' => 'Ingresá tu email'];
        }

        return new JsonResponse($res);
    }

    protected function sendToICOMM($email, $icommData) {
        $url = $icommData['newsletter_url'];
        $apiKey = $icommData['api_key'];
        $profileKey = $icommData['profile_key'];

        $sendData = array(
            'ProfileKey'    => $profileKey,
            'Contact'       => array(
                'Email'         => $email,
                'CustomData'    => array(),
            ),
        );

        $ch = curl_init();
        try {
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Authorization: '.$apiKey.':',
                'Content-Type: application/json'
            ));

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($sendData));

            $result = curl_exec($ch);
        } catch(\Exception $e) {
            die(var_dump($e->getMessage()));
        }
        die(var_dump($result));

        return $result;
    }

    protected function enviarEmailNewsletter($send_to, $descuento) {
        
    
        $mail = (new \Swift_Message('Gracias por Suscribirte'))
                ->setFrom(array( $this->getParameter('mailer_user') => $this->getParameter('mailer_send_name') ))
                ->setTo($send_to);


        $mail->setBody(
            $this->get('templating')->render(
                'frontend/emails/newsletter.html.twig',
                array('descuento' => $descuento )
            ), 'text/html'
        );

        $sent = $this->get('mailer')->send($mail);


    }

    private function getSuscribeForm(){
        return $this->createForm(NewsletterEmailFormType::class, null, array(
            'action' => $this->generateUrl('frontend_suscribe'),
            'method' => 'POST',
            ));
    }
}
