<?php

namespace AppBundle\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use AppBundle\Entity\Pedido;
/**
 * MercadoPagoNotifications controller.
 *
 * @Route("/api")
 */

class MercadoPagoNotificationsController extends Controller
{
    /**
     * @Route("/mpnotifications", name="api_mercadopago_notifications")
     * @Method({"POST", "GET"})
     */
    public function notificationsAction(Request $request)
    {   
        $mp = $this->container->get('mercadopago');
        $logger = $this->get('logger');

        $id    = $request->query->get('id');
        $topic = $request->query->get('topic');

        if ($id && $topic) {

            $logger->alert('[MercadoPago Notificacion Attrs]', ['id' => $id, 'topic' => $topic]);

            $result = $mp->getInfoForNotification($id, $topic);
            $logger->alert('[MercadoPago Notificacion]', $result);

            if ('ok' == $result['status']) {
                $preferenceId   = $result['preference_id'];
                $merchantId     = $result['merchant_id'];
                $merchantStatus = $result['merchant_status'];
                $mp->setMerchantOrderByPreferenceId($preferenceId, $merchantId, $merchantStatus);
            }

            if ('ok' == $result['status'] && $result['paid']) {
                $pedido_manager = $this->container->get('pedido_manager');
                $pedido_manager->setPedidoAndOrderAsPaid($result['pedido_id'], $result['preference_id']);

                $pedido = $this->getDoctrine()->getManager()->getRepository(Pedido::class)->getById($result['pedido_id']);
                $pedido_mailer = $this->container->get('pedidos_mailer');
                if($pedido && !$pedido_mailer->hasCompraMailSent($pedido)){
                    $pedido_mailer->sendCompraMail($pedido);
                }

                $logger->alert('[MercadoPago Notificacion Pedido Pagado]');
            }elseif ('ok' == $result['status'] && !$result['paid']) {
                $pedido_manager = $this->container->get('pedido_manager');
                $paidToNotPaid = $pedido_manager->setPedidoAndOrderAsNotPaid($result['pedido_id'], $result['merchant_status'], $result['preference_id']);
                $logger->alert('[MercadoPago Notificacion Pedido #'.$result['pedido_id'].' No Pagado]');
                if ($paidToNotPaid) {
                    $this->sendEmailManualCheck($result['pedido_id']);
                }
            }

            $http_status = ('ok' == $result['status']) ? Response::HTTP_CREATED : Response::HTTP_OK;
            return new JsonResponse($result, $http_status);
        } else {
            return new JsonResponse('', Response::HTTP_OK);
        }
    }

    private function sendEmailManualCheck($pedido_id){
        $mailer = $this->get('mailer');
        $sendTo = $this->getParameter('check_pedido.mail');
        
        $url = $this->generateUrl('backend_pedido_show',array('id' => $pedido_id), UrlGeneratorInterface::ABSOLUTE_URL);
        $body  = "El pedido #".$pedido_id." pasó del estado 'Pagado' a otro estado y requiere control manual\n";
        $body .= "Link: ".$url." \n";

        $mail = new \Swift_Message('Requerimiento de control Manual del pedido #'.$pedido_id);
        $mail->setTo($sendTo);
        $mail->setBody($body);

        // El servidor de SMTP hace timeout cada tanto...
        $sent = 0;
        for ($i=0; $i < 5 && 0 == $sent; $i++) { 
            try {
                $sent = $mailer->send($mail);
            } catch (Exception $e) {
                $logger = $this->get('logger');
                $logger->alert($e);
            }
        }
    }
  
}
