<?php

namespace AppBundle\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use AppBundle\Entity\Pedido;
/**
 * Mercadolibre controller.
 *
 * @Route("/api/mercadolibre")
 */

class MercadolibreController extends Controller
{
    /**
     * @Route("/init", name="api_mercadolibre_init")
     * @Method({"POST", "GET"})
     */
    public function initAction(Request $request)
    {   
        $mercadolibreService = $this->get('mercadolibre');
        $redirectURL = $mercadolibreService->getAuthURL();

        return $this->redirect($redirectURL);
    }

    /**
     *
     * @Route("/get_access_token", name="api_mercadolibre_access_token")
     *
     */

    public function getAccessTokenAction(Request $request)
    {
        $code = $request->query->get('code');
        $mercadolibreService = $this->get('mercadolibre');
        $host = $redirectURL = $this->getParameter('host');
        $redirectURI = $host . '/api/mercadolibre/get_access_token';

        $mercadolibreService->getFirstAccessToken($code, $redirectURI); 
        return $this->redirect($this->generateUrl('homepage'));
    }

    /**
     *
     * @Route("/get_notification", name="api_mercadolibre_notification")
     * @Method({"POST", "GET"})
     */

    public function getNotificationAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $params = json_decode($request->getContent());
        $mercadolibreService = $this->get('mercadolibre');
        $stockManager = $this->get('stock_manager');

        $productos = $mercadolibreService->handleNotification($params);

        foreach ($productos as $producto) {
            $producto = $stockManager->getProductoWithNewStock($producto);
            $em->persist($producto);
        }

        $em->flush();

        return new Response('', 200);
    }

    /**
     *
     * @Route("/sincronizar_stock", name="api_mercadolibre_sincronizar")
     * @Method({"POST", "GET"})
     */

    public function sincronizarStockAction(Request $request)
    {
        $offset = $request->query->get('offset');
        $em = $this->getDoctrine()->getManager();
        $params = json_decode($request->getContent());

        $allMLProducts = $em->getRepository('AppBundle\Entity\Producto')->getMeliProducts();
        $productos = array_slice($allMLProducts, $offset);
        $mercadolibreService = $this->get('mercadolibre');

        $i = 0; $limit = 50;
        foreach ($productos as $product) {
            $mercadolibreService->updateStock($product, $product->getStock());
            $mercadolibreService->updatePrecio($product, $product->getPrecio());
            if ($i == $limit) {
                break;
            }	
            $i++;
        }
        return new Response('Done', 200);
    }
}
