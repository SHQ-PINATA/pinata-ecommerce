<?php

namespace AppBundle\UserBundle\EventListener;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Http\SecurityEvents;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Listener responsible to change the redirection at the end of the password resetting
 */
class LoginListener implements EventSubscriberInterface
{
    private $carrito_manager;

    public function __construct($carrito_manager)
    {
        $this->carrito_manager = $carrito_manager;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::SECURITY_IMPLICIT_LOGIN => ['onLogin', -10],
            SecurityEvents::INTERACTIVE_LOGIN => ['onLogin', -10],
        );
    }


    public function onLogin($event)
    {
        // FYI
        /*if ($event instanceof UserEvent) {
           $user = $event->getUser();
        }
        if ($event instanceof InteractiveLoginEvent) {
           $user = $event->getAuthenticationToken()->getUser();
        }*/

        $this->carrito_manager->mergeCarts();
    }
}