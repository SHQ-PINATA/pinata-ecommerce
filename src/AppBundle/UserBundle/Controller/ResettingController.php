<?php

namespace AppBundle\UserBundle\Controller;

use FOS\UserBundle\Controller\ResettingController as BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Email;



class ResettingController extends BaseController
{
	public function getParent()
    {
        return 'FOSUserBundle';
    }

     public function sendEmailAction(Request $request)
    {
        $username = $request->request->get('username');

       	$validator = $this->get('validator');
		$violations = $validator->validate($username, array(
	    	new NotBlank(),
    		new Email(),
		));

		if (0 !== count($violations)) {
	    	return new JsonResponse(array('status' => 'error'), 200);
		}

		$reset_response = parent::sendEmailAction($request);

		return new JsonResponse(array('status' => 'ok'), 200);
	}

	public function resetSuccessAction(Request $request){
		 return $this->render('@FOSUser/Registration/confirmed.html.twig', array(
    		'message' => '¡Tu nueva contraseña se <br> restableció con éxito!',
            'targetUrl' => 'homepage',
        ));
	}

}