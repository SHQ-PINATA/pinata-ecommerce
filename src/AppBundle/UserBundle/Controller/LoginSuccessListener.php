<?php

namespace AppBundle\EventListener;

use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class LoginSuccessListener implements AuthenticationSuccessHandlerInterface
{
    private $carrito_manager;

    public function __construct($carrito_manager)
    {
        $this->carrito_manager = $carrito_manager;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $this->carrito_manager->mergeCarts();
    }
}


