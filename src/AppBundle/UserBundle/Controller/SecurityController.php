<?php
namespace AppBundle\UserBundle\Controller;

use FOS\UserBundle\Controller\SecurityController as BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

class SecurityController extends BaseController
{
    public function loginAction(Request $request)
    {
        $authChecker = $this->container->get('security.authorization_checker');
        $router = $this->container->get('router');

        if ($authChecker->isGranted('ROLE_ADMIN')) {
            return new RedirectResponse($router->generate('backend_dashboard_index'), 307);
        } 

        if ($authChecker->isGranted('ROLE_USER') ) {
            return new RedirectResponse($router->generate('homepage'), 307);
        }

        return parent::loginAction($request);
    }

    public function renderLogin(array $data)
    {   
        if ('/backend/login' == $this->container->get('router')->getContext()->getPathInfo()) {
            $data['check_route'] = 'backend_login_check';
            return $this->container->get('templating')->renderResponse('backend/login/login.html.twig', $data);
        } else {
            $data['check_route'] = 'login_check';
        }

        return parent::renderLogin($data);
    }
}