<?php

namespace AppBundle\UserBundle\Controller;

use FOS\UserBundle\Controller\RegistrationController as BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RegistrationController extends BaseController
{
	public function getParent()
    {
        return 'FOSUserBundle';
    }

    public function registerAction(Request $request)
    {
		return parent::registerAction($request);
	}

	/**
     * Tell the user his account is now confirmed.
     */
    public function confirmedAction()
    {
        $user = $this->getUser();
        /*if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }*/

        return $this->render('@FOSUser/Registration/confirmed.html.twig', array(
    		'message' => '¡Tu nueva cuenta se <br> confirmó con éxito!',
            'user' => $user,
            'targetUrl' => $this->getTargetUrlFromSession(),
        ));
    }

    /**
     * @return mixed
     */
    private function getTargetUrlFromSession()
    {
        $key = sprintf('_security.%s.target_path', $this->get('security.token_storage')->getToken()->getProviderKey());

        if ($this->get('session')->has($key)) {
            return $this->get('session')->get($key);
        }
    }

    /**
     * Overwrite default FOS action with difference that we redirect to login page
     * in case the token is expired/invalid.
     *
     * => if users click the confirm link twice...
     */
    public function confirmAction(Request $request, $token)
    {
        return $this->redirectToRoute('frontend_checkout');
    }
}
