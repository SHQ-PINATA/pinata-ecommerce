<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Usuario;
use AppBundle\Entity\Producto;
use AppBundle\Entity\Factura;
use AppBundle\Entity\Pedido;
use AppBundle\Presenter\PedidoPresenter;

class ReportesService
{
	public function __construct(EntityManager $entityManager, $phpExcel, $router, $templating){
		$this->em = $entityManager;
		$this->excel = $phpExcel;
        $this->router = $router;
        $this->templating = $templating;
	}

	public function getUsuariosReporte(){

		$data = $this->em->getRepository(Usuario::class)->getUsuariosReporte();

		$hoy = new \Datetime();
		$filename = 'Informe de Usuarios '.$hoy->format('Y-m-d').'.xls';
		$excelObject = $this->excel->create($filename);
		$activeSheet = $this->excel->getActiveSheet( $excelObject );
		$activeSheet->setTitle('Informe de Usuarios');

		$this->excel->setHeader($activeSheet, 'A1', '#')
		->setHeader($activeSheet, 'B1', 'Nombre')
		->setHeader($activeSheet, 'C1', 'Apellido')
		->setHeader($activeSheet, 'D1', 'Direccion')
		->setHeader($activeSheet, 'E1', 'Telefono')
		->setHeader($activeSheet, 'F1', 'Cantidad Compras Realizadas')
		->setHeader($activeSheet, 'G1', 'Cant. ultimos 30 dias')
		->setHeader($activeSheet, 'H1', 'Cant. ultimos 90 dias')
		->setHeader($activeSheet, 'I1', 'Fecha de Registro')
		->setHeader($activeSheet, 'J1', 'Mail');

		$linea = 2;
		foreach ($data as $row) {
			$this->excel->setData( $activeSheet, 'A' . $linea, $row['id'])
			->setData( $activeSheet, 'B' . $linea, $row['nombre'])
			->setData( $activeSheet, 'C' . $linea, $row['apellido'])
			->setData( $activeSheet, 'D' . $linea, $this->getDireccion($row))
			->setData( $activeSheet, 'E' . $linea, $row['telefono'])
			->setData( $activeSheet, 'F' . $linea, $row['compras_totales'])
			->setData( $activeSheet, 'G' . $linea, $row['compras_ultimos_30'])
			->setData( $activeSheet, 'H' . $linea, $row['compras_ultimos_90'])
			->setData( $activeSheet, 'I' . $linea, $row['fecha_creacion'])
			->setData( $activeSheet, 'J' . $linea, $row['email']);
			$linea++;
		}

		$this->excel->autoWith( $activeSheet, 'A', 'J' );
		$file = tmpfile();
		$path = stream_get_meta_data($file)['uri'];

		return $this->excel->download($excelObject, $filename);
	}

	private function getDireccion($row){
		if (!$row['calle']) {
			return '';
		}

		$direccion = $row['calle'].' '. $row['numero'];
		$direccion .= isset($row['piso']) ? ' piso '.$row['piso'] : '';
		$direccion .= isset($row['depto_nro']) ? ' '.$row['depto_nro'] : '';
		$direccion .= isset($row['localidad']) ? ', '.$row['localidad'] : '';
		$direccion .= isset($row['provincia']) ? ' ('.$row['provincia'] .')': '';

		return $direccion;
	}

	public function getProductosGoogleShopping(){
		$hoy = new \Datetime('now');
		$filename = 'Google shopping '.$hoy->format('Y-m-d').'.xls';
		$excelObject = $this->excel->create($filename);
		$activeSheet = $this->excel->getActiveSheet( $excelObject );
		$activeSheet->setTitle('Planilla de google shopping');

		$productos = $this->em->getRepository(Producto::class)->getProductosGoogleShopping();

		$this->excel->setHeader($activeSheet, 'A1', 'id')
		->setHeader($activeSheet, 'B1', 'title')
		->setHeader($activeSheet, 'C1', 'description')
		->setHeader($activeSheet, 'D1', 'link')
		->setHeader($activeSheet, 'E1', 'Image_link')
		->setHeader($activeSheet, 'F1', 'availability')
		->setHeader($activeSheet, 'G1', 'price')
		->setHeader($activeSheet, 'H1', 'brand')
		->setHeader($activeSheet, 'I1', 'gtin')
		->setHeader($activeSheet, 'J1', 'condition');

		$linea = 2;
		foreach ($productos as $producto) {

            $baseURL = 'http://www.pinataweb.com.ar';
            $productoLink = $this->router->generate('frontend_producto', array('id' => $producto->getId(), 'slug' => $producto->getSlug()));
            $imageLink = '/uploads/imagenes/productos/' . $producto->getFirstImage()->getImagen();

			$this->excel->setData( $activeSheet, 'A' . $linea, $producto->getCodigoSku());
			$this->excel->setData( $activeSheet, 'B' . $linea, $producto->getNombre());
			$this->excel->setData( $activeSheet, 'C' . $linea, $producto->getDescripcion());
			$this->excel->setData( $activeSheet, 'D' . $linea, $baseURL . $productoLink);
			$this->excel->setData( $activeSheet, 'E' . $linea, $baseURL . $imageLink);
			$this->excel->setData( $activeSheet, 'F' . $linea, 'in stock');
			$this->excel->setData( $activeSheet, 'G' . $linea, $producto->getPrecio());
			$this->excel->setData( $activeSheet, 'H' . $linea, 'Piñata');
			$this->excel->setData( $activeSheet, 'I' . $linea, $producto->getCodigoEan());
			$this->excel->setData( $activeSheet, 'J' . $linea, 'new');

			$linea++;
		}

		$this->excel->autoWith( $activeSheet, 'A', 'J' );
		$file = tmpfile();
		$path = stream_get_meta_data($file)['uri'];

		return $this->excel->download($excelObject, $filename);
	}


	public function getProductosVentasReporte(){
		$hoy = new \Datetime('now');
		$filename = 'Informe Productos '.$hoy->format('Y-m-d').'.xls';
		$excelObject = $this->excel->create($filename);
		$activeSheet = $this->excel->getActiveSheet( $excelObject );
		$activeSheet->setTitle('Informe Rotacion de Productos');

		$data = $this->em->getRepository(Producto::class)->getProductosReporte();
		$ventas = [];
		foreach ($data['ventas'] as $row) {
			$ventas[$row['producto_id']][$row['year_month']] = $row['cantidad'];
		}

		// Obtengo los ultimos 12 meses desde ahora para atras (Feb (2017),	Mar (2017),	Apr (2017),	May (2017)...)
		$meses = []; $i = 11;
		$meses_headers = ['H1','I1', 'J1', 'K1', 'L1', 'M1', 'N1', 'O1', 'P1', 'Q1', 'R1', 'S1'];
		foreach ($meses_headers as $col) {
			$month = new \Datetime(date("Y-m-d", strtotime( date( 'Y-m-01' )." -$i months")));
			$meses[$col]['header'] = $month->format('M (Y)');
			$meses[$col]['month_year'] = $month->format('Y-m');
			$i--;
		}

		$this->excel->setHeader($activeSheet, 'A1', '#')
		->setHeader($activeSheet, 'B1', 'Categoria')
		->setHeader($activeSheet, 'C1', 'Subcategoria')
		->setHeader($activeSheet, 'D1', 'SKU')
		->setHeader($activeSheet, 'E1', 'EAN')
		->setHeader($activeSheet, 'F1', 'Producto')
		->setHeader($activeSheet, 'G1', 'Ultima fecha de ingreso')
		->setHeader($activeSheet, 'T1', 'Ultimos 12 meses')
		->setHeader($activeSheet, 'U1', 'Stock')
		->setHeader($activeSheet, 'V1', 'Precio')
		->setHeader($activeSheet, 'W1', 'Discontinuado');

		foreach ($meses as $col => $mes) {
			$this->excel->setHeader($activeSheet, $col, $mes['header']);
		}

		$linea = 2;
		foreach ($data['productos'] as $row) {
			$this->excel->setData( $activeSheet, 'A' . $linea, $row['id']);

			// Seteo categoria/subcategoria
			if (is_null($row['categoria_padre'])) {
				$this->excel->setData( $activeSheet, 'B' . $linea, $row['categoria']);
				$this->excel->setData( $activeSheet, 'C' . $linea, '');
			}else{
				$this->excel->setData( $activeSheet, 'B' . $linea, $row['categoria_padre']);
				$this->excel->setData( $activeSheet, 'C' . $linea, $row['categoria']);
			}

            $discontinuado = $row['discontinuado'] ? 'Sí' : null;

			// Seteo el resto de la informacion del producto
			$this->excel->setData( $activeSheet, 'D' . $linea, Producto::getSkuFromEan($row['codigo_ean']))
			->setData( $activeSheet, 'E' . $linea, $row['codigo_ean'])
			->setData( $activeSheet, 'F' . $linea, $row['nombre'])
			->setData( $activeSheet, 'G' . $linea, $row['ultimo_ingreso'])
			->setData( $activeSheet, 'U' . $linea, $row['stock'])
			->setData( $activeSheet, 'V' . $linea, $row['precio_oferta'] ? $row['precio_oferta'] : $row['precio'])
			->setData( $activeSheet, 'W' . $linea, $discontinuado);

			// Seteo la cantidad de items vendidos del producto por mes
			$producto_id = $row['id'];
			$vendidos_total = 0;
			foreach ($meses as $col => $mes) {
				$cantidad_vendida_mes = isset($ventas[$producto_id][$mes['month_year']]) ? $ventas[$producto_id][$mes['month_year']] : 0;
				$vendidos_total += $cantidad_vendida_mes;
				$this->excel->setData( $activeSheet, $col[0] . $linea, $cantidad_vendida_mes );
			}
			$this->excel->setData( $activeSheet, 'T' . $linea, $vendidos_total );

			$linea++;
		}

		$this->excel->autoWith( $activeSheet, 'A', 'X' );
		$file = tmpfile();
		$path = stream_get_meta_data($file)['uri'];

		return $this->excel->download($excelObject, $filename);
	}

	public function getFacturasReporte($fechaDesde, $fechaHasta){

		$data = $this->em->getRepository(Factura::class)->getFacturasReporte($fechaDesde, $fechaHasta);

		$hoy = new \Datetime();
		$filename = 'Informe de Facturacion '.$hoy->format('Y-m-d').'.xls';
		$excelObject = $this->excel->create($filename);
		$activeSheet = $this->excel->getActiveSheet( $excelObject );
		$activeSheet->setTitle('Informe de Facturacion');

		$this->excel
			->setHeader($activeSheet, 'A1', 'Fecha')
			->setHeader($activeSheet, 'B1', 'Comprobante')
			->setHeader($activeSheet, 'C1', 'CAE')
			->setHeader($activeSheet, 'D1', 'Vencimiento CAE')
			->setHeader($activeSheet, 'E1', 'Importe Total')
			->setHeader($activeSheet, 'F1', 'Provincia')
			->setHeader($activeSheet, 'G1', 'DNI del usuario')
			->setHeader($activeSheet, 'H1', 'Nombre del usuario');


		$linea = 2;
		foreach ($data as $row) {
            $userFullName = $row['nombre_usuario'] . ' ' . $row['apellido_usuario'];

			$this->excel->setData( $activeSheet, 'A' . $linea, date('d/m/Y H:i', strtotime($row['fecha'])) );
			$this->excel->setData( $activeSheet, 'B' . $linea, $row['comprobante']);

			$activeSheet->getCell( 'C' . $linea )->setValueExplicit($row['cae'], \PHPExcel_Cell_DataType::TYPE_STRING);

			$this->excel->setData( $activeSheet, 'D' . $linea, date('d/m/Y', strtotime($row['cae_vencimiento'])) );
			$this->excel->setData( $activeSheet, 'E' . $linea, $row['importe_total']);
			$this->excel->setData( $activeSheet, 'F' . $linea, $row['provincia']);
			$this->excel->setData( $activeSheet, 'G' . $linea, $row['dni_usuario']);
			$this->excel->setData( $activeSheet, 'H' . $linea, $userFullName);

			$linea++;
		}

		$this->excel->autoWith( $activeSheet, 'A', 'H' );
		$file = tmpfile();
		$path = stream_get_meta_data($file)['uri'];

		return $this->excel->download($excelObject, $filename);
	}

	public function getPedidosReporte($fechaDesde, $fechaHasta){

		$data = $this->em->getRepository(Pedido::class)->getPedidosReporte($fechaDesde, $fechaHasta);

		$hoy = new \Datetime();
		$filename = 'Informe de Pedidos '.$hoy->format('Y-m-d').'.xls';
		$excelObject = $this->excel->create($filename);
		$activeSheet = $this->excel->getActiveSheet( $excelObject );
		$activeSheet->setTitle('Informe de Pedidos');


		$this->excel
			->setHeader($activeSheet, 'A1', 'Fecha del pedido')
			->setHeader($activeSheet, 'B1', 'Fecha del pago')
			->setHeader($activeSheet, 'C1', 'Nombre y Apellido')
			->setHeader($activeSheet, 'D1', 'Email')
			->setHeader($activeSheet, 'E1', 'Id Pedido')
			->setHeader($activeSheet, 'F1', 'Producto')
			->setHeader($activeSheet, 'G1', 'SKU')
			->setHeader($activeSheet, 'H1', 'Categoria')
			->setHeader($activeSheet, 'I1', 'Cantidad')
			->setHeader($activeSheet, 'J1', 'Precio unitario')
			->setHeader($activeSheet, 'K1', 'Precio envío')
			->setHeader($activeSheet, 'L1', 'Código descuento')
			->setHeader($activeSheet, 'M1', 'Porcentaje de descuento')
			->setHeader($activeSheet, 'N1', 'Provincia')
            ->setHeader($activeSheet, 'O1', 'Importe abonado');


		$linea = 2;
		foreach ($data as $row) {

            $pedido = $this->em->getRepository(Pedido::class)->findOneById($row['id_pedido']);
            $presenter = new PedidoPresenter($pedido);
            
            $eanWithoutLastChar = substr($row['ean_producto'], 0, -1);
            $sku = substr($eanWithoutLastChar, -4);
            $codigoDescuento = $row['codigo_descuento'] ? $row['codigo_descuento']: '-';

            $precioUnitarioProducto = $row['precio_unitario_producto'];
            $porcentajeDescuento = $row['porcentaje_descuento'];
            $cantUnidades = $row['cantidad_producto'];
            $precioSinDescuento = $precioUnitarioProducto * $cantUnidades;

            if ($porcentajeDescuento && $row['nombre_categoria'] != 'Outlet') {
                if (!$row['es_sale'] || ($row['es_sale'] && !$row['excluye_sale'])) { 
                    $precioTotalPorProducto = $precioSinDescuento - $precioSinDescuento * $porcentajeDescuento/100;
                } else {
                    $precioTotalPorProducto = $precioSinDescuento;
                }
            } else {
                $precioTotalPorProducto = $precioSinDescuento;
            }

			$this->excel->setData( $activeSheet, 'A' . $linea, date('d/m/Y H:i', strtotime($row['fecha_pedido'])) );
			$this->excel->setData( $activeSheet, 'B' . $linea, date('d/m/Y H:i', strtotime($row['fecha_pago'])) );
			$this->excel->setData( $activeSheet, 'C' . $linea, $row['nombre_usuario'] . " " . $row['apellido_usuario'] );
			$this->excel->setData( $activeSheet, 'D' . $linea, $row['email_usuario']);
			$this->excel->setData( $activeSheet, 'E' . $linea, $row['id_pedido']);
			$this->excel->setData( $activeSheet, 'F' . $linea, $row['nombre_producto']);
			$this->excel->setData( $activeSheet, 'G' . $linea, $sku);
			$this->excel->setData( $activeSheet, 'H' . $linea, $row['nombre_categoria']);
			$this->excel->setData( $activeSheet, 'I' . $linea, $row['cantidad_producto']);
			$this->excel->setData( $activeSheet, 'J' . $linea, $row['precio_unitario_producto']);
			$this->excel->setData( $activeSheet, 'K' . $linea, $presenter->getPrecioEnvio());
			$this->excel->setData( $activeSheet, 'L' . $linea, $codigoDescuento);
			$this->excel->setData( $activeSheet, 'M' . $linea, $row['porcentaje_descuento']);
			$this->excel->setData( $activeSheet, 'N' . $linea, $row['provincia']);
			$this->excel->setData( $activeSheet, 'O' . $linea, $precioTotalPorProducto);


			$linea++;
		}

		$this->excel->autoWith( $activeSheet, 'A', 'O' );
		$file = tmpfile();
		$path = stream_get_meta_data($file)['uri'];

		return $this->excel->download($excelObject, $filename);
	}

	public function getSuscriptosReporte() {

		$data = $this->em->getRepository(Usuario::class)->getSuscriptosReporte();

		$hoy = new \Datetime();
		$filename = 'Reporte de Suscriptos '.$hoy->format('Y-m-d').'.xls';
		$excelObject = $this->excel->create($filename);
		$activeSheet = $this->excel->getActiveSheet( $excelObject );
		$activeSheet->setTitle('Reporte de Suscriptos');

		$this->excel
			->setHeader($activeSheet, 'A1', 'Email')
			->setHeader($activeSheet, 'B1', 'Fecha de adhesión');

		$linea = 2;
		foreach ($data as $row) {

			$this->excel->setData( $activeSheet, 'A' . $linea, $row['email'] );
			$this->excel->setData( $activeSheet, 'B' . $linea, $row['fecha']);

			$linea++;
		}

		$this->excel->autoWith( $activeSheet, 'A', 'B' );
		$file = tmpfile();
		$path = stream_get_meta_data($file)['uri'];

		return $this->excel->download($excelObject, $filename);
	}



}


