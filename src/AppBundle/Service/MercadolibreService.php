<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Plataforma;
use AppBundle\Entity\Producto;
use AppBundle\Entity\Stock;
use AppBundle\Entity\StockTipo;
use AppBundle\Entity\MercadolibreOrder;
use AppBundle\Resources\Meli;


class MercadolibreService
{
    CONST TOPIC_ORDER = 'orders';
    CONST TOPIC_ITEMS = 'items';


    protected $em;
    protected $mercadoLibreConfig = array();
    protected $dataConfiguracion  = array();
    protected $host;

    public function __construct($entityManager, $host, $mercadoLibreConfig)
    {
        $this->em                 = $entityManager;
        $this->host               = $host;
        $this->mercadoLibreConfig = $mercadoLibreConfig;
    }

    protected function getClient($accessToken = null, $refreshToken = null)
    {
        return new Meli($this->mercadoLibreConfig['client_id'], $this->mercadoLibreConfig['client_secret'], $accessToken, $refreshToken);
    }

    public function getAuthURL() {
        $clientId = $this->mercadoLibreConfig['client_id'];

        $meli = $this->getClient();
        $redirectURL = $meli->getAuthUrl("",Meli::$AUTH_URL['MLA']);

        return $redirectURL;
    }

    public function getFirstAccessToken($code, $redirectURI) {
         $response = $this->getClient()->authorize(
            $code, $redirectURI 
        );

        return $this->updateDataConfiguracion($response);
    }

    public function getAccessToken()
    {
        if ( !count( $this->dataConfiguracion ) ) {
            $plataformaConfiguracionRepo = $this->em->getRepository('AppBundle\Entity\PlataformaConfiguracion');
            $plataformaConfiguracion     = $plataformaConfiguracionRepo->findOneByPlataforma( Plataforma::MERCADOLIBRE );
            $this->dataConfiguracion = $plataformaConfiguracion->getDataConfiguracion();
        }

        // Si no tiene configuracion
        if ( empty($this->dataConfiguracion) ) {
            return false;
        }

        if ( $this->dataConfiguracion['expires'] < time() ) {
            // Si expiro se obtiene un nuevo access token
            return $this->refreshAccessToken();
        } else {
            // Si no, se retorna el access token actual
            return $this->dataConfiguracion['access_token'];
        }
    }

    public function refreshAccessToken()
    {
        $accessToken = $this->dataConfiguracion['access_token'];
        $refreshToken = $this->dataConfiguracion['refresh_token'];
        
        $response = $this->getClient($accessToken, $refreshToken)->refreshAccessToken();

        $this->updateDataConfiguracion($response);

        if ( isset( $response['body']->access_token ) ) {
            return $response['body']->access_token;
        } else {
            return false;
        }
    }

    protected function updateDataConfiguracion($response)
    {
        $plataformaConfiguracionRepo = $this->em->getRepository('AppBundle\Entity\PlataformaConfiguracion');
        $plataformaConfiguracion     = $plataformaConfiguracionRepo->findOneByPlataforma( Plataforma::MERCADOLIBRE );

        
        if ( isset($response['body']) && isset( $response['body']->access_token ) ) {

            $userId = $response['body']->user_id;

            $dataConfiguracion = array(
                'access_token'  => $response['body']->access_token,
                'refresh_token' => $response['body']->refresh_token,
                'expires'       => time() + $response['body']->expires_in,
                'user_id'       => $userId,
                'access_token'  => $response['body']->access_token
            );

            $plataformaConfiguracion->setDataConfiguracion( $dataConfiguracion );

            $this->dataConfiguracion = $dataConfiguracion;

            $this->em->persist($plataformaConfiguracion);
            $this->em->flush();

            return true;
        }

        return false;
    }

    public function updateStock(Producto $producto, $stock) {
        $this->updateAttribute($producto, 'available_quantity', $stock);
    }

    public function updatePrecio(Producto $producto, $precio) {
        $this->updateAttribute($producto, 'price', $precio);
    }

    public function updateAttribute(Producto $producto, $name, $value) {
        $meliId = $producto->getMeliId();

        if ($meliId) {
            $accessToken = $this->getAccessToken();       
            $params = array('access_token' => $accessToken);
            $body = array($name => $value);

            $meli = $this->getClient();
            $response = $meli->put("/items/$meliId", $body, $params);
        }
    }

    private function getResource($resource, $accessToken) {
        $params = array('access_token' => $accessToken);
        $meli = $this->getClient();
        $result = $meli->get($resource, $params);  

        return $result;
    }

    //NOTIFICATIONS

    public function handleNotification($params) {
        $topic = $params->topic;
        $accessToken = $this->getAccessToken();

        $productos = array();
        switch ($topic) {
            case MercadolibreService::TOPIC_ORDER:
                $resource = $this->getResource($params->resource, $accessToken);
                $meliOrder = $this->em->getRepository(MercadolibreOrder::class)->findOneByMeliOrderId($resource['body']->id);
                if (($resource['body']->status == 'paid' || $resource['body']->status == 'confirmed') && !$meliOrder) {
                    foreach($resource['body']->order_items as $item) {
                        $itemObj = $item->item;
                        $meliId = $itemObj->id;
                        $stockDiff = 0 - $item->quantity;
                        $producto = $this->em->getRepository(Producto::class)->findOneByMeliId($meliId);
                        if ($producto) {
                            $stockTipo = $this->em->getRepository(StockTipo::class)->getByCodigo(StockTipo::CODIGO_MERCADOLIBRE);

                            $meliOrder = new MercadolibreOrder();
                            $meliOrder->setProductoId($producto->getId());
                            $meliOrder->setMeliOrderId($resource['body']->id);
                            $meliOrder->setCantidad($item->quantity);
                            $this->em->persist($meliOrder);
                            $this->em->flush();
                            $this->updateLocalStock($producto, $stockDiff, $stockTipo);
                            $productos[] = $producto;
                        }
                    }
                }   
                break;
            case MercadolibreService::TOPIC_ITEMS:
                $resource = $this->getResource($params->resource, $accessToken);
                $meliId = $resource['body']->id;
                $meliStock = $resource['body']->available_quantity;

                $producto = $this->em->getRepository(Producto::class)->findOneByMeliId($meliId);
                if ($producto) {
                    $stockTipo = $this->em->getRepository(StockTipo::class)->getByCodigo(StockTipo::CODIGO_OTRO);
                    $currentStock = $producto->getStock();
                    $stockDiff = $meliStock - $currentStock;
                    if ($stockDiff) {
                        $this->updateLocalStock($producto, $stockDiff, $stockTipo);
                    }
                    $productos[]= $producto;
                } 
                break; 
        }
        return $productos;
    }

    private function updateLocalStock(Producto $producto, $stockDiff, StockTipo $stockTipo) {
        $stock = new Stock();
        $stock->setProducto($producto);
        $stock->setCantidad($stockDiff);
        $stock->setStockTipo($stockTipo);
        $producto->setStock($producto->getStock() + $stockDiff);
        $this->em->persist($stock);
        $this->em->flush();
    }
}
