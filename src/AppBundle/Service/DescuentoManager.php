<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Descuento;
use AppBundle\Entity\DescuentoAplicado;
use AppBundle\Entity\CarritoProductoItem;

class DescuentoManager
{
    private $em;

	function __construct(EntityManager $entityManager)
	{
		$this->em = $entityManager;
	}

	public function applyDescuento($carritoId, $codigo, $items, $precio_envio){
		$descuento = $this->em->getRepository(Descuento::class)->getByCodigo($codigo);
		
		$canUse = $this->canUseDescuento($descuento, $carritoId, $items, $precio_envio);
		if(!$canUse['success']){
			return ['applied' => false, 'msg' => $canUse['msg']];
		}

		$montoDecuento = $canUse['monto_descontado'];
		$descuentoAplicado = $this->em->getRepository(DescuentoAplicado::class)->getByCarritoId($carritoId);
		if ($this->getsBetterDescuento($descuentoAplicado, $montoDecuento)) {
			if (!$descuentoAplicado) {
				$descuentoAplicado = new DescuentoAplicado;
				$descuentoAplicado->setCarritoId($carritoId);
			}
		
			$descuentoAplicado->setCodigo($descuento->getCodigo());
			$descuentoAplicado->setMontoDescontado($montoDecuento);
			$descuentoAplicado->setTipo($descuento->getTipo());

			$this->em->persist($descuentoAplicado);
			$this->em->flush();
			return ['applied' => true, 'msg' => 'El descuento fue aplicado exitosamente', 'monto_descontado' => $montoDecuento];
		}

		return ['applied' => false, 'msg' => 'Ya cuentas con un descuento mejor'];;
	}

	public function appliedDescuentoIsOk($carritoId, $items, $precio_envio){
		$descuentoAplicado = $this->em->getRepository(DescuentoAplicado::class)->getByCarritoId($carritoId);
		if (!$descuentoAplicado) {
			return true;
		}

		$descuento = $this->em->getRepository(Descuento::class)->getByCodigo($descuentoAplicado->getCodigo());
		
		$result = $this->canUseDescuento($descuento, $carritoId, $items, $precio_envio);
		return $result['success'] && (abs($result['monto_descontado'] - $descuentoAplicado->getMontoDescontado()) < 0.01);
	}

	public function deleteDescuentoAplicado($carritoId){
		$descuentoAplicado = $this->em->getRepository(DescuentoAplicado::class)->getByCarritoId($carritoId);
		$this->em->remove($descuentoAplicado);
		$this->em->flush();
	}

	private function canUseDescuento($descuento, $carritoId, $items, $precio_envio){
		if (!$descuento) {
			return ['success' => false, 'msg' => 'Código Inválido'];
		}
		// Chequear que si es de uso unico no exista un descuento aplicado con pedido id seteado
		if($descuento->getUsoUnico()){
			if (0 < $this->em->getRepository(DescuentoAplicado::class)->countUsedByCodigo($descuento->getCodigo())){
				return ['success' => false, 'msg' => 'El descuento ya fue usado'];
			}
		}

		// Obtengo los items sobre los que se puede aplicar el descuento
		$personajes  = $descuento->getPersonajes();
		$categorias  = $descuento->getCategorias();
		$excluirSale = $descuento->getExcluirSale();
		$items_aplicables_descuento = $this->em->getRepository(CarritoProductoItem::class)->getByCategoriasPersonajesCarritoId($carritoId, $excluirSale, $personajes, $categorias);

		$precio_items = 0;
		foreach ($items_aplicables_descuento as $item) {
			$producto = $item->getProducto();
            $precio = $producto->getPrecioOferta() ? $producto->getPrecioOferta() : $producto->getPrecio();
            $precio_items += ($item->getCantidad() * $precio);
		}

		// Obtengo el monto que se descontaría. Si es 0 no aplica el descuento.
		$montoDescontado = $this->getMontoDescontar($descuento, $precio_items, $precio_envio);

		// Chequeo que el precio de los items sobre los que lo puedo aplicar esta en el rango de los montos
		// del desceunto
		if ( ( $descuento->getMontoMin() && $precio_items < $descuento->getMontoMin()) || 
			  ($descuento->getMontoMax() && $precio_items > $descuento->getMontoMax())) {
			return ['success' => false, 'msg' => 'El descuento no aplica a la compra'];
		}

        if (($descuento->getMontoDisponible() != null && $descuento->getMontoDisponible() == 0)) {
            return ['success' => false, 'msg' => 'La giftcard no tiene más saldo']; 
        }

		if ($montoDescontado <= 0 ) {
			return ['success' => false, 'msg' => 'El descuento no aplica a la compra'];
		}

		if ($montoDescontado > $precio_items + $precio_envio){
			throw new Exception("Error Processing Descuento", 1);
		}

		return ['success' => true, 
				'msg' => 'El descuento se puede aplicar a la compra', 
				'monto_descontado' => $montoDescontado];
	}

	// Chequeo que el descuento que uso que reemplaza al viejo me descuente mas plata
	// Si no tiene un descuento ya aplicado o si el monto nuevo que descuenta es mayor al actual
	private function getsBetterDescuento($descuentoAplicado, $nuevoMontoDescuento){
		return !$descuentoAplicado || $descuentoAplicado->getMontoDescontado() < $nuevoMontoDescuento;
	}

	// Obtengo el monto a descontar neto
	private function getMontoDescontar($descuento, $precio_items, $precio_envio){
		switch ($descuento->getTipo()) {
			case Descuento::PORCENTAJE:
				return ($descuento->getValor() * $precio_items) / 100;
			break;

			case Descuento::MONTO_FIJO:
				return min($precio_items, $descuento->getValor());
			break;
			
			case Descuento::GIFTCARD:
				return min($precio_items, $descuento->getMontoDisponible());
			break;

			case Descuento::ENVIO_GRATIS:
				return $precio_envio;
			break;
		}
	}

	// Seteo el pedido al descuento y le borro el carrito id
	public function updateWithPedido($descuentoAplicado, $pedido){
		if ($descuentoAplicado) {
			$descuentoAplicado->setPedido($pedido);
			$descuentoAplicado->setCarritoId(null);
			$this->em->flush($descuentoAplicado);
		}
	}
}
