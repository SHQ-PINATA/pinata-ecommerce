<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Pedido;
use AppBundle\Entity\PedidoMail;
use AppBundle\Entity\EnvioPickup;

/**
* Envio mails relacionados a los pedidos y guardo en una tabla los mails que se enviaron 
*/
class PedidosMailer
{
	
	private $em;

	public function __construct(EntityManager $entityManager, $mailer, $templating, EnvioManager $envioManager, $fromUser, $fromName){
		$this->em = $entityManager;
		$this->mailer = $mailer;
		$this->templating = $templating;		
		$this->envioManager = $envioManager;
		$this->fromUser = $fromUser;
		$this->fromName = $fromName;
	}

	public function sendCompraMail(Pedido $pedido){
        $envio = $pedido->getEnvio();
        $direccion_envio = $this->envioManager->getDireccionEnvio($pedido->getEnvio());
        
        $items = $pedido->getItems();
        $usuario = $pedido->getUsuario();
        $descuentoAplicado = $pedido->getDescuentoAplicado();

        $precio_envio = $envio->getPrecio();
        $precio_items = $pedido->getPrecioTotalItems();
        $precio_total = $precio_envio + $precio_items;
        $montoDescontado = $descuentoAplicado ? $descuentoAplicado->getMontoDescontado() : null;

        $mail_params = array( 'items' => $items,
                              'user' => $usuario,
                              'precio_envio' => $precio_envio,
                              'precio_items' => $precio_items,
                              'precio_total' => $precio_envio + $precio_items,
                              'precio_descuento' => $precio_envio + $precio_items - $montoDescontado,
                              'envio_is_pickup' => $envio->isPickup(),
                              'monto_descontado' => $montoDescontado,
                              'direccion_envio' => $direccion_envio );

        $send_to = $usuario->getEmail();
		$mail = (new \Swift_Message('Detalle de compra'))
		->setFrom(array($this->fromUser => $this->fromName))
		->setTo($send_to)
		->setBody($this->templating->render(
				'frontend/emails/compra.html.twig', // app/Resources/views/Emails/registration.html.twig
				$mail_params
			), 'text/html');

		$sent = $this->mailer->send($mail);

		if ($sent) {
			$this->savePedidoMail($send_to, PedidoMail::COMPRA, $pedido);
		}

		return $sent > 0;
	}

	public function sendEnvioMail(Pedido $pedido, $num_seguimiento = null){
		$send_to = $pedido->getUsuario()->getEmail();
		$envio = $pedido->getEnvio();
        $nroPedido = $pedido->getId();
		
		$is_pickup = $envio instanceof EnvioPickup;

		$title = $is_pickup ? 'Retiro de pedido' : 'Envio de pedido';
		$mail = (new \Swift_Message($title))
				->setFrom(array($this->fromUser => $this->fromName))
				->setTo($send_to);

		if ($is_pickup) {
			$mail->setBody($this->templating->render(
							'frontend/emails/retiro.html.twig',
                            array(
                                'nombre' => $pedido->getUsuario()->getNombre(), 
                                'nroPedido' => $nroPedido
                            )
							),'text/html');
		}else{
			$mail->setBody($this->templating->render(
							'frontend/emails/envio.html.twig',
							array('nombre' => $pedido->getUsuario()->getNombre(),
						  		  'num_seguimiento' => $num_seguimiento)
							),'text/html');
		}

		$sent = $this->mailer->send($mail);

		if ($sent) {
			$this->savePedidoMail($send_to, PedidoMail::ENVIO, $pedido);
		}

		return $sent > 0;
	}

	public function sendPagoMail(Pedido $pedido){
		$send_to = $pedido->getUsuario()->getEmail();
		$envio = $pedido->getEnvio();
		
		$mail = (new \Swift_Message('Finalizá tu compra'))
				->setFrom(array($this->fromUser => $this->fromName))
				->setTo($send_to);

		$mail->setBody($this->templating->render(
						'frontend/emails/pago.html.twig',
						array('nombre' => $pedido->getUsuario()->getNombre(),
							  'mp_url' => $pedido->getMercadoPagoOrder()->getPaymentUrl(),
							)
						),'text/html');

		$sent = $this->mailer->send($mail);

		if ($sent) {
			$this->savePedidoMail($send_to, PedidoMail::PAGO, $pedido);
		}

		return $sent > 0;
	}
	
	private function savePedidoMail($send_to, $tipo, $pedido){
		$sentMail = new PedidoMail;
		$sentMail->setSentTo($send_to);
		$sentMail->setTipo($tipo);
		$sentMail->setPedido($pedido);

		$this->em->persist($sentMail);
		$this->em->flush();
		
	}

	public function hasCompraMailSent(Pedido $pedido){
		return 0 < $this->em->getRepository(PedidoMail::class)->countByPedidoAndTipo($pedido, PedidoMail::COMPRA);
	}

	public function sendCompraMailIfNotSent($pedido){
		if ($pedido && $pedido->getFechaPago() && !$this->hasCompraMailSent($pedido)) {
			return $this->sendCompraMail($pedido);
		}
		return false;
	}
}
