<?php

namespace AppBundle\Service;

use AppBundle\Entity\Pedido;
use AppBundle\Entity\Factura;
use AppBundle\Service\Afip;
use AppBundle\Presenter\PedidoPresenter;
use Doctrine\ORM\EntityManager;
use AppBundle\Repository\FacturaRepository;

class FacturaManager
{
	function __construct(EntityManager $entityManager, Afip $afip, FacturaRepository $factura_repository, $templating)
	{
		$this->em = $entityManager;
		$this->afip = $afip;
		$this->facturaRepo = $factura_repository;
		$this->templating = $templating;
	}

	public function createFacturas($pedidos){
		$facturas = []; $pedidosFacturasCreadas = []; $pedidosYaFacturados = []; $pedidosNoPagados = [];
		$pedido_presenter = new PedidoPresenter();
		foreach ($pedidos as $pedido) {
			if (!$pedido->getFechaPago()) {
				$pedidosNoPagados[] = $pedido->getId();
				continue;
			}
			$factura = $pedido->getFactura();
			$pedido_presenter->setPedido($pedido);
			if (!$factura) {
				$factura = new Factura();
				$factura->setPedido($pedido);
				$factura->setTipoComprobante(Afip::FACTURA_B);
				$factura->setImporteTotal($pedido_presenter->getPrecioTotal());
				$factura->setEntornoAfip($this->afip->getEnviroment());
				$pedidosFacturasCreadas[] = $pedido->getId();
			}else{
				$pedidosYaFacturados[] = $pedido->getId();
			}
			
			$facturas[] = $factura;

			$this->em->persist($factura);
		}
		$this->em->flush();

		return ['pedidos_ids_nuevas_facturas'     => $pedidosFacturasCreadas,
				'pedidos_ids_facturas_ya_creadas' => $pedidosYaFacturados,
				'pedidos_no_pagados' => $pedidosNoPagados];
	}

	public function facturar(){
		$facturas = $this->facturaRepo->getFacturasNoProcesadas();
		$results = $this->afip->emitirFacturas($facturas)['facturasB'];

		$facturados = []; $errores = [];
		foreach ($results as $factura_resultado) {
			$factura = $factura_resultado['factura'];
			$afipData = $factura_resultado['data'];
			$error_facturacion = $factura_resultado['errores'];

			if ( $this->afipSuccess($afipData, $errores) ){
				$factura->setCAE($afipData->CAE);
	            $factura->setResultado($afipData->Resultado);
				$factura->setCAEVencimiento($afipData->CAEFchVto);
				$factura->setComprobante($afipData->CbteDesde);
                $factura->setFechaProcesada(new \DateTime('now'));

				$vencimientoCAE = $afipData->CAEFchVto;
                $d = substr($vencimientoCAE, 6, 2);
                $m = substr($vencimientoCAE, 4, 2);
                $y = substr($vencimientoCAE, 0, 4);
                $factura->setCAEVencimiento(new \DateTime("$y-$m-$d"));
                $factura->setComprobante($afipData->CbteDesde);

                // Se marca el pedido como facturado
                $pedido = $factura->getPedido();
                $pedido->setFechaFacturacion(new \Datetime('now'));

				$this->em->persist($factura);
				$this->em->persist($pedido);
				$facturados[] = $pedido->getId();
			}else{
				$error_msg = isset($afipData->Observaciones->Obs[0]->Msg) ? $afipData->Observaciones->Obs[0]->Msg : '';
				$errores[] = ['msg' => 'Pedido #'.$factura->getPedido()->getId().' no facturado',
							  'error_msg' => $error_msg,
							  'factura' => $factura];
			}
		}
		$this->em->flush();

		return ['pedidos_facturados' => $facturados, 'errores' => $errores];
	}

	private function afipSuccess($afipData, $errores){
		return $afipData && $afipData->CAE && Afip::FECAE_RESULTADO_APROBADO == $afipData->Resultado;
	}

	public function facturaIsProcessed($factura){
		return ($factura && $factura->getFechaProcesada());
	}

	public function facturaBelongsToUser($factura, $usuario){
		if (!$factura || !$usuario) {
			return false;
		}

		return $factura->getPedido()->getUsuario()->getId() === $usuario->getId();
	}

	public function getFacturaAsHTML($facturaPresenter, $afipConfig){
		$html = $this->templating->render('frontend/afip/facturaPDF.html.twig', array(
            'pedido' => $facturaPresenter->getPedido(),
            'factura' => $facturaPresenter->getFactura(),
            'factura_presenter' => $facturaPresenter,
            'afipConfig' => $afipConfig,
        ));

        return $html;
	}
}