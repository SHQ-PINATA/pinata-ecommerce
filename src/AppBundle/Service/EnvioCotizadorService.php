<?php

namespace AppBundle\Service;

use AppBundle\Service\EnviopackService;
use AppBundle\Entity\EnvioDomicilio ;
use AppBundle\Entity\EnvioSucursalCorreo;
use AppBundle\Entity\EnvioPickup;
use AppBundle\Repository\ConfiguracionRepository;

class EnvioCotizadorService
{

    private $carrito_manager;

    function __construct(EnviopackService $enviopack, CarritoManager $carrito_manager, ConfiguracionRepository $configRepo)
    {
        $this->enviopack = $enviopack;
        $this->carrito_manager = $carrito_manager;
        $this->configRepo = $configRepo;

    }

    public function cotizarEnvioDomicilio($provincia_codigo, $codigo_postal){
        $peso = $this->carrito_manager->getProductosPeso();
        $paquetes = implode(',', $this->carrito_manager->getProductosDimensiones());
        $cotizaciones = $this->enviopack->getCotizacionPorProvincia($provincia_codigo, $codigo_postal, $peso, $paquetes);
        $minimumForFreeShipping = (float) $this->configRepo->getValueByCode('envio_gratis_precio_minimo');
        $maximumForFreeShipping = (float) $this->configRepo->getValueByCode('envio_gratis_precio_maximo');

        if ($minimumForFreeShipping && $this->carrito_manager->getCarritoPrecioTotal() >= $minimumForFreeShipping) {
            if ($maximumForFreeShipping && $this->carrito_manager->getCarritoPrecioTotal() <= $maximumForFreeShipping) {
                $cotizaciones = $this->setStandardToFreeShippings($cotizaciones);
            }
        }

        return $cotizaciones;
    }

    public function cotizarEnvioSucursal($provincia_codigo, $localidad_id){
        $peso = $this->carrito_manager->getProductosPeso();
        $paquetes = implode(',', $this->carrito_manager->getProductosDimensiones());
        $cotizaciones = $this->enviopack->getCotizacionSucursal($provincia_codigo, $localidad_id, $peso, $paquetes);
        $minimumForFreeShipping = (float) $this->configRepo->getValueByCode('envio_gratis_precio_minimo');
        $maximumForFreeShipping = (float) $this->configRepo->getValueByCode('envio_gratis_precio_maximo');

        if ($minimumForFreeShipping && $this->carrito_manager->getCarritoPrecioTotal() >= $minimumForFreeShipping) {
            if ($maximumForFreeShipping && $this->carrito_manager->getCarritoPrecioTotal() <= $maximumForFreeShipping) {
                $cotizaciones = $this->setStandardToFreeShippings($cotizaciones);
            }
        }

        return $cotizaciones;
    }

    public function getPrecioCotizadoForEnvio($envio){
        $precio = null;
        if ($envio instanceof EnvioDomicilio ) {
            $servicio = $envio->getServicio();
            $provincia_codigo = $envio->getProvincia()->getCodigo();
            $codigo_postal = $envio->getCodigoPostal();

            $cotizaciones = $this->cotizarEnvioDomicilio($provincia_codigo, $codigo_postal);
            foreach ($cotizaciones as $cotizacion) {
                if($cotizacion['servicio'] == $servicio){
                    $precio = $cotizacion['valor'];
                    break;
                };
            }
        }elseif($envio instanceof EnvioSucursalCorreo ){
            $sucursal_id = $envio->getSucursalId();
            $provincia_codigo = $envio->getProvincia()->getCodigo();
            $localidad_id = $envio->getLocalidadId();

            $cotizaciones = $this->cotizarEnvioSucursal($provincia_codigo, $localidad_id);
            foreach ($cotizaciones as $cotizacion) {
                if($cotizacion['sucursal']['id'] == $sucursal_id){
                    $precio = $cotizacion['valor'];
                    break;
                };
            }
        }elseif($envio instanceof EnvioPickup){
            $precio = 0;
        }

        return $precio;
    }

    public function setDataFromCotizacion($envio){
        if ($envio instanceof EnvioDomicilio ) {
            $servicio = $envio->getServicio();
            $provincia_codigo = $envio->getProvincia()->getCodigo();
            $codigo_postal = $envio->getCodigoPostal();

            $cotizaciones = $this->cotizarEnvioDomicilio($provincia_codigo, $codigo_postal);
            foreach ($cotizaciones as $cotizacion) {
                if($cotizacion['servicio'] == $servicio){
                    $envio->setPrecio($cotizacion['valor']);
                    break;
                };
            }
        }elseif($envio instanceof EnvioSucursalCorreo ){
            $sucursal_id = $envio->getSucursalId();
            $provincia_codigo = $envio->getProvincia()->getCodigo();
            $localidad_id = $envio->getLocalidadId();

            $cotizaciones = $this->cotizarEnvioSucursal($provincia_codigo, $localidad_id);
            foreach ($cotizaciones as $cotizacion) {
                if($cotizacion['sucursal']['id'] == $sucursal_id){
                    $envio->setCorreoId($cotizacion['sucursal']['correo']['id']);
                    $envio->setCorreoNombre($cotizacion['sucursal']['correo']['nombre']);
                    $envio->setPrecio($cotizacion['valor']);
                    $envio->setSucursalInfo($this->getSucursalInfoAsString($cotizacion));
                    break;
                };
            }
        }

        return $envio;
    }

    private function getSucursalInfoAsString($cotizacion){
        $sucursalInfo = '';
        try {
            // Nota: usar "\n" y no '\n'
            $piso = isset($cotizacion['sucursal']['piso']) ? ' Piso '.$cotizacion['sucursal']['piso'] : '';
            $sucursalInfo = 'Calle: '.$cotizacion['sucursal']['calle'].' '.$cotizacion['sucursal']['numero'].$piso."\n";
            $sucursalInfo .= 'Telefonos: '.$cotizacion['sucursal']['telefono']."\n";
            $sucursalInfo .= 'Horario: '.$cotizacion['sucursal']['horario']."\n";
            $sucursalInfo .= 'Codigo Postal: '.$cotizacion['sucursal']['codigo_postal']."\n";
            $sucursalInfo .= 'Localidad: '.$cotizacion['sucursal']['localidad']['nombre']."\n";
        } catch (Exception $e) {
            return '';
        }

        return $sucursalInfo;
    }

    // Chequeo que el precio de la cotizacion no cambie. El precio de
    // la cotizacion es null si no se encontro la cotizacion elegida
    public function envioIsValid($envio){
        if ($envio instanceof EnvioDomicilio || $envio instanceof EnvioSucursalCorreo) {
            $precio = $envio->getPrecio();
            $precio_cotizado = $this->getPrecioCotizadoForEnvio($envio);

            return ($precio == $precio_cotizado);
        } elseif($envio instanceof EnvioPickup) {
            return true;
        }

        return false;
    }

    public function setStandardToFreeShippings($cotizaciones){
        foreach ($cotizaciones as &$cotizacion) {
            if ($cotizacion['servicio'] == EnviopackService::SERVICIO_ESTANDARD) {
                $cotizacion['valor'] = 0;
                $cotizacion['bonificado'] = true;
            }
        }

        return $cotizaciones;
    }

/*    public function getChosenEnvioCotizacion($envio){
        if ($envio instanceof EnvioDomicilio ) {
            $servicio_envio = $envio->getServicio();
            $servicio = $servicio_envio->getServicio();
            $provincia_codigo = $envio->getProvincia()->getCodigo();
            $codigo_postal = $envio->getCodigoPostal();

            $cotizaciones = $this->cotizarEnvioDomicilio($provincia_codigo, $codigo_postal);
            foreach ($cotizaciones as $cotizacion) {
                if($cotizacion['servicio'] == $servicio){
                    return $cotizacion;
                };
            }
        }elseif($envio instanceof EnvioSucursalCorreo ){
            $sucursal_id = $envio->getServicio()->getSucursalId();
            $provincia_codigo = $envio->getProvincia()->getCodigo();
            $localidad_id = $envio->getLocalidadId();

            $cotizaciones = $this->cotizarEnvioSucursal($provincia_codigo, $localidad_id);
            foreach ($cotizaciones as $cotizacion) {
                if($cotizacion['sucursal']['id'] == $sucursal_id){
                    return $cotizacion;
                };
            }
        }

        return null;
    }*/
    
}
