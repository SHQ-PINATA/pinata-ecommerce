<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use AppBundle\Entity\BloqueBanner;

class BloqueBannerManager
{
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function reorderBloque(BloqueBanner $bloque, $move)
    {   
        if ('arriba' == $move) {
            $bloque_adyacente = $this->entityManager->getRepository(BloqueBanner::class)->getPreviousBanner($bloque->getOrden());
        }else{
            $bloque_adyacente = $this->entityManager->getRepository(BloqueBanner::class)->getNextBanner($bloque->getOrden());
        }

        if ($bloque_adyacente) {
            $orden = $bloque->getOrden();
            $orden_adyacente = $bloque_adyacente->getOrden();
            $bloque->setOrden($orden_adyacente);
            $bloque_adyacente->setOrden($orden);

            $this->entityManager->persist($bloque);
            $this->entityManager->persist($bloque_adyacente);
            $this->entityManager->flush();
        }
    }
}