<?php

namespace AppBundle\Service;

use AppBundle\Entity\Factura;
use Doctrine\ORM\EntityManager;

class Afip
{
	const FACTURA_A = 1;
	const FACTURA_B = 6;
	const MONEDA = 'PES';

	const IVA_NO_GRAVADO = 1;
	const IVA_EXENTO = 2;
	const IVA_0 = 3;
	const IVA_10_50 = 4;
	const IVA_21 = 5;
	const IVA_27 = 6;

	const DOC_DNI = 96;
	const DOC_SIN_IDENTIFICAR = 99;

	const FECAE_RESULTADO_APROBADO 	= 'A';
	const FECAE_RESULTADO_RECHAZADO = 'R';
	const FECAE_RESULTADO_PARCIAL 	= 'P';

	protected $em;
	protected $logger;

	protected $afipConfig;

	protected $wsdlWSAA;
	protected $wsdlWSFE;
	protected $wsdlWSP5;

	protected $urlWSAA;
	protected $urlWSFE;
	protected $urlWSP5;

	protected $cuit;
	protected $puntoDeVenta;

	protected $client;

	public function __construct(EntityManager $entityManager, $logger, $templating, $afipConfig)
	{
    	$this->em 			  = $entityManager;
		$this->logger		  = $logger;
        $this->templating 	  = $templating;

		$this->afipConfig 	= $afipConfig;
		$this->cuit 		= doubleval( str_replace('-', '', $this->afipConfig['cuit'] ) );

		if ( $this->afipConfig['env'] == 'prod' )
		{
			$this->urlWSAA   = "https://wsaa.afip.gov.ar/ws/services/LoginCms";
			$this->urlWSFE   = "https://servicios1.afip.gov.ar/wsfev1/service.asmx";
			$this->urlWSP5  = "https://aws.afip.gov.ar/sr-padron/webservices/personaServiceA5";
		}
		else 
		{
			$this->urlWSAA   = "https://wsaahomo.afip.gov.ar/ws/services/LoginCms";
			$this->urlWSFE   = "https://wswhomo.afip.gov.ar/wsfev1/service.asmx";
			$this->urlWSP5  = "https://awshomo.afip.gov.ar/sr-padron/webservices/personaServiceA5";
		}
						
		$this->wsdlWSAA     = $this->afipConfig['dir'] . "/wsaa.wsdl";
		$this->wsdlWSFE     = $this->afipConfig['dir'] . "/wsfev1.wsdl";
		$this->wsdlWSP5    = $this->afipConfig['dir'] . "/wsp5.wsdl";
		$this->afipDir 	    = $this->afipConfig['dir'] . "/" . $this->getEnviroment();
	}

	public function getEnviroment()
	{
		return $this->afipConfig['env'];
	}

	public function getEntityName($tipoComprobante)
	{
        $indexEntity = array(
            Afip::FACTURA_A         => 'EnvioPack\Entity\Factura',
            Afip::FACTURA_B         => 'EnvioPack\Entity\Factura'
        );

        return $indexEntity[$tipoComprobante];
	}

	public function getDenominacionComprobante($tipoComprobante)
	{
        $indexDenominacionComprobante = array(
            Afip::FACTURA_A         => 'Factura A',
            Afip::FACTURA_B         => 'Factura B'
        );

        return $indexDenominacionComprobante[$tipoComprobante];
	}


	protected function generateAccessTicket($service)
	{
		$dirTmp = $this->afipDir . "/tmp";

  		$tra = new \SimpleXMLElement
  		(
		    '<?xml version="1.0" encoding="UTF-8"?>' .
		    '<loginTicketRequest version="1.0">'.
		    '</loginTicketRequest>'
  		);
  		
		$tra->addChild('header');
		$tra->header->addChild('uniqueId',date('U'));
		
		$tra->header->addChild('generationTime',date('c',date('U')-120));
		$tra->header->addChild('expirationTime',date('c',date('U')+120));
		$tra->addChild('service',$service);
		$tra->asXML( $dirTmp . '/TRA_' . $service . '.xml');		
		
		// SignTRA		
		$status = openssl_pkcs7_sign(
					$dirTmp . "/TRA_" . $service . ".xml",
					$dirTmp . "/TRA_" . $service . ".tmp",
					"file://" . $this->afipDir . "/docs/pinata.crt",
					array("file://" . $this->afipDir . "/docs/pinata.key", ""),
					array(),
					!PKCS7_DETACHED
				);
				
		if (!$status) {
			throw new \Exception("ERROR generating PKCS#7 signature\n");
		}

		$inf = fopen($dirTmp . "/TRA_" . $service . ".tmp", "r");

		$i	= 0;
		$cms	= '';

		while ( !feof( $inf ) ) {
			$buffer = fgets( $inf );
			if ( $i++ >= 4 ) { $cms .= $buffer; }
		}

		fclose($inf);

		@unlink($dirTmp . "/TRA_" . $service . ".tmp");
		@unlink($dirTmp . "/TRA_" . $service . ".xml");

		// CallWSAA
		$client = new \SoapClient( $this->wsdlWSAA,
			array(
				'soap_version' => SOAP_1_2,
				'location' => $this->urlWSAA,
				'trace' => 1,
				'exceptions' => 0
		));
		  			 
		$results = $client->loginCms( array('in0' => $cms) );
		
		if ( is_soap_fault($results) ) 
		{
			throw new \Exception("SOAP Fault: " . $results->faultcode. "\n" . $results->faultstring . "\n");
		}
	
		// Se elimina el ticket de acceso anterior
		$accessTicketFile = $dirTmp . "/TA_" . $service . ".xml";	
		@unlink($accessTicketFile);

		// Se crea un nuevo ticket de acceso
		file_put_contents($accessTicketFile, $results->loginCmsReturn);
		return new \SimpleXMLElement($accessTicketFile, null, true);
	}
	
	protected function getAccessTicket($service)
	{
		$accessTicketFile = $this->afipDir . "/tmp/TA_" . $service . ".xml";

		if ( file_exists( $accessTicketFile ) ) {

			$accessTicket = new \SimpleXMLElement($accessTicketFile, null, true);
			
			// Si el access ticket expira en menos de 10 minutos se genera uno nuevo
			if ( ( time() + 600 ) >= strtotime( $accessTicket->header->expirationTime ) ) {
				$accessTicket = $this->generateAccessTicket( $service );
			}

		} else  {
			// Si no existe un access ticket se genera uno
			$accessTicket = $this->generateAccessTicket( $service );
		}
		
		return $accessTicket;
	}

	protected function getClient()
	{
		if (!$this->client)
		{
			$this->client = new \SoapClient( $this->wsdlWSFE,
				array(
					'soap_version' => SOAP_1_1,
					'location'	   => $this->urlWSFE,
					'exceptions'   => 0,
					'encoding'     => 'ISO-8859-1',
					'features'     => SOAP_USE_XSI_ARRAY_TYPE + SOAP_SINGLE_ELEMENT_ARRAYS,
					'trace'        => 1
			));	
		}
		
		return $this->client; 
	}

	protected function execute($method, $params = array() )
	{		
		$credentials = $this->getAccessTicket('wsfe')->credentials;

		$params['Auth'] = new \stdClass();
		$params['Auth']->Token = $credentials->token;
		$params['Auth']->Sign  = $credentials->sign;
		$params['Auth']->Cuit  = $this->cuit;

 		$params = (object) $params;

	  	$results = $this->getClient()->$method($params);
	  	
	  	$resultProperty = $method . 'Result';
	  	return $results->$resultProperty;
	}

	// Nota: Funciona solo en prod. En homo tira timeout.
	public function checkStatus()
	{
		$results = $this->getClient()->FEDummy();

	    $appServer = $results->FEDummyResult->AppServer;
	    $dbServer = $results->FEDummyResult->DbServer;
	    $authServer = $results->FEDummyResult->AuthServer;
	    
	    if ( $appServer != 'OK' || $dbServer != 'OK' || $authServer != 'OK' )
	    {
	    	throw new \Exception("Fallo de infraestructura AFIP - AppServer: $appServer | DbServer: $dbServer | AuthServer $authServer");
	    }

		return ['appServer' => $appServer, 'dbServer' => $dbServer, 'authServer' => $authServer];
	}

	public function getUltimoCompAutorizado($tipoComprobante)
	{		
		$params = array(
			'PtoVta'   => $this->afipConfig['punto_de_venta'],
			'CbteTipo' => $tipoComprobante,
		);
		
	  	$results = $this->execute('FECompUltimoAutorizado', $params);
	  	
	  	return $results->CbteNro;
	}

	/*public function getPersona($documento)
	{		
		$documento = str_replace('-', '', $documento);
		$documento = str_replace('.', '', $documento);

		$client = new \SoapClient( $this->wsdlWSP5,
			array(
				'soap_version' => SOAP_1_1,
				'location' => $this->urlWSP5,
				'trace' => 1,
				'exceptions' => 1
		));
		
		$credentials = $this->getAccessTicket('ws_sr_padron_a5')->credentials;

		$params                     = array('');
		$params['token']            = $credentials->token;
		$params['sign']             = $credentials->sign;
		$params['cuitRepresentada'] = $this->cuit;
		$params['idPersona']        = $documento;
		
	  	$response = $client->getPersona($params);
	  	$response = $response->personaReturn;

	  	if ( isset($response->datosGenerales) ) {
	  		if ( isset( $response->datosGenerales->razonSocial ) ) {
	  			$razonSocial = $response->datosGenerales->razonSocial;
	  		} else {
	  			$razonSocial = $response->datosGenerales->nombre . ' ' . $response->datosGenerales->apellido;
	  		}
	    } else if ( isset( $response->errorConstancia ) ) {
	  		$nombre   = isset( $response->errorConstancia->nombre ) ? $response->errorConstancia->nombre : '';
	  		$apellido = isset( $response->errorConstancia->apellido ) ? $response->errorConstancia->apellido : '';
	  		$razonSocial = trim($nombre . ' ' . $apellido);
	    } else {
			$razonSocial = '';
	    }

	    return array( 'razon_social' => $razonSocial );
	}*/

	public function consultarComprobanteEmitido($numeroComprobante, $tipoComprobante)
	{		
		$feCompConsReq = new \stdClass();
		$feCompConsReq->CbteTipo = $tipoComprobante;
		$feCompConsReq->CbteNro  = $numeroComprobante;
		$feCompConsReq->PtoVta   = $this->afipConfig['punto_de_venta'];

		$params = array('FeCompConsReq' => $feCompConsReq);

	  	$results = $this->execute('FECompConsultar', $params);
	  	
	  	return $results;
	}

	public function emitirFacturas($facturas)
	{
		/*		
		$facturasA = array();
		$facturasB = array();
		foreach ($facturas as $factura) {

			$condicionIva = $factura->getEmpresa()->getCondicionIva();

			if ( $condicionIva->getTipoComprobante() == self::FACTURA_A ) {
				$facturasA[] = $factura;
			} else {
				$facturasB[] = $factura;
			}
		}

		return array(
			'facturasA' => $this->doEmitirFacturas($facturasA, self::FACTURA_A),
			'facturasB' => $this->doEmitirFacturas($facturasB, self::FACTURA_B)
		);*/

		return ['facturasB' => $this->doEmitirFacturas($facturas, self::FACTURA_B)];
	}

	protected function doEmitirFacturas($facturas, $tipoComprobante)
	{
		$results = [];
		foreach ($facturas as $factura)
		{
			// Se obtiene el numero del proximo comprobante
			$proxComprobante = $this->getUltimoCompAutorizado($tipoComprobante) + 1;

			// Se arma la cabecera del request
			$feCabReq = new \stdClass();
			$feCabReq->CantReg	= 1;
			$feCabReq->PtoVta	= $this->afipConfig['punto_de_venta'];
			$feCabReq->CbteTipo	= $tipoComprobante;

			$feCAEReq = new \stdClass();
			$feCAEReq->FeCabReq = $feCabReq;

			$pedido = $factura->getPedido();
			
			$baseImponible = $factura->getImporteTotal() / 1.21;
			$baseImponibleRedondeado = round($baseImponible, 2, PHP_ROUND_HALF_EVEN);

			$importeIva = $baseImponible * 0.21;
			$importeIvaRedondeado = round($importeIva, 2, PHP_ROUND_HALF_EVEN);
			$factura->setImporteIva( $importeIvaRedondeado );

			$importeTotal = $baseImponibleRedondeado + $importeIvaRedondeado;
			$importeTotal = round($importeTotal, 2, PHP_ROUND_HALF_EVEN);
			$factura->setImporteTotal( $importeTotal );
			
			// Si el importe es mayor a $1000, necesito el dni. Sino, es sin dni y el numero es 0.
			$docTipo = 1000 <= $factura->getImporteTotal() ? self::DOC_DNI : self::DOC_SIN_IDENTIFICAR;
			$docNro = self::DOC_DNI == $docTipo ? $pedido->getDni() : 0;
			$docNro = str_replace('-', '', $docNro);
			$docNro = str_replace('.', '', $docNro);

			$fEDetRequest = new \stdClass();
			$fEDetRequest->Concepto	  = 1;
			$fEDetRequest->DocTipo	  = $docTipo;
			$fEDetRequest->DocNro	  = $docNro;
			$fEDetRequest->CbteDesde  = $proxComprobante;
			$fEDetRequest->CbteHasta  = $proxComprobante;
			$fEDetRequest->CbteFch	  = date('Ymd');
			$fEDetRequest->ImpTotal	  = $importeTotal;
			$fEDetRequest->ImpTotConc = 0;
			$fEDetRequest->ImpNeto	  = $baseImponibleRedondeado;
			$fEDetRequest->ImpOpEx	  = 0;
			$fEDetRequest->ImpTrib	  = 0;
			$fEDetRequest->ImpIVA	  = $importeIvaRedondeado;
			$fEDetRequest->MonId	  = self::MONEDA;
			$fEDetRequest->MonCotiz	  = 1;
			
			// Gestion del array de IVA en el detalle del request
			$IVAs = array();
			$IVAs[0]		    = new \stdClass();
			$IVAs[0]->Id		= self::IVA_21;
			$IVAs[0]->BaseImp	= $baseImponibleRedondeado;
			$IVAs[0]->Importe	= $importeIvaRedondeado;
			$fEDetRequest->Iva	= $IVAs;
			

			// Se executa el request
			$feCAEReq->FeDetReq = [$fEDetRequest];
			$params = array('FeCAEReq' => $feCAEReq);

			$result = $this->execute('FECAESolicitar', $params);
						
			// Se escribe en el log tanto el request como el response
			$time = time();
			$message = 'Time: ' . $time . ' | Request : ' . $this->getClient()->__getLastRequest() . ' | Response: ' . $this->getClient()->__getLastResponse();
			$this->logger->debug( str_replace("\n", "", $message) );
			
			// Se arma la respuesta
			$data    = isset( $result->FeDetResp ) ? $result->FeDetResp->FECAEDetResponse : array();
			$data    = isset($data[0]) ? $data[0] : array();
			$errores = isset( $result->Errors )    ? $result->Errors : false;

			$results[] = ['factura' => $factura, 'time' => $time, 'data' => $data, 'errores' => $errores];
		}

		return $results;

		/*return array(
			'time'	   		=> $time,
			'facturas' 		=> $facturas,
			'data'	   		=> $data,
			'errores'  		=> $errores,
			'no_facturados' => $noFacturados,
		);*/
	}

/*	public function getFacturaPDF(Factura $factura)
	{
		$dirFacturaPDF = $this->afipDir . "/factura";
		$filepath = $dirFacturaPDF . '/' . $factura->getId() . '.pdf';

		if ( !file_exists($filepath) )  {
			$params = array();
			$params['empresaEnvioPack'] = $this->empresaEnvioPack;
			$params['empresa'] = $factura->getEmpresa();
			$params['factura'] = $factura;
			$params['letra']   = $factura->getLetra();

			$params['afipConfig'] = $this->afipConfig;			

	        $html = $this->templating->render('AppBundle:afip:facturaPDF.html.twig', $params );
			$this->pdfGenerator->generateFromHtml( $html, $filepath );
		}

        return $filepath;
	}*/

}



/*/var/www/pinata/src/AppBundle/Service/FacturaManager.php:56:
array (size=5)
  'time' => int 1505236468
  'facturas' => 
    array (size=3)
      0 => 
        object(AppBundle\Entity\Factura)[1649]
          private 'id' => int 17
          private 'pedido' => 
            object(AppBundle\Entity\Pedido)[1066]
              ...
          protected 'fechaProcesada' => null
          protected 'importeTotal' => float 250
          protected 'importeIva' => float 43.39
          protected 'CAE' => null
          protected 'CAEVencimiento' => null
          protected 'comprobante' => null
          protected 'tipoComprobante' => int 6
          protected 'entornoAfip' => string 'homo' (length=4)
          protected 'resultado' => null
          protected 'mailEnviado' => boolean false
      1 => 
        object(AppBundle\Entity\Factura)[1733]
          private 'id' => int 18
          private 'pedido' => 
            object(AppBundle\Entity\Pedido)[1079]
              ...
          protected 'fechaProcesada' => null
          protected 'importeTotal' => float 500
          protected 'importeIva' => float 86.78
          protected 'CAE' => null
          protected 'CAEVencimiento' => null
          protected 'comprobante' => null
          protected 'tipoComprobante' => int 6
          protected 'entornoAfip' => string 'homo' (length=4)
          protected 'resultado' => null
          protected 'mailEnviado' => boolean false
      2 => 
        object(AppBundle\Entity\Factura)[1169]
          private 'id' => int 16
          private 'pedido' => 
            object(AppBundle\Entity\Pedido)[1147]
              ...
          protected 'fechaProcesada' => null
          protected 'importeTotal' => float 299
          protected 'importeIva' => float 51.89
          protected 'CAE' => null
          protected 'CAEVencimiento' => null
          protected 'comprobante' => null
          protected 'tipoComprobante' => int 6
          protected 'entornoAfip' => string 'homo' (length=4)
          protected 'resultado' => null
          protected 'mailEnviado' => boolean false
  'data' => 
    array (size=3)
      0 => 
        object(stdClass)[1761]
          public 'Concepto' => int 1
          public 'DocTipo' => int 99
          public 'DocNro' => int 0
          public 'CbteDesde' => int 102
          public 'CbteHasta' => int 102
          public 'CbteFch' => string '20170912' (length=8)
          public 'Resultado' => string 'A' (length=1)
          public 'CAE' => string '67374561072856' (length=14)
          public 'CAEFchVto' => string '20170922' (length=8)
      1 => 
        object(stdClass)[1766]
          public 'Concepto' => int 1
          public 'DocTipo' => int 99
          public 'DocNro' => int 0
          public 'CbteDesde' => int 103
          public 'CbteHasta' => int 103
          public 'CbteFch' => string '20170912' (length=8)
          public 'Resultado' => string 'A' (length=1)
          public 'CAE' => string '67374561072869' (length=14)
          public 'CAEFchVto' => string '20170922' (length=8)
      2 => 
        object(stdClass)[1767]
          public 'Concepto' => int 1
          public 'DocTipo' => int 99
          public 'DocNro' => int 0
          public 'CbteDesde' => int 104
          public 'CbteHasta' => int 104
          public 'CbteFch' => string '20170912' (length=8)
          public 'Resultado' => string 'A' (length=1)
          public 'CAE' => string '67374561072872' (length=14)
          public 'CAEFchVto' => string '20170922' (length=8)
  'errores' => boolean false
  'no_facturados' => 
    array (size=0)*/