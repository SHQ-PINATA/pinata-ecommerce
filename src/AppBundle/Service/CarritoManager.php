<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use AppBundle\Entity\CarritoProductoItem;
use AppBundle\Entity\Producto;
use AppBundle\Entity\Stock;
use AppBundle\Entity\CarritoResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
* Cada vez que actualizo el stock de producto, le actualizo el stock a ese producto.
*/
class CarritoManager
{
    private $entityManager;
    private $session;
    private $interval_reserve;

	function __construct(EntityManager $entityManager, Session $session, TokenStorage $tokenStorage, $interval_reserve_minutes_cart = 15)
	{
		$this->em = $entityManager;
        $this->session = $session;
        $this->user = $tokenStorage->getToken() ? $tokenStorage->getToken()->getUser() : null;
        
        if (!is_object($this->user)) { // devuelve 'anon.' para usuario anonimos
            $this->user = null;
        }

        $this->interval_reserve = $interval_reserve_minutes_cart;
	}

     /**
     * Obtengo el id del carrito actual de acuerdo a lo que tengo en sesion.
     *
     * @return string
     */
    public function getCarritoId(){
        if($this->session->has('carrito_id')){
            return $this->session->get('carrito_id');
        }

        $carrito_id = $this->session->getId();
        $this->session->set('carrito_id', $carrito_id);

        return $carrito_id;     
    }

     /**
     * Trato de agregar un producto al carrito. Si ya existe, le aumento la cantidad
     * Sino creo el item en carrito. Actualiza la fecha del item en el carrito agregado.
     *
     * @return CarritoResponse
     */
	public function addToCarrito(Producto $producto, int $cantidad){
        $reservado_en_carritos = $this->em->getRepository(CarritoProductoItem::class)->getStockReservadoByProducto($producto, $this->interval_reserve);
        $stock = $producto->getStock();

        $carrito_id = $this->getCarritoId();

        if($this->canAddThisQuantity($producto, $cantidad)) {
        	$item = $this->em->getRepository(CarritoProductoItem::class)->getByProductoAndCarritoId($producto, $carrito_id);
        	if (!$item) {
				$item = new CarritoProductoItem();
				$item->setCarritoId($carrito_id);
				$item->setProducto($producto);
				$item->setCantidad($cantidad);
            	$item->setFecha(new \DateTime());
            }else{
        		$item->setCantidad($item->getCantidad() + $cantidad);
                $item->setFecha(new \DateTime());
        	}

            if($this->user){
                $item->setUsuario($this->user);
            }
	        
            $this->updateSessionCountItems($this->session->get('carrito_item_count', 0) + $cantidad);

        	$this->em->persist($item);
	        $this->em->flush();

            $result = new CarritoResponse();
            $result->setStatusToOk();
            $result->setCantidad($cantidad);
            $result->setStockDisponible($stock - $reservado_en_carritos - $cantidad);

            return $result;
        }else{
            $error = $cantidad > 0 ? 'No se cuenta con suficiente stock' : 'La cantidad a agregar debe ser positiva';
            $result = new CarritoResponse();
            $result->setStatusToError();
            $result->setError($error);
            $result->setCantidad($cantidad);
            $result->setStockDisponible($stock - $reservado_en_carritos);

        	return $result;
        }
	}

    public function getCurrentCarritoCount(){
        // Traigo la cantidad de items que tengo en sesion dependiendo de la ultima vez que agregue algun producto
        /* $last_addition = $this->session->get('carrito_item_count_time');
        $last_addition = new \DateTime($last_addition);
        $now = new \DateTime();
        if ($last_addition->add(new \DateInterval('PT'.$this->interval_delete.'M')) > $now) {
            return $this->session->get('carrito_item_count', 0);
        }*/

        return $this->em->getRepository(CarritoProductoItem::class)->getCountByCarritoId($this->getCarritoId());
    }

    public function updateSessionCountItems($newQuantity){
        $now = new \DateTime();
        $this->session->set('carrito_item_count', $newQuantity);
        $this->session->set('carrito_item_count_time', $now->format('Y-m-d H:i:s'));
    }

    public function getCurrentCarrito(){
        $carrito_items = $this->em->getRepository(CarritoProductoItem::class)->getByCarritoId($this->getCarritoId());
        $producto_ids = array();
        $producto_items = array();
        foreach ($carrito_items as $item) {
            $producto_items[$item->getProductoId()]['item'] = $item; 
            $producto_ids[] = $item->getProductoId();
        }

        $productos = $this->em->getRepository(Producto::class)->getByIds($producto_ids);
        foreach ($productos as $producto) {
            $producto_items[$producto->getId()]['producto'] = $producto;
        }

        ksort($producto_items);

        return $producto_items;
    }

    public function getCarritoItems(){
        return $this->em->getRepository(CarritoProductoItem::class)->getByCarritoId($this->getCarritoId());
    }

    /**
     * Modifico la cantidad de producto en mi carrito. Actualiza la fecha del item en el carrito agregado.
     *
     * @return CarritoResponse
     */
    public function modifyQuantityCarrito(Producto $producto, int $cantidad){
        $carrito_id = $this->getCarritoId();
        $item = $this->em->getRepository(CarritoProductoItem::class)->getByProductoAndCarritoId($producto, $carrito_id);

        $result = new CarritoResponse();
        if ($item) {
            if ($this->canModifyToThisQuantity($producto, $cantidad, $item)) {
                $item->setCantidad($cantidad);
                $item->setFecha(new \DateTime());
                
                $this->updateSessionCountItems($cantidad);

                $this->em->persist($item);
                $this->em->flush();

                $result->setStatusToOk();
                $result->setCantidad($cantidad);
            }else{
                $result->setStatusToError();
                $result->setError('No se cuenta con suficiente stock del producto');
                $result->setCantidad($item->getCantidad());
            }
        }else{
            $result->setStatusToError();
            $result->setError('No se encontró el producto entre la selección de items');
        }
        
        return $result;
    }
     
     /**
     * Chequeo si puedo agregar esa cantidad de producto a mi carrito
     * teniendo en cuenta el stock ya reservados por los demas y por mi (si no tengo reservado, no lo cuenta)
     *
     * @return bool
     */
    private function canAddThisQuantity(Producto $producto, int $cantidad){
        $reservado_en_carritos = $this->em->getRepository(CarritoProductoItem::class)->getStockReservadoByProducto($producto, $this->interval_reserve);
        $stock = $producto->getStock();
        return $cantidad > 0 && $stock - $reservado_en_carritos >= $cantidad;
    }

    /**
     * Chequeo si puedo modificar la cantidad de un producto en carrito
     * teniendo en cuenta el stock ya reservados por los demas y el mio propio
     *
     * @return bool
     */
    private function canModifyToThisQuantity(Producto $producto, int $cantidad, CarritoProductoItem $item_in_carrito){
        $reservado_en_carritos = $this->em->getRepository(CarritoProductoItem::class)->getStockReservadoByProducto($producto, $this->interval_reserve);
        $stock = $producto->getStock();
        $stock_reservado_por_otros = $reservado_en_carritos - $item_in_carrito->getCantidad();
        return $cantidad > 0 && $stock - $stock_reservado_por_otros >= $cantidad;
    }

    /**
     * Borro un producto del carrito
     *
     * @return CarritoResponse
     */
    public function deleteFromCarrito($producto){
        $carrito_id = $this->getCarritoId();
        $item = $this->em->getRepository(CarritoProductoItem::class)->getByProductoAndCarritoId($producto, $carrito_id);
        $result = new CarritoResponse();
        
        if ($item) {
            $this->em->remove($item);
            $this->em->flush();
            $result->setStatusToOk();
        }else{
            $result->setStatusToError();
        }

        return $result;
    }

    /**
     * Al loguearse, fusionar el carrito actual del usuario no logueado con los guardados del usuario logueado
     * Siguiendo la siguiente logica de acuerdo a si estan reservados o no.
     *
     *            | previo | actual |
     *            |    ✓   |   ✓    | Sumo ambos en uno solo. Remuevo el producto viejo.
     * reservado  |    ✗   |   ✓    | Trato de sumarlos si tengo el stock, sino dejo el actual solo. Remuevo el viejo.
     *            |    ✓   |   ✗    | No sucede
     *            |    ✗   |   ✗    | Trato de reservar la suma, sino solo el actual y sino saco ambos.
     *
     * @return bool Hubo algun cambio en el carrito actual (se agregaron/modificaron productos del carrito)
     */
    public function mergeCarts(){
        $current_carrito_id = $this->getCarritoId();
        $current_items = $this->em->getRepository(CarritoProductoItem::class)->getByCarritoId($current_carrito_id);
        $previous_items = $this->em->getRepository(CarritoProductoItem::class)->getByUser($this->user);
        $carrito_changed = false;
        
        foreach ($current_items as $current_item_key => $current_item) {
            foreach ($previous_items as $previous_item_key => $previous_item) {
                if ($current_item->getProductoId() == $previous_item->getProductoId()) {
                    $sum = $current_item->getCantidad() + $previous_item->getCantidad();

                    // Condiciones de chequeo de acuerdo a la tabla
                    $can_add_from_non_previous_reserved = !$this->itemIsReserved($previous_item) && $this->itemIsReserved($current_item) && $this->canModifyToThisQuantity($current_item->getProducto(), $sum, $current_item );
                    $can_add_from_both_reserved = $this->itemIsReserved($previous_item) && $this->itemIsReserved($current_item);
                    $can_add_from_both_not_reserved = $this->canAddThisQuantity($current_item->getProducto(), $sum);
                    $can_add_from_current_not_reserved = $this->canAddThisQuantity($current_item->getProducto(), $current_item->getCantidad());
                    
                    if ($can_add_from_non_previous_reserved || $can_add_from_both_reserved || $can_add_from_both_not_reserved) {
                        // Puedo sumar las cantidades de ambos
                        $current_item->setFecha(new \DateTime());
                        $current_item->setCantidad($sum);
                        $this->em->persist($current_item);
                    }elseif ($can_add_from_current_not_reserved) {
                        // Puedo quedarme solo con el que agregue recien pero no el anterior
                        $current_item->setFecha(new \DateTime());
                        $this->em->persist($current_item);
                    }else{
                        // No tengo stock ni para ambos, ni para el mas reciente. Lo saco
                        $this->em->remove($current_item);
                        unset($current_items[$current_item_key]);
                    }

                    // Lo mergie, me quedo con el item mas nuevo y borro el item viejo.
                    $this->em->remove($previous_item);
                    unset($previous_items[$previous_item_key]);
                    $carrito_changed = true;
                }
            }
            // Seteo el usuario logueado a los productos mas recientes que no estaba logueado
            $current_item->setUsuario($this->user);
            $this->em->persist($current_item);
        }

        // A los productos que no mergie, me fijo si les puedo reservar el stock o los borro
        foreach ($previous_items as $previous_item) {
            $cant = $previous_item->getCantidad();
            if ($this->itemIsReserved($previous_item) || 
                $this->canAddThisQuantity($previous_item->getProducto(), $cant)) {
                $previous_item->setCarritoId($current_carrito_id);
                $previous_item->setFecha(new \DateTime());
                $this->em->persist($previous_item);
                $carrito_changed = true;
            }else{
                $this->em->remove($previous_item);
            }
        }

        $this->em->flush();

        return $carrito_changed;
    }

    public function updateReservation(){
        $current_items = $this->em->getRepository(CarritoProductoItem::class)->getByCarritoId($this->getCarritoId());
        $productos_borrados = [];
        foreach ($current_items as $current_item) {
            if ( $this->itemIsReserved($current_item) || 
                 $this->canAddThisQuantity($current_item->getProducto(), $current_item->getCantidad())) {
                $current_item->setFecha(new \DateTime());
                $this->em->persist($current_item);
            }else{
                $productos_borrados[] = $current_item->getProducto()->getNombre();
                $this->em->remove($current_item);
            }
        }
        $this->em->flush();
        return $productos_borrados;
    }

    public function itemIsReserved($item){
        return $item->getFecha() > new \DateTime("now -" . $this->interval_reserve . " minutes");
    }

    public function getProductosPeso(){
        $carrito_items = $this->em->getRepository(CarritoProductoItem::class)->getByCarritoId($this->getCarritoId());
        $peso = 0.0;
        foreach ($carrito_items as $item) {
            $peso += (float) $item->getCantidad() * (float) $item->getProducto()->getPeso();
        }

        return $peso;
    }

    public function getProductosDimensiones(){
        $carrito_items = $this->em->getRepository(CarritoProductoItem::class)->getByCarritoId($this->getCarritoId());
        $dimensiones = [];
        foreach ($carrito_items as $item) {
            for ($i=0; $i < $item->getCantidad(); $i++) { 
                $dimensiones[] =  $item->getProducto()->getDimensiones();
            }
        }

        return $dimensiones;
    }

    // precio total (sin envio) del carrito
    public function getCarritoPrecioTotal(){
        $carrito_items = $this->em->getRepository(CarritoProductoItem::class)->getByCarritoId($this->getCarritoId());

        $precio_total = 0;
        foreach ($carrito_items as $item) {
            $producto = $item->getProducto();
            $precio = $producto->getPrecioOferta() ? $producto->getPrecioOferta() : $producto->getPrecio();
            $precio_total += $item->getCantidad() * $precio;
        }

        return $precio_total;
    }

    public function emptyCarrito(){
        return $this->em->getRepository(CarritoProductoItem::class)->deleteByCarritoId($this->getCarritoId());
    }
}