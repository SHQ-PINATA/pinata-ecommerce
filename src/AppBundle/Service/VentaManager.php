<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use AppBundle\Entity\StockTipo;
use AppBundle\Entity\Stock;

/**
* Cada vez que actualizo el stock de producto, le actualizo el stock a ese producto.
*/
class VentaManager
{
    private $entityManager;
    
    function __construct(EntityManager $entityManager, StockManager $stockManager)
	{
		$this->em = $entityManager;
		$this->stockManager = $stockManager;
	}

	public function makeVenta($itemsPedido, $itemsCarrito){
		$stockTipoVenta = $this->em->getRepository(StockTipo::class)->getByCodigo(StockTipo::CODIGO_VENTA);

		$this->em->getConnection()->beginTransaction();
		// Try and make the transaction
		try {
			foreach ($itemsPedido as $item) {
				$producto = $item->getProducto();
				$stock = new Stock();
				$stock->setProducto($producto);
				$stock->setCantidad(-1 * $item->getCantidad());
				$stock->setStockTipo($stockTipoVenta);
				$stock->setPedidoItem($item);

				$producto->addStockAction($stock);
				$producto = $this->stockManager->getProductoWithNewStock($producto);

				$this->em->persist($stock);
				$this->em->persist($producto);
			}

			foreach ($itemsCarrito as $item) {
				$this->em->remove($item);
			}
			
			$this->em->flush();
			// Try and commit the transaction
			$this->em->getConnection()->commit();
		} catch (Exception $e) {
			// Rollback the failed transaction attempt
			$this->em->getConnection()->rollback();
			throw $e;
		}
	}
}