<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Producto;
use AppBundle\Entity\CarritoProductoItem;
use AppBundle\Entity\Stock;
use AppBundle\Entity\StockTipo;

class StockManager
{
    private $em;
    private $reservartion_interval;
    private $stockTipoVenta;
	private $stockTipoBaja;

	function __construct(EntityManager $entityManager, int $reservartion_interval, MercadolibreService $mercadolibreService, DafitiService $dafitiService, $mailer, $templating, $stockAlertEmail1, $stockAlertEmail2, $fromUser, $fromName)
	{
		$this->em = $entityManager;
		$this->reservartion_interval = $reservartion_interval;
		$this->stockTipoVenta = null;
		$this->stockTipoBaja = null;
        $this->mercadolibreService = $mercadolibreService;
        $this->dafitiService = $dafitiService;
		$this->mailer = $mailer;
		$this->templating = $templating;		
        $this->stockAlertEmail1 = $stockAlertEmail1;
        $this->stockAlertEmail2 = $stockAlertEmail2;
		$this->fromUser = $fromUser;
		$this->fromName = $fromName;
	}

	/**
	* Cada vez que actualizo el stock de producto, le actualizo el stock a ese producto.
	*/
	public function recalculateStock(Producto $producto){
		$producto = $this->getProductoWithNewStock($producto);
		
        $this->em->persist($producto);
        $this->em->flush();
	}

	public function getAvailableStockForProduct(Producto $producto){
    	$stock = $producto->getStock();
    	$reservado = $this->em->getRepository(CarritoProductoItem::class)->getStockReservadoByProducto($producto, $this->reservartion_interval);

    	return max(0, $stock - $reservado);
	}

	public function getProductoWithNewStock(Producto $producto){
		$stock_actions = $producto->getStockActions();
		$stock = 0;
		foreach ($stock_actions as $stock_action) {
			$stock += $stock_action->getCantidad();
		}

		if ($stock < 0) {
			$stock = 0;
		}

        $this->mercadolibreService->updateStock($producto, $stock);
        $this->dafitiService->updateStock($producto, $stock);

		$stock_actions = $producto->getStockActions();
		$stock = 0;
		foreach ($stock_actions as $stock_action) {
			$stock += $stock_action->getCantidad();
		}

		if ($stock < 0) {
			$stock = 0;
		}

		$producto->setStock($stock);

        if (($stock <= 2 || $stock <= 10 && $producto->getAltaDemanda()) && !$producto->getDiscontinuado()) {
            $title = $producto->getAltaDemanda() ? "Aviso de stock (ALTA DEMANDA)" : "Aviso de stock";
            $title .= " - Producto id: " . $producto->getId();
            $mail = (new \Swift_Message($title))
                ->setFrom(array($this->fromUser => $this->fromName))
                ->setTo(array($this->stockAlertEmail1, $this->stockAlertEmail2))
                ->setBody($this->templating->render(
				'backend/emails/stock.html.twig', 
				array('producto' => $producto)
			), 'text/html');

            $sent = $this->mailer->send($mail);
        }

		return $producto;
	}


	public function getNewStockActionVenta($producto, $cantidad, $pedidoItem){
		if(!$this->stockTipoVenta){
			$this->stockTipoVenta = $this->em->getRepository(StockTipo::class)->getByCodigo(StockTipo::CODIGO_VENTA);
		}

		$stock = new Stock();
		$stock->setProducto($producto);
		$stock->setCantidad(-1 * abs($cantidad));
		$stock->setStockTipo($this->stockTipoVenta);
		$stock->setPedidoItem($pedidoItem);

		return $stock;
	}

	// !! Nice to have: Seleccionar los stocks relacionados al pedidoItem y chequear
	// que no se esta dando de baja 2 veces. Esto no pasa porque se chequea la fecha 
	// dado de baja antes de llamar a este metodos.
	public function getNewStockActionBaja($producto, $cantidad, $pedidoItem){
		if(!$this->stockTipoBaja){
			$this->stockTipoBaja = $this->em->getRepository(StockTipo::class)->getByCodigo(StockTipo::CODIGO_BAJA_PEDIDO);
		}

		$stock = new Stock();
		$stock->setProducto($producto);
		$stock->setCantidad(abs($cantidad));
		$stock->setStockTipo($this->stockTipoBaja);
		$stock->setPedidoItem($pedidoItem);

		return $stock;
	}
}
