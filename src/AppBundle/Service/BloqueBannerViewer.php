<?php

namespace AppBundle\Service;

class BloqueBannerViewer
{
	public function fuseFullscreenBloques($bloques){
		for ($i = count($bloques)-1 ; $i > 0; $i--) { 
			if ($bloques[$i]->getTipo() == 'MAINBAN' && $bloques[$i-1]->getTipo() == 'MAINBAN') {
				$this->fuseBloques($bloques[$i-1], $bloques[$i]);
				unset($bloques[$i]);
			}
		}
		return $bloques;
	}

	/*
 	public function setBloquesMinHeight(&$bloques){
		foreach ($bloques as $bloque) {
			$banners = $bloque->getBanners();
			$heights = array();
			foreach ($banners as $banner) {
				if ($banner->getImagenArchivo()) {
					$heights[] = getimagesize($banner->getImagenArchivo())[1];
				}
			}
			$bloque->minHeight = min($heights);
		}
	}*/

	/**
     * Fusiona dos bloques en uno solo
     * @param BloqueBanner $banner_p (primer bloque), $banner_s (segundo bloque)
     * @return $bloque_p + $bloque_s
     */
	private function fuseBloques($bloque_p, $bloque_s){
		$banners = $bloque_s->getBanners();
		$banners_count = count($bloque_p->getBanners());
		foreach ($banners as $banner) {
			$banner->setOrden($banners_count);
			$bloque_p->addBanner($banner);
			$banners_count++;
		}

	}

}