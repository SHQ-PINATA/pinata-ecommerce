<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Pedido;
use AppBundle\Presenter\PedidoPresenter;
use AppBundle\Entity\PedidoItem;
use AppBundle\Entity\MercadoPagoOrder;

/**
* Cada vez que actualizo el stock de producto, le actualizo el stock a ese producto.
*/
class PedidoManager
{
    private $entityManager;
    
    function __construct(EntityManager $entityManager, StockManager $stockManager, $trackURL)
	{
		$this->em = $entityManager;
		$this->stockManager = $stockManager;
        $this->trackURL = $trackURL;
	}

	public function makePedido($itemsCarrito, $envio, $usuario, $dni = null, $tarjeta365 = null){
		// Hago un nuevo pedido
		$pedido = new Pedido();
		$pedido->setEnvio($envio);
		$pedido->setDni($dni);
		$pedido->setUsuario($usuario);
		$pedido->setClarin365($tarjeta365);
		$envio->setPedido($pedido);
		
		// Le asigno los productos elegidos
		foreach ($itemsCarrito as $item) {
			$producto = $item->getProducto();
			$precio = $producto->getPrecioOferta() ? $producto->getPrecioOferta() : $producto->getPrecio();

			$pedidoItem = new PedidoItem();
			$pedidoItem->setCantidad($item->getCantidad());
			$pedidoItem->setProducto($producto);
			$pedidoItem->setPrecio($precio);
			$pedidoItem->setPedido($pedido);
			$pedido->addItem($pedidoItem);
			$this->em->persist($pedidoItem);
		}

		// el envio pasa a ser del pedido
		$envio->setCarritoId(null);
		$this->em->persist($envio);
		$this->em->persist($pedido);

		// actualizo el stock de los items del pedido
		$this->updateStockVenta($pedido->getItems());

		// remuevo los items del carrito
		foreach ($itemsCarrito as $item) {
			$this->em->remove($item);
		}

		$this->em->flush();

		return $pedido;
	}

	private function updateStockVenta($itemsPedido){
		$this->em->getConnection()->beginTransaction();
		try {
			foreach ($itemsPedido as $item) {
				$producto = $item->getProducto();
				// Hago una nueva accion de stock de tipo Venta
				$stock = $this->stockManager->getNewStockActionVenta($producto, $item->getCantidad(), $item);

				// agrego la nueva accion de stock al producto porque todavia no esta persistida y le calculo el
				// nuevo stock al producto
				$producto->addStockAction($stock); 
				$producto = $this->stockManager->getProductoWithNewStock($producto);

				$this->em->persist($stock);
				$this->em->persist($producto);
			}
			
			$this->em->flush();
			$this->em->getConnection()->commit();
		} catch (Exception $e) {
			$this->em->getConnection()->rollback();
			throw $e;
		}
	}

	public function undoPedido($pedido){
		$itemsPedido = $pedido->getItems();
		$pedido->setFechaBaja(new \DateTime());
		$this->em->persist($pedido);
		$this->updateStockBaja($itemsPedido);
		$this->em->flush();
	}

	private function updateStockBaja($itemsPedido){
		$this->em->getConnection()->beginTransaction();
		try {
			foreach ($itemsPedido as $item) {
				$producto = $item->getProducto();
				// Hago una nueva accion de stock de tipo Baja
				$stock = $this->stockManager->getNewStockActionBaja($producto, $item->getCantidad(), $item);

				// agrego la nueva accion de stock al producto porque todavia no esta persistida y le calculo el
				// nuevo stock al producto
				$producto->addStockAction($stock); 
				$producto = $this->stockManager->getProductoWithNewStock($producto);

				$this->em->persist($stock);
				$this->em->persist($producto);
			}
			
			$this->em->flush();
			$this->em->getConnection()->commit();
		} catch (Exception $e) {
			$this->em->getConnection()->rollback();
			throw $e;
		}
	}

	/*
     * Setea el pedido como pagado y actualiza el estado de la orden de MercadoPago
     * return void
     */

    public function setPedidoAndOrderAsPaid($pedidoId, $preferenceId){
        $pedido = $this->em->getRepository(Pedido::class)->getById($pedidoId);
        $fechaPago = new \DateTime();
        $pedido->setFechaPago( $fechaPago );

		$this->em->persist($pedido);

        if ($pedido->getClarin365()) {
            $this->track365($pedido);
        }

        if ($preferenceId) {
            $mpOrder = $this->em->getRepository(MercadoPagoOrder::class)->getOrderByPreferenceIdAndPedido($preferenceId, $pedido);

            if ($mpOrder) {
                $mpOrder->setStatus(MercadoPagoOrder::STATUS_CLOSED);
                $this->em->persist($mpOrder);
            }
        }

        $this->em->flush();
    }

    /*
     * Setea el pedido como no pagado y actualiza el estado de la orden de MercadoPago
     * return bool El estado previo del pedido era closed y el nuevo no lo es
     */

    public function setPedidoAndOrderAsNotPaid($pedidoId, $status, $preferenceId){
        $pedido = $this->em->getRepository(Pedido::class)->getById($pedidoId);
        $pedido->setFechaPago( null );
        
		$this->em->persist($pedido);

        $mpOrder = $this->em->getRepository(MercadoPagoOrder::class)->getOrderByPreferenceIdAndPedido($preferenceId, $pedido);

        $prevStatus = null;
        if ($mpOrder) {
	        $prevStatus = $mpOrder->getStatus();
	        $mpOrder->setStatus($status);
			$this->em->persist($mpOrder);
        }

        $this->em->flush();
        return ($prevStatus == MercadoPagoOrder::STATUS_CLOSED) && ($status != MercadoPagoOrder::STATUS_CLOSED);
    }

    private function track365(Pedido $pedido) {
		$pedido_presenter = new PedidoPresenter();
        $pedido_presenter->setPedido($pedido);

        $precioTotal = $pedido_presenter->getPrecioItems();
        $montoDescontado = $pedido_presenter->getMontoDescontado();

        $date = $pedido->getFechaCreacion();
        $curlDate = $date->format("YmdHis");

        $data = array(
            'idSucursal' => 391980,
            'idBeneficio' => 421489,
            'fechaHora' => $curlDate,
            'nroCredencial' => $pedido->getClarin365(),
            'tipoTransaccion' => 'V',
            'importe' => $precioTotal,
            'importeDescuento' => $montoDescontado,
            'idTerminal' => 1,
        );
        $data_string = json_encode($data);                                                                                   
                                                                                                                             
        $ch = curl_init($this->trackURL);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',                                                                                
                    'Content-Length: ' . strlen($data_string))                                                                       
                );                                                                                                                   
                                                                                                                             
        $result = curl_exec($ch);
    }
}
