<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use AppBundle\Entity\CarritoProductoItem;
use AppBundle\Form\Frontend\AgregarProductoCarritoFormType;


class AgregarProductoCarritoFormFactory
{
	private $entityManager;
	private $router;
	private $formFactory;

	public function __construct(EntityManager $entityManager, $router, $formFactory, $max_add_to_cart = 3, $interval_reserve = 15)
	{
		$this->em = $entityManager;
		$this->router = $router;
		$this->formFactory = $formFactory;
		$this->max_add_to_cart = $max_add_to_cart;
		$this->interval_reserve = $interval_reserve;
	}

	public function createFormsViewsForProducts($productos){
		$forms = $this->createFormsForProducts($productos);
		foreach ($forms as $key => $form) {
			$forms[$key] = $form->createView();
		}
		return $forms;
	}	

	public function createFormViewForProduct($producto){
		$forms = $this->createFormsForProducts(array($producto));
		return $forms[$producto->getId()]->createView();
	}

	public function createFormsForProducts($productos){
		$reservados_por_producto = $this->em->getRepository(CarritoProductoItem::class)->getStockReservadoByProductos($productos, $this->interval_reserve);

		$forms = array();
		foreach ($productos as $producto) {
			if (!isset($reservados_por_producto[$producto->getId()])) {
				$reservados_por_producto[$producto->getId()] = 0;
			}

			$max = min($this->max_add_to_cart, $producto->getStock() - $reservados_por_producto[$producto->getId()]);

			$forms[$producto->getId()] = $this->formFactory->create(AgregarProductoCarritoFormType::class, array('cantidad' => 1, 'producto_id' => $producto->getId()), array(
				'action' => $this->router->generate('ajax_add_to_cart', array('id' => $producto->getId())),
				'max' => $max,
				));
		}

		return $forms;
	}

	public function getAvailableStockForProduct(Producto $producto){
		$stock = $producto->getStock();
		$reservado = $this->em->getRepository(CarritoProductoItem::class)->getStockReservadoByProducto($producto, $this->interval_reserve);

		return max(0, $stock - $reservado);
	}

	/*public function createFormsForCarritoItems($productos_items){
		$productos = array();
		foreach ($variable as $key => $value) {
			# code...
		}

		$reservados_por_producto = $this->em->getRepository(CarritoProductoItem::class)->getStockReservadoByProductos($productos);

		$forms = array();
		foreach ($productos as $producto) {
			if (!isset($reservados_por_producto[$producto->getId()])) {
				$reservados_por_producto[$producto->getId()] = 0;
			}

			$max = min($this->max_add_to_cart, $producto->getStock() - $reservados_por_producto[$producto->getId()]);

			$forms[$producto->getId()] = $this->formFactory->create(AgregarProductoCarritoFormType::class, null, array(
				'action' => $this->router->generate('ajax_add_to_cart', array('id' => $producto->getId())),
				'max' => $max,
				'producto_id' => $producto->getId(),
				));
		}

		return $forms;
	}	*/
}
