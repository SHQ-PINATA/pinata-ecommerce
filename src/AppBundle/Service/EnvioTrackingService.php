<?php

namespace AppBundle\Service;

use AppBundle\Service\EnviopackService;
use AppBundle\Repository\EnvioSucursalCorreoRepository;
use AppBundle\Repository\EnvioDomicilioRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class EnvioTrackingService
{

	/* El tracking number puede coincidir entre mas de un correo 
	 * asi que hay que chequear que lo que devuelve el service de enviopack
	 * es del correo que se usó
	 */

    function __construct(EnviopackService $enviopack, TokenStorage $tokenStorage, EnvioDomicilioRepository $envioDomRepo, EnvioSucursalCorreoRepository $envioSucRepo)
    {
        $this->enviopack = $enviopack;
        $this->user = $tokenStorage->getToken() ? $tokenStorage->getToken()->getUser() : null;
     	   
        if (!is_object($this->user)) { // devuelve 'anon.' para usuario anonimos
            $this->user = null;
            throw new Exception("No User Set", 1);
        }

        $this->envioDomRepo = $envioDomRepo;
        $this->envioSucRepo = $envioSucRepo;
    }

    public function getByTrackingNumber($tracking_number){
		if (!$tracking_number) {
			return null;
		}
		
		$envios = $this->getEnviosByUsuarioAndTrackingNumber($this->user, $tracking_number);
		if (count($envios) == 0) {
			return null;
		}

		$tracking_data = $this->getTrackingData($tracking_number);
		if (count($tracking_data) == 0) {
			return null;
		}

		foreach ($envios as $envio) {
			foreach ($tracking_data as $tracking) {
				if ($envio->getCorreoId() == $tracking['correo']['id']) {
					return $tracking;
				}
			}
		}

		return null;
    }

    private function getEnviosByUsuarioAndTrackingNumber($usuario, $tracking_number){
    	$enviosSuc = $this->envioSucRepo->getByUsuarioAndTrackingNumber($usuario, $tracking_number);
    	$enviosDom = $this->envioDomRepo->getByUsuarioAndTrackingNumber($usuario, $tracking_number);
    	return array_merge($enviosSuc, $enviosDom);
    }

    private function getTrackingData($tracking_number){
    	return $this->enviopack->getTrackingInfo($tracking_number);
    }
}