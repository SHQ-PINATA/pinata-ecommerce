<?php

namespace AppBundle\Service;
use AppBundle\Repository\CajaRepository;

class EnviopackService
{
    const ACCES_TOKEN_URL = 'https://api.enviopack.com/auth';
    const LOCALIDADES_URL = 'https://api.enviopack.com/localidades';
    const COTIZAR_PRECIO_POR_PROVINCIA = 'https://api.enviopack.com/cotizar/precio/a-domicilio';
    const COTIZAR_PRECIO_A_SUCURSAL = 'https://api.enviopack.com/cotizar/precio/a-sucursal';
    const CONSULTAR_SUCURSAL = 'https://api.enviopack.com/sucursales';
    const CONSULTAR_TRACKING = 'https://api.enviopack.com/tracking';
    const CONSULTAR_CORREOS = 'https://api.enviopack.com/correos';

    const ACCES_TOKEN_REFRESH_MINUTES = 60;

    const SERVICIO_ESTANDARD    = 'N';
    const SERVICIO_PRIORITARIO  = 'P';
    const SERVICIO_EXPRESS      = 'X';
    const SERVICIO_DEVOLUCION   = 'R';
    const SERVICIO_CAMBIO       = 'C';

    private $api_key;
    private $secret_key;
    private $access_token = null;
    private $refresh_token = null;
    private $access_token_time_received = null;

    function __construct($api_key, $secret_key, $tiempo_estimado_envio, CajaRepository $cajasRepository)
    {
        $this->api_key = $api_key;
        $this->secret_key = $secret_key;
        $this->tiempo_estimado_envio = $tiempo_estimado_envio;
        $this->cajasRepo = $cajasRepository;
    }

    private function getAccessToken(){
        /*$now = new \DateTime();
        if ($this->access_token_time_received ){
            if($this->access_token_time_received > new \DateTime("now -" . self::ACCES_TOKEN_REFRESH_MINUTES . " minutes")){
                return $this->access_token;
            }else{
                // refresh
            }
        }*/

        $parameters = [
            'api-key'    => $this->api_key,
            'secret-key' => $this->secret_key,
        ];

        $ch = curl_init(self::ACCES_TOKEN_URL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
        $response = json_decode(curl_exec($ch), $assoc = true);
        curl_close($ch);

        $this->access_token = $response['token'];
        $this->refresh_token = $response['refresh_token'];
        $this->access_token_time_received = new \DateTime();

        return $this->access_token;
    }


    public function getLocalidadesByProvincia($provincia_id){
        $parameters = [
            'id_provincia' => $provincia_id,
        ];

        $ch = curl_init($this->getUrlWithParameters(self::LOCALIDADES_URL, $parameters));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = json_decode(curl_exec($ch), $assoc = true);
        curl_close($ch);

        return $response;
    }

    //['Devoto' => 140, 'Boedo' => 141];
    public function getLocalidadesOptionsByProvincia($provincia_id){
        $localidades = $this->getLocalidadesByProvincia($provincia_id);
        $choices = array();
        foreach ($localidades as $localidad) {
            $choices[$localidad['nombre']] = $localidad['id'];
        }
        return $choices;
    }

    private function getUrlWithParameters($baseUrl, $parameters){
        $parameters['access_token'] = $this->getAccessToken();
        return $baseUrl .'?'. http_build_query($parameters);
    }

    /**
     * @param string $provincia_id ISO 3166-2 sin prefijo AR (A, B, C...)
     * @param string $codigo_postal
     * @param string $peso Hasta 2 dígitos decimales
     *
     * @return json
     */
    public function getCotizacionPorProvincia($provincia_id, $codigo_postal, $peso, $paquetes){
        $cajas = $this->acomodarEnCajas($paquetes);
        $paquetes = $this->cajasToDimensiones($cajas);
        $parameters = [
            'provincia' => $provincia_id,
            'codigo_postal' => $codigo_postal,
            'peso' => $peso,
            'paquetes' => $paquetes,
        ];

        $ch = curl_init($this->getUrlWithParameters(self::COTIZAR_PRECIO_POR_PROVINCIA, $parameters));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $cotizaciones = json_decode(curl_exec($ch), $assoc = true);
        curl_close($ch);

        foreach ($cotizaciones as &$cotizacion) {
            $cotizacion['servicio_nombre'] = $this->getServicioNombre($cotizacion['servicio']);

            if( $this->tiempo_estimado_envio ) {
                $cotizacion['estimacion_entrega'] = $this->tiempo_estimado_envio;
            }else{
                $cotizacion['estimacion_entrega'] = isset($cotizacion['horas_entrega']) ? $cotizacion['horas_entrega'] . ' hs hábiles' : '';
            }
            unset($cotizacion['horas_entrega']);
                
        }

        return $cotizaciones;
    }

    public function acomodarEnCajas($paquetes) {
        $arrPaquetes = explode(',', $paquetes);
        $itemsCarrito = array();

        $volumenes = array();
        foreach($arrPaquetes as $paquete) {
            $arrPaquete = explode('x', $paquete);
            $volumen = $arrPaquete[0] * $arrPaquete[1] * $arrPaquete[2];
            $item = array(
                'ancho' => $arrPaquete[0], 
                'largo' => $arrPaquete[1], 
                'alto' => $arrPaquete[2], 
                'volumen' => $volumen,
            );

            $volumenes[] = $volumen;
            $itemsCarrito[]= $item; 
        }

        rsort($volumenes);
        
        $orderedItems = array();
        foreach ($volumenes as $volumen) {
            foreach ($itemsCarrito as $itemCarrito) {
                if ($volumen == $itemCarrito['volumen']) {
                    $orderedItems[]= $itemCarrito;
                    break;
                }
            }
        }

        $cajasPaquetes = $this->acomodar($itemsCarrito);

        return $cajasPaquetes;
    }

    private function cajasToDimensiones($cajasPaquetes) {
        $dimensiones = '';
        $i = 1;
        foreach($cajasPaquetes as $caja) {
            $alto = $caja->getAlto();
            $ancho = $caja->getAncho();
            $largo = $caja->getLargo();
            $cajaDimension = $alto.'x'.$ancho.'x'.$largo;

            if ($i != count($cajasPaquetes)) {
                $cajaDimension .= ',';
            }

            $dimensiones .= $cajaDimension; 
            $i++;
        }
        return $dimensiones;
    }

    private function acomodar($itemsCarrito, $cajasDefinitivas = array()) {
        $cajas = $this->cajasRepo->findAll();

        foreach ($itemsCarrito as $item) {
            $largos[] = $item['largo'];
            $altos[]  = $item['alto'];
            $anchos[] = $item['ancho'];
        }

        $maxAlto = max($altos);
        $maxLargo = max($largos);
        $maxAncho = max($anchos);

        //Caja a utilizar segun tamaño de items
        foreach ($cajas as $caja) {
            $altoCaja = $caja->getAlto();
            $anchoCaja = $caja->getAncho();
            $largoCaja = $caja->getLargo();

            if ($largoCaja > $maxLargo && $anchoCaja > $maxAncho && $altoCaja > $maxAlto || $largoCaja > $maxAncho && $anchoCaja > $maxLargo && $altoCaja > $maxAlto ) {
                $cajaAUtilizar = $caja;
                break;
            }
        }

        //Coloco los items dentro de la caja y los saco de itemsCarrito
        $altoCaja = $cajaAUtilizar->getAlto();
        $anchoCaja = $cajaAUtilizar->getAncho();
        $largoCaja = $cajaAUtilizar->getLargo();
        $altoDisponible = $altoCaja;

        foreach ($itemsCarrito as $key => $itemCarrito) {
            $largoItem = $itemCarrito['largo'];
            $anchoItem = $itemCarrito['ancho'];
            $altoItem = $itemCarrito['alto'];

            if ($largoCaja > $largoItem && $anchoCaja > $anchoItem || $largoCaja > $anchoItem && $anchoCaja > $largoItem) {
                if ($altoDisponible > $altoItem) {
                    $altoDisponible = $altoDisponible - $altoItem;
                    unset($itemsCarrito[$key]);
                    if (!count($itemsCarrito)) {
                        $cajasDefinitivas[]= $cajaAUtilizar;
                    }
                } else {
                    $cajasDefinitivas []= $cajaAUtilizar;
                    break;
                }
            }  
        }

        if (count($itemsCarrito)) {
            $cajasDefinitivas = $this->acomodar($itemsCarrito, $cajasDefinitivas);
            return $cajasDefinitivas;
        } else {
            return $cajasDefinitivas;
        }
    }

    /**
     * @param string $provincia_id              ISO 3166-2 sin prefijo AR (A, B, C...)
     * @param float $peso                       Hasta 2 dígitos decimales
     * @param [Optional] string $localidad_id   Valor ID devuelto por el webservice de localidades.
     * @param [Optional] string $paquetes       20x2x10,20x2x10 indica que se envian 2 paquetes y 
     *                                          cada uno tiene 20 cm de alto x 2 cm de ancho x 10 cm de largo. 
     *
     * @return json
     */
    public function getCotizacionSucursal($provincia_id, $localidad_id, $peso, $paquetes){
        $cajas = $this->acomodarEnCajas($paquetes);
        $paquetes = $this->cajasToDimensiones($cajas);
        $parameters = [
            'provincia' => $provincia_id,
            'localidad' => $localidad_id,
            'peso' => $peso,
            'paquetes' => $paquetes,
        ];

        $ch = curl_init($this->getUrlWithParameters(self::COTIZAR_PRECIO_A_SUCURSAL, $parameters));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $cotizaciones = json_decode(curl_exec($ch), $assoc = true);
        curl_close($ch);

        foreach ($cotizaciones as   &$cotizacion) {
            unset($cotizacion['anomalos']);
            unset($cotizacion['cumplimiento']); 
            if( $this->tiempo_estimado_envio ) {
                $cotizacion['estimacion_entrega'] = $this->tiempo_estimado_envio;
            }else{
                $cotizacion['estimacion_entrega'] = isset($cotizacion['horas_entrega']) ? $cotizacion['horas_entrega'] . ' hs hábiles' : '';
            }
            unset($cotizacion['horas_entrega']);
        }

        return $cotizaciones;
    }

    /**
     * @param string $sucursal_id       Id de la Sucursal a consultar
     * 
     * @return json
     */
    public function getSucursalInfo($sucursal_id){

        $ch = curl_init($this->getUrlWithParameters(self::CONSULTAR_SUCURSAL.'/'.$sucursal_id, array()));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $sucursal = json_decode(curl_exec($ch), $assoc = true);
        curl_close($ch);

        return $sucursal;
    }

    /**
     * @param string $tracking_number       Numero de tracking del pedido
     * 
     * @return json
     */
    public function getTrackingInfo($tracking_number){

        $parameters = [
            'tracking_number' => $tracking_number,
        ];

        $ch = curl_init($this->getUrlWithParameters(self::CONSULTAR_TRACKING, $parameters));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $tracking = json_decode(curl_exec($ch), $assoc = true);
        curl_close($ch);

        return $tracking;
    }

    /** 
     * @return json
     */
    public function getAllCorreos(){

        $ch = curl_init($this->getUrlWithParameters(self::CONSULTAR_CORREOS, array()));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $correos = json_decode(curl_exec($ch), $assoc = true);
        curl_close($ch);

        return $correos;
    }

    public function getCorreosOptions(){
        $allCorreos = $this->getAllCorreos();
        $correos = array();
        foreach ($allCorreos as $correo) {
            $correos[$correo['nombre']] = $correo['id'];
        }

        return $correos;
    }

    public function getServicioNombre($servicio){
        switch ($servicio) {
            case self::SERVICIO_ESTANDARD:
                $nombre = 'Estándar';
                break; 
            case self::SERVICIO_PRIORITARIO:
                $nombre = 'Prioritario';
                break; 
            case self::SERVICIO_EXPRESS:
                $nombre = 'Express';
                break; 
            case self::SERVICIO_DEVOLUCION:
                $nombre = 'Devolución';
                break; 
            case self::SERVICIO_CAMBIO:
                $nombre = 'Cambio';
                break; 
            default:
                $nombre = '';
                break;
            }

        return $nombre;
    }
}

