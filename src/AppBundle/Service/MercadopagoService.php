<?php

namespace AppBundle\Service;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use AppBundle\Entity\MercadoPagoOrder;

class MercadopagoService
{
    protected $em;
    protected $router;
    /*protected $pedidoService;
    protected $hostApp;
    protected $hostApi;*/
    protected $mp;
    //protected $mercadoPagoConfig;
    protected $env;
    private $init_point;

    public function __construct($client_id, $client_secret, $env, $entityManager, $router)
    {
        $this->em = $entityManager;
        $this->router = $router;
        $this->client_id = $client_id;
        $this->client_secret = $client_secret;
        $this->env = $env;
        $this->apiRoute = 'https://www.pinataweb.com.ar';
        $this->init_point = $env == "dev" ? 'sandbox_init_point' : 'init_point';
        $this->mp = new \MP($this->client_id, $this->client_secret);
        
        if($env == "dev"){
            $this->mp->sandbox_mode(true);
        }
    }

    public function getClient() {
        return $this->mp;
    }

    /*
     * Genera el link de pago a MP con la información del pedido y el usuario
     * Se lo redirije al usuario al link generado y se utiliza el mismo link
     * para que pueda volver a pagarlo en caso de un fallo
     */
    public function checkout( $pedido, float $envio_precio, $usuario, $monto_descuento, $metodo = null )
    {
        $preference_data = array();
        $pedido_id = $pedido->getId();
        $monto = $pedido->getPrecioTotalItems() + $envio_precio - $monto_descuento;
        $nombre = $usuario->getNombre();
        $apellido = $usuario->getApellido();
        $email = $usuario->getEmail();

        $preference_data['items'] = array(
            array(
                'id' => $pedido_id,
                'title' => '$' .$monto. ' de compra en Piñata',
                'quantity' => 1,
                'currency_id' => 'ARS',
                'unit_price' => (float) $monto
           )
        );

        // Se setea la informacion del comprador
        $preference_data['payer'] = array(
            'name' => $nombre,
            'surname' => $apellido,
            'email' => $email,
        );

        // Se setea la informacion de metodo de pago
        if ( $metodo ) {
            $preference_data['payment_methods'] = array(
                'default_payment_method_id' => $metodo 
            );
        }

        // Se setean las urls de respuesta
        $preference_data['back_urls'] = array(
            'success' => $this->router->generate('frontend_checkout_pago_success', 
                array(),
                UrlGeneratorInterface::ABSOLUTE_URL),
            'pending' => $this->router->generate('frontend_checkout_pago_pending', 
                array(),
                UrlGeneratorInterface::ABSOLUTE_URL),
            'failure' => $this->router->generate('frontend_checkout_pago_error', 
                array(),
                UrlGeneratorInterface::ABSOLUTE_URL),
        );
        
        $preference_data['notification_url'] = $this->apiRoute . '/api/mpnotifications';
        $preference_data['auto_return'] = 'approved';

        // Se setean variables generales
        $preference_data['external_reference'] = $pedido_id;
        
        $preference_data['expires'] = true;
        $preference_data['expiration_date_from'] = $pedido->getFechaCreacion()->format('Y-m-d\TH:i:s\.000P');
        $preference_data['expiration_date_to'] = $pedido->getFechaCreacion()->add(new \DateInterval('P7D'))->format('Y-m-d\TH:i:s\.000P');

        // Se crea la preferencia
        $preference = $this->getClient()->create_preference($preference_data);
        $response = $preference['response'];

        $mpUrl = $response[$this->init_point];
        $preference_id = $response['id'];
        
        $mpOrder = new MercadoPagoOrder;
        $mpOrder->setPaymentUrl($mpUrl);
        $mpOrder->setPedido($pedido);
        $mpOrder->setPreferenceId($preference_id);
        
        $this->em->persist($mpOrder);
        $this->em->flush();

        return $mpUrl;
    }

    public function getInfoForNotification($id, $topic){
        if (!$id || !$topic || !ctype_digit($id)) {
            return ['status' => 'error', 'msg' => 'El Id o el Topico no esta seteado'];
        }

        // Obtengo la informacion sobre la venta
        $merchant_order_info = null;
        switch ($topic) {
            case 'payment':
                $payment_info = $this->mp->get("/collections/notifications/".$id);
                $merchant_order_info = $this->mp->get("/merchant_orders/".$payment_info["response"]["collection"]["merchant_order_id"]);
                break;
            case 'merchant_order':
                $merchant_order_info = $this->mp->get("/merchant_orders/".$id);
                break;
            default:
                $merchant_order_info = null;
        }

        // Si no devuelve nada, hubo un error
        if (!$merchant_order_info) {
            return ['status' => 'error', 'msg' => 'Mechant order no encontrado'];
        }

        // Calculo cuanto se pagó
        $paid_amount = 0;
        foreach ($merchant_order_info["response"]["payments"] as  $payment) {
            if ($payment['status'] == 'approved'){
                $paid_amount += $payment['transaction_amount'];
            }
        }

        $paid = ($paid_amount >= $merchant_order_info["response"]["total_amount"]);
        $response = ['status' => 'ok', 
                    'pedido_id' => $merchant_order_info["response"]['external_reference'],
                    'preference_id' => $merchant_order_info["response"]["preference_id"],
                    'merchant_id' => $merchant_order_info["response"]["id"],
                    'merchant_status' => $merchant_order_info["response"]["status"],
                    'paid' => $paid,
                    'cancelled' => $merchant_order_info["response"]["cancelled"],
                    'refunded_amount' => $merchant_order_info["response"]["refunded_amount"],
                    'total_amount' => $merchant_order_info["response"]["total_amount"],
                    'date_created' => $merchant_order_info["response"]["date_created"],
                    'last_update' => $merchant_order_info["response"]["last_update"],
                    'payments' => $merchant_order_info["response"]["payments"],
                    'paid_amount' => $paid_amount,
                    ]; 
        
        return $response;
    }

    public function setMerchantOrderByPreferenceId($preferenceId, $merchantId, $status){
        $mpOrder = $this->em->getRepository(MercadoPagoOrder::class)->getOrderByPreferenceId($preferenceId);
        if (!$mpOrder) {
            // Aca se podria crear el merchant order sino, pero se crea al realizar el pedido...
            return;
        }

        if (!$mpOrder->getMerchantOrderId()) {
            $mpOrder->setMerchantOrderId($merchantId);
            if ( in_array($status, MercadoPagoOrder::getStatusOptions())) {
                $mpOrder->setStatus($status);
            }
        }

        $this->em->persist($mpOrder);
        $this->em->flush();
    }

    /* public function getMerchantOrder($id) {
        return $this->getClient()->get("/merchant_orders/" . $id);
    }

    public function getNotification($id) {
        return $this->getClient()->get("/collections/notifications/" . $id );
    }*/

    /*public function consultar( $pedido )
    {
        $dataCredito = $pedido->getData();

        if ( isset( $dataCredito['merchant_order_id'] ) ) {
            $merchantOrderId = $dataCredito['merchant_order_id'];
        } else {
            $response = $this->getClient()->search_payment( array( 'external_reference' => $pedido->getId() ) );

            if ( !isset( $response['response'] ) ) return false;
            if ( !isset( $response['response']['results'] ) ) return false;
            if ( !count( $response['response']['results'] ) ) return false;

            $merchantOrderId = $response['response']['results'][0]['collection']['merchant_order_id'];

        }

        // Se obtiene la informacion completa desde MP
        $merchantOrder = $this->getMerchantOrder( $merchantOrderId );
        $data          = $merchantOrder["response"]["payments"];
        
        // Seteo la fecha de ultima comprobacion
        $pedido->updateUltimaComprobacion();
        $pedido->setData( array( 'merchant_order_id' => $merchantOrderId ) );
        $this->em->persist($pedido);
        $this->em->flush();

        return $this->verificarPago($pedido, $data);
    }

    public function verificarPago( $pedido, $data )
    {
        $pagos = array();
        $montoPagado = 0;

        foreach ($data as $row) {

            if( $row['status'] == 'approved' ){
                $montoPagado += $row['transaction_amount'];
            }
            
            $rows[] = array(
                'estado' => $row['status'],
                'monto'  => $row['transaction_amount']
            );
        }

        // Si se pago la totalidad se lo marca como pagado
        $estaPagado = ( $montoPagado >= $pedido->getMontoMercadoPago() );

        $response = array
        (
            'esta_pagado'           => $estaPagado,
            'pagos'                 => $pagos
        );

        return $response;
    }


    public function procesar( $pagoNotificacion )
    {
        // Se utiliza una transaccion para atomizar la persistencia
        try {
            $this->em->getConnection()->beginTransaction();
            $this->doProcesar( $pagoNotificacion );
            $this->em->getConnection()->commit();
            return true;
        } catch (\Exception $exception) {
            $this->em->getConnection()->rollback();
            return false;
        }
    }

    public function doProcesar( $pagoNotificacion )
    {
        $data = $pagoNotificacion->getResponse();
        if (!$data) { return null; }

        $pedido = $pagoNotificacion->getCredito();
        $estaPagado = $data['esta_pagado'];
        $pagos = $data['pagos'];

        if ( $estaPagado && $pedido->fueEliminado() ) {
            $this->creditoService->marcarPagado( $pedido );
            $pagoNotificacion->setMensaje('Se reactiva, dado que esta pagado y habia sido dado de baja.');
        } else if ( !$estaPagado && $pedido->estaPagado() ) {
            $this->creditoService->marcarIntervencionManual( $pedido );
            $pagoNotificacion->setMensaje('Se marca para intervención manual, debido a que no esta pagado realmente y figura como pagado en nuestro sistema');
        } else if ( $estaPagado ) {
            if ( $pedido->estaPagado() ) {
                $pagoNotificacion->setMensaje('Ya estaba marcado como pagado.');
            } else {
                $this->creditoService->marcarPagado( $pedido );
                $pagoNotificacion->setMensaje('Se marcó como pagado.');
            }
        } else if ( !$estaPagado) {
            $this->creditoService->marcarBaja( $pedido );
            $pagoNotificacion->setMensaje('Se dio de baja.');
        }
    
        // Se marca como procesada a la notificacion de pago
        $pagoNotificacion->setProcesado(true);
        $this->em->persist($pagoNotificacion);
        $this->em->flush();
    }


    public function getIdCredito($paymentId)
    {
        $data = $this->getClient()->get_payment_info( $paymentId );
        return $data['response']['collection']['external_reference'];
    }
*/
    
}
