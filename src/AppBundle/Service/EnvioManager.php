<?php

namespace AppBundle\Service;

use AppBundle\Entity\Envio;
use AppBundle\Entity\EnvioDomicilio;
use AppBundle\Entity\EnvioSucursalCorreo;
use AppBundle\Entity\EnvioPickup;

class EnvioManager
{
    private $enviopack;

    function __construct(EnviopackService $enviopack, $direccionPickup, $horarioPickup)
    {
        $this->enviopack = $enviopack;
        $this->direccionPickup = $direccionPickup;
        $this->horarioPickup = $horarioPickup;
    }

    public function getDireccionEnvio($envio){
        $direccion = array();
        if ($envio instanceof EnvioDomicilio) {
            $direccion['provincia'] = $envio->getProvincia()->getNombre();
            $direccion['localidad'] = $envio->getLocalidad();
            $direccion['direccion'] = $envio->getCalle() . ' '.$envio->getNumero();
            if ( !empty($envio->getPiso()) ) {
                $direccion['direccion'] .= ' piso ' . $envio->getPiso();
            }
            if ( !empty($envio->getDeptoNro()) ) {
                $direccion['direccion'] .= ' ' . $envio->getDeptoNro();
            }
        } elseif ($envio instanceof EnvioSucursalCorreo) {
            $sucursalInfo = $this->enviopack->getSucursalInfo($envio->getSucursalId());

            $direccion['provincia'] = $envio->getProvincia()->getNombre();
            $direccion['localidad'] = '';
            if ( !empty($sucursalInfo['localidad']) && !empty($sucursalInfo['localidad']['nombre']) ) {
                $direccion['localidad'] = $sucursalInfo['localidad']['nombre'];
            }

            $direccion['direccion'] = $sucursalInfo['calle'].' '.$sucursalInfo['numero'];
            if ( !empty($sucursalInfo['horario']) ) {
                $direccion['horario'] = $sucursalInfo['horario'];
            }
            
        } elseif ($envio instanceof EnvioPickup) {
            $direccion['direccion'] = $this->direccionPickup;
            $direccion['horario'] = $this->horarioPickup;
        }

        return $direccion;
    }

    public function cloneEnvio($envio){
        $nuevoEnvio = null;
        if ($envio instanceof Envio) {
            $nuevoEnvio = clone $envio;
            $nuevoEnvio->setCarritoId(null);
            $nuevoEnvio->setPedido(null);
            $nuevoEnvio->setCreatedAt(new \Datetime('now'));
        }

        if ($envio instanceof EnvioDomicilio || $envio instanceof EnvioSucursalCorreo) {
            $nuevoEnvio->setPrecio(null);
            $nuevoEnvio->setNumSeguimiento(null);
            $nuevoEnvio->setProvincia($envio->getProvincia());
            $nuevoEnvio->setCorreoId(null);
            $nuevoEnvio->setCorreoNombre(null);
            $nuevoEnvio->setServicio(null);
        }

        if ($envio instanceof EnvioSucursalCorreo) {
            $nuevoEnvio->setSucursalId(null);
        }
        
        return $nuevoEnvio;
    }
    
}