<?php

namespace AppBundle\Service;

use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class Excel
{
    protected $phpExcel;
    protected $formatCurrencyCell = '$#,##0.00';
    protected $headerStyle;
    protected $dataStyle;

    public function __construct($phpExcel)
    {    
        \PHPExcel_Shared_Font::setAutoSizeMethod(\PHPExcel_Shared_Font::AUTOSIZE_METHOD_APPROX);

        $this->phpExcel = $phpExcel;
        
        // Seteos generales de PHPExcel
        $this->headerStyle = array(
            'font' => array('bold' => true, 'size' => "10"),
            'alignment' => array(
                    'horizontal' =>\PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000')
                )
            )
        );

        $this->dataStyle = array(
            'font' => array('size' => "10"),
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000')
                )
            )
        );
    }

    public function create($title)
    {    
        $excelObject = $this->phpExcel->createPHPExcelObject();

        $excelObject->getProperties()
                    ->setCreator("EnvioPack")
                    ->setTitle($title);

        $activeSheet = $excelObject->setActiveSheetIndex(0);

        return $excelObject;
    }

    public function getActiveSheet($excelObject)
    {
        return $excelObject->getActiveSheet();   
    }

    public function setHeader($activeSheet, $cell, $value)
    {    
        $activeSheet->setCellValue($cell, $value);
        $activeSheet->getStyle($cell)->applyFromArray( $this->headerStyle );
        return $this;
    }

    public function setData($activeSheet, $cell, $value)
    {    
        $activeSheet->setCellValue($cell, $value);
        $activeSheet->getStyle($cell)->applyFromArray( $this->dataStyle );
        return $this;
    }

    public function setString($activeSheet, $cell, $value)
    {    
        $activeSheet->getCell($cell)->setValueExplicit($value, \PHPExcel_Cell_DataType::TYPE_STRING);
        $activeSheet->getStyle($cell)->applyFromArray( $this->dataStyle );
        return $this;
    }

    public function setCurrency($activeSheet, $cell, $value)
    {    
        $activeSheet->setCellValue($cell, $value);
        $activeSheet->getStyle($cell)->applyFromArray( $this->dataStyle );
        $activeSheet->getStyle($cell)->getNumberFormat()->setFormatCode( $this->formatCurrencyCell );
        return $this;
    }

    public function autoWith($activeSheet, $from, $to)
    {
        $to++;
        for( $i = 'A' ; $i !== $to ; $i++ ) {
            $activeSheet->getColumnDimension($i)->setAutoSize(true);
            
        }
    }

    public function download($excelObject, $filename)
    {    
        $writer = $this->phpExcel->createWriter($excelObject, 'Excel5');
        $response = $this->phpExcel->createStreamedResponse($writer);
        

        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename
        );

        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;
    }

    public function save($excelObject, $filepath)
    {    
        $writer = $this->phpExcel->createWriter($excelObject, 'Excel5');
        $writer->save($filepath);
    }

};