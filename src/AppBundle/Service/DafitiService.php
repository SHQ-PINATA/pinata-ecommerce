<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Plataforma;
use AppBundle\Entity\Producto;
use AppBundle\Entity\DafitiUpdate;
use AppBundle\Entity\Stock;
use AppBundle\Entity\StockTipo;


class DafitiService
{

    protected $em;
    protected $mercadoLibreConfig = array();
    protected $dataConfiguracion  = array();
    protected $host;

    private $listarCompras      = 'GetOrders';
    private $listarCompraItems  = 'GetMultipleOrderItems';
    private $updateProducto     = 'ProductUpdate';

    public function __construct($entityManager, $dafitiConfig)
    {
        $this->em                 = $entityManager;
        $this->dafitiConfig       = $dafitiConfig;
    }


    //API METHODS

    public function doRequest($parameters, $operation, $xml = false) {
        ksort($parameters);
        $encoded = array();
        foreach ($parameters as $name => $value) {
            $encoded[] = rawurlencode($name) . '=' . rawurlencode($value);
        }
        $concatenated = implode('&', $encoded);
        $parameters['Signature'] = rawurlencode(hash_hmac('sha256', $concatenated, $this->dafitiConfig['api_key'], false));
        $url = $this->dafitiConfig['endpoint'] . '?Action=' . $operation;
        $queryString = http_build_query($parameters, '', '&', PHP_QUERY_RFC3986);

        $ch = curl_init();
        try {
            curl_setopt($ch, CURLOPT_URL, $url."&".$queryString);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            if ($operation == $this->updateProducto) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
            }
            $result = curl_exec($ch);
        } catch(\Exception $e) {
            die(var_dump($e->getMessage($e)));
        }

        $response = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $result);
        $xml      = new \SimpleXMLElement($response);

        if ($operation == $this->updateProducto) {
            $info = curl_getinfo($ch);
            if ($info['http_code'] == 200) {
                $xml = true;
            } else {
                $xml = false;
            }

        }

        curl_close($ch);

        return $xml;
    }

    public function getDafitiLatestPurchases()
    {
        $now = new \DateTime(); 
        $dafitiUpdateRepo = $this->em->getRepository('AppBundle\Entity\DafitiUpdate');
        $lastUpdates = $dafitiUpdateRepo->findBy(array(),array('id'=>'DESC'));

        if (count($lastUpdates)) {
            $lastUpdate = $lastUpdates[0]->getLastUpdate()->format(\DateTime::ISO8601);
        } else {
            $lastUpdate = "2014-12-31T16:08:56-0300"; 
        }

        $parameters = array(
            'Action'        => $this->listarCompras,
            'Format'        => 'json',
            'Timestamp'     => $now->format(\DateTime::ISO8601),
            'UserID'        => $this->dafitiConfig['user'],
            'Version'       => '1.0',
            'CreatedAfter'  => $lastUpdate,
            'Offset'        => 0,
        );

        $response = $this->doRequest($parameters, $this->listarCompras);

        $orderIds = array();
        $latestPurchases = array();

        if ($response->Head->TotalCount) {
            $ordersAPI = $response->Body->Orders->Order;
            foreach ($ordersAPI as $orderAPI) {
                $orderIds[] = $orderAPI->OrderId->__toString();
            }

            //Hago request para obtener la info de los productos comprados en Dafiti
            
            $latestPurchases = $this->getDafitiItemsForOrdersIds($orderIds);
        }

        return $latestPurchases;
    }

    public function getDafitiItemsForOrdersIds($orderIds) {

        $now = new \DateTime(); 
        $orderIdList = '[';

        $i = 0;
        foreach ($orderIds as $orderId) {
            $orderIdList .= $orderId;
            if ($i != count($orderIds) - 1) {
                $orderIdList .= ', ';
            }
            $i++;
        }

        $orderIdList .= ']';
        
        $parameters = array(
            'Action'        => $this->listarCompraItems,
            'Format'        => 'json',
            'Timestamp'     => $now->format(\DateTime::ISO8601),
            'UserID'        => $this->dafitiConfig['user'],
            'Version'       => '1.0',
            'OrderIdList'   => $orderIdList,
        );

        $response = $this->doRequest($parameters, $this->listarCompraItems);
        $orders = $response->Body->Orders;

        $rawSkus = array();
        foreach($orders as $order) {
            $order = $order->Order;
            foreach($order->OrderItems as $orderItems) {
                $orderItems = $orderItems->OrderItem;
                foreach($orderItems as $orderItem) {
                    $sku = $orderItem->Sku->__toString();
                    $rawSkus []= $sku;
                }
            }
        }

        $latestPurchases = array();
    
        $i = 0;
        foreach($rawSkus as $sku) {
            if (array_key_exists($sku, $latestPurchases)) {
                $latestPurchases[$sku] = $latestPurchases[$sku] + 1;
            } else {
                $latestPurchases[$sku] = 1; 
            }
             $i++;
        }

        return $latestPurchases;
    }

    private function updateDafitiStock($producto, $stock) {
        $now = new \DateTime(); 

        $parameters = array(
            'Action'        => $this->updateProducto,
            'Format'        => 'json',
            'Timestamp'     => $now->format(\DateTime::ISO8601),
            'UserID'        => $this->dafitiConfig['user'],
            'Version'       => '1.0',
        );

        $sku = $producto->getDafitiSku();
        $xml ='<?xml version="1.0" encoding="UTF-8"?><Request><Product><SellerSku>'.$sku.'</SellerSku><Quantity>'.$stock.'</Quantity></Product></Request>';

        $response = $this->doRequest($parameters, $this->updateProducto, $xml);
        return $response;
    }


    //STOCK METHODS
    
    public function refreshStock() {

        $now = new \DateTime(); 
        //Voy a chequear las últimas compras en dafiti para actualizar el stock local de todos los productos
        $latestPurchases = $this->getDafitiLatestPurchases();
        $stockTipo = $this->em->getRepository(StockTipo::class)->getByCodigo(StockTipo::CODIGO_DAFITI);

        foreach($latestPurchases as $sku => $cantidad) {
            $producto = $this->em->getRepository(Producto::class)->findOneBy(array('dafitiSku' => $sku));
            $stockDiff = $cantidad - ($cantidad * 2);
	    if ($producto) {
		$this->updateLocalStock($producto, $stockDiff, $stockTipo);
	    }
        }
        
        $dafitiUpdate = new DafitiUpdate();
        $dafitiUpdate->setLastUpdate($now);
        $this->em->persist($dafitiUpdate);
        $this->em->flush();
        
    }

    public function updateStock($producto, $stock) {
        //Recibo el producto y el stock a actualizar en Dafiti.
        if ($producto->getDafitiSku()) {
            $this->updateDafitiStock($producto, $stock);
        } 
    }

    private function updateLocalStock(Producto $producto, $stockDiff, StockTipo $stockTipo) {
        $stock = new Stock();
        $stock->setProducto($producto);
        $stock->setCantidad($stockDiff);
        $stock->setStockTipo($stockTipo);
        $producto->setStock($producto->getStock() + $stockDiff);
        $this->em->persist($stock);
        $this->em->persist($producto);
        $this->em->flush();
    }

}
