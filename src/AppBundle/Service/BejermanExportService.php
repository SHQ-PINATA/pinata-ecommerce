<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Usuario;
use AppBundle\Entity\Producto;
use AppBundle\Entity\Factura;

class BejermanExportService
{

    const FILE_CABECERA = 'VCABECER.txt';
    const FILE_ITEMS = 'VITEMS.txt';
    const FILE_MEDIOS_PAGO = 'VMEDPAGO.txt';

	public function __construct(EntityManager $entityManager, $afipConfig){
		$this->em = $entityManager;
        $this->afipConfig = $afipConfig;
	}

    public function exportFiles($fechaDesde, $fechaHasta) {
        $time = time();
        $dirname = '/tmp/bejerman-' . $time;
        mkdir($dirname);
        $pathCabecera = $this->getCabecera($fechaDesde, $fechaHasta, $dirname);
        $pathComprobantes = $this->getComprobantes($fechaDesde, $fechaHasta, $dirname);
        $pathMediosPago = $this->getMediosPago($fechaDesde, $fechaHasta, $dirname);

        $zip = new \ZipArchive();
        $zipPath = '/tmp/Bejerman.zip';
        $zip->open($zipPath, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

        $zip->addFile($pathCabecera, $this::FILE_CABECERA);
        $zip->addFile($pathComprobantes, $this::FILE_ITEMS);
        $zip->addFile($pathMediosPago, $this::FILE_MEDIOS_PAGO);

        $zip->close();

        $this->deleteBejermanDirectory($dirname);

        return $zipPath;
    }

    public function getCabecera($fechaDesde, $fechaHasta, $dirname) {
        $fileName = $this::FILE_CABECERA;
        $path = $dirname . '/' . $fileName;
        $cabeceraFile = fopen($path, 'w+');
		$data = $this->em->getRepository(Factura::class)->getFacturasReporte($fechaDesde, $fechaHasta, true);

        foreach ($data as $row) {
            $lineToAdd = '';
            $tipoComprobante = 'FC ';
            $lineToAdd .= $tipoComprobante;

            $letra = 'B';
            $lineToAdd .= $letra;

            $puntoDeVentaRaw = $this->afipConfig['punto_de_venta'];
            $puntoDeVenta = str_pad($puntoDeVentaRaw, 4, '0', STR_PAD_LEFT);
            $lineToAdd .= $puntoDeVenta;

            $comprobante = str_pad($row['comprobante'], 8, "0", STR_PAD_LEFT);
            $lineToAdd .= $comprobante;

            $nroHasta = str_repeat(' ', 8);
            $lineToAdd .= $nroHasta;

            $time = strtotime($row['fecha_procesado']);
            $fechaComprobante = date('Ymd', $time);
            $lineToAdd .= $fechaComprobante;

            $codigoCliente = str_repeat(' ', 6);
            $lineToAdd .= $codigoCliente;

            $nombreApellido = $row['name'] . ' ' . $row['lastname']; 
            $razonSocialCliente = str_pad($nombreApellido, 40, " ");
            $lineToAdd .= $razonSocialCliente;

            $tipoDocumento = '5 ';
            $lineToAdd .= $tipoDocumento;

            $codigoBejerman = $row['codigo_bejerman'];
            $provincia = str_pad($codigoBejerman, 3, '0', STR_PAD_LEFT);
            $lineToAdd .= $provincia;

            $situacionDeIva = 3;
            $lineToAdd .= $situacionDeIva;

            $dni = $row['dni'];
            $CUITraw = $dni ? $dni : 0;
            $CUIT = str_pad($CUITraw, 11, ' ');
            $lineToAdd .= $CUIT;

            $ingresosBrutos = str_repeat(' ', 15);
            $lineToAdd .= $ingresosBrutos;

            $codigoVendedor = '0001';
            $lineToAdd .= $codigoVendedor;

            $codigoZona = str_repeat(' ', 4);
            $lineToAdd .= $codigoZona;

            $codigoClasificacionCliente = str_pad('EC', 4, ' ');
            $lineToAdd .= $codigoClasificacionCliente;

            $codigoClasificacionCliente = str_pad('01', 3, ' ');
            $lineToAdd .= $codigoClasificacionCliente;
            
            $codigoCausaEmision = str_repeat(' ', 4);
            $lineToAdd .= $codigoCausaEmision;

            $fechaVencimiento = $fechaComprobante;
            $lineToAdd .= $fechaVencimiento;

            $importeTotalRaw = $row['importe_total'];
            $importeTotal = str_pad($importeTotalRaw, 16, ' ', STR_PAD_LEFT);
            $lineToAdd .= $importeTotal;

            $codigoDescuentoComercial = str_repeat(' ', 2);
            $lineToAdd .= $codigoDescuentoComercial;

            $codigoDescuentoFinanciero = str_repeat(' ', 2);
            $lineToAdd .= $codigoDescuentoFinanciero;

            $codigoDescuentoPieComprobante = str_repeat(' ', 2);
            $lineToAdd .= $codigoDescuentoPieComprobante;

            $aperturaContable = str_repeat(' ', 4);
            $lineToAdd .= $aperturaContable;

            $tipoCliente = "06  ";
            $lineToAdd .= $tipoCliente;

            $direccion = str_repeat(' ', 30);
            $lineToAdd .= $direccion;

            $codigoPostal = str_repeat(' ', 8);
            $lineToAdd .= $codigoPostal;

            $localidad = str_repeat(' ', 25);
            $lineToAdd .= $localidad;

            $codigoClasificacionAdicionalCliente2 = '1   ';
            $lineToAdd .= $codigoClasificacionAdicionalCliente2;

            $mensaje = str_repeat(' ', 40);
            $lineToAdd .= $mensaje;

            $comprobanteAnulado = ' ';
            $lineToAdd .= $comprobanteAnulado; 

            $actualizaStock = 'S';
            $lineToAdd .= $actualizaStock;

            $descripcionClasificacion1Raw = 'ec';
            $descripcionClasificacion1 = str_pad($descripcionClasificacion1Raw, 15, ' ');
            $lineToAdd .= $descripcionClasificacion1;

            $descripcionClasificacion2Raw = 'Sin credito';
            $descripcionClasificacion2 = str_pad($descripcionClasificacion2Raw, 15, ' ');
            $lineToAdd .= $descripcionClasificacion2;

            $descripcionTipoCliente = "Ecommerce Pinat";
            $lineToAdd .= $descripcionTipoCliente;

            $descripcionZona = str_repeat(' ', 15);
            $lineToAdd .= $descripcionZona;

            $descripcionVendedorRaw = 'Central';
            $descripcionVendedor = str_pad($descripcionVendedorRaw, 25, ' '); 
            $lineToAdd .= $descripcionVendedor;

            $noDisponible = ' ';
            $lineToAdd .= $noDisponible;

            $tasaDescuentoComercial1Raw = '0.00';
            $tasaDescuentoComercial2Raw = '0.00';
            $tasaDescuentoComercial3Raw = '0.00';
            $tasaDescuentoComercial1 = str_pad($tasaDescuentoComercial1Raw, 8, ' ', STR_PAD_LEFT);
            $tasaDescuentoComercial2 = str_pad($tasaDescuentoComercial2Raw, 8, ' ', STR_PAD_LEFT);
            $tasaDescuentoComercial3 = str_pad($tasaDescuentoComercial3Raw, 8, ' ', STR_PAD_LEFT);
            $lineToAdd .= $tasaDescuentoComercial1;
            $lineToAdd .= $tasaDescuentoComercial2;
            $lineToAdd .= $tasaDescuentoComercial3;

            $tasaDescuentoFinancieroRaw = '0.00';
            $tasaDescuentoFinanciero = str_pad($tasaDescuentoFinancieroRaw, 8, ' ', STR_PAD_LEFT);
            $lineToAdd .= $tasaDescuentoFinanciero;

            $tasaDescuentoVarioPieRaw = '0.00';
            $tasaDescuentoVarioPie = str_pad($tasaDescuentoVarioPieRaw, 8, ' ', STR_PAD_LEFT);
            $lineToAdd .= $tasaDescuentoVarioPie;

            $nroCAI = str_repeat(' ', 15);
            $lineToAdd .= $nroCAI;

            $fechaVencimientoCAI = str_repeat(' ', 8);
            $lineToAdd .= $fechaVencimientoCAI;

            $controladorFiscal = str_repeat(' ', 1);
            $lineToAdd .= $controladorFiscal;

            $emailCliente = str_repeat(' ', 50); 
            $lineToAdd .= $emailCliente;

            $telefonoCliente = str_repeat(' ', 30); 
            $lineToAdd .= $telefonoCliente;

            $faxCliente = str_repeat(' ', 30); 
            $lineToAdd .= $faxCliente;

            $contactoObservaciones = str_repeat(' ', 50);
            $lineToAdd .= $contactoObservaciones;

            $tipoOperacion = str_repeat(' ', 4);
            $lineToAdd .= $tipoOperacion;

            $nroCuota = str_repeat(' ', 3);
            $lineToAdd .= $nroCuota;

            $importeCuotaRaw = '0.00';
            $importeCuota = str_pad($importeCuotaRaw, 16, ' ', STR_PAD_LEFT);
            $lineToAdd .= $importeCuota;

            $comprobanteCuotas = ' ';
            $lineToAdd .= $comprobanteCuotas;

            $monedaComprobante = str_repeat(' ', 3);
            $lineToAdd .= $monedaComprobante;

            $tipoCambio = str_repeat(' ', 3);
            $lineToAdd .= $tipoCambio;

            $cotizacion = str_repeat(' ', 8);
            $lineToAdd .= $cotizacion;

            $fechaEntrega = str_repeat(' ', 8);
            $lineToAdd .= $fechaEntrega;

            $grupo = str_repeat(' ', 15);
            $lineToAdd .= $grupo;

            $proyecto = str_repeat(' ', 15);
            $lineToAdd .= $proyecto;

            $listaDePrecios = 'LN ';
            $lineToAdd .= $listaDePrecios;

            $lugarEntrega = str_repeat(' ', 3);
            $lineToAdd .= $lugarEntrega;

            $autorizacionNP = ' ';
            $lineToAdd .= $autorizacionNP;

            $lineToAdd .= "\n";
            fwrite($cabeceraFile, $lineToAdd);
        }
        fclose($cabeceraFile);

        return $path;
	}

	public function getComprobantes($fechaDesde, $fechaHasta, $dirname) {
        $fileName = 'VITEMS.txt';
        $path = $dirname . '/' . $fileName;
        $itemsFile = fopen($path, 'w+');
		$data = $this->em->getRepository(Factura::class)->getFacturasReporte($fechaDesde, $fechaHasta, true);

        foreach ($data as $row) {
            $lineToAdd = '';
            $tipoComprobante = 'FC ';
            $lineToAdd .= $tipoComprobante;

            $letra = 'B';
            $lineToAdd .= $letra;

            $puntoDeVentaRaw = $this->afipConfig['punto_de_venta'];
            $puntoDeVenta = str_pad($puntoDeVentaRaw, 4, '0', STR_PAD_LEFT);
            $lineToAdd .= $puntoDeVenta;

            $comprobante = str_pad($row['comprobante'], 8, "0", STR_PAD_LEFT);
            $lineToAdd .= $comprobante;

            $nroHasta = str_repeat(' ', 8);
            $lineToAdd .= $nroHasta;

            $time = strtotime($row['fecha_procesado']);
            $fechaComprobante = date('Ymd', $time);
            $lineToAdd .= $fechaComprobante;

            $codigoCliente = str_repeat(' ', 6);
            $lineToAdd .= $codigoCliente;

            $tipoItem = 'A';
            $lineToAdd .= $tipoItem;

            $ean = $row['codigo_ean'];
            $codigoTemporada = $row['codigo_temporada'];
            $sku = substr($ean, -5, 4);
            $codigoConceptoRaw = $sku;
            if ($codigoTemporada) {
                $codigoConceptoRaw .= $codigoTemporada;
            }
            $codigoConcepto = str_pad($codigoConceptoRaw, 23, ' ');
            $lineToAdd .= $codigoConcepto;

            $cantidad1Raw = $row['cantidad'] . '.00';
            $cantidad1 = str_pad($cantidad1Raw, 16, ' ', STR_PAD_LEFT);
            $lineToAdd .= $cantidad1;

            $cantidad2Raw = '0.00';
            $cantidad2 = str_pad($cantidad2Raw, 16, ' ', STR_PAD_LEFT);
            $lineToAdd .= $cantidad2;

            $descripcionRaw = substr($row['descripcion'], 0, 50);
            $descripcionRaw = preg_replace("/[^ \w]+/", "", $descripcionRaw);
            $descripcion = str_pad($descripcionRaw, 50, ' ');
            $lineToAdd .= $descripcion;

            $precioUnitarioRaw = $row['precio_unidad'];
            $tasaDeIvaInscriptoRaw = '21.00';
            $importeIvaRaw = $precioUnitarioRaw * $tasaDeIvaInscriptoRaw / 100;
            $precioUnitarioRaw = number_format($precioUnitarioRaw - $importeIvaRaw, 2, '.', '');
            $precioUnitario = str_pad($precioUnitarioRaw, 16, ' ');
            $lineToAdd .= $precioUnitario;
            
            $tasaDeIvaInscripto = str_pad($tasaDeIvaInscriptoRaw, 8, ' ', STR_PAD_LEFT);
            $lineToAdd .= $tasaDeIvaInscripto;

            $tasaDeIvaNoInscriptoRaw = '0.00';
            $tasaDeIvaNoInscripto = str_pad($tasaDeIvaNoInscriptoRaw, 8, ' ', STR_PAD_LEFT);
            $lineToAdd .= $tasaDeIvaNoInscripto;

            $importeIva = str_pad($importeIvaRaw, 16, ' ', STR_PAD_LEFT);
            $lineToAdd .= $importeIva;
 
            $importeDeIvaNoInscriptoRaw = '0.00';
            $importeDeIvaNoInscripto = str_pad($importeDeIvaNoInscriptoRaw, 16, ' ', STR_PAD_LEFT);
            $lineToAdd .= $importeDeIvaNoInscripto;

            $importeTotalRaw = number_format($row['importe_total'], 2, '.', '');
            $importeTotal = str_pad($importeTotalRaw, 16, ' ', STR_PAD_LEFT);
            $lineToAdd .= $importeTotal;

            $importeDeDescuentoComercialRaw = '0.00';
            $importeDeDescuentoComercial = str_pad($importeDeDescuentoComercialRaw, 16, ' ', STR_PAD_LEFT);
                $lineToAdd .= $importeDeDescuentoComercial;

            $importeDeDescuentoFinancieroRaw = '0.00';
            $importeDeDescuentoFinanciero = str_pad($importeDeDescuentoFinancieroRaw, 16, ' ', STR_PAD_LEFT);
            $lineToAdd .= $importeDeDescuentoFinanciero;

            $importeDeDescuentoPieComprobanteRaw = '0.00';
            $importeDeDescuentoPieComprobante = str_pad($importeDeDescuentoPieComprobanteRaw, 16, ' ', STR_PAD_LEFT);
            $lineToAdd .= $importeDeDescuentoPieComprobante;

            $codigoConceptoNoGravado = str_repeat(' ', 4);
            $lineToAdd .= $codigoConceptoNoGravado;

            $importeDeIvaNoGravado = '0.00';
            $importeDeIvaNoGravado = str_pad($importeDeIvaNoGravado, 16, ' ', STR_PAD_LEFT);
            $lineToAdd .= $importeDeIvaNoGravado;

            $tipoDeIva = '1';
            $lineToAdd .= $tipoDeIva;

            $codigoDescuentoPorLinea = '  ';
            $lineToAdd .= $codigoDescuentoPorLinea;

            $importeDescuentoPorLineaRaw = '0.00';
            $importeDescuentoPorLinea = str_pad($importeDescuentoPorLineaRaw, 16, ' ', STR_PAD_LEFT);
            $lineToAdd .= $importeDescuentoPorLinea;

            $deposito = 'ECO';
            $lineToAdd .= $deposito;

            $partidaRaw = 'ECO';
            $partida = str_pad($partidaRaw, 26, ' ');
            $lineToAdd .= $partida;

            $montoDescontado = $row['monto_descontado'];
            $tasaDescuentoRaw = $montoDescontado * 100 / $importeTotal;
            if ($tasaDescuentoRaw) {
                $tasaDescuentoRaw = '-' . $tasaDescuentoRaw;
            }
            $tasaDescuento = str_pad($tasaDescuentoRaw, 8, ' ', STR_PAD_LEFT);
            $lineToAdd .= $tasaDescuento;

            $importeDelRenglon = $importeTotal;
            $lineToAdd .= $importeTotal;
            
            $claseDelArticulo = 'COL';
            $lineToAdd .= $claseDelArticulo;

            $definibleDelArticulo = str_repeat(' ', 4);
            $lineToAdd .= $definibleDelArticulo;

            $descDefinible1 = str_repeat(' ', 25);
            $lineToAdd .= $descDefinible1;

            $definibleDelArticulo2 = str_repeat(' ', 4);
            $lineToAdd .= $definibleDelArticulo2;

            $descDefinible2 = str_repeat(' ', 25);
            $lineToAdd .= $descDefinible2;

            $codigoProveedor = str_repeat(' ', 6);
            $lineToAdd .= $codigoProveedor;

            $razonSocialProveedor = str_repeat(' ', 40);
            $lineToAdd .= $razonSocialProveedor;
            
            $descElemento1 = str_repeat(' ', 15);
            $descElemento2 = str_repeat(' ', 15);
            $descElemento3 = str_repeat(' ', 15);
            $lineToAdd .= $descElemento1;
            $lineToAdd .= $descElemento2;
            $lineToAdd .= $descElemento3;

            $codigoDeBarrasRaw = $row['codigo_ean']; 
            $codigoDeBarras = str_pad($codigoDeBarrasRaw, 20, ' ');
            $lineToAdd .= $codigoDeBarras;

            $fechaEntrega = str_repeat(' ', 8);
            $lineToAdd .= $fechaEntrega;

            $numeroRenglon = str_repeat(' ', 3);
            $lineToAdd .= $numeroRenglon;
            
            $lineToAdd .= "\n";
            fwrite($itemsFile, $lineToAdd);
        }

        fclose($itemsFile);
        return $path;
    } 

	public function getMediosPago($fechaDesde, $fechaHasta, $dirname) {
        $fileName = 'VMEDPAGO.txt';
        $path = $dirname . '/' . $fileName;
        $medioPagoFile = fopen($path, 'w+');
		$data = $this->em->getRepository(Factura::class)->getFacturasReporte($fechaDesde, $fechaHasta, true);

        foreach ($data as $row) {
            $lineToAdd = '';
            $tipoComprobante = 'FC ';
            $lineToAdd .= $tipoComprobante;

            $letra = 'B';
            $lineToAdd .= $letra;

            $puntoDeVentaRaw = $this->afipConfig['punto_de_venta'];
            $puntoDeVenta = str_pad($puntoDeVentaRaw, 4, '0', STR_PAD_LEFT);
            $lineToAdd .= $puntoDeVenta;

            $comprobante = str_pad($row['comprobante'], 8, "0", STR_PAD_LEFT);
            $lineToAdd .= $comprobante;

            $nroHasta = str_repeat(' ', 8);
            $lineToAdd .= $nroHasta;

            $time = strtotime($row['fecha_procesado']);
            $fechaComprobante = date('Ymd', $time);
            $lineToAdd .= $fechaComprobante;

            $codigoCliente = str_repeat(' ', 6);
            $lineToAdd .= $codigoCliente;

            $medioDePago = 1;
            $lineToAdd .= $medioDePago;

            $moneda = str_pad('1', 3, ' ');
            $lineToAdd .= $moneda;

            $tipoDeCambio = 'UNI';
            $lineToAdd .= $tipoDeCambio;

            $caja = str_pad('04', 15, ' '); 
            $lineToAdd .= $caja;

            $tipoCheque = str_repeat(' ', 3);
            $lineToAdd .= $tipoCheque;

            $fechaVencimientoCheque = str_repeat(' ', 8);
            $lineToAdd .= $fechaVencimientoCheque;

            $importeMovimientoRaw = $row['importe_total'];
            $importeMovimiento = str_pad($importeMovimientoRaw, 16, ' ', STR_PAD_LEFT);
            $lineToAdd .= $importeMovimiento;

            $numeroCheque = str_repeat(' ', 8);
            $lineToAdd .= $numeroCheque;

            $codigoBanco = str_repeat(' ', 3);
            $lineToAdd .= $codigoBanco;

            $sucursalBanco = str_repeat(' ', 4);
            $lineToAdd .= $sucursalBanco;

            $clearing = '  0';
            $lineToAdd .= $clearing;

            $origen = ' ';
            $lineToAdd .= $origen;

            $codigoCuentaBancaria = str_repeat(' ', 15);
            $lineToAdd .= $codigoCuentaBancaria;

            $nroTarjeta = str_repeat(' ', 25);
            $lineToAdd .= $nroTarjeta;

            $nroAutorizacion = str_repeat(' ', 25);
            $lineToAdd .= $nroAutorizacion;

            $nombreLibrador = str_repeat(' ', 30);
            $lineToAdd .= $nombreLibrador;

            $direccionLibrador = str_repeat(' ', 30);
            $lineToAdd .= $direccionLibrador;

            $cpLibrador = str_repeat(' ', 8);
            $lineToAdd .= $cpLibrador;

            $provinciaLibrador = str_repeat(' ', 3);
            $lineToAdd .= $provinciaLibrador;

            $localidadLibrador = str_repeat(' ', 25);
            $lineToAdd .= $localidadLibrador;

            $telefonoLibrador = str_repeat(' ', 30);
            $lineToAdd .= $telefonoLibrador;

            $importeMovimientoMonedaLocalRaw = $importeMovimiento;
            $importeMovimientoMonedaLocal = str_pad($importeMovimientoMonedaLocal, 16, ' ', STR_PAD_LEFT);
            $lineToAdd .= $importeMovimiento;

            $lineToAdd .= "\n";
            fwrite($medioPagoFile, $lineToAdd);
        }

        fclose($medioPagoFile);
        return $path;
    }

    private function deleteBejermanDirectory($dirPath) {
        $it = new \RecursiveDirectoryIterator($dirPath, \RecursiveDirectoryIterator::SKIP_DOTS);
        $files = new \RecursiveIteratorIterator($it,
            \RecursiveIteratorIterator::CHILD_FIRST);
        foreach($files as $file) {
            if ($file->isDir()){
                rmdir($file->getRealPath());
            } else {
                unlink($file->getRealPath());
            }
        }
        rmdir($dirPath); 
    }
}
