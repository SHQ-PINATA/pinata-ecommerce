<?php

namespace AppBundle\Presenter;

use AppBundle\Entity\Factura;
use AppBundle\Entity\Pedido;
use AppBundle\Entity\Envio;
class FacturaPresenter
{
	function __construct(Factura $factura, Pedido $pedido = null, Envio $envio = null)
	{
		$this->factura = $factura;
		$this->pedido = $pedido;
		$this->envio = $envio;
	}

	public function getPedido(){
		if (!$this->pedido) {
			$this->pedido = $this->factura->getPedido();
		}
		return $this->pedido;
	}
	
	public function getEnvio(){
		if (!$this->envio) {
			$this->envio = $this->getPedido()->getEnvio();
		}
		return $this->envio;
	}

	public function getFactura(){
		return $this->factura;
	}

	public function getDni(){
		return $this->getPedido()->getDni();
	}

	public function getNombreApellido(){
		$envio = $this->getEnvio();
		$nombre = $envio->getNombre();
		$apellido = $envio->getApellido();
		return $nombre.' '.$apellido;
	}

	public function getItems(){
		$items = $this->getPedido()->getItems();
		$detalle = [];
		foreach ($items as $item) {
			$detalle[] = ['descripcion' => $item->getProducto()->getNombre(), 
						  'precio_unidad' => sprintf("$%1\$.2f", $item->getPrecio()),
						  'cantidad' => $item->getCantidad(),
						  'subtotal' => sprintf("$%1\$.2f", $item->getCantidad()*$item->getPrecio())];
		}

		$envio = $this->getEnvio();
		if ($envio->getPrecio() > 0) {
			$detalle[] = ['descripcion' => 'Envío', 
						  'precio_unidad' => sprintf("$%1\$.2f", $envio->getPrecio()),
						  'cantidad' => 1,
						  'subtotal' => sprintf("$%1\$.2f", $envio->getPrecio())];
		}

		$descuento = $this->getPedido()->getDescuentoAplicado();
		if ($descuento) {
			$detalle[] = ['descripcion' => 'Descuento', 
						 'precio_unidad' => sprintf("-$%1\$.2f", $descuento->getMontoDescontado()) ,
						 'cantidad' => 1,
						 'subtotal' => sprintf("-$%1\$.2f", $descuento->getMontoDescontado()),];
		}

		return $detalle;
	}

	public function hasNombreAndDni(){
		return $this->getImporteTotal() >= 1000;
	}

	public function getImporteTotal(){
		return $this->factura->getImporteTotal();
	}
	public function getImporteIva(){
		return $this->factura->getImporteIva();
	}
	public function getCAE(){
		return $this->factura->getCAE();
	}

	public function setPedido(Pedido $pedido){
		$this->pedido = $pedido;
	}

	
}