<?php

namespace AppBundle\Presenter;

use AppBundle\Entity\Pedido;


class PedidoPresenter
{
	function __construct(Pedido $pedido = null)
	{
		$this->pedido = $pedido;
	}

	public function getPrecioTotal(){
		return $this->getPrecioItems() + $this->getPrecioEnvio() - $this->getMontoDescontado();
	}

	public function getPrecioItems(){
		return $this->pedido->getPrecioTotalItems();
	}

	public function getPrecioEnvio(){
		return $this->pedido->getEnvio()->getPrecio();
	}

	public function getMontoDescontado(){
		$descuentoAplicado = $this->pedido->getDescuentoAplicado();
		return $descuentoAplicado ? $descuentoAplicado->getMontoDescontado() : 0;
	}

	public function setPedido(Pedido $pedido){
		$this->pedido = $pedido;
	}

	public function getDni(){
		return $this->pedido->getDni();
	}

	public function hasFacturaCreated(){
		return (bool) $this->pedido->getFactura();
	}

	public function facturaIsProcessed(){
		$factura = $this->pedido->getFactura();
		return ($factura && $factura->getFechaProcesada());
	}
}